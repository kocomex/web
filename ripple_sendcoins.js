const RippleAPI = require('ripple-lib').RippleAPI;

var toaddress=process.argv[2];
var value=process.argv[3];
var address=process.argv[4];
var secret = process.argv[5]; 

//console.log(toaddress+value+address+secret);

var instructions = {maxLedgerVersionOffset: 5};
const api = new RippleAPI({
  server: 'wss://s1.ripple.com' // Public rippled server hosted by Ripple, Inc.
});
 
var payment = {
source: {
address: address,
maxAmount: {
value: value,
currency: 'XRP'
}
},
destination: {
address: toaddress,
amount: {
value: value,
currency: 'XRP'
},
}
}; 


function quit(message) {
console.log(+"~"+JSON.stringify(message));
process.exit(0);
}
function fail(message) {
console.error(message);
process.exit(1);
}

api.connect().then(() => {
//  console.log('Connected...');
  return api.preparePayment(address, payment,instructions).then(prepared => {
//console.log('Payment transaction prepared...'+prepared.txJSON);
  var signed = api.sign(prepared.txJSON, secret);
  //console.log('Payment transaction signed...');
  txid = signed.id;
   console.log(+"~"+JSON.stringify({txid : txid }));
  api.submit(signed.signedTransaction).then(quit, fail);
 


  });
}).catch(fail); 




