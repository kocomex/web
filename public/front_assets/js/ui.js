$(document).ready(function(){

	// Top scroll
	$( ".btn_top" ).click( function(){
	  $( "html, body" ).animate( { scrollTop : 0 }, 200 );
	  return false;
	});

	var last_left = 0;
	var maxScrollLeft = $(".scroll-x table").width() - $(".scroll-x").width();
	$(".scroll-x").scroll(function() {
		var this_left = $(this).scrollLeft();
		if( this_left == maxScrollLeft ) {
			$(this).parent().find(".ic_right").stop().fadeOut(100);
		} else if ( this_left > last_left ) {
			$(this).parent().find(".ic_left").stop().fadeIn(100);
			$(this).parent().find(".ic_right").stop().fadeIn(100);
		} else if ( this_left == last_left ) {
			$(this).parent().find(".ic_left").stop().fadeOut(100);
		} 
	});
});

// 모바일 상단바
var last_top = 0;
$(window).scroll(function() {
	var this_top = $(this).scrollTop();
	if( this_top > last_top ) {
		$(".logo").addClass("hide");
		$(".btn_gnb").addClass("hide");
		$(".page_title").addClass("hide");
	} else {
		$(".logo").removeClass("hide");
		$(".btn_gnb").removeClass("hide");
		$(".page_title").removeClass("hide");
	}
	last_top = this_top;  
});

// Gnb open/close
function gnbOpen (_this) {
	var _this= $(_this);
	_this.parent().find(".gnb_wrap").animate({right:"0"},200);
	_this.parent().find(".opacity_bg").show();
}
function gnbClose (_this) {
	var _this= $(_this);
	_this.parents(".gnb_wrap").animate({right:"-280px"},200);
	_this.parents("header").find(".opacity_bg").hide();
}

// Accodian
function accodianFunc (_this) {
	var _this = $(_this);
	if (_this.parent().hasClass("active")) {
		_this.parent().removeClass("active");
		_this.parent().find("ul").stop().slideUp(200);
	}else {
		_this.parents(".gnb").find(">li").removeClass("active");
		_this.parents(".gnb").find("ul").stop().slideUp(200);
		_this.parent().addClass("active");
		_this.parent().find("ul").stop().slideDown(200);
	}
}

// ToolTip
function toolTipFunc (_this) {
	var _this = $(_this);
	if (_this.hasClass("active")) {
		_this.parent().find(".tooltip_box").fadeOut();
		_this.removeClass("active");
	}else {
		_this.parent().find(".tooltip_box").fadeIn();
		_this.addClass("active");
	}
}

// 그래프 형태
function showUp (_this) {
	var _this = $(_this);
	_this.parents(".realTime").find(".up_table").css({"overflow-y" : "scroll"});
	_this.parents(".realTime").find(".up_table").animate({height: "683px"}, 300);
	_this.parents(".realTime").find(".down_table").animate({height: "0"}, 300);
	_this.parents(".realTime").find(".newest").animate({top: "35px"}, 300);
}
function showDown (_this) {
	var _this = $(_this);
	_this.parents(".realTime").find(".down_table").css({"overflow-y" : "scroll"});
	_this.parents(".realTime").find(".down_table").animate({height: "688px"}, 300);
	_this.parents(".realTime").find(".up_table").animate({height: "0"}, 300);
	_this.parents(".realTime").find(".newest").animate({top: "728px"}, 300);
}
function showBoth (_this) {
	var _this = $(_this);
	_this.parents(".realTime").find(".down_table, .up_table").css({"overflow" : "hidden"});
	_this.parents(".realTime").find(".down_table").animate({height: "341px"}, 300);
	_this.parents(".realTime").find(".up_table").animate({height: "336px"}, 300);
	_this.parents(".realTime").find(".newest").animate({top: "380px"}, 300);
}