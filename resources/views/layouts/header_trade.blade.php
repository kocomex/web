<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{"WIIX | ".$title}}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

   <link rel="shortcut icon" href="{{ URL::asset('theme/front_users/images/favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ URL::asset('theme/front_users/images/favicon.ico') }}" type="image/x-icon">
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >

    <link rel="stylesheet" href="{{ URL::asset('theme/front_users/css/jquery.mCustomScrollbar.css') }}" >

   <link rel="stylesheet" href="{{ URL::asset('theme/front_users/css/bootstrap.min.css') }}" >
  <link rel="stylesheet" href="{{ URL::asset('theme/front_users/css/font-awesome.min.css') }}" >
  <link rel="stylesheet" href="{{ URL::asset('theme/front_users/css/style.css') }}" >
   <link rel="stylesheet" href="{{ URL::asset('theme/front_users/css/owl.carousel.css') }}" >
    <link rel="stylesheet" href="{{ URL::asset('theme/front_users/css/responsive.css') }}" >
     <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500" >
     <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.2.4.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('theme/front_users/js/notifIt.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ URL::asset('theme/front_users/css/notifIt.css') }}">

<script type="text/javascript">
  $( document ).ready(function() {
 <?php if(session()->has('success')) { ?>

  var sucess= '{!! session('success') !!}';
  notif({

msg: '<img src="{{ URL::asset("theme/front_users/images/sucesssmile.png") }}" height="50px" width="50px"/>'+' '+' '+' '+sucess,
 width: 700,
height: 60,

type: "success"

});
  <?php } ?>

   <?php if(session()->has('error')) { ?>

  var error= '{!! session('error') !!}';
 notif({
        type: "error",
        msg: '<img src="{{ URL::asset("theme/front_users/images/sadsmile.png") }}" height="50px" width="50px"/>'+' '+' '+' '+error,
        
        width: 700,
        height: 60,
        autohide: true
      });
  <?php } ?>
});

</script>
<style type="text/css">
.error {
    color: #ED583D;
    font-family: Times;
    font-style: italic;
    font-size: 17px;
    font-weight: bolder;
}
input.form-control.error {
    border-color: red;
    color: red;
}
</style>

<style type="text/css">
  /* Preloader */
#preloader {
  position: fixed;
  top:0;
  left:0;
  right:0;
  bottom:0;
  background-color:rgba(0,0,0,0.3); /* change if the mask should have another color then white */
  z-index:99; /* makes sure it stays on top */
}

#status {
  width:200px;
  height:200px;
  position:absolute;
  left:50%; /* centers the loading animation horizontally one the screen */
  top:50%; /* centers the loading animation vertically one the screen */
  background-image:url(http://bitcoins.idealogue.io/assets/img/illustrations/bitcoin_circulation_64.gif); /* path to your loading animation */
  background-repeat:no-repeat;
  background-position:center;
  margin:-100px 0 0 -100px; /* is width and height divided by two */
}
</style>
<script type="text/javascript">
    //<![CDATA[
        $(window).on('load', function() { // makes sure the whole site is loaded 
            $('#status').fadeOut(); // will first fade out the loading animation 
            $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
            $('body').delay(350).css({'overflow':'visible'});
          })
    //]]>
</script>
 <script type="text/javascript">
    function show_loader()
{
    $('#preloader').fadeOut('slow',function(){$(this).show();});
    $('#status').fadeOut('slow',function(){$(this).show();});
}
function hide_loader()
{
    $('#preloader').fadeOut('slow',function(){$(this).hide();});
       $('#status').fadeOut('slow',function(){$(this).show();});
}
  </script>
</head>

<body>
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<section class="tradeTabSec navbar-expand-lg w-100">
  <div class="container-fluid">
    <div class="row no-gutters">
      <div class="col-md-3">
        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ URL::asset('theme/front_users/images/'.$settings->logo) }}" alt="WIIX" /></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogg"
          aria-controls="navbarToggler02" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
      </div>
      <div class="col-md-6">
        <div class="currSlider">
          <div class="owl-carousel1">

 @foreach($trade_rates as $rates)

                     <div class="items">
              <div class="coinSlider">
                <div class="slMidSec">{{$rates->pair_name}}:  <span class="clrNeg">- {{$rates->sell_rate}}</span></div>
              </div>
            </div>
       
            @endforeach
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="collapse navbar-collapse justify-content-end hidden-sm-down" id="navbarTogg">
            <?php if(Session::get('user_id') != "") { ?> 
            <ul class="navbar-nav logUsr">
            <li class="nav-item dropdown">
            <?php $profe=Helper::displayprof_pic(Session::get('user_id'));?>
              <div class="logUsrImg"><img src="{{ URL::asset('public/verify/document/'.$profe) }}" height="30" width="30" /></div>
              <a class="nav-link dropdown-toggle" href="#" id="" data-toggle="dropdown">{{ Session::get('consumer_name') }}</a>
              <ul class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ url('/profile') }}">Profile</a>
                <a class="dropdown-item" href="{{ url('/logout') }}">Logout</a>
         
              </ul>
            </li>
        </ul>
        <?php } ?>
        </div>
      </div>
    </div>
</section>
