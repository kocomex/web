<!-- Footer section starts here -->
<footer class="footerSec">
  <div class="container">
    <div class="row footerLinks justify-content-center">
      <div class="col-md-12 text-center">
        <div class="row">
          <div class="col-12">
            <ul class="list-inline">
              <li class="list-inline-item"><a href="{{ url('/about') }}">About Us</a></li>
              <li class="list-inline-item"><a href="{{ url('/news') }}">News</a></li>
              <li class="list-inline-item"><a href="{{ url('/faq') }}">FAQ</a></li>
              <li class="list-inline-item"><a href="{{ url('/terms') }}">Terms of Service</a></li>
              <li class="list-inline-item"><a href="{{ url('/policy') }}">Privacy Policy</a></li>
              <li class="list-inline-item"><a href="{{ url('/contact') }}">Contact Us</a></li>
            </ul>
          </div>
        </div>
        <hr/>
        <div class="row mt-30">
          <div class="col-12 socialIcons">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="{{$settings->facebook_url}}" class="fbIcon"><span class="fa fa-facebook"></span></a>
              </li>
              <li class="list-inline-item">
                <a href="{{$settings->twitter_url}}" class="twIcon"><span class="fa fa-twitter"></span></a>
              </li>
              <li class="list-inline-item">
                <a href="{{$settings->google_url}}" class="gplusIcon"><span class="fa fa-google-plus"></span></a>
              </li>
              <li class="list-inline-item">
                <a href="{{$settings->linkedin_url}}" class="inIcon"><span class="fa fa-linkedin"></span></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
        <p class="copyRgtLbl">Copyrights @ 2017. All Rights Reserved.</p>
      </div>
    </div>
  </div>
</footer>
<!-- Footer section end here -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<!-- <script src="jquery-3.2.1.slim.min.js"></script>
<script src="js/bootstrap.min.js"></script>
 <script src="js/popover.js"></script>   -->


<!-- <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
(function($){
  $(window).on("load",function(){
    var width = $(window).width();

      if(width>1024){
      $(".abtScroll").mCustomScrollbar({
        scrollButtons:{
        enable:false
        },

        scrollbarPosition: 'inside',
        autoExpandScrollbar:true,
        theme: 'minimal-dark',
        axis:"y",
        setWidth: "auto"
        });

      }else{
      $(".abtScroll").mCustomScrollbar({
      scrollButtons:{
      enable:false
      },

      scrollbarPosition: 'inside',
      autoExpandScrollbar:true,
      theme: 'minimal-dark',
      axis:"x",
      setWidth: "auto"
      });

  }
  });
})(jQuery);

</script> -->
<script>
$(function () {
  $(document).scroll(function () {
    var $nav = $(".fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  });
});
</script>

<script type="text/javascript" src="{{ URL::asset('theme/front_users/js/owl.carousel.js') }}"></script>

<script type="text/javascript">
  var owl = jQuery('.owl-carousel');
  owl.owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    responsive: {
  0: {
  items:1,
  nav: true
  },
  600: {
  items: 3,
  nav: true,
  margin: 10
  },
  1150: {
  items: 5,
  nav: true,
  loop: true,
  margin:10
  }
  }
  });
</script>
<!-- <script>
  setTimeout(function() {
    document.getElementById('loader').style.display="none";
  }, 2000);
</script> -->
<script>
  // Javascript to enable link to tab
  var url = document.location.toString();
  if (url.match('#')) {
      $('.nav-tabs a[href="#' + url.split('#')[1] + '-tab"]').tab('show');
  } //add a suffix

  // Change hash for page-reload
  $('.nav-tabs a').on('shown.bs.tab', function (e) {
      window.location.hash = e.target.hash;
  })
</script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
  $(document).ready(function() {
    $('#depHisTbl').DataTable();
    $('#wdwHisTbl').DataTable();
    $('#tradeHisTbl').DataTable();
  });
</script>
<script>

  $(document).ready(function() {
    $('.hiwDet ul li').click(function(){
          var tab_id = $(this).attr('data-tab');
  
          $('ul li').removeClass('current');
          $('.twTabpanel').removeClass('current');
  
          $(this).addClass('current');
          $("#"+tab_id).addClass('current');
    });

    $('ul.nav li').click(function(){
          var tab_id = $(this).attr('data-tab');
  
          $('ul.nav li').removeClass('active');
          $('.tab-pane').removeClass('active');
  
          $(this).addClass('active');
          $("#"+tab_id).addClass('active');
    });
  });

  
</script>
</script>





</body>
</html>
