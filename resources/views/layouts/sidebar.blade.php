
<?php 
$url1 = Request::segment(2);
$url2 = Request::segment(3);
?>

<aside class="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <div class="brand">
                <div class="logo"> <span class="l l1"></span>
                    <span class="l l2"></span>
                    <span class="l l3"></span>
                    <span class="l l4"></span>
                    <span class="l l5"></span></div> WIIX Admin </div>
                </div>
                <nav class="menu">
                    <ul class="nav metismenu" id="sidebar-menu">

                        <li class="<?php if($url1 == 'dashboard'){ echo 'active';} else { echo ' ';}?>"> <a href="{{ url('/WiPlytaIIX2/dashboard') }}">
                          <i class="fa fa-home"></i> Dashboard  
                      </a> </li>

                      <li class="<?php if($url1 == 'users'){ echo 'active';} else { echo ' ';}?>"> <a href="{{ url('/WiPlytaIIX2/users') }}">
                        <i class="fa fa-user"></i> User management  
                    </a> </li>

                    <!-- History mangement -->

                    <li class="<?php if($url1 == 'dephistory' || $url1 == 'withhistory' || $url1 == 'tradehistory'){ echo 'active';} else { echo ' ';}?>" > <a href=""><i class="fa fa-bars"></i> History Management<i class="fa arrow"></i></a>

                        <ul class="collapse <?php if($url1 == 'dephistory' || $url1 == 'withhistory' || $url1 == 'tradehistory'){ echo 'in';} else { echo ' ';}?>">
                            <!-- <li class="" > <a href="{{ url('/WiPlytaIIX2/content/addPage') }}">Add CMS Page</a> </li> -->

                            <li class="<?php if($url1 == 'dephistory'){ echo 'active';} else { echo ' ';}?>" > <a href="{{ url('/WiPlytaIIX2/dephistory') }}">Deposit history</a> </li>

                            <li class="<?php if($url1 == 'withhistory'){ echo 'active';} else { echo ' ';}?>" > <a href="{{ url('/WiPlytaIIX2/withhistory') }}">Withdraw history</a> </li>

                             <li class="<?php if($url1 == 'tradehistory'){ echo 'active';} else { echo ' ';}?>" > <a href="{{ url('/WiPlytaIIX2/tradehistory') }}">Trade history</a> </li>

                             <li class="<?php if($url1 == 'orderhistory'){ echo 'active';} else { echo ' ';}?>" > <a href="{{ url('/WiPlytaIIX2/orderhistory') }}">Order history</a> </li>
                        </ul>
                    </li>  

                    <li class="<?php if($url1 == 'profit'){ echo 'active';} else { echo ' ';}?>"> <a href="{{ url('/WiPlytaIIX2/profit') }}">
                          <i class="fa fa-money"></i> Admin profit  
                      </a> </li>



                    <!-- End history -->

                    <li class="<?php if($url1 == 'template'){ echo 'active';} else { echo ' ';}?>"> <a href="{{ url('/WiPlytaIIX2/template') }}">
                        <i class="fa fa-envelope"></i> Email Template  
                    </a> </li>
                    <li class="<?php if($url1 == 'Subscribers'){ echo 'active';} else { echo ' ';}?>"> <a href="{{ url('/WiPlytaIIX2/Subscribers') }}">
                        <i class="fa fa-list"></i> Subcribers  
                    </a> </li>

                    <li class="<?php if($url1 == 'enquires'){ echo 'active';} else { echo ' ';}?>"> <a href="{{ url('/WiPlytaIIX2/enquires') }}">
                        <i class="fa fa-headphones"></i> support  
                    </a> </li>

                    <li class="<?php if($url1 == 'content' || $url1 == 'addFaq' || $url1 == 'addnews' || $url1 == 'viewFaqs'|| $url1 == 'viewnews'|| $url1 == 'editFaq'|| $url1 == 'editnewsq'){ echo 'active';} else { echo ' ';}?>" > <a href=""><i class="fa fa-file-text-o"></i> Content Management<i class="fa arrow"></i></a>
                        <ul class="nav metismenu <?php if($url2 == 'viewPages' || $url1 == 'addFaq' 
                        || $url1 == 'viewFaqs' || $url1 == 'addnews' || $url1 == 'editnews' || $url1 == 'viewnews'){ echo 'collapse in';} else { echo ' ';}?>">
                        <li class=""><a href=""> CMS Pages <i class="fa arrow"></i></a>
                            <ul class="collapse <?php if($url2 == 'viewPages'){ echo 'in';} else { echo ' ';}?>">


                                <li class="<?php if($url2 == 'viewPages'){ echo 'active';} else { echo ' ';}?>" > <a href="{{ url('/WiPlytaIIX2/content/viewPages') }}">View All Pages</a> </li>
                            </ul>
                        </li>  

                        <li class=""><a href=""> FAQ <i class="fa arrow"></i></a>
                            <ul class="collapse <?php if($url1 == 'addFaq' 
                            || $url1 == 'editFaq' || $url1 == 'viewFaqs'){ echo 'in';} else { echo ' ';}?>">
                            <li class="<?php if($url1 == 'addFaq'){ echo 'active';} else { echo ' ';}?>" > <a href="{{ url('/WiPlytaIIX2/addFaq') }}">Add FAQ</a> </li>
                            <li class="<?php if($url1 == 'viewFaqs'){ echo 'active';} else { echo ' ';}?>"><a href="{{ url('/WiPlytaIIX2/viewFaqs') }}">View All FAQs</a> </li>
                        </ul>
                    </li>

                    <li class=""><a href=""> TestFAQ <i class="fa arrow"></i></a>
                            <ul class="collapse <?php if($url1 == 'addFaq' 
                            || $url1 == 'editFaq' || $url1 == 'viewFaqs'){ echo 'in';} else { echo ' ';}?>">
                            <li class="<?php if($url1 == 'addFaq'){ echo 'active';} else { echo ' ';}?>" > <a href="{{ url('/WiPlytaIIX2/addFaqtest') }}">Add Test FAQ</a> </li>
                            <li class="<?php if($url1 == 'viewFaqs'){ echo 'active';} else { echo ' ';}?>"><a href="{{ url('/WiPlytaIIX2/viewFaqstest') }}">View Test All FAQs</a> </li>
                        </ul>
                    </li>

                     <li class=""><a href=""> News <i class="fa arrow"></i></a>
                            <ul class="collapse <?php if($url1 == 'addnews' || $url1 == 'editnews' || $url1 == 'viewnews'){ echo 'in';} else { echo ' ';}?>">
                            <li class="<?php if($url1 == 'addnews'){ echo 'active';} else { echo ' ';}?>" > <a href="{{ url('/WiPlytaIIX2/addnews') }}">Add News</a> </li>
                            <li class="<?php if($url1 == 'viewFaqs'){ echo 'active';} else { echo ' ';}?>"><a href="{{ url('/WiPlytaIIX2/viewnews') }}">View All News</a> </li>
                        </ul>
                    </li>

                </ul> 
            </li> 



        </ul>
    </nav>
</div>
<footer class="sidebar-footer">
    <ul class="nav metismenu" id="customize-menu">
        <li>
            <ul>
                <li class="customize">
                    <div class="customize-item">
                        <div class="row customize-header">
                            <div class="col-xs-4"> </div>
                            <div class="col-xs-4"> <label class="title">fixed</label> </div>
                            <div class="col-xs-4"> <label class="title">static</label> </div>
                        </div>
                        <div class="row hidden-md-down">
                            <div class="col-xs-4"> <label class="title">Sidebar:</label> </div>
                            <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed" >
                                <span></span>
                            </label> </div>
                            <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="sidebarPosition" value="">
                                <span></span>
                            </label> </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4"> <label class="title">Header:</label> </div>
                            <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="headerPosition" value="header-fixed">
                                <span></span>
                            </label> </div>
                            <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="headerPosition" value="">
                                <span></span>
                            </label> </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4"> <label class="title">Footer:</label> </div>
                            <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="footerPosition" value="footer-fixed">
                                <span></span>
                            </label> </div>
                            <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="footerPosition" value="">
                                <span></span>
                            </label> </div>
                        </div>
                    </div>
                    <div class="customize-item">
                        <ul class="customize-colors">
                            <li> <span class="color-item color-red" data-theme="red"></span> </li>
                            <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
                            <li> <span class="color-item color-green active" data-theme=""></span> </li>
                            <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
                            <li> <span class="color-item color-blue" data-theme="blue"></span> </li>
                            <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
                        </ul>
                    </div>
                </li>
            </ul>  </li>
        </ul>
    </footer>
</aside>
<div class="sidebar-overlay" id="sidebar-overlay"></div>



