
<header class="header">
	<div class="header-block header-block-collapse hidden-lg-up">
		<button class="collapse-btn" id="sidebar-collapse-btn">
		<i class="fa fa-bars"></i>
</button> </div>

<div class="header-block header-block-search hidden-sm-down">
<h3 class="title"><?php if(isset($title)){ echo $title; }else{ ?> Admin Panel <?php } ?></h3>
</div>


 
 
    	
<div class="header-block header-block-nav">
<ul class="nav-profile">
	
<li class="profile dropdown">


<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
<span class="name">
<i class="fa fa-gear icon"></i>
Settings
</span></a>

<div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">

 <a class="dropdown-item" href="{{ url('/WiPlytaIIX2/profile') }}">
<i class="fa fa-user icon"></i>
Profile
</a> <a class="dropdown-item" href="{{ url('/WiPlytaIIX2/settings') }}">
<i class="fa fa-gear icon"></i>
Site Settings
</a>


<a class="dropdown-item" href="{{ url('/WiPlytaIIX2/adminbank') }}">
<i class="fa fa-university icon"></i>
Admin bank details
</a>


<div class="dropdown-divider"></div> <a class="dropdown-item" href="{{ url('/WiPlytaIIX2/logout') }}">
<i class="fa fa-power-off icon"></i>
Logout
</a> </div>
</li>
</ul>
</div>
</header>
