@extends('wallet.layouts/admin')
<div class="loader" id="myLoad">
  <div class="spinner">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
  </div>
</div>
@section('content')

<?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success!</strong><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php } ?>
    
<ul class="breadcrumb cm_breadcrumb">
	<li><a href="#">Home</a></li>
	</ul>
	<div class="mainWrapper">
	<div class="cardsTopSec mb-20">
	 <div class="row">
	    <div class="col-md-3 col-sm-3">
			<div class="cardsBlk cardsClr4">
				<p>Wallet BTC Balance </p>
				<div class="midSec">
			  		 <!-- {{ 0.00000000 }} -->
					 <?php echo $btc_balance; ?>
				</div>
			</div>
		</div>

		<div class="col-md-3 col-sm-3">
			<div class="cardsBlk cardsClr3">
				<p>Wallet ETH Balance </p>
				<div class="midSec">
			  		 {{ 0.00000000 }}
				</div>
			</div>
		</div>
      <div class="col-md-3 col-sm-3">
      <div class="cardsBlk cardsClr3">
        <p>Wallet LTC Balance </p>
        <div class="midSec">
            <!-- {{ 0.00000000 }} -->
            <?php echo $ltc_balance; ?>
        </div>
      </div>
    </div>
      <div class="col-md-3 col-sm-3">
      <div class="cardsBlk cardsClr3">
        <p>Wallet XRP Balance </p>
        <div class="midSec">
              {{ 0.00000000 }}
        </div>
      </div>
    </div>
    </div>

   
	
	  <div class="row">
	    <div class="col-md-3 col-sm-3">
			<div class="cardsBlk cardsClr4">
				<p>Wallet DOGE Balance </p>
				<div class="midSec">
			  		<!-- {{ 0.00000000 }} -->
			  		<?php echo $doge_balance; ?>
				</div>
			</div>
		</div>

		<div class="col-md-3 col-sm-3">
			<div class="cardsBlk cardsClr4">
				<p>Wallet ETC Balance </p>
				<div class="midSec">
			  		{{ 0.00000000 }}
				</div>
			</div>
		</div>

    <div class="col-md-3 col-sm-3">
      <div class="cardsBlk cardsClr4">
        <p>Wallet XMR Balance </p>
        <div class="midSec">
            {{ 0.00000000 }}
        </div>
      </div>
    </div>
       <div class="col-md-3 col-sm-3">
      <div class="cardsBlk cardsClr4">
        <p>Wallet BTG Balance </p>
        <div class="midSec">
            <!-- {{ 0.00000000 }} -->
            <?php echo $btg_balance; ?>
        </div>
      </div>
    </div>
	  </div>
      <div class="row">
      <div class="col-md-3 col-sm-3">
      <div class="cardsBlk cardsClr4">
        <p>Wallet DASH Balance </p>
        <div class="midSec">
            <!-- {{ 0.00000000 }} -->
            <?php echo $dash_balance; ?>
        </div>
      </div>
    </div>
      

    </div>
	</div>


	<div class="fancy-collapse-panel usrsFees">
		<div class="panel-group" id="accordionUsrsFees" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
		  <div class="panel-heading" role="tab" id="headingOne">
		      <h4 class="panel-title">
		          <div class="panelLft">
		            <img src="{{asset('/').('public/admin_assets/images/users-icon.png')}}" /><span>Profit</span>
		          </div>
		         
		      </h4>
		  </div>
		  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      <div class="panel-body panelMinHgt">
		      <div id="chartdiv2"></div>
		         
		      </div>
		  </div>
		</div>
		</div>
	</div>

	<ul id="draggablePanelList" class="list-unstyled">
        <li class="panel">
          
        </li>

        <li class="panel">
          
        </li>

        <li class="panel">
          
        </li>
    </ul>

</div>
<style type="text/css">
    #chartdiv2 {
  width   : 100%;
  height    : 350px;
  font-size : 11px;
}  
</style>
<script type="text/javascript">
    function getprofit_chart(year,currency) {
$.getJSON("{{ URL::to('Wi72IX/billetera/profitChart?year=') }}"+year+'&currency='+currency, function (data) {

var chart = AmCharts.makeChart("chartdiv2", {
    "hideCredits":true,
    "theme": "none",
    "type": "serial",
    "startDuration": 2,
    "dataProvider": data,
    "valueAxes": [{
        "position": "left",
        "axisAlpha":0,
        "gridAlpha":0
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "colorField": "color",
        "fillAlphas": 0.85,
        "lineAlpha": 0.1,
        "type": "column",
        "topRadius":1,
        "valueField": "profit"
    }],
    "depth3D": 40,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "Month",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha":0,
        "gridAlpha":0

    },
    "export": {
      "enabled": true
     }

});
});
}


$(function () {
  var dt = new Date();
  year = dt.getFullYear();
  $('#dep_with_chart_year2').val(year);
  getprofit_chart(year,'KRW');
  $('#dep_with_chart_curr2').val('KRW');
});

function set_year_chart2()
{
  year = $('#dep_with_chart_year2').val();
  currency = $('#dep_with_chart_curr2').val();
  getprofit_chart(year,currency);
}
function set_curr_chart2()
{
  currency = $('#dep_with_chart_curr2').val();
  year = $('#dep_with_chart_year2').val();
  getprofit_chart(year,currency);
}
</script>

<script type="text/javascript">
/*search menu click jquery starts*/
$('.showresult').click(function(){
$('.searc_drop').css('display','block');
});

$(document).mouseup(function(e)
{
var container = $(".searc_drop");

if (!container.is(e.target) && container.has(e.target).length === 0)
{
 $('.searc_drop').css('display','none');
}
});


/*notification checkbox starts*/
$(document).ready(function(e) {
$('.setting_drp input').lc_switch();

// triggered each time a field changes status
$('body').delegate('.lcs_check', 'lcs-statuschange', function() {
var status = ($(this).is(':checked')) ? 'checked' : 'unchecked';
console.log('field changed status: '+ status );
});

// triggered each time a field is checked
$('body').delegate('.lcs_check', 'lcs-on', function() {
console.log('field is checked');
});

// triggered each time a is unchecked
$('body').delegate('.lcs_check', 'lcs-off', function() {
console.log('field is unchecked');
});
});
/*notification checkbox ends*/
</script>
<script>
jQuery(function($) {
    var panelList = $('#draggablePanelList');

    panelList.sortable({
       
        handle: '.panel-heading',
        update: function() {
            $('.panel', panelList).each(function(index, elem) {
                 var $listItem = $(elem),
                     newIndex = $listItem.index();
            });
        }
    });
});
</script>
<script>
(function () {
    var Message;
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
    $(function () {
        var getMessageText, message_side, sendMessage;
        message_side = 'right';
        getMessageText = function () {
            var $message_input;
            $message_input = $('.message_input');
            return $message_input.val();
        };
        sendMessage = function (text) {
            var $messages, message;
            if (text.trim() === '') {
                return;
            }
            $('.message_input').val('');
            $messages = $('.messages');
            message_side = message_side === 'left' ? 'right' : 'left';
            message = new Message({
                text: text,
                message_side: message_side
            });
            message.draw();
            return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        };
        $('.send_message').click(function (e) {
            return sendMessage(getMessageText());
        });
        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                return sendMessage(getMessageText());
            }
        });
        sendMessage('Hello Philip! :)');
        setTimeout(function () {
            return sendMessage('Hi Sandy! How are you?');
        }, 1000);
        return setTimeout(function () {
            return sendMessage('I\'m fine, thank you!');
        }, 2000);
    });
}.call(this));
</script>
<script src="{{asset('/').('public/admin_assets/js/moment.min.js')}}"> </script>

<script type = "text/javascript">
  setTimeout(function(){
     document.getElementById("myLoad").style.display="none";
  }, 3000);
</script>

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>




@stop
