<header class="main-header">
  <?php $getSite = Helper::getSiteLogo(); ?>
    <div class="tp_layer1"> <a href="{{ URL::to('Wi72IX/billetera') }}" class="logo">
      <!-- Logo -->
      <span class="logo-mini">
      <!--<b>A</b>H-admin-->
      <img src="{{ $getSite }}" alt="logo_small"> </span> <span class="logo-lg">
      <!--<b>Admin</b>H-admin-->
      <img src="{{ $getSite }}" alt="logo_large"> </span> </a> <a href="#" class="sidebar-toggle hidden-norm" data-toggle="offcanvas" role="button"> <span class="fa fa-bars hdt_cnt">Dashboard</span> </a> </div>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
      <div class="mn_righ">
        <div class="mn_rightp fd_rw">
          <div class="tp_sear1">
           
          </div>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-right">
              <!-- user -->
              <li class="dropdown dropdown-user"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <div class="user-image"><span class="hidden-xs">Hi, {{Session::get('walletName')}}</span> <img src="{{asset('/').('public/admin_assets/images/mn_imusr.png')}}" class="img-responsive" alt="User"> </div>
                </a>
                <ul class="dropdown-menu usr_drpmn">
                  <!-- User image -->
                  <li class="user-header">
                    <div class="usr_mask">
                      <p> {{Session::get('adminName')}} </p>
                    </div>
                   
                    <div class="admin_profile_icon">
                      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFkxodPFiEpNyPnRiPut4WL7FwryFDPdD5g7nBO_7ei9DV5TIq" class="img-responsive" alt="User Image"> 
                    </div>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="">
                    
                      <div class=""> <a href="{{ URL::to('Wi72IX/billetera/changePassword') }}" class="active">Change Password</a> </div>
                    </div>
                    <!-- /.row -->
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer text-center"> <a href="{{ URL::to('Wi72IX/billetera/logout') }}" class="btn btn-flat center-block">Logout</a> </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  </header>