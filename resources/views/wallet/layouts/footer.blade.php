<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('/').('public/admin_assets/js/jquery-ui.js')}}"> </script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('/').('public/admin_assets/js/bootstrap.min.js')}}"> </script>
<script src="{{asset('/').('public/admin_assets/js/dashboard.js')}}"> </script>
<script src="{{asset('/').('public/admin_assets/js/lc_switch.js')}}"> </script>

<link rel="stylesheet" href="{{asset('/').('public/admin_assets/css/lc_switch.css')}}">
<link rel="stylesheet" href="{{asset('/').('public/admin_assets/css/jquery.dataTables.min.css')}}">

<script src="{{asset('/').('public/admin_assets/js/jquery.dataTables.min.js')}}"> </script>

<script type="text/javascript">
/*search menu click jquery starts*/
$('.showresult').click(function(){
$('.searc_drop').css('display','block');
});
/*search menu click jquery ends*/

/*search menu close outside the container (i.e)., while clicking body starts*/
$(document).mouseup(function(e)
{
var container = $(".searc_drop");
// if the target of the click isn't the container nor a descendant of the container
if (!container.is(e.target) && container.has(e.target).length === 0)
{
 $('.searc_drop').css('display','none');
}
});
/*search menu close outside the container (i.e)., while clicking body ends*/

/*notification checkbox starts*/
$(document).ready(function(e) {
$('.setting_drp input').lc_switch();

// triggered each time a field changes status
$('body').delegate('.lcs_check', 'lcs-statuschange', function() {
var status = ($(this).is(':checked')) ? 'checked' : 'unchecked';
console.log('field changed status: '+ status );
});

// triggered each time a field is checked
$('body').delegate('.lcs_check', 'lcs-on', function() {
console.log('field is checked');
});

// triggered each time a is unchecked
$('body').delegate('.lcs_check', 'lcs-off', function() {
console.log('field is unchecked');
});
});
/*notification checkbox ends*/
</script>

<script>
$(document).ajaxSuccess(function(event, jqXHR, settings) { 
    resetAjaxToken(jqXHR);
});

function resetAjaxToken(jqXHR) 
{  
   var token = jqXHR.getResponseHeader("CI-CSRF-Token"); 
   $('input[name="csrf_test_name"]').val(token);
}

$(document).ready(function(){
  setTimeout(function() {
    $('.alert').fadeOut('fast');
  }, 3000); // <-- time in milliseconds
});

$(document).ready(function() {
    (function(seconds) {
      var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {
               location.href = "{{ URL::to('Wi72IX/billetera/logout') }}";
            }, seconds * 60000);
        };
    $(document).on('keypress click mousemove', function() { intvrefresh() });
    intvrefresh();
    }(5)); 

    window.onbeforeunload = function () {
        location.href = "{{ URL::to('Wi72IX/billetera/logout') }}";
    };

  });
</script>

</script>