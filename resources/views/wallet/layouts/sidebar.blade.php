<aside class="main-sidebar">
    <div class="sidebar">
      <!-- sidebar menu -->
      <ul class="sidebar-menu" id="accord_side">
        <li class="hidden-xs"> 
            <a href="{{ URL::to('Wi72IX/billetera') }}" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="fa fa-bars hdt_cnt">Dashboard</span> </a>
        </li>
        <?php 
          $currentRoute = \Route::getCurrentRoute()->getActionName();
          $explodeRoute = explode('@', $currentRoute);
          $uri = $explodeRoute[1];
        ?>
       
       <?php if(Session::get('walletId') != "") { ?> 

        <li class="<?php if($uri == "index") { echo "active"; } ?>"> 
            <a href="{{ URL::to('Wi72IX/billetera') }}" class="mn_catgcur"><span>Dashboard</span> </a>
        </li>

        <li class="<?php if($uri == "walletDeposit") { echo "active"; } ?>"> 
            <a href="{{ URL::to('Wi72IX/billetera/viewAdminDeposit') }}" class="mn_catgcur"><span>Deposit</span> </a>
        </li>

        <li class="<?php if($uri == "walletWithdraw") { echo "active"; } ?>"> 
            <a href="{{ URL::to('Wi72IX/billetera/viewAdminWithdraw') }}" class="mn_catgcur"><span>Withdraw</span> </a>
        </li>

        <li class="<?php if($uri == "walletWithdrawHist" || $uri == "walletDepositHist") { echo "active"; } ?>"> <a class="mn_sib" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn2" aria-expanded="true"><span>Transaction History</span></a>
          <ul id="sid_mn2" class="mnsub_catglis collapse <?php if($uri == "walletWithdrawHist" || $uri == "walletDepositHist") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to('Wi72IX/billetera/viewDepositHistory') }}" class="mnsub_catg">Deposit History</a></li>
            <li><a href="{{ URL::to('Wi72IX/billetera/viewWithdrawHistory') }}" class="mnsub_catg">Withdraw History</a></li>
          </ul>
        </li>

        <li class="<?php if($uri == "walletProfit") { echo "active"; } ?>"> 
            <a href="{{ URL::to('Wi72IX/billetera/viewAdminProfit') }}" class="mn_catgcur"><span>Profit</span> </a>
        </li>

        <?php } ?>
      </ul>
    </div>
</aside>