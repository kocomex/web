@extends('wallet.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to('Wi72IX/billetera') }}">Home</a></li>
  <li><a href="#">Admin Profit</a></li>
  <li><a href="#">Profit List</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>Profit List</h3>
    </div>
     <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success!</strong><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php } ?>

    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="mb-20">
        <button type="button" class="btn cm_blacbtn" id="view_admin_profit">View Admin Profit</button>
      </div>
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S-No<span class="fa fa-sort"></span></th>
                    <th>User Email<span class="fa fa-sort"></span></th>
                    <th>Profit amount<span class="fa fa-sort"></span></th>
                    <th>Currency<span class="fa fa-sort"></span></th>
                    <th>Type<span class="fa fa-sort"></span></th>
                    <th>Date<span class="fa fa-sort"></span></th>
                  </tr>
                </thead>
                <tbody> 

                  <?php if($result) { $ii=1; foreach($result as $res) {  
                  $getUser1 = DB::table('wiix_consumers')->where('user_id',$res->userId)->get();

                  $getUser=$getUser1[0];
                  $secmail=Helper::encrypt_decrypt("decrypt",$getUser->secret_key)."@".Helper::encrypt_decrypt("decrypt",$getUser->display);
                 
                  ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo $secmail ?></td>
                    <td><?php echo number_format(strip_tags($res['theftAmount']),8, '.', '') ?></td>
                    <td><?php echo strip_tags($res['theftCurrency']); ?></td>
                    <td><?php echo strip_tags($res['Type']); ?></td>
                    <td><?php echo strip_tags($res['coin_date']); ?></td>
                  </tr>
                  <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      </div>
    
  </form>
</div>

<div id="ticketAlert" class="modal fade modalPop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Admin Profit</h4>
        <button type="button" class="close" data-dismiss="modal"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></button>
      </div>

      <div class="modal-footer text-center">
        <table class="table m-0"> 
          <tr><td>Total profit</td><td><?php echo $profit['total_amount']; ?></td></tr>
          <tr><td>BTC profit</td><td><?php echo $profit['BTC_profit']; ?></td></tr>
          <tr><td>ETH profit</td><td><?php echo $profit['ETH_profit']; ?></td></tr>
          <tr><td>LTC profit</td><td><?php echo $profit['LTC_profit']; ?></td></tr>
          <tr><td>XRP profit</td><td><?php echo $profit['XRP_profit']; ?></td></tr>
            <tr><td>BTC profit</td><td><?php echo $profit['BTC_profit']; ?></td></tr>
          <tr><td>DOGE profit</td><td><?php echo $profit['DOGE_profit']; ?></td></tr>
          <tr><td>ETC profit</td><td><?php echo $profit['ETC_profit']; ?></td></tr>
          <tr><td>XMR profit</td><td><?php echo $profit['XMR_profit']; ?></td></tr>
            <tr><td>BTG profit</td><td><?php echo $profit['BTG_profit']; ?></td></tr>
          <tr><td>DASH profit</td><td><?php echo $profit['DASH_profit']; ?></td></tr>
          <tr><td>KRW profit</td><td><?php echo $profit['KRW_profit']; ?></td></tr>
       
        </table>
      </div>

    </div>
  </div>
</div>
<script>
$(document).ready(function(){
    $('#data_table_').DataTable();
});
$('#view_admin_profit').click(function(){
    $('#ticketAlert').modal('show');
})
</script>

@stop