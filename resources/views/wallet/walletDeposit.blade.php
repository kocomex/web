@extends('wallet.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to('Wi72IX/billetera') }}">Home</a></li>
  <li><a href="#">Deposit</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
 
    <div class="cm_head1">
      <h3>Admin BTC deposit</h3>
    </div>
    
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send bitcoins to you from mobile wallets by scanning this code.</h4></center>
   <?php //$btcAddress = Helper::getBTCAddress();
    $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$btc_address&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $btc_address }}</h4></center>

    <div class="cm_head1">
      <h3>Admin ETH deposit</h3>
    </div>
    
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send etherum to you from mobile wallets by scanning this code.</h4></center>
   <?php  //$ethAddress = Helper::getETHAddress();
   $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$eth_address&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $eth_address }}</h4></center>



    <div class="cm_head1">
      <h3>Admin LTC deposit</h3>
    </div>
    
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send litecoin to you from mobile wallets by scanning this code.</h4></center>
   <?php  $ltcAddress = Helper::getLTCAddress();
   $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$ltcAddress&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $ltcAddress }}</h4></center>




    <div class="cm_head1">
      <h3>Admin XRP deposit</h3>
    </div>
    
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send ripple to you from mobile wallets by scanning this code.</h4></center>
   <?php  $xrpAddress = Helper::getXMRAddress();
   $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$xrpAddress&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $xrpAddress }}</h4></center>

    <div class="cm_head1">
      <h3>Admin DOGE deposit</h3>
    </div>
    
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send doge to you from mobile wallets by scanning this code.</h4></center>
   <?php  $dogeAddress = Helper::getDOGEAddress();
   $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$dogeAddress&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $dogeAddress }}</h4></center>

    <div class="cm_head1">
      <h3>Admin ETC deposit</h3>
    </div>
    
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send etherum classic to you from mobile wallets by scanning this code.</h4></center>
   <?php  //$etcAddress = Helper::getETCAddress();
   $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$etc_address&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $etc_address }}</h4></center>

    <div class="cm_head1">
      <h3>Admin XMR deposit</h3>
    </div>
    
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send xmr to you from mobile wallets by scanning this code.</h4></center>
   <?php  $xmrAddress = Helper::getXMRAddress();
   $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$xmrAddress&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $xmrAddress }}</h4></center>

<div class="cm_head1">
      <h3>Admin BTG deposit</h3>
    </div>
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send bitcoingold to you from mobile wallets by scanning this code.</h4></center>
   <?php  //$btgAddress = Helper::getBTGAddress();
   $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$btg_address&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $btg_address }}</h4></center>


  <div class="cm_head1">
      <h3>Admin DASH deposit</h3>
    </div>
    
    <br>
  <center><h4>This is your receiving address as a QR code. It is possible to send dash to you from mobile wallets by scanning this code.</h4></center>
   <?php  //$dashAddress = Helper::getDASHAddress();
   $link = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$dash_address&choe=UTF-8&chld=L"; ?>
  <center><img src="<?php echo $link; ?>"  height="300px" width="300px"> </center> 
  <center><h4 style="color:#000;">{{ $dash_address }}</h4></center>
  
  
    
   </form> 
</div>

@stop