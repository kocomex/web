<script>
// alert('site');

$(document).ready(function() { 
 
 });
    jQuery.validator.addMethod("upper", function(value, element) {
        return this.optional(element) || /^(.*[A-Z].*)/.test(value);
    }, "Must include 1 uppercase character");
   
    jQuery.validator.addMethod("lower", function(value, element) {
        return this.optional(element) || /^(.*[a-z].*)/.test(value);
    }, "Must include 1 lowercase character");

    jQuery.validator.addMethod("specialchars", function(value, element) {
        return this.optional(element) || /^(?=.*[!@#$%&*()_+}])/.test(value);
    }, "Must include 1 special character");

    jQuery.validator.addMethod("onenumeric", function(value, element) {
        return this.optional(element) || /^(?=.*\d)/.test(value);
    }, "Must include 1 numberic");

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z,A-Z," "]+$/i.test(value);
    }, "Please enter alphabets only");

    jQuery.validator.addMethod("noSpace", function(value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "Space not allowed");

    jQuery.validator.addMethod("validEmail", function(value, element) {
        return this.optional( element ) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test( value );
    }, 'Please enter valid email address.');
    jQuery.validator.addMethod("alphanumeric", function(value, element) {
      var testregex = /^[a-zA-Z0-9]+$/i;
        return testregex.test(value);
    }, "Special characters not allowed");
    jQuery.validator.addMethod("num_only", function(value, element) {
      var testregex = /^[0-9]+$/i;
        return testregex.test(value);
    }, "Numbers only allowed");

</script> 


