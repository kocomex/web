<script>
	$('#logindetails').validate({
		rules: {
			email: {
          		required: true,
          		validEmail: true,
      		},
      		password: {
      			required: true,
				minlength: 8,
				upper: true,
				specialchars: true,
				onenumeric: true,
				lower: true,
				noSpace: true
      		},
      		fill_captcha : {
         		required:true,
         		alphanumeric:true
      		}
		},
		messages: {
			email: {
				required: "Please enter email address"
			},
			password: {
				required: "Please enter password",
				minlength: "Minimum length is 8",
				upper: "Must include 1 uppercase character",
				specialchars: "Must include 1 special character",
				onenumeric: "Must include 1 numberic",
				lower: "Must include 1 lowercase character",
				noSpace: "Space not allowed",
      		},
      		fill_captcha : {
          		required:"Please enter captcha code"
      		}
		}
	})

	$("#refresh").click(function(){
    	$.ajax({
			type:'GET',
			url:site_url+'/refresh',
			beforeSend:function(){ $('#refresh').hide(); },
			success:function(output){ $('#captcha_value').html('');$('#captcha_value').html(output);$('#refresh').show(); }
    	});
	});
</script>