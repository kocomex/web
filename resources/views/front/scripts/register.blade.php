<script>

// alert('register');
$(document).ready(function() { 
 
 });
    
//register 
$('#registerdetail').validate({
  rules: {
      usermail: {
          required: true,
          validEmail: true,
      },
      username: {
          required: true,
          lettersonly: true,
          minlength: 3,
          maxlength:20
      },
      mobile: {
          required: true,
          number: true,
          minlength: 7,
          maxlength:10
      },
      password: {
          required: true,
          minlength: 8,
          upper: true,
          specialchars: true,
          onenumeric: true,
          lower: true,
          noSpace: true
      },
      password_confirmation: {
          required: true,
          equalTo: '[name="password"]'
      },
      accept  : {
          required: true
      },
      fill_captcha : {
         required:true,
         alphanumeric:true
      }
  },
  messages: {
     usermail: {
          required: "Please enter email"
      },
      username: {
          required: "Please enter Username",
          lettersonly:"please enter characters only",
          minlength: "Minimum length is 3",
          maxlength: "Minimum length is 20",
      },
       mobile: {
          required:  "Please enter Mobilenumber",
          number:    "please enter numeric only",
          minlength: "Minimum length is 7",
          maxlength: "Minimum length is 10",
      },
      password: {
          required:     "Please enter password",
          minlength:    "Minimum length is 8",
          upper:        "Must include 1 uppercase character",
          specialchars: "Must include 1 special character",
          onenumeric:   "Must include 1 numberic",
          lower:        "Must include 1 lowercase character",
          noSpace:      "Space not allowed",
      },
      password_confirmation: {
          required: "Please enter confirm password",
          equalTo : "Please enter the same as password"
      },
      accept: {
           required: "Please Agree our terms and condition",
      },
      fill_captcha : {
          required:"Please enter captcha code"
      }
  }
});

$("#refresh").click(function(){
    $.ajax({
      type:'GET',
      url:site_url+'/refresh',
      beforeSend:function(){ $('#refresh').hide(); },
      success:function(output){ $('#captcha_value').html('');$('#captcha_value').html(output);$('#refresh').show(); }
    });
});
</script> 


