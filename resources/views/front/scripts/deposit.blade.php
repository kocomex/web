<script>

$('document').ready(function(){
	var currency = $("#crypto_select").val();
  	$('.select_curr').text(currency);
});

$("#crypto_select").change(function() {
	var currency = $("#crypto_select").val();
  	$('.select_curr').text(currency);
	getCurrencyInfo(currency);
	window.history.pushState("", "", currency);
});


$("#view_qr").click(function() {
  	$('#qr_view').css('display','block');
});

function getCurrencyInfo(currency) {
	$.ajax({
		url:site_url+"/getCryptoInfo",  
		method:"POST",
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		data:{ 'currency': currency },
		success:function(data) {
			data = $.parseJSON(data);
			$('#qr_view').css('display','none');
			$('#wallet_bal').text(data.wallet);
			$('#transit_bal').text(data.transit);
			$('#avail_bal').text(data.available);
			$('#copy_addr').text(data.address);
			$('#addr_url').attr('src',data.QR_code);
		},
		error:function() {
			notif({ msg: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please try again!', type: "error" });
		}
	});
}


function copyAddress(element) {
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val($(element).text()).select();
	document.execCommand("copy");
	$temp.remove();
	notif({ msg: '<i class="fa fa-check-circle" aria-hidden="true"></i> Address copied to the clipboard', type: "success" });
}

</script> 


