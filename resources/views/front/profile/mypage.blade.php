<!-- header -->
@include('front.common/header')
<!-- //header -->

<!-- container -->
<div class="container sub_container my_wrap inner1">
	<!-- Sub Menu -->
	<section class="sub_menu">
		<dl class="fl_wrap">
			<dt>My Page</dt>
			<dd class="active"><a href="{{url('mypage')}}">Profile</a></dd>
			<dd><a href="#">Security Authentication</a></dd>
			<dd><a href="#">Connection Management</a></dd>
		</dl>
	</section>
	<!-- //Sub Menu -->

	<!-- Page Title -->
	<section class="page_title">
		<div>
			My Page
			<a href="#" class="btn btn_back">Back</a>
		</div>
	</section>
	<!-- //Page Title -->

	<section class="mypage">
		<div class="my_info">
			<p class="title">Profile</p>
			<table class="form_type2">
				<caption></caption>
				<colgroup>
					<col style="width:25%;">
					<col style="width:75%;">
				</colgroup>
				<tbody>
					<tr>
						<th>e-mail</th>
						<td><?php echo App\Model\User::endecryption(2,$user->wiix_content).App\Model\User::endecryption(2,$user->unusual_user_key); ?></td>
					</tr>
					<tr>
						<th>name</th>
						<td>{{$user->consumer_name}}</td>
					</tr>
					<tr>
						<th>cellphone</th>
						<td><?php $phone = $user->phone; if($phone != "") { echo '******'.substr($phone,-4); } else { echo "--"; } ?></td>
					</tr>
					<tr>
						<th>Security rating</th>
						<td>Level2</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="push">
			<p class="title">Receiving events (email, SMS)</p>
			<table class="form_type2">
				<caption></caption>
				<colgroup>
					<col style="width:25%;">
					<col style="width:75%;">
				</colgroup>
				<tbody>
					<tr>
						<th>Set / deny event guide reception</th>
						<td>
							<span class="radio_style">
								<input type="radio" name="push" value="pushOn" id="pushOn"/>
								<label for="pushOn" class="radio_img"></label>
								<label for="pushOn">ON</label>
							</span>
							<span class="radio_style">
								<input type="radio" name="push" value="pushOff" id="pushOff"/>
								<label for="pushOff" class="radio_img"></label>
								<label for="pushOff">OFF</label>
							</span>
							<a href="#" class="btn btn_border_gray">Change opt-in settings</a>
							<span class="gray">※ Important notices such as check-in / out instructions are sent by e-mail regardless of whether you agree to receive the event.</span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="factor_auth">
			<p class="title">2-factor authentication</p>
			<ul>
				<li class="fl_wrap bg_phone">
					<p class="fl_left">
						Enable SMS authentication <?php if($phone != "") { echo '('.substr($phone,3).'******'.substr($phone,-3).')'; } ?><br/>
						<span>Mobile phone authentication is used for withdrawal, password change and security.</span>
					</p>
					<div class="flatCheck fl_right">
						<input type="checkbox" value="auth" id="auth_sms" name="auth_sms"/>
						<label for="auth_sms"></label>
						<div></div>
					</div>
				</li>
				<li class="fl_wrap bg_pc">
					<p class="fl_left">
						Google authentication<br/>
						<span>Google (OTP) certification is used for withdrawal, password change and security.</span>
					</p>
					<div class="flatCheck fl_right">
						<input type="checkbox" value="auth" id="auth_google" name="auth_google"/>
						<label for="auth_google"></label>
						<div></div>
					</div>
				</li>
			</ul>
		</div>
	</section>
</div>
<!-- //container -->

<!-- footer -->
@include('front.common/footer')
<!-- //footer -->