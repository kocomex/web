@include('front.common/header')
<!-- //header -->

<!-- container -->
<div class="container sub_container member_warp inner1">

	<!-- Page Title -->
	<section class="page_title">
		<div>
			REGISTER
			<a href="#" class="btn btn_back">뒤로가기</a>
		</div>
	</section>
	<!-- //Page Title -->

	<section class="member_box register">
		<div class="title fl_wrap">
			<a href="{{url('/')}}" class="fl_left"><img src="{{asset('/').('public/front_assets/images/member/img_logo.png')}}" alt="이코인뱅크 로고"/></a>
			<b class="fl_right">Sign Up</b>
		</div>

		<form id="registerdetail" action="{{url('/signup')}}" method="post" accept-charset="UTF-8">
        {{csrf_field()}}
		<dl class="write_wrap fl_wrap">
			<dd>
				<input type="text" id="usermail" name="usermail" class="input_mail" placeholder="Email Address">
			</dd>
			<dd>
				<input type="text" id="username" name="username" class="input_people" placeholder="Username">
			</dd>
			<dd>
				<input type="password" id="password" name="password" class="input_password" placeholder="Password">
			</dd>
			<dd>
				<input type="password" id="password_confirmation" name="password_confirmation" class="input_password" placeholder="Confirm Password">
			</dd>
			<dd class="capcha fl_wrap captcha_dd">
				<input type="image"  src="{{asset('/').('public/front_assets/images/common/cap_bg.jpg')}}" class="fl_left nohand"/> <span class="captcha_span" id="captcha_value" > <?php echo $recaptcha_code; ?> </span> 
				<input type="" value="" id="refresh" class="btn btn_border_gray btn_reload fl_left atmcursor" readonly/>
				<input type="text" name="fill_captcha" id="fill_captcha" placeholder="Auto-fill character" class="fl_right"/>
			</dd>
			<label for="fill_captcha" class="error" style="display: none;"></label>

			<dd class="fl_wrap">
				<span class="check_style fl_left">
					<!-- <input type="checkbox" value="" name="member_agree" id="member_agree"/>
					<label for="member_agree" class="check_img"></label> 					
					<label for="member_agree">Accept terms and conditions</label> -->
				</span>
				<input type="checkbox" id="accept" name="accept"> <span >I read</span> <a href="#" target="_blank">Accept terms and conditions</a>

				<div class="fl_right">
					<span class="btn btn_tooltip" onclick="toolTipFunc(this);"></span>
					<span>Terms of Use</span>
					<div class="tooltip_box">
						tooltip tooltip tooltip tooltip tooltip tooltip tooltip tooltip tooltip
					</div>
				</div>
			</dd>
			<label for="accept" class="error" style="display: none;">Please Agree our terms and condition</label>
			<dd class="btn_wrap">
				<button class="btn btn_lightblue" type="submit" id="register_button">Join</button>
			</dd>
			<div id="loading-regcircle-overlay" style="display:none;">
                     <div class="loading_bg">
                       <div align="center">
                         <img src="{{asset('/').('public/front_assets/images/common/loading.gif')}}" height="50px" />
                       </div>
                     </div>
            </div>
			<dd class="text_right go_login">
				Already signed up? <a href="{{url('login')}}">login</a>
			</dd>
		</dl>
		</form>
	</section>
</div>
<!-- //container -->


<!-- footer -->
@include('front.common/footer')