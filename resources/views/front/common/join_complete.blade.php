@include('front.common/header')
<!-- //header -->

<!-- container -->
<div class="container sub_container member_warp inner1">

	<!-- Page Title -->
	<section class="page_title">
		<div>
			Signed up
			<a href="#" class="btn btn_back">Back</a>
		</div>
	</section>
	<!-- //Page Title -->

	<section class="member_box join_com">
		<dl class="">
			<dt>
				<img src="{{asset('/').('public/front_assets/images/member/img_post_check.png')}}" alt="가입신청 확인"/><br/>
				Confirm your application
			</dt>
			<dd>
				<p>“<span>Welcome to</span> E-coinbank”</p>
				Please click the button below to complete the authentication.
			</dd>
			<dd class="btn_wrap text_center">
				<a href="{{url('/login')}}"><input type="submit" value="Continue Login" class="btn btn_lightblue"/></a>
			</dd>
			<dd class="msg_box">
				<p>Other guidance text. Other guidance text. Other guidance text.</p>
			</dd>
		</dl>
	</section>
</div>
<!-- //container -->

<!-- footer -->
@include('front.common/footer')