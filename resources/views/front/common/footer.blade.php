<footer>
	<div class="footer_menu">
		<ul class="inner1 fl_wrap">
			<li class="mobile_none"><a href="#">NOTICE</a></li>
			<li><a href="#">Terms of Service</a></li>
			<li><a href="#"><b>Privacy Policies</b></a></li>
			<li><a href="#">Policy & Usage Notice</a></li>
			<li class="mobile_none"><a href="#">FAQ</a></li>
		</ul>
	</div>

	<div class="footer_wrap">
		<div class="inner1">
			<p class="address">
				<span>KOCOMEX CO,. LTD.</span><span>CEO JIN HA YONG</span><span>15 Halladaehak-ro Jeju-si Jeju-do</span><span>Biz-No 689-81-011058</span><span class="bd_right_0">통신판매업신고 2014-서울강남-02571호</span><br/><br class="mobile_block"/>
				Call-Center <b>1588-5682</b>  운영시간 평일 09:00~18:00 (본사 방문 상담은 진행하지 않습니다.)<br/>
				<span>계정문의 cs-account@upbit.com</span><span>입출금문의 cs-teller@upbit.com</span><span>사용문의 cs-help@upbit.com</span><span class="bd_right_0">기타문의 cs-etc@upbit.com</span>
			</p>
			<p class="copy">COPYRIGHT ⓒCOCOMAX. ALL RIGHT RESERVED.</p>

			<!-- Go top -->
			<a href="#" class="btn btn_top" title="상단으로 0이동">상단으로 이동</a>
		</div>
	</div>
</footer>
<!-- //footer -->
</div>

<!--atm  script start here-->
<!-- jquery validation script start here-->
<script src="{{asset('/').('public/front_assets/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{asset('/').('public/front_assets/js/jquery_validation/additional-methods.js')}}"></script>

<script>
   var site_url  = '{{url("")}}';
</script>
@include('front.scripts/site_validator')
@include('front.scripts/common')

<?php if(!empty($page_key)){ ?>
	<?php require_once "resources/views/front/scripts/".$page_key.".blade.php"; ?>
<?php } ?>
<!-- jquery validation script end  here--> 
<!--isnumkey script start here-->
<script src="{{asset('/').('public/front_assets/js/isnumkey/minisnumkey.js')}}"></script>
<!--isnumkey script end here-->

<!-- To display session messages -->
<script src="{{asset('/').('public/front_assets/js/notifIt.min.js')}}"></script>
<script type="text/javascript">
  	$( document ).ready(function() {
    	<?php if(Session::has('success')) { ?> 
      		var sucess= "{{ Session::get('success') }}"; 
      		notif({ msg: '<i class="fa fa-check-circle" aria-hidden="true"></i>'+" "+sucess, type: "success" }); 
    	<?php } ?>
    	<?php if(Session::has('error')) { ?> 
      		var error= "{{ Session::get('error') }}"; 
      		notif({ msg: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i>'+" "+error, type: "error" }); 
    	<?php } ?>
    	<?php if(count($errors) > 0) { foreach($errors->all() as $er) { ?>
    		var error= "{{ $er }}"; 
  			notif({ msg: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i>'+" "+error, type: "error" }); 
		<?php break; } } ?>
  	});
</script>

<!--atm  script end here-->
</body>
</html>