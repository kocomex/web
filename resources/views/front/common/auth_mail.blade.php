@include('front.common/header')
<!-- //header -->

<!-- container -->
<div class="container sub_container member_warp inner1">

	<!-- Page Title -->
	<section class="page_title">
		<div>
			<!-- 이메일 인증 -->
			Email Authentication
			<a href="#" class="btn btn_back"><!-- 뒤로가기 -->Back</a>
		</div>
	</section>
	<!-- //Page Title -->

	<section class="member_box auth_mail">
		<dl class="">
			<dt>
				<img src="{{asset('/').('public/front_assets/images/member/img_post_check.png')}}" alt="이메일 인증"/><br/>
				<!-- 이메일 인증 -->Email Authentication
			</dt>
			<dd>
				We sent a verification email to your registered <span>{{$mail['usermail']}}</span>.<br/>
				Please complete your subscription through email verification.<br/><br/>
				If you do not receive a verification email, please check your email address  <br/>
				and click the Resend Email button below .
			</dd>
			<form id="authdetail" action="{{url('/auth_mail')}}" method="post" accept-charset="UTF-8">
        	{{csrf_field()}}
        	<input type="hidden" value="{{$mail['email_content']}}" name="email_content" />
        	<input type="hidden" value="{{$mail['toDetails']}}" name="toDetails" />
			<dd class="btn_wrap text_center">
				<input type="submit" value="Resend email" class="btn btn_lightblue"/>
			</dd>
			</form>
		</dl>
	</section>
</div>
<!-- //container -->

<!-- footer -->
@include('front.common/footer')