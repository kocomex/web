@include('front.common/header')
<!-- //header -->

<!-- container -->
<div class="container sub_container member_warp inner1">

	<!-- Page Title -->
	<section class="page_title">
		<div>
			LOGIN
			<a href="#" class="btn btn_back">Back</a>
		</div>
	</section>
	<!-- //Page Title -->

	<section class="member_box register login">
		<div class="title fl_wrap">
			<a href="#" class="fl_left"><img src="{{asset('/').('public/front_assets/images/member/img_logo.png')}}" alt="이코인뱅크 로고"/></a>
			<b class="fl_right">Login</b>
		</div>
		<form id="logindetails" action="{{url('/login')}}" method="post" accept-charset="UTF-8">
        {{csrf_field()}}
		<dl class="write_wrap fl_wrap">
			<dd>
				<input type="text" name="email" placeholder="Email Address" class="input_mail"/>
			</dd>
			<dd>
				<input type="password" name="password" placeholder="Password" class="input_password"/>
			</dd>
			<dd class="capcha fl_wrap captcha_dd">
				<input type="image"  src="{{asset('/').('public/front_assets/images/common/cap_bg.jpg')}}" class="fl_left nohand"/> <span class="captcha_span" id="captcha_value" > <?php echo $recaptcha_code; ?> </span> 
				<input type="" value="" id="refresh" class="btn btn_border_gray btn_reload fl_left atmcursor" readonly/>
				<input type="text" name="fill_captcha" id="fill_captcha" placeholder="Auto-fill character" class="fl_right" autocomplete="off" />
			</dd>
			<label for="fill_captcha" class="error" style="display: none;"></label>
			<dd class="btn_wrap">
				<input type="submit" value="Login" class="btn btn_lightblue"/>
			</dd>
			<dd class="fl_wrap go_login">
				<div  class="fl_left">
					<a href="{{url('forgot')}}">Forgot Password</a>
				</div>
				<div class="fl_right">
					Do not have an account? <a href="{{url('signup')}}">Join</a>
				</div>
			</dd>
		</dl>
		</form>
	</section>
</div>
<!-- //container -->

<!-- footer -->
@include('front.common/footer')