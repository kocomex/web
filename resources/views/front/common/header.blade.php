<!doctype html>
<!-- <html lang="ko"> -->
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<title></title>

<!-- Css -->
<link rel="stylesheet" type="text/css" href="{{asset('/').('public/front_assets/css/common.css')}}"/>
<link rel="stylesheet" href="{{asset('/').('public/front_assets/css/notifIt.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/').('public/front_assets/css/style.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('/').('public/front_assets/css/media.css')}}"/>

<!-- Js -->
<script src="{{asset('/').('public/front_assets/js/jquery-1.12.0.min.js')}}"> </script>
<script type="text/javascript" src="{{asset('/').('public/front_assets/js/ui.js')}}"> </script>

<style>
    #ui_notifIt {
  		height: 60px; width: 500px !important; left: center !important;
    }
</style>

</head>
<body>
<div id="wrapper">


<!-- header -->
<header>
	<div class="header_wrap inner1 fl_wrap">
		<div class="logo fl_left">
			<a href="{{url('/')}}"><img src="{{asset('/').('public/front_assets/images/common/logo.png')}}" alt="e-coinbank 로고"/></a>
		</div>
		
		<div class="gnb_wrap fl_wrap">
			<ul class="utill fl_right">
				<?php if(Session::has('wiix_userId')) { ?>
				<li><a  href="{{url('logout')}}" class="btn btn_login">LOG OUT</a></li>
				<?php } else { ?>
				<li><a  href="{{url('login')}}" class="btn btn_login">LOG IN</a></li>
				<li><a  href="{{url('signup')}}" class="btn btn_join">REGISTER</a></li>
				<li><a href="#none" class="btn btn_close mobile_block" onclick="gnbClose(this);">닫기</a></li>
				<?php } ?>
			</ul>

			<ul class="gnb">
				<li><a href="trade/trade.html">EXCHANGES</a></li>
				<li>
					<a href="assets/status.html" class="mobile_none">ASSETS</a>
					<button type="button" class="mobile_block" onclick="accodianFunc(this);">ASSETS</button>
					<ul>
						<li><a href="assets/status.html">자산현황</a></li>
						<li><a href="assets/deposit.html">입금</a></li>
						<li><a href="assets/withdraw.html">출금</a></li>
						<li><a href="assets/history.html">거래내역</a></li>
					</ul>
				</li>
				<li>
					<a href="#" class="mobile_none">ORDERS</a>
					<button type="button" class="mobile_block" onclick="accodianFunc(this);">ORDERS</button>
					<ul>
						<li><a href="#">대기주문</a></li>
						<li><a href="#">주문내역</a></li>
						<li><a href="#">거래내역</a></li>
					</ul>
				</li>
				<li>
					<a href="#" class="mobile_none">SUPPORT</a>
					<button type="button" class="mobile_block" onclick="accodianFunc(this);">SUPPORT</button>
					<ul>
						<li><a href="#">공지사항</a></li>
						<li><a href="#">이용안내</a></li>
						<li><a href="#">정책 및 고지</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">서류제출 안내</a></li>
					</ul>
				</li>
				<li class="mobile_block">
					<a href="#" class="mobile_none">마이페이지</a>
					<button type="button" class="mobile_block" onclick="accodianFunc(this);">마이페이지</button>
					<ul>
						<li><a href="#">회원정보</a></li>
						<li><a href="#">보안인증</a></li>
						<li><a href="#">접속관리</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<a href="#none" class="btn btn_gnb mobile_block" onclick="gnbOpen(this);">메뉴열기</a>
		<div class="opacity_bg"></div>
	</div>
</header>