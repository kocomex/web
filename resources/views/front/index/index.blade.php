@include('front.common/header')

<!-- //header -->


<!-- container -->
<div class="container main_contents">
	<section class="cont1">
		<div class="inner2">
			<h1>고객 중심의 암호화폐 거래소 사이트</h1>
			<p class="roboto">The Most Advanced<br class="mobile_block"/> Korea's Digital Currency Trading Platforms</p>
			<div class="btn_wrap">
				<a href="#" class="btn btn_black">둘러보기</a>
				<a href="#" class="btn btn_blue">거래시작</a>
			</div>
		</div>
	</section>

	<section class="cont2">
		<div class="inner2">
			<h1>강력한 멀티팩터 인증,<br/>최고의 보안으로<br/>24시간 <b>안심거래</b></h1>
			<p>글로벌 최고 수준의 파트너들과 함께<br/>가장 안전한 보안체계 구축</p>
		</div>
	</section>

	<section class="cont3">
		<div class="inner2">
			<h1>
				<img src="{{asset('/').('public/front_assets/images/main/img_beatcoin.png')}}" alt="코인을 알면 백전백승! 실시간 코인 동향과 정보"/><br/>
				코인을 알면 백전백승!<br/><b>실시간 코인 동향과 정보</b>
			</h1>
		</div>
	</section>

	<section class="cont4">
		<div class="inner2">
			<h1>이코인뱅크 암호화 화폐 거래소 <b>이용방법</b></h1>
			<ul>
				<li>
					<img src="{{asset('/').('public/front_assets/images/main/img_roptop.png')}}" alt="Create Accounts"/><br/>
					Create Accounts
				</li>
				<li>
					<img src="{{asset('/').('public/front_assets/images/main/img_safe.png')}}" alt="Authoization"/><br/>
					Authoization
				</li>
				<li>
					<img src="{{asset('/').('public/front_assets/images/main/img_coin.png')}}" alt="Deposit"/><br/>
					Deposit
				</li>
				<li>
					<img src="{{asset('/').('public/front_assets/images/main/img_graph.png')}}" alt="Coin Exchange"/><br/>
					Coin Exchange 
				</li>
			</ul>
			<div class="btn_wrap">
				<a href="{{url('login')}}" class="btn btn_blue">LOG IN</a>
				<a href="{{url('signup')}}" class="btn btn_lightblue">REGISTER</a>
			</div>
		</div>
	</section>
</div>
<!-- //container -->


<!-- footer -->
@include('front.common/footer')