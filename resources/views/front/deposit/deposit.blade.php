@include('front.common/header')
<!-- //header -->


<!-- container -->
<div class="container sub_container assets_warp inner1">
	<!-- Sub Menu -->
	@include('front.deposit.submenu')
	<!-- //Sub Menu -->

	<!-- Page Title -->
	<section class="page_title">
		<div>
			ASSETS
			<a href="#" class="btn btn_back">Back</a>
		</div>
	</section>
	<!-- //Page Title -->

	<section class="deposit fl_wrap">
		<div class="fl_left">
			<div class="coin_info">
				<select name="currency" id="crypto_select">
					<?php foreach($currency as $curr) { ?>
					<option value="{{$curr['symbol']}}" <?php if($type == $curr['symbol']) { echo "selected";} ?> >{{$curr['symbol']}} - {{$curr['name']}}</option>
					<?php } ?>
				</select>
				<?php $wallet = $getUser->wallet; ?>
				<dl class="fl_wrap">
					<dt><b>Total holdings</b></dt>
					<dd><b id="wallet_bal">{{$coin['wallet']}}</b> <span class="select_curr"></span></dd>
					<dt>In transit</dt>
					<dd><span id="transit_bal">{{$coin['transit']}}</span> <span class="select_curr"></span></dd>
					<dt>Available amount</dt>
					<dd><span id="avail_bal">{{$coin['available']}}</span> <span class="select_curr"></span></dd>
				</dl>
			</div>
			<div class="warning">
				<dl>
					<dt>Please know before depositing.!</dt>
					<dd>- Only the <span class="select_curr"></span> should send it to your account. If you send another currency, you can lose your assets.</dd>
				</dl>
			</div>
			<div class="address_box">
				<dl>
					<dt><span class="select_curr"></span> Incoming address</dt>
					<dd>
						<div class="text_box" id="copy_addr">{{$coin['address']}}</div>
					</dd>
				</dl>
				<div id="qr_view" style="display: none;">
					<dt><span class="select_curr"></span> QR Code</dt>
					<dd>
						<div>
							<img src="{{$coin['QR_code']}}" id="addr_url">
						</div>
					</dd>
				</div>
				<div class="btn_wrap">
					<input type="button" value="View QR code" id="view_qr" class="btn btn_lightblue"/>
					<input type="button" value="Copy clipboard" class="btn btn_blue" onclick="copyAddress('#copy_addr')"/>
				</div>
			</div>
			<div class="trade_move">
				<dl class="fl_wrap">
					<dt>Move to exchange</dt>
					<dd>
						<input type="button" value="BTC/LTC" class="btn btn_border_gray"/>
					</dd>
					<dd>
						<input type="button" value="BTC/ETH" class="btn btn_border_gray"/>
					</dd>
				</dl>
			</div>
		</div>
		<?php $deposits = $getUser->deposits; if(!$deposits->isEmpty()) { ?>
		<div class="fl_right history">
			<div class="fl_wrap title">
				<p>Transaction history</p>
				<a href="#" class="btn btn_more">View more</a>
			</div>
			<div class="history_cont">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:18%;"/>
						<col style="width:18%;"/>
						<col style="width:18%;"/>
						<col style="width:18%;"/>
						<col style="width:28%;"/>
					</colgroup>
					<thead>
						<tr>
							<th>Condition</th>
							<th>Coin</th>
							<th>Quantity</th>
							<th>Time</th>
							<th>Information</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($deposits as $deposit) { ?>
						<tr>
							<td>Condition</td>
							<td>{{$deposit->currency}}</td>
							<td>{{$deposit->amount}}</td>
							<td>{{$deposit->created_at}}</td>
							<td>{{$deposit->status}}</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<div class="scroll">
					
				</div>
			</div>
		</div>
		<?php } else { ?>
		<div class="fl_right history">
			<div class="fl_wrap title">
				<p>Transaction history</p>
			</div>
			<div class="history_cont empty"> <!-- If there is no transaction history empty -->
				<p class="empty_text">No deposit history</p>

				<div class="scroll">
					
				</div>
			</div>
		</div>
		<?php } ?>
	</section>
</div>
<!-- //container -->


<!-- footer -->
@include('front.common/footer')