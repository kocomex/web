<?php $url = Request::segment(1);  ?>
<section class="sub_menu">
	<dl class="fl_wrap">
		<dt>ASSETS</dt>
		<dd class="<?php echo ($url == 'asset_status')?'active':''; ?>"><a href="{{url('asset_status')}}">Asset Status</a></dd>
		<dd class="<?php echo ($url == 'deposit')?'active':''; ?>"><a href="{{url('deposit/BTC')}}">Deposit</a></dd>
		<dd class="<?php echo ($url == 'withdraw')?'active':''; ?>" ><a href="{{url('withdraw/BTC')}}">Withdraw</a></dd>
		<dd class="<?php echo ($url == 'history')?'active':''; ?>"><a href="{{url('history')}}">Transaction History</a></dd>
	</dl>
</section>
	