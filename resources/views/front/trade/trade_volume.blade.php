<!-- Page Title -->
<section class="page_title">
	<div>EXCHANGES<a href="#" class="btn btn_back">뒤로가기</a>
	</div>
</section>
<!-- //Page Title -->
<section class="info_box">
	<div class="fl_wrap inner2">
		<p class="fl_left">
			<b>NANO</b> / BTC
			<span><img src="{{asset('/').('public/front_assets/images/trade/ic_ex.png')}}" alt="nano"/> NANO</span>
		</p>
		<ul class="fl_wrap fl_right">
			<li>
				<dl>
					<dt>마지막체결가</dt>
					<dd class="red">0.0013208＄14.16</dd>
				</dl>
			</li>
			<li>
				<dl>
					<dt>전일대비</dt>
					<dd class="green">-0.0013208 -6.08%</dd>
				</dl>
			</li>
			<li>
				<dl>
					<dt>24h 고가</dt>
					<dd>0.0013208</dd>
				</dl>
			</li>
			<li>
				<dl>
					<dt>24h 저가</dt>
					<dd>0.0028103</dd>
				</dl>
			</li>
			<li>
				<dl>
					<dt>24h 거래량</dt>
					<dd>0.0018203 BTC</dd>
				</dl>
			</li>
		</ul>
	</div>
</section>