<div class="order_box">
{!! Form::open(array('id'=>'trade_form')) !!}
	<p class="fees_text">Fees : <del>0.03%</del> 0.015%</p>
	<ul class="tab_header fl_wrap">
		<li class="active"><a href="#">Limit</a></li>
		<li><a href="#">Market</a></li>
		<li>
			<a href="#">Stop</a>
			<span class="btn btn_tooltip" onclick="toolTipFunc(this);"></span>
			<div class="tooltip_box">
				tooltip tooltip tooltip tooltip tooltip tooltip tooltip tooltip tooltip
			</div>
		</li>
	</ul>
	<div class="tab_contents">
		<!-- Limit -->
		<div class="tab_cont fl_wrap" id="summury1">
			<div class="fl_left">
				<p class="title fl_wrap">Buy ADX<span class="fl_right">BTC Balance: 0.04520670</span></p>
				<dl class="fl_wrap">
					<dt>Amount</dt>
					<dd class="count_m">
						<input type="text" onkeypress="return isNumberKey(event)" class="fl_left" onkeyup="calculation('buy');" name="buy_amount" id="buy_amount"/>
						<span>BTC</span>
						<!-- <div class="btn_wrap fl_right">
							<input type="button" value="" class="up"/>
							<input type="button" value="" class="down"/>
						</div> -->
					</dd>
					<dt>Price</dt>
					<dd>
						<input type="text" onkeypress="return isNumberKey(event)" name="buy_price" id="buy_price" onkeyup="calculation('buy');"/>
						<span>ADX</span>
					</dd>
					<!-- <dt></dt>
					<dd class="btn_num">
						<div class="btn_wrap">
							<input type="button" value="25%"/>
							<input type="button" value="50%"/>
							<input type="button" value="75%"/>
							<input type="button" value="100%"/>
						</div>
					</dd> -->
				<div class="fl_left">
					<span class="title fl_wrap">Total : <span name="buy_total" id="buy_total">0.0000</span></span>
				</div>

				</dl>
				<input type="submit" value="Order" class="btn btn_lightblue"/>
			</div>
			<div class="fl_left">
				<p class="title fl_wrap">Sale ADX<span class="fl_right">BTC Balance: 0.04520670</span></p>
				<dl class="fl_wrap">
					<dt>Amount</dt>
					<dd class="count_m">
						<input type="text" onkeypress="return isNumberKey(event)" class="fl_left" onkeyup="calculation('sell');" name="sell_amount" id="sell_amount"/>
						<span>BTC</span>
						<!-- <div class="btn_wrap fl_right">
							<input type="button" value="" class="up"/>
							<input type="button" value="" class="down"/>
						</div> -->
					</dd>
					<dt>Price</dt>
					<dd>
						<input type="text" onkeypress="return isNumberKey(event)" name="sell_price" id="sell_price" onkeyup="calculation('sell');"/>
						<span>ADX</span>
					</dd>
					<!-- <dt></dt>
					<dd class="btn_num">
						<div class="btn_wrap">
							<input type="button" value="25%"/>
							<input type="button" value="50%"/>
							<input type="button" value="75%"/>
							<input type="button" value="100%"/>
						</div>
					</dd> -->
				<div class="fl_left">
					<span class="title fl_wrap" name="sell_total">Total : <span  id="sell_total" name="sell_total">0.0000 </span></span>
				</div>
				</dl>
				<input type="submit" value="Sale" class="btn btn_blue"/>
			</div>
		</div>
		<!-- Limit -->
	</div>
{!! Form::close() !!}
</div>