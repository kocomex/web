<div class="realTime">
			<div class="graph_title_box fl_wrap">
				<div class="btn_wrap fl_left">
					<a href="#none" class="btn btn_border_gray" onclick="showBoth(this);"><img src="{{asset('/').('public/front_assets/images/trade/ic_ex.png')}}" alt="전체"/></a>
					<a href="#none" class="btn btn_border_gray" onclick="showUp(this);"><img src="{{asset('/').('public/front_assets/images/trade/ic_graph_down.png')}}" alt="상승"/></a>
					<a href="#none" class="btn btn_border_gray" onclick="showDown(this);"><img src="{{asset('/').('public/front_assets/images/trade/ic_graph_up.png')}}" alt="하락"/></a>
				</div>
				<select name="" id="" class="fl_right">
					<option value="">8 소수점</option>
					<option value="">8 소수점</option>
				</select>
			</div>
			<div class="graph_content_box">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:28%"/>
						<col style="width:42%"/>
						<col style="width:30%"/>
					</colgroup>
					<thead>
						<tr>
							<th>가격(BTC)</th>
							<th>수량(NANO)</th>
							<th>총액(BTC)</th>
						</tr>
					</thead>
				</table>
				<div class="down_table scroll">
					<table>
						<caption></caption>
						<colgroup>
							<col style="width:28%"/>
							<col style="width:42%"/>
							<col style="width:30%"/>
						</colgroup>
						<tbody><!-- 요약 -->
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="red_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="red_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="red_bar" style="width:100px;"></span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="newest">
					<p class="green">0.0013208</p> <!-- gray(유지) / green(상승) / red(하락) -->
				</div>

				<div class="up_table scroll">
					<table>
						<caption></caption>
						<colgroup>
							<col style="width:28%"/>
							<col style="width:42%"/>
							<col style="width:30%"/>
						</colgroup>
						<tbody><!-- 요약 -->
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013300</span></td>
								<td><span>33.99</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:20px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013293</span></td>
								<td><span>1.14</span></td>
								<td>
									<span>0.00151540</span>
									<span class="green_bar" style="width:50px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013271</span></td>
								<td><span>26.86</span></td>
								<td>
									<span>0.03564591</span>
									<span class="green_bar" style="width:200px;"></span>
								</td>
							</tr>
							<tr>
								<td><span>0.0013267</span></td>
								<td><span>434.75</span></td>
								<td>
									<span>0.04520670</span>
									<span class="green_bar" style="width:300px;"></span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>