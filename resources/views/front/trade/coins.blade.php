<div class="interest">
				<div class="graph_title_box fl_wrap">
					<span><img src="{{asset('/').('public/front_assets/images/trade/ic_star.png')}}" alt=""/>관심코인</span>
					<div class="btn_wrap fl_right">
						<a href="#" class="btn btn_border_gray active ">BTC</a>
						<a href="#" class="btn btn_border_gray">ETH</a>
						<a href="#" class="btn btn_border_gray">BNB</a>
						<a href="#" class="btn btn_border_gray">USDT</a>
					</div>
				</div>
				<div class="graph_content_box">
					<!-- 검색 -->
					<div class="search_box fl_wrap">
						<div class="fl_left">
							<input type="search" value="" placeholder="코인명/심볼검색"/>
							<input type="submit" value="" class="btn_search"/>
						</div>
						<div class="fl_right">
							<span class="radio_style">
								<input type="radio" name="volume" value="volume1" id="volume1"/>
								<label for="volume1" class="radio_img"></label>
								<label for="volume1">전일대비</label>
							</span>
							<span class="radio_style">
								<input type="radio" name="volume" value="volume2" id="volume2"/>
								<label for="volume2" class="radio_img"></label>
								<label for="volume2">거래량</label>
							</span>
						</div>
					</div>
					<!-- //검색 -->
					<table>
						<caption></caption>
						<colgroup>
							<col style="width:33%;"/>
							<col style="width:33%;"/>
							<col style="width:33%;"/>
						</colgroup>
						<thead>
							<tr>
								<th>페어</th>
								<th>현재가</th>
								<th>전일대비</th>
							</tr>
						</thead>
					</table>
					<div class="scroll">
						<table>
						<caption></caption>
						<colgroup>
							<col style="width:33%;"/>
							<col style="width:33%;"/>
							<col style="width:33%;"/>
						</colgroup>
							<tbody><!-- 요약 -->
								<tr>
									<td>ADA/BTC</td>
									<td class="red">0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td >0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>AE/BTC</td>
									<td>0.0013262</td>
									<td class="green">-4.98%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td class="red">0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td >0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>AE/BTC</td>
									<td>0.0013262</td>
									<td class="green">-4.98%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td class="red">0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td >0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>AE/BTC</td>
									<td>0.0013262</td>
									<td class="green">-4.98%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td class="red">0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td >0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>AE/BTC</td>
									<td>0.0013262</td>
									<td class="green">-4.98%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td class="red">0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td >0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>AE/BTC</td>
									<td>0.0013262</td>
									<td class="green">-4.98%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td class="red">0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td >0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>AE/BTC</td>
									<td>0.0013262</td>
									<td class="green">-4.98%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td class="red">0.0013262</td>
									<td>-6062%</td>
								</tr>
								<tr>
									<td>ADA/BTC</td>
									<td >0.0013262</td>
									<td>-6062%</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>