@include('front.common/header')
<!-- //header -->
<!-- container -->
<div class="container sub_container trade_warp">
	<!-- Trade Volume -->
		@include('front.trade/trade_volume')
	<!-- Trade Volume -->
	<section class="graph_wrap fl_wrap inner1">
		<!-- 실시간 가격 // Last trade -->
		@include('front.trade/last_trade')
		<!-- //실시간 가격  // Last trade -->
		<!-- 그래프 / 주문  // Graph / Orders -->
		<div class="graph">
			<!-- 그래프 // Graph -->
				@include('front.trade/graph')
			<!-- //그래프 //Graph  -->
			<!-- 주문 // Orders -->
				@include('front.trade/order')
			<!-- //주문 // Orders -->
		</div>
		<!-- //그래프 / 주문 // Graph / Orders -->
		<!-- 관심코인 / 거래기록  // Coins / History -->
		<div class="my_coin">
			<!-- 관심코인 // Coins -->
				@include('front.trade/coins')
			<!-- //관심코인 // Coins -->
			<!-- 거래기록 // History -->
				@include('front.trade/trade_history')
			<!-- //거래기록 // History -->
		</div>
		<!-- //관심코인 / 거래기록 // Coins / History -->
	</section>

	<section class="table_wrap inner1">
		<!-- 대기주문 // Active orders -->
			@include('front.trade/active_orders')
		<!-- //대기주문 // Active orders -->

		<!-- 나의 주문내역 -->
		
			@include('front.trade/my_orders')
		<!-- //나의 주문내역 -->
	</section>
</div>
<!-- //container -->


<!-- footer -->

@include('front.common/footer')