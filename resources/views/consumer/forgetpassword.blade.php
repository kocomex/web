@include('layouts.front_header')
<section class="bannerSec fgtPwdBg">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-4">
        <div class="loginForm">
          <h4 class="midInTit mb-20">Reset your password</h4>
         
         
                <form method="post" class="form-horizontal" action="{{ url('/forgetpwd') }}" name="forget_form" id="forget_form">
      {!! csrf_field() !!}
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  <img src="{{ URL::asset('theme/front_users/images/mail-ic.png') }}">
                </span>
                <input type="text"  id="for_email" name="for_email" class="form-control" aria-label="" placeholder="Enter Email ID">
              </div>
            </div>
            <label id="for_email-error" class="error" for="for_email" style="display: none;">This field is required.</label>
            <div class="form-group mt-20">
              <button type="submit" class="btn btnSignIn btn-block" id="restbtn">Submit</button>
            </div>
               <div id="loader" style="display:none;margin: 0px 45px;">

                                  <div class="loadinh_bg">
                                    <div align="center">
                                      <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
                                    </div>
                                  </div>
                                </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  
  $( "#forget_form" ).validate({
  rules: {
    for_email: {
      required: true,
      email: true,
     
    },

 
  },
     messages:{
       for_email:{
        email:"Please enter a valid email id",
      },
     
    }

});

   $( "#forget_form" ).submit(function( event ) {

  

    if($("#forget_form").valid())
    {
 $('#loader').show();
 $('#restbtn').hide();
}else{
$('#loader').hide();
 $('#restbtn').show();
}

});
</script>