@include('layouts.front_header')

<!-- Banner section starts here -->
<section class="alBannerSec">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="innerMidSec cmsMidSec">
          <h4 class="secTit text-center">Contact us</h4>
          <div class="cmsBlock">
                <form class="form-horizontal" id="contact_form" method="post" action="{{ url('/contact') }}">
                {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Your Name *</label>
                                <input type="text" name="user_name" class="form-control formLg" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>your Email ID *</label>
                                <input type="text" name="user_mail" class="form-control formLg" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Subject *</label>
                                <input type="text" name="user_subject" class="form-control formLg" placeholder="">
                            </div>
                            <label id="user_subject-error" class="error" for="user_subject" style="display: none;">This field is required.</label>
                            <div class="form-group">
                                <label>Message *</label>
                                <textarea cols="4" name="user_message" rows="4" class="textArea"></textarea>
                                 <label id="user_message-error" class="error" for="user_message" style="display: none;">This field is required.</label>
                            </div>
                           
                            <div class="form-group text-center mt-20 mb-20">
                                <button type="submit" class="btn btnSaveSm">Send</button>
                            </div>
                        </div>
                        <div class="col-md-6 contRgt">
                            <h4>Information</h4>
                            <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
commodo consequat. Duis aute irure dolor in reprehenderit in voluptate 
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="contactForm">
                                        <div class="addInfo">
                                            <div class="addLftIcon">
                                                <span class="fa fa-map-marker"></span>
                                                <div class="addRight">
                                                    {{$settings->address}}
                                                </div>
                                            </div>
                                            <div class="addLftIcon">
                                                <span class="fa fa-mobile"></span>
                                                <div class="addRight">
                                                    {{$settings->phone}}
                                                </div>
                                            </div>
                                            <div class="addLftIcon">
                                                <span class="fa fa-envelope"></span>
                                                <div class="addRight">
                                                    {{$settings->email}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-20">
                                <div class="col-12 socialIcons">
                                    <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="{{$settings->facebook_url}}" class="fbIcon"><span class="fa fa-facebook"></span></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{$settings->twitter_url}}" class="twIcon"><span class="fa fa-twitter"></span></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{$settings->google_url}}" class="gplusIcon"><span class="fa fa-google-plus"></span></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{$settings->linkedin_url}}" class="inIcon"><span class="fa fa-linkedin"></span></a>
                                    </li>
                                    </ul>
                                </div>
                                </div>
                        </div>
                    </div>
                </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>




<script type="text/javascript">


 $('#contact_form').validate({

  rules:{


    user_name:{
      required:true,
      
      alphabetonly: true,
      
    },
    user_mail:{
      required:true,
      email:true,
    },
    user_subject:{
      required:true,
     // lettersonly:true,
      

    },
    user_message:{
      required:true,
     // lettersonly:true,
      
    },
   


  },    messages:{
    
      user_mail:{
        
        email:"Please enter a valid email id",
      },
     /* country_code:{
        required:"",
      }*/
    }


  
});
 jQuery.validator.addMethod("lettersonly", function(value, element) 
 {
  return this.optional(element) || /^[a-zA-Z0-9\s]+$/i.test(value);
}, "Letters and numbers only please"); 

 jQuery.validator.addMethod("alphabetonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please"); 
</script>




<!-- Banner section end here -->

@include('layouts.front_footer')