@include('layouts.front_header')

<!-- Banner section starts here -->
<section class="bannerSec">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6 col-lg-4">
        <div class="loginForm">
          <h4 class="midInTit mb-20">Register</h4>
        <form class="form-horizontal" id="register_form" method="post" action="">
                {!! csrf_field() !!}
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  <img src="{{ URL::asset('theme/front_users/images/user-ic.png') }}">
                </span>
                <input type="text" name="username" class="form-control" aria-label="" placeholder="Enter Username">
              </div>
            </div>
            <label id="username-error" class="error" for="username" style="display: none;">This field is required.</label>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  <img src="{{ URL::asset('theme/front_users/images/mail-ic.png') }}">
                </span>
                <input type="text" name="email" class="form-control" aria-label="" placeholder="Enter Email ID">
              </div>
            </div>
             <label id="email-error" class="error" for="email" style="display: none;">This field is required.</label>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  <img src="{{ URL::asset('theme/front_users/images/lock-ic.png') }}">
                </span>
                <input type="password" name="pwd" id="pwd" class="form-control" aria-label="" placeholder="Password">
              </div>
            </div>
             <label id="pwd-error" class="error" for="pwd" style="display: none;">This field is required.</label>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  <img src="{{ URL::asset('theme/front_users/images/lock-ic.png') }}">
                </span>
                <input type="password" name="cpwd" class="form-control" aria-label="" placeholder="Confirm Password">
              </div>
            </div>
             <label id="cpwd-error" class="error" for="cpwd" style="display: none;">This field is required.</label>
            <div class="form-group">
              <label class="custom-control custom-checkbox mb-0 mt-10">
                <input type="checkbox" name="terms" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">I accept the <a href="{{ url('/terms') }}">terms & conditions</a></span>
              </label>
            </div>
            <label id="terms-error" class="error" for="terms" style="display: none;">This field is required.</label>
            <div class="form-group mt-20">
              <button type="submit" class="btn btnSignIn btn-block" id="regbtn">Register</button>
            </div>

            <div class="form-group row no-gutters">
              <div class="col text-center">
                Already have an account? <a href="{{ url('/') }}" class="" data-toggle="modal" data-target="#fgPwd">Login</a>
              </div>
            </div>

                  <div id="loader2" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            </div>
            <div id="reg_error" style="color:red;font-size: 100%;text-align:center;"></div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner section end here -->



<script type="text/javascript">
  
  $(document).ready(function () {

   $('#register_form').validate({
    
    rules:{
      username:{
        required:true,
         minlength:3,
        maxlength:15,
        noSpace:true,

      },
      email:{
        required:true,
        email:true,
        remote: {
                url: '{{ url('/email_check') }}',
                type: 'Post',
                dataType:'json',
                data: {
          "_token": "{{ csrf_token() }}",
                    email_id: function() {

                        return $('#register_form #email').val();
                    }
                }
             }

      },
    
      terms:{
        required:true,
      },
      pwd:{
        required:true,
        minlength:8,
        maxlength:64,
        noSpace:true,
        one_upper: true,
      one_lower : true,
      one_numeric:true,
     specialcharacter:true,
      one_numeric : true,
      },
      cpwd:{
        required:true,
         equalTo: "#pwd",
      },
    },
    messages:{
      terms:{
        required:"Please agree terms",
      },
      email:{
        remote:"EmailId already taken",
        email:"Please enter a valid email id",
      },
     /* country_code:{
        required:"",
      }*/
    },submitHandler:function(form,e)
  {
      e.preventDefault();
     
      $.ajax({  
      url:"{{ url('/register') }}",
      data: $('#register_form').serialize(),
      type:'post', 
      dataType : 'json',
  beforeSend:function(){
        $('#loader2').show();
        $('#regbtn').hide();
        
      },
     
      success: function(data){
        $('#loader2').hide();
        $('#regbtn').show();
     
if((data.title == 'success'))
        {
location.reload();

        }
        else{
        $('#reg_error').css('display', '');
                  $('#reg_error').html(data.title);
                  $('#reg_error').delay(2000).fadeOut(1400); 

                  
        }
    
     
       
      
       }        
       }); 
  }
})
    });



jQuery.validator.addMethod("one_upper", function(value, element) {
return this.optional( element ) || /[A-Z]+/.test( value );
}, 'Minimum one upper case letter required');
jQuery.validator.addMethod("one_lower", function(value, element) {
return this.optional( element ) || /[a-z]+/.test( value );
}, 'Minimum one lower case letter required');

jQuery.validator.addMethod("one_numeric", function(value, element) {
return this.optional( element ) || /[0-9]+/.test( value );
}, 'Minimum one numeric required');

 jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "Space not allowed");
  jQuery.validator.addMethod("specialcharacter", function(value, element) 
 {
  return this.optional(element) || /[!@#$%^&*()]+/i.test(value);
}, "Must have at least one symbol"); 
   jQuery.validator.addMethod("lettersonly", function(value, element) 
 {
  return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please"); 
</script>