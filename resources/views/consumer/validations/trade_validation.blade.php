

<script src="http://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<script>
  var pair_data = {};
  var socket = io.connect( 'http://192.168.1.15:8315',{secure:true});

  socket.on('getpairdata',function(res){
    if(res.msg == "1") {
        get_pair_info1('none');
    }

    setTimeout(function(){ 
      get_pair_info1('none');
      get_chart_data();
    }, 15000);
  })


  // buy/sell order
  function buy_sell_ord(val) {
     $(".hide_buy_sell").hide();
     $("."+val).show();

     // active set
     $(".buy_sell").removeClass("active");
     $(".set_"+val).addClass("active");

  }

  // active stp order
  function clickfn_active_stop(val) {
     $(".act_stp_hide").hide();
     $("."+val).show();

     // active set
     $(".acti_stp_link").removeClass("active");
     $(".set_"+val).addClass("active");
  }

  // trade history
  function click_trade_hist(val) {
     $(".history_hide").hide();
     $("."+val).show();

     // active set
     $(".trade_hist_link").removeClass("active");
     $(".set_"+val).addClass("active");
  }

  //pair click
  function pair_click(val) {
    pair_data = {};


    var pair = val.replace("_", "/");
    $('#get_pair1').val(pair);
    $('#get_pair2').val(pair);
    get_pair_info1("none");

   
    get_chart_data();

    $('#price1').val(pair_data.buy_rate);
    $('#price2').val(pair_data.sell_rate);

    $('.error').css('display','none');
    $('#amount1_error').css('display','none');
    $('#amount2_error').css('display','none');

    balance = pair.split('/');
      if(balance[1] == "KRW") {
      $('#total1').text("0.00 "+balance[1]);
      $('#total2').text("0.00 "+balance[1]);
      $('#fees1Amount').text("0.00 "+balance[1]);
      $('#fees2Amount').text("0.00 "+balance[1]);
    } else {
      $('#total1').text("0.00000000 "+balance[1]);
      $('#total2').text("0.00000000 "+balance[1]);
      $('#fees1Amount').text("0.00000000 "+balance[1]);
      $('#fees2Amount').text("0.00000000 "+balance[1]);
    }

    buy_click('buy_instant',1);
    sell_click('sell_instant',1);
    buy_sell_ord('buy_order');
    clickfn_active_stop('active_ord_2');
    click_trade_hist('full_trade_hist');

  }

  // sell ord
  function sell_click(val,start) {
    var order = $('ul#sell_selection').find('li.active').data('order');
    if('sell_'+order == val && start == 0) {
    } else {
     $("."+val).show();

     // active set
     $(".sell_active_link").removeClass("active");
     $(".set_"+val).addClass("active");

      if(val == "sell_instant") {
       $("#show_amount_option1").html('<div class="form-group"><div class="col-md-12"> <label>Amount</label> <div class="input-group"> <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2" id="amount1" name="amount" onkeyup="calculate_amt()" onkeypress="number_only(event,8,this.value)" autocomplete="off"> <span class="input-group-addon" id="sell_first-currency">BTC</span> </div></div> <label id="amount1_error" for="amount1" style="display: block;color:red; font-size: 14px; text-transform: none;"></label><input type="hidden" name="price1" id="price1"> <label id="amount1-error" class="error" for="amount1" style="display: none;"></label></div>')
      } else {
        $("#show_amount_option1").html('<div class="form-group"><div class="col-md-12"><label>Amount</label><div class="input-group"><input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2" id="amount1" name="amount" onkeyup="calculate_amt()" onkeypress="number_only(event,8,this.value)" autocomplete="off"> <span class="input-group-addon" id="sell_first-currency">BTC</span> </div></div><label id="amount1_error" for="amount1" style="display: block;color:red;font-size: 14px; text-transform: none;"></label><label id="amount1-error" class="error" for="amount1" style="display: none;"></label></div><div class="form-group"><div class="col-md-12"><label>Price</label> <div class="input-group"><input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2" onkeyup="calculate_amt_limit()" type="text" onkeypress="number_only(event,2,this.value)" id="price1" name="price" autocomplete="off"><span class="input-group-addon" id="sell_second-currency">USD</span></div></div><label id="price1_error" for="price1" style="display: block;color:red; font-size: 14px; text-transform: none;"></label>  <label id="price1-error" class="error" for="price1" style="display: none;"></label></div>');
      }

      type = val.split('_');
      $("#order_type1").val(type[1]);

      if(start == 0) {
        get_pair_info1('none');
      }
      $('#price1').val(pair_data.buy_rate);
      $('#sell_submit').attr('disabled',false);
    }
  }

  // buy ord
  function buy_click(val,start) {
     $("."+val).show();

    var order = $('ul#buy_selection').find('li.active').data('order');
    if('buy_'+order == val && start == 0) {
    } else {
     // active set
     $(".buy_active_link").removeClass("active");
     $(".set_"+val).addClass("active");

     if(val == "buy_instant") {
       $('#show_amount_option').html('<div class="form-group"><div class="col-md-12"><label>Amount</label><div class="input-group"><input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2"  id="amount2" name="amount" onkeyup="calculate_amt1()" onkeypress="number_only(event,8,this.value)" autocomplete="off"><span class="input-group-addon" id="buy_first-currency">BTC</span></div></div><label id="amount2_error" for="amount2" style="display: block;color:red;     font-size: 14px; text-transform: none;"></label><input type="hidden" name="price1" id="price2"><label id="amount2-error" class="error" for="amount2" style="display: none;"></label></div>');
     } else {
        $("#show_amount_option").html('<div class="form-group"><div class="col-md-12"><label>Amount</label><div class="input-group"><input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2"  id="amount2" name="amount" onkeyup="calculate_amt1()" onkeypress="number_only(event,8,this.value)" autocomplete="off"><span class="input-group-addon" id="buy_first-currency">BTC</span></div></div><label id="amount2_error" for="amount2" style="display: block;color:red;     font-size: 14px; text-transform: none;"></label>  <label id="amount2-error" class="error" for="amount2" style="display: none;"></label>    </div>    <div class="form-group"><div class="col-md-12">  <label>Price</label>  <div class="input-group"> <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon2" onkeyup="calculate_amt_limit1()" type="text" onkeypress="number_only(event,2,this.value)" id="price2" name="price" autocomplete="off">  <span class="input-group-addon" id="buy_second-currency">USD</span> </div></div><label id="price2_error" for="price2" style="display: block;color:red; font-size: 14px; text-transform: none;"></label>  <label id="price2-error" class="error" for="price2" style="display: none;"></label> </div>')
     }

     type = val.split('_');
     $("#order_type").val(type[1]);

      if(start == 0) {
        get_pair_info1('none');
      }
      $('#price2').val(pair_data.sell_rate);
      $('#buy_submit').attr('disabled',false);
    }
  }

$(document).ready(function() {
  pair_click('BTC_KRW');
  // buy_click('buy_instant',1);
  // sell_click('sell_instant',1);

$(".toggle-accordion").on("click", function() {
  var accordionId = $(this).attr("accordion-id"),
    numPanelOpen = $(accordionId + ' .collapse.in').length;

  $(this).toggleClass("active");

  if (numPanelOpen == 0) {
    openAllPanels(accordionId);
  } else {
    closeAllPanels(accordionId);
  }
})

openAllPanels = function(aId) {
  console.log("setAllPanelOpen");
  $(aId + ' .panel-collapse:not(".in")').collapse('show');
}
closeAllPanels = function(aId) {
  console.log("setAllPanelclose");
  $(aId + ' .panel-collapse.in').collapse('hide');
}
});

  function number_only(event,type,value) {
    if ((event.which != 8) && (event.which != 0) && (event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
    if(type == "8") {
      digits = 8; 
    } else {
      digits = 2;
    }
  }

  function get_pair_info1(fromdata) {
    if(fromdata == 'none') {
      var pair = $('#get_pair1').val();
    } else {
      var pair = fromdata;
    }     
    $.ajax({
      url: "getPairData",
      type: 'POST',
     
      data: {'_token':'{{ csrf_token() }}','pair': pair},
      success:function(data) {

  

        sell = $.parseJSON(data);
        

        pair = $('#get_pair1').val();

        balance = pair.split('/');

        pair_data.fees = sell.fees;
        pair_data.second_currency = balance[1];
        pair_data.first_currency = balance[0];
        pair_data.buy_rate = sell.buy_rate;
        pair_data.sell_rate = sell.sell_rate;

        $('#buy_rate1').text('Market Price ( '+pair+' ) : '+sell.buy_rate+" "+balance[1]);
        $('#sell_rate1').text('Market Price ( '+pair+' ) : '+sell.sell_rate+" "+balance[1]);
        $('#buy_rate2').text('Market Price ( '+pair+' ) : '+sell.buy_rate+" "+balance[1]);
        $('#sell_rate2').text('Market Price ( '+pair+' ) : '+sell.sell_rate+" "+balance[1]);

        var price1 = $('#price1').val();
        var price2 = $('#price2').val();

        if(price1 == "") {
          $('#price1').val(sell.buy_rate);
        }
        if(price2 == "") {
          $('#price2').val(sell.sell_rate);
        }
        /* NEed analyze*/

        //balance update
var fcry=balance[0];
var scry=balance[1];

 $('#f_curname').text(fcry);
  $('#s_curname').text(scry);

        
        $('.pairValClr #f_curs').html(sell.balance[fcry]);
        $('.pairValClr2 #s_curs').html(sell.balance[scry]);

        
        $('#fees1').text(sell.fees+" %");
        $('#fees2').text(sell.fees+" %");

        $('#sell_first-currency').html(balance[0]);
        $('#sell_second-currency').html(balance[1]);
        $('#buy_first-currency').html(balance[0]);
        $('#buy_second-currency').html(balance[1]);

        $('[name=amount]').attr('placeholder',balance[0]);
        $('[name=price]').attr('placeholder',balance[1]);

        $('.table_heading_first_currency').text(balance[0]);
        $('.table_heading_second_currency').text(balance[1]);

        $.each(sell.balance,function(index,value){
          $('#'+index+'_bal').text(value);
          bal = parseFloat(value);
           if(balance[0] == index) {
              pair_data.first_cur = value;
           }
           if(balance[1] == index) {
              pair_data.second_cur = value;
           }
        })
      $('#buy_content').mCustomScrollbar();
        $('#sell_content').mCustomScrollbar();
        $('#active_content').mCustomScrollbar();
        $('#stop_content').mCustomScrollbar();
        $('#histroy_content').mCustomScrollbar();
        $('#user_histroy_content').mCustomScrollbar();

        $('#buy_content .mCSB_container').html(sell.buyResult);
        $('#sell_content .mCSB_container').html(sell.sellResult);
        $('#active_content .mCSB_container').html(sell.activeResult);
        $('#stop_content .mCSB_container').html(sell.stop_result);
        $('#histroy_content .mCSB_container').html(sell.trade_result);
        $('#user_histroy_content .mCSB_container').html(sell.my_trade_result);

        //header data
        $('#lastbid').text(sell.lastbid);
        $('#change_val').text(sell.change);
        $('#high_24hr').text(sell.high);
        $('#low_24hr').text(sell.low);
        $('#base_volume').text(sell.volume);

        if(sell.arrow == "up") {
          $('#arrow_image').attr('src',"{{asset('/').('public/frontend/img/top_arr.png')}}");
        } else if(sell.arrow == "down") {
          $('#arrow_image').attr('src',"{{asset('/').('public/frontend/img/bot_arr.png')}}");
        } else {
          $('#arrow_image').attr('src',"");
        }
      }
    })
  }

  //calculate amount for sell form
  function calculate_amt() {
    var amt = parseFloat($('#amount1').val());
    var fees = parseFloat(pair_data.fees);
    var buyrate =  parseFloat($('#price1').val());

    total = (amt * buyrate); 
    fees = (total * fees) / 100;

    sub_total = total - fees;

    pair = $('#get_pair1').val();
    balance = pair.split('/');

    if(balance[1] != "KRW") {
      digit = 8;
    } else {
      digit = 2;
    }
    if(isNaN(sub_total)){
      sub_total = 0;
    }
    var feeAmount = isNaN(fees)? "0.00" : fees.toFixed(digit);
    $('#fees1Amount').text(feeAmount+" "+balance[1]);
    $('#total1').text(sub_total.toFixed(digit)+" "+balance[1]);
    show_bal_error();
  }

  //amount limit for sell form
  function calculate_amt_limit() {
    var amt = parseFloat($('#amount1').val());
    var fees = parseFloat(pair_data.fees);
    var price = parseFloat($('#price1').val());

    var order = $('ul#sell_selection').find('li.active').data('order');
    if(order == "stop") {
      if(price >= parseFloat(pair_data.buy_rate)) {
        if($('#trade_sell_form').valid() == true) {
          $('#price1_error').html('Please enter below market price');
        } else {
          $('#price1_error').html('');
        }
        $('#sell_submit').attr('disabled',true);
        return false;
      } else {
        $('#price1_error').html('');
        $('#sell_submit').attr('disabled',false);
      }
    }
    calc = (amt * price);
    fees_amt = (calc * fees) / 100;
    total = (calc - fees_amt);
    pair = $('#get_pair1').val();
    balance = pair.split('/');

    if(balance[1] != "KRW") {
      digit = 8;
    } else {
      digit = 2;
    }

    if(isNaN(total)) {
      total = 0;
    }
    var feeAmount = isNaN(fees_amt)? "0.00" : fees_amt.toFixed(digit);
    $('#fees1Amount').text(feeAmount+" "+balance[1]);
    $('#total1').text(total.toFixed(digit)+" "+balance[1]);
    show_bal_error();
  }

  //error for sell form
  function show_bal_error() {
    var total =  parseFloat($('#amount1').val());
    var balance = parseFloat(pair_data.first_cur);
    if(total == 0) {
      if($('#trade_sell_form').valid() == true) {
        $('#amount1_error').html('Please enter valid amount');
      } else {
        $('#amount1_error').html('');
      }
      $('#sell_submit').attr('disabled',true);
      return false;
    } else {
      if(balance < total) {
        if($('#trade_sell_form').valid() == true) {
          $('#amount1_error').html('Insufficient Balance');
        } else {
          $('#amount1_error').html('');
        }
        $('#sell_submit').attr('disabled',true);
        return false;
      } else {
        $('#amount1_error').html('');
        $('#sell_submit').attr('disabled',false);
        return true;
      }
    }
  }

  //sell form
  function check_status() {
    s = show_bal_error();
    return  s === false ? false : true;
  }

  //buy form
  function check_status1() {
    s = show_bal_error1();
    return  s === false ? false : true;
  }

  //calculate amount for buy form
  function calculate_amt1() {
    var amt = parseFloat($('#amount2').val());
    var fees = parseFloat(pair_data.fees);
    var sellrate = parseFloat($('#price2').val());
    if(sellrate == "") {
      var sellrate =  parseFloat(pair_data.sell_rate);
    }

    total = (amt * sellrate); 
    fees = (total * fees) / 100;
    sub_total = total + fees;
    pair = $('#get_pair2').val();
    balance = pair.split('/');

    if(balance[1] != "KRW") {
      digit = 8;
    } else {
      digit = 2;
    }

    if(isNaN(sub_total)) {
      sub_total = 0;
    }
    var feeAmount = isNaN(fees)? "0.00" : fees.toFixed(digit);
    $('#fees2Amount').text(feeAmount+" "+balance[1]);
    $('#total2').text(sub_total.toFixed(digit)+" "+balance[1]);
    show_bal_error1();
  }

  //calculate amount limit for buy form
  function calculate_amt_limit1() {
    var amt = parseFloat($('#amount2').val());
    var fees = parseFloat(pair_data.fees);
    var price = parseFloat($('#price2').val());

    var order = $('ul#buy_selection').find('li.active').data('order');
    if(order == "stop") {
      if(price <= parseFloat(pair_data.sell_rate)) {
        if($('#trade_buy_form').valid() == true) {
          $('#price2_error').html('Please enter above market price');
        } else {
          $('#price2_error').html('');
        }
        $('#buy_submit').attr('disabled',true);
        return false;
      } else {
        $('#price2_error').html('');
        $('#buy_submit').attr('disabled',false);
      }
    }
    calc = (amt * price);
    fees_amt = (calc * fees) / 100;
    total = (calc + fees_amt);
    pair = $('#get_pair2').val();
    balance = pair.split('/');
    if(balance[1] != "KRW") {
      digit = 8;
    } else {
      digit = 2;
    }
    if(isNaN(total)) {
      total = 0;
    }
    var feeAmount = isNaN(fees_amt)? "0.00" : fees_amt.toFixed(digit);
    $('#fees2Amount').text(feeAmount+" "+balance[1]);
    $('#total2').text(total.toFixed(digit)+" "+balance[1]);
    show_bal_error1();
  }

  //balance error for buy form
  function show_bal_error1() {
    var total = parseFloat($('#total2').text());
    var balance = parseFloat(pair_data.second_cur);
    if(total == 0) {
      if($('#trade_buy_form').valid() == true) {
        $('#amount2_error').html('Please enter valid amount');
      } else {
        $('#amount2_error').html('');
      }
      $('#buy_submit').attr('disabled',true);
      return false;
    } else {
      if(balance < total) {
        if($('#trade_buy_form').valid() == true) {
          $('#amount2_error').html('Insufficient balance');
        } else {
          $('#amount2_error').html('');
        }
        $('#buy_submit').attr('disabled',true);
        return false;
      } else {
        $('#amount2_error').html('');
        $('#buy_submit').attr('disabled',false);
        return true;
      }
    }
  }

  $('#trade_sell_form').validate({
    ignore:"",
    rules:{
      order_type:{
          required:true,
      },
      get_pair1:{
          required:true,
      },
      amount:{
          required:true,
          min:0.00000001,
          number:true,
          
      },
      price:{
          required:true,
         
          number:true,

            remote:{
            url: "checkSecondCurrency",
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
              currency: function() {
                return $('#sell_second-currency').html();
              },
              amount: function() {
                return $('#trade_sell_form #price1').val();
              } 
            },
            dataFilter: function(data) {
              
              if(data != "true") {
                return "\"" + data + "\"";
              } else {
                return "true";
              }
            }
          }
          
      },
    },
    messages:{
      amount:{
        required:"Please enter amount",
        min:"Enter greater than 0.00000001",
        number:"Please enter valid amount"
      },
      price:{
        required:"Please enter price",
        min:"Enter greater than 0.01",
        number:"Please enter valid price"
      }
    },
    submitHandler:function() {

      var a = "sell";
      var type = $('#order_type1').val();
    
      var buycount = $('[name=buycount]').val();
      if(type == "instant" || type == "limit") {
        var url = "{{ URL::to('createSellOrder') }}";
      } else {
        var url = "{{ URL::to('createSellStopOrder') }}";
      }
      pair = $('#trade_sell_form #get_pair1').val();

      $.ajax({
        url:url,
        method:"POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:$('#trade_sell_form').serialize(),
        beforeSend:function(){
          $('#sell_submit').hide();
          $('#sell_loader').show();
        },
        success:function(data,status,xhr) {
          data = $.parseJSON(data);
          if(data.status == "1") {
            mapping(a,data.lastid,pair);
          } else {
            alert(data.msg);
            get_pair_info1('none');
            $('#sell_submit').show();
            $('#sell_loader').hide();
          }
        },
        error:function() {
          alert('Error encountered!');
          $('#sell_submit').show();
          $('#sell_loader').hide();
          get_pair_info1('none');
        }
      })
    }
  })

  $('#trade_buy_form').validate({
    ignore:"",
    rules:{
      order_type:{
          required:true,
      },
      get_pair1:{
          required:true,
      },
      amount:{
          required:true,
          min:0.00000001,
          number:true,
         
      },
      price:{
          required:true,
          
          number:true,
               remote:{
            url: "checkSecondCurrency",
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {
              currency: function() {
                return $('#sell_second-currency').html();
              },
              amount: function() {
                return $('#trade_sell_form #price1').val();
              } 
            },
            dataFilter: function(data) {
              if(data != "true") {
                return "\"" + data + "\"";
              } else {
                return "true";
              }
            }
          }
          
      }, 
    },
    messages:{
      amount:{
        required:"Please enter amount",
        min:"Enter greater than 0.00000001",
        number:"Please enter valid amount"
      },
      price:{
        required:"Please enter price",
        min:"Enter greater than 0.01",
        number:"Please enter valid price"
      }
    },
    submitHandler:function() {

      var a = "buy";
      var type = $('#order_type').val();
      var sellcount = $('[name=sellcount]').val();
      if(type == "instant" || type == "limit") {
        var url = "{{ URL::to('createBuyOrder') }}";
      } else {
        var url = "{{ URL::to('createBuyStopOrder') }}";
      }
      pair = $('#trade_buy_form #get_pair2').val();

      $.ajax({
        url:url,
        data:$('#trade_buy_form').serialize(),
        method:"POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend:function(){
          $('#buy_submit').hide();
          $('#buy_loader').show();
        },
        success:function(data,status,xhr) {
          data = $.parseJSON(data);
          if(data.status == "1") {
            mapping(a,data.lastid,pair);
          } else {
            alert(data.msg);
            get_pair_info1('none');
            $('#buy_submit').show();
            $('#buy_loader').hide();
          }
        },
        error:function() {
          alert('Error encountered!');
          $('#buy_submit').show();
          $('#buy_loader').hide();
          get_pair_info1('none');
        }
      })
    }
  })

  function mapping(a,id,pair) {
    $.ajax({
      url:"{{ URL::to('tradeMapping') }}",
      method:"POST",
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: "type="+a+"&freshorderid="+id+"&pair="+pair,
      success: function(res,status,xhr) {
        var res = res.replace(/(\r\n|\n|\r)/gm,"");
        if(res=="failure") {
          var output = res;
          var filledamount = 0;
        } else {
          var partsOfStr = res.split('_');
          var output = partsOfStr[0];
          var filledamount = partsOfStr[1];
        }

        if(output=="success") {
          if(a=='buy') {
            alert('Your Buy Order Placed.!');
          } else {
            alert('Your Sell Order Placed.!');
          }
        } else {
          if(a=='buy') {
            alert('Your Buy Order Placed.!');
          } else {
            alert('Your Sell Order Placed.!');
          }
        }
        balance = pair.split('/');
        if(a=='buy') {
          $('#buy_submit').show();
          $('#buy_loader').hide();
          $('#trade_buy_form').trigger('reset');
         

            if(balance[1] != "BTC") {
            $('#total2').text("0.00 "+balance[1]);
            $('#fees2Amount').text("0.00 "+balance[1]);
          } else {
             if(balance[1] != "BTC") {
            $('#total1').text("0.00 "+balance[1]);
            $('#fees1Amount').text("0.00 "+balance[1]);
          } else {
            $('#total1').text("0.00000000 "+balance[1]);
            $('#fees1Amount').text("0.00000000 "+balance[1]);
          }
          }
        } else {
          $('#sell_submit').show();
          $('#sell_loader').hide();
          $('#trade_sell_form').trigger('reset');
          $('#total1').text("0.00 "+balance[1]);
          $('#fees1Amount').text("0.00 "+balance[1]);
        }
        //get_pair_info1('none');
        socket.emit('receiverequest',{'msg':'1'});
      },
      error:function(error) {
        alert("Failure! Please try again");
        get_pair_info1('none');
        ('#buy_submit').show();
        $('#buy_loader').hide();
        $('#sell_submit').show();
        $('#sell_loader').hide();
      }
    })
  }

  function cancel_order(id,type) {
    $('#tradeid').val(id);
    $('#type').val(type);
    $('#ticketAlert').modal('show');
  }

  $('#cancel_order_form').validate({
    rules:{
      tradeid:{
        required:true,
      }
    },
    submitHandler:function() {
      $('#ticketAlert').modal('hide');
      var type = $('#type').val();
      if(type == '1') {
        var url = "{{ URL::to('cancelOrder') }}";
      } else {
        var url = "{{ URL::to('cancelStopOrder') }}";
      }

      $.ajax({
        url:url,
        data:$('#cancel_order_form').serialize(),
        method:"POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success:function(data,status,xhr) {
          data = $.parseJSON(data);
          if(data.status == "2") {
            alert('Error! '+data.msg);
            window.location.href = "{{ URL::to('/') }}";
          } else if(data.status == "1") {
            alert('Success! '+data.msg);
          } else {
            alert('Error! '+data.msg);
          }
           socket.emit('receiverequest',{'msg':'1'});
         // get_pair_info1('none');
        }
      })
    }
  })

  $('#close_no').click(function(){
    $('#ticketAlert').modal('hide');
  })

  var userId = "{{ Session::get('user_id') }}";
  if(userId != "") {
    $(function () {
      setInterval(function(){ 
        get_pair_info1('none');
        get_chart_data();
      }, 30000);
    });
  }

  function get_chart_data() {
    var pair = $('#get_pair1').val();
    $.getJSON("{{ URL::to('getChartData?pair=') }}"+pair, function (data) {
      var chart = AmCharts.makeChart("chartdiv", {
        "hideCredits":true,
        "type": "serial",
        "theme": "none",
        "dataDateFormat":"YYYY-MM-DD",
        "valueAxes": [{
          "position": "left"
        }],
        "graphs": [{
          "id": "g1",
          "proCandlesticks": true,
          "balloonText": "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",
          "closeField": "close",
          "fillColors": "#7f8da9",
          "highField": "high",
          "lineColor": "#7f8da9",
          "lineAlpha": 1,
          "lowField": "low",
          "fillAlphas": 0.9,
          "negativeFillColors": "#db4c3c",
          "negativeLineColor": "#db4c3c",
          "openField": "open",
          "title": "Price:BTC/USD",
          "type": "candlestick",
          "valueField": "close"
        }],
        "chartScrollbar": {
          "graph": "g1",
          "graphType": "line",
          "scrollbarHeight": 30
        },
        "chartCursor": {
          "valueLineEnabled": true,
          "valueLineBalloonEnabled": true
        },
        "categoryField": "date",
        "categoryAxis": {
          "parseDates": true
        },
        "dataProvider": data,
        "export": {
          "enabled": false,
          "position": "bottom-right"
        }
      });
    });
  }

</script>
