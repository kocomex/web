@include('layouts.front_header')

<!-- Banner section starts here -->
<section class="bannerSec">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="bannercaption">
            <p>
              <span>{!! $banner1->page_content !!}</span>
              {!! $banner2->page_content !!}
            </p>
            <a href="{{ url('/trade') }}" class="btn btnBnr">Get Coins Now</a>
          </div>
        </div>


        <div class="col-md-4">

          <?php if(Session::get('user_id') == "") { ?> 
          <div class="loginForm">
            <h4 class="">Login</h4>
            <form class="form-horizontal" name="login_form" id="login_form">
             {!! csrf_field() !!}
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><img src="{{ URL::asset('theme/front_users/images/user-ic.png') }}"></span>
                  <input type="text" name="emailid" class="form-control" aria-label="" placeholder="Email ID">
                </div>
              </div>
              <label id="emailid-error" class="error" for="emailid" style="display: none;">This field is required.</label>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><img src="{{ URL::asset('theme/front_users/images/lock-ic.png') }}"></span>
                  <input type="password" class="form-control" name="password" aria-label="" placeholder="Password">
                </div>
              </div>
               <label id="password-error" class="error" for="password" style="display: none;">This field is required.</label>
              <div class="form-group mt-30" >
                <button type="submit" class="btn btnSignIn btn-block" id="login_btn">Login</button>
            </div>
            <div id="loader1" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            
            </div>
            <div id="login_error" style="color:red;font-size: 100%;text-align:center;"></div>
              <div class="form-group row no-gutters">
                  <div class="col text-left">
                    <a href="{{ url('/register') }}" class="" >Register</a> 
                  </div>
                  <div class="col d-flex justify-content-end">
                    <a href="{{ url('/forgetpwd') }}">Forgot Password?</a>
                  </div>
              </div>


 <div class="modal fade csl_login_for" id="twofactor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="vertical-alignment-helper">
<div class="vertical-align-center">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 40%;">
      <div class="modal-header" id="two_factor_popup">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
        <h4 class="modal-title" id="myModalLabel">Two factor authentication</h4>
      </div>
      <div class="modal-body cls_login_sec cls_for_pad text-center"> 
      
        <p class="mar-top-20">Enter TFA code</p>
         <!-- <div id="two_error" style="color:#a94442;font-size: 85%;text-align:center; display: block;"></div> -->
        <div class="input-group form-group mar-top-20">
          <input type="text" id="onecode" name="onecode" class="form-control" aria-describedby="basic-addon1">
         
        </div>
        <div id="two_error1" style="color:red;font-size: 100%;text-align:center;"></div>
           <div id="loadertfa" style="display:none;margin: 0px 45px; ">
 
            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            <
            </div>
        <div class="mar-top-20 mar-bot-30" align="">
          <button type="submit" id="twofactor_button" class="cls_sub_btn"> Submit </button>
        

        
            </div> 
        <!--loader-->
        </div>
         </div>
    </div>
  </div>
  </div>
  </div>
</div>

<?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- Banner section end here -->


<!-- Currency slider section starts here -->
<section class="marketSlider">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="owl-carousel">
        
      @foreach($trade_rates as $rates)
          <div class="items">
            <div class="coinSlider">
              <div class="slMidSec">{{$rates->pair_name}}  :  <span class="clrNeg">{{$rates->sell_rate}} </span></div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Currency slider section end here --> 
  

	<!-- graph section starts here -->
	<section class="graphSec">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-10">
         <!--  <h4 class="d-inline-block mr-20">BTC / KRW</h4>
          <ul class="list-inline d-inline-block currVal">
            <li class="list-inline-item">24h High  :  400.00</li>
            <li class="list-inline-item">24h Low  :  360.00</li>
            <li class="list-inline-item">24h Change  :  - 6.11%</li>
            <li class="list-inline-item">24h Vol  :  380.50</li>
            <li>
          </ul> -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center" id="chrting">
          <div class="buy_graph wow fadeInLeft" id="chartdiv" style="width: 100%; height: 100%;">
          </div>
        </div>
      </div>
    </div>
	</section>
	<!-- graph section end here --> 

	<!-- Our features section starts here -->
	<section class="ourFeatureSec">
    <div class="container">
        <div class="col-12 text-center">
          <h3 class="mb-50">Our Features</h3>
        </div>
      <div class="row">
        <div class="col-md-6">
          <img src="{{ URL::asset('theme/front_users/images/of-img-1.png') }}" alt=""  class="img-fluid">
        </div>
        <div class="col-md-6 ofTabSec featu_sec">
          <ul class="list-inline" role="tablist">
            <li class="nav-item list-inline-item">
              <a class="nav-link change_pwd_list set_fea2 active" href="javascript:change_pwd('fea1')">Easy to Use </a>
            </li>
            <li class="nav-item list-inline-item">
              <a class="nav-link change_pwd_list set_fea2" href="javascript:change_pwd('fea2')">Easy Interface</a>
            </li>
            <li class="nav-item list-inline-item">
              <a class="nav-link change_pwd_list set_fea3" href="javascript:change_pwd('fea3')">Speed & Reliable</a>
            </li>
            <li class="nav-item list-inline-item">
              <a class="nav-link change_pwd_list set_fea4" href="javascript:change_pwd('fea4')">Efficient</a>
            </li>
          </ul>
          <div class=" mt-30">

            <div  class="pwd_hide fea1">
              <div class="row">
                  <div class="col-md-3 d-flex align-items-center">
                    <img src="{{ URL::asset('theme/front_users/images/of-ic-1.png') }}">

                  </div>
                 {!! $easyuse->page_content !!}
                </div> 
            </div>
            <div  class="pwd_hide fea2"  style="display: none;">
              <div class="row">
                  <div class="col-md-3 d-flex align-items-center">
                    <img src="{{ URL::asset('theme/front_users/images/of-ic-2.png') }}">
                  </div>
                {!! $easyinterface->page_content !!}
                </div>  
            </div>
            <div  class="pwd_hide fea3"  style="display: none;">
              <div class="row">
                <div class="col-md-3 d-flex align-items-center">
                  <img src="{{ URL::asset('theme/front_users/images/of-ic-3.png') }}">
                </div>
              {!! $speedreliable->page_content !!}
              </div> 
            </div>
            <div  class="pwd_hide fea4"  style="display: none;">
              <div class="row">
                  <div class="col-md-3 d-flex align-items-center">
                    <img src="{{ URL::asset('theme/front_users/images/of-ic-4.png') }}">
                  </div>
                 {!! $efficient->page_content !!}
                </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>
	<!-- Our features section end here -->

  <!-- How it works section starts here -->
	<section class="hiwSec">
    <div class="container">
      <div class="row">
       <div class="col-12 text-center">
         <h4 class="inTit clrWht mt-30">How it works ?</h4>
       </div>
      </div>
      <div class="row">
          <div class="col-12">
            <div class="hiwContent">
              <ul class="list-inline">
                <li class="list-inline-item">
                  <div class="icSec">
                    <img src="{{ URL::asset('theme/front_users/images/hw-1.png') }}" alt="">
                  </div>
                  <p>Create account</p>
                </li>
                <li class="list-inline-item">
                  <div class="icSec">
                    <img src="{{ URL::asset('theme/front_users/images/hw-2.png') }}" alt="">
                  </div>
                  <p>Verification</p>
                </li>
                <li class="list-inline-item">
                  <div class="icSec">
                    <img src="{{ URL::asset('theme/front_users/images/hw-3.png') }}" alt="">
                  </div>
                  <p>Add funds</p>
                </li>
                <li class="list-inline-item">
                  <div class="icSec">
                    <img src="{{ URL::asset('theme/front_users/images/hw-4.png') }}" alt="">
                  </div>
                  <p>Place an offer</p>
                </li>
                <li class="list-inline-item">
                  <div class="icSec">
                    <img src="{{ URL::asset('theme/front_users/images/hw-5.png') }}" alt="">
                  </div>
                  <p>Get Payments</p>
                </li>
              </ul>
            </div>
          </div>
         </div>
    </div>
	</section>
	<!-- How it works section end here -->

  <!-- Start trading section starts here -->
  <section class="sTSec">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6 text-center">
          <h4>Start Trading within minutes</h4>
         <form class="form-horizontal" id="sub_form" method="post" action="{{ url('/subscribe') }}">
                {!! csrf_field() !!}
          <div class="input-group">
         
            <input type="text" class="form-control" name="email" placeholder="Your Email Address">


            <span class="input-group-btn">
              <button class="btn btnPrime" type="submit">Subscribe</button>
            </span>

            
          </div>
          <label id="email-error" class="error" for="email" style="display: none;">This field is required.</label>
          </form>
        </div>
      </div>
    </div>
  </section>

 


  <!-- Start trading section end here -->

   <script type="text/javascript">


 $('#sub_form').validate({

  rules:{


    email:{
      required:true,
      email:true,
        remote: {
                url: '{{ url('/subemail_check') }}',
                type: 'Post',
                dataType:'json',
                data: {
          "_token": "{{ csrf_token() }}",
                    email_id: function() {

                        return $('#sub_form #email').val();
                    }
                }
             }
      
    },
   
   


  },
   messages:{
     
      email:{
        remote:"Emailid already taken",
        email:"Please enter a valid email id",
      },
     /* country_code:{
        required:"",
      }*/
    }
  
});

</script>
<script type="text/javascript">
  
       $('#login_form').validate({
    
    rules:{
    
      emailid:{
        required:true,
        email:true,
      

      },
      
      
      password:{
        required:true,
      },
      
    },submitHandler:function(form,e)
  {
      e.preventDefault();
     
      $.ajax({  
      url:"{{ url('/login') }}",
      data: $('#login_form').serialize(),
      type:'post', 
      dataType : 'json',
  beforeSend:function(){
        $('#loader1').show();
        $('#loadertfa').show();
        $('#login_btn').hide();
         
      },
     
      success: function(data){
       
        $('#loader1').hide();
         $('#loadertfa').hide();
        
        $('#login_btn').show();
        

        
        
if(data.status)
        {
if(data.title == "tfa"){
            $("#twofactor").modal("show");
                      }
          else
          {
           location.href="{{ url('profile') }}";
          }

        }
        else{
          if(data.title == 'Enter a valid code')

          {
             $('#two_error1').css('display', '');
                  $('#two_error1').html(data.title);
                  $('#two_error1').delay(2000).fadeOut(1400);
          }
          else{
        $('#login_error').css('display', '');
                  $('#login_error').html(data.title);
                  $('#login_error').delay(2000).fadeOut(1400); 

}
                  

                  
        }
    
     
       
      
       }        
       }); 
  }
  
});




  
  </script>

@include('layouts.front_footer')

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script type="text/javascript">
  var chartData = generateChartData();
  var curncy="BTC/KRW";


  var chart = AmCharts.makeChart("chartdiv", {
    "hideCredits":true,
    "type": "serial",
    "theme": "light",
    "legend": {
        "useGraphSettings": true
    },
    "dataProvider": chartData,
    "synchronizeGrid":true,
    "valueAxes": [{
        "id":"v1",
        "axisColor": "#FF6600",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "left"
    }, {
        "id":"v2",
        "axisColor": "#FCD202",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "right"
    }, {
        "id":"v3",
        "axisColor": "#B0DE09",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "left"
    },  {
        "id":"v4",
        "axisColor": "#6fa1f2",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "right"
    },{
        "id":"v5",
        "axisColor": "#FF6600",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "left"
    }, {
        "id":"v6",
        "axisColor": "#FCD202",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "right"
    }, {
        "id":"v7",
        "axisColor": "#B0DE09",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "left"
    },  {
        "id":"v8",
        "axisColor": "#6fa1f2",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "right"
    },{
        "id":"v9",
        "axisColor": "#FF6600",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "left"
    }, {
        "id":"v10",
        "axisColor": "#FCD202",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "right"
    }, {
        "id":"v11",
        "axisColor": "#B0DE09",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "left"
    },  {
        "id":"v12",
        "axisColor": "#6fa1f2",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "right"
    },{
        "id":"v13",
        "axisColor": "#FF6600",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "left"
    }, {
        "id":"v14",
        "axisColor": "#FCD202",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "right"
    }, {
        "id":"v15",
        "axisColor": "#B0DE09",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "left"
    },  {
        "id":"v16",
        "axisColor": "#6fa1f2",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "right"
    },{
        "id":"v17",
        "axisColor": "#FF6600",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "left"
    }, {
        "id":"v18",
        "axisColor": "#FCD202",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "right"
    }, {
        "id":"v19",
        "axisColor": "#B0DE09",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "left"
    },  {
        "id":"v20",
        "axisColor": "#6fa1f2",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "right"
    },{
        "id":"v21",
        "axisColor": "#FF6600",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "left"
    }, {
        "id":"v22",
        "axisColor": "#FCD202",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "right"
    }, {
        "id":"v23",
        "axisColor": "#B0DE09",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "left"
    },  {
        "id":"v24",
        "axisColor": "#6fa1f2",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "right"
    },{
        "id":"v25",
        "axisColor": "#FF6600",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "left"
    }, {
        "id":"v26",
        "axisColor": "#FCD202",
        "axisThickness": 2,
        "axisAlpha": 1,
        "position": "right"
    }, {
        "id":"v27",
        "axisColor": "#B0DE09",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "left"
    },  {
        "id":"v28",
        "axisColor": "#6fa1f2",
        "axisThickness": 2,
        "gridAlpha": 0,
        "offset": 50,
        "axisAlpha": 1,
        "position": "right"
    }],
    "graphs": [{
        "valueAxis": "v1",
        "lineColor": "#FF6600",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "BTC/KRW",
        "valueField": "visits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v2",
        "lineColor": "#FCD202",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "ETH/KRW",
        "valueField": "hits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v3",
        "lineColor": "#B0DE09",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "LTC/KRW",
        "valueField": "views",
    "fillAlphas": 0
    },  {
        "valueAxis": "v4",
        "lineColor": "#6fa1f2",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "XRP/KRW",
        "valueField": "high",
    "fillAlphas": 0
    },{
        "valueAxis": "v5",
        "lineColor": "#FF6600",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "DOGE/KRW",
        "valueField": "visits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v6",
        "lineColor": "#FCD202",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "ETC/KRW",
        "valueField": "hits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v7",
        "lineColor": "#B0DE09",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "XMR/KRW",
        "valueField": "views",
    "fillAlphas": 0
    },  {
        "valueAxis": "v8",
        "lineColor": "#6fa1f2",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "IOTA/KRW",
        "valueField": "high",
    "fillAlphas": 0
    },{
        "valueAxis": "v9",
        "lineColor": "#FF6600",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "BTG/KRW",
        "valueField": "visits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v10",
        "lineColor": "#FCD202",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "DASH/KRW",
        "valueField": "hits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v11",
        "lineColor": "#B0DE09",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "ETH/BTC",
        "valueField": "views",
    "fillAlphas": 0
    },  {
        "valueAxis": "v12",
        "lineColor": "#6fa1f2",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "LTC/BTC",
        "valueField": "high",
    "fillAlphas": 0
    },{
        "valueAxis": "v13",
        "lineColor": "#FF6600",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "XRP/BTC",
        "valueField": "visits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v14",
        "lineColor": "#FCD202",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "DOGE/BTC",
        "valueField": "hits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v15",
        "lineColor": "#B0DE09",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "ETC/BTC",
        "valueField": "views",
    "fillAlphas": 0
    },  {
        "valueAxis": "v16",
        "lineColor": "#6fa1f2",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "XMR/BTC",
        "valueField": "high",
    "fillAlphas": 0
    },{
        "valueAxis": "v17",
        "lineColor": "#FF6600",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "IOTA/BTC",
        "valueField": "visits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v18",
        "lineColor": "#FCD202",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "BTG/BTC",
        "valueField": "hits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v19",
        "lineColor": "#B0DE09",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "DASH/BTC",
        "valueField": "views",
    "fillAlphas": 0
    },  {
        "valueAxis": "v20",
        "lineColor": "#6fa1f2",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "BTC/ETH",
        "valueField": "high",
    "fillAlphas": 0
    },{
        "valueAxis": "v21",
        "lineColor": "#FF6600",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "LTC/ETH",
        "valueField": "visits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v22",
        "lineColor": "#FCD202",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "XRP/ETH",
        "valueField": "hits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v23",
        "lineColor": "#B0DE09",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "DOGE/ETH",
        "valueField": "views",
    "fillAlphas": 0
    },  {
        "valueAxis": "v24",
        "lineColor": "#6fa1f2",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "ETC/ETH",
        "valueField": "high",
    "fillAlphas": 0
    },{
        "valueAxis": "v25",
        "lineColor": "#FF6600",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "XMR/ETH",
        "valueField": "visits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v26",
        "lineColor": "#FCD202",
        "bullet": "round",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "IOTA/ETH",
        "valueField": "hits",
    "fillAlphas": 0
    }, {
        "valueAxis": "v27",
        "lineColor": "#B0DE09",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "BTG/ETH",
        "valueField": "views",
    "fillAlphas": 0
    },  {
        "valueAxis": "v28",
        "lineColor": "#6fa1f2",
        "bullet": "square",
        "bulletBorderThickness": 1,
        "hideBulletsCount": 30,
        "title": "DASH/ETH",
        "valueField": "high",
    "fillAlphas": 0
    }],
    "chartScrollbar": {},
    "chartCursor": {
        "cursorPosition": "mouse"
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "axisColor": "#DADADA",
        "minorGridEnabled": true
    },
    "export": {
      "enabled": false,
        "position": "bottom-right"
     }
  });

  chart.addListener("dataUpdated", zoomChart);
  zoomChart();


// generate some random data, quite different range
function generateChartData() {
    var chartData = [];
    var firstDate = new Date();
    firstDate.setDate(firstDate.getDate() - 45);

    <?php
      $end_date = date("Y-m-d H:i:s");
      $start_date = date('Y-m-d H:i:s', strtotime($end_date . ' - 45 days'));
    ?>

    var visits = 0;
    var hits = 0;
    var views = 0;
    var high = 0;
    var long = 0;
    var short = 0;
    var aero = 0;
    var crazy = 0;
    var want = 0;
    var buy = 0;
    var went = 0;
    var dragon = 0;
    var pulsar = 0;
    var yum = 0;
    var wapp = 0;
    var heyo = 0;
    var metro = 0;
    var music = 0;
    var marvel = 0;
    var vamp = 0;
    var xmas = 0;
    var bajal = 0;
    var forse = 0;
    var dorge = 0;
     var joge = 0;
      var caty = 0;
       var dogie = 0;

    <?php
    for ($i = 0; $i <= 45; $i++) { ?>
        var newDate = new Date(firstDate);
        newDate.setDate(newDate.getDate() + <?php echo $i; ?>);

        <?php
          $interval = date('Y-m-d H:i:s', strtotime($start_date . ' + '.$i.' days'));
          $date = explode(' ',$interval)[0];
         
        ?>

     
        visits = "<?php echo App\Trade::forIndexChart($date,"BTC/KRW"); ?>";
        hits = "<?php echo App\Trade::forIndexChart($date,"ETH/KRW"); ?>";
        views = "<?php echo App\Trade::forIndexChart($date,"LTC/KRW"); ?>";
        high = "<?php echo App\Trade::forIndexChart($date,"XRP/KRW"); ?>";
        long = "<?php echo App\Trade::forIndexChart($date,"DOGE/KRW"); ?>";
        short = "<?php echo App\Trade::forIndexChart($date,"ETC/KRW"); ?>";
        aero = "<?php echo App\Trade::forIndexChart($date,"XMR/KRW"); ?>";
        crazy = "<?php echo App\Trade::forIndexChart($date,"IOTA/KRW"); ?>";
        want = "<?php echo App\Trade::forIndexChart($date,"BTG/KRW"); ?>";
        buy = "<?php echo App\Trade::forIndexChart($date,"DASH/KRW"); ?>";
        went = "<?php echo App\Trade::forIndexChart($date,"ETH/BTC"); ?>";
        dragon = "<?php echo App\Trade::forIndexChart($date,"LTC/BTC"); ?>";
        pulsar = "<?php echo App\Trade::forIndexChart($date,"XRP/BTC"); ?>";
        yum = "<?php echo App\Trade::forIndexChart($date,"DOGE/BTC"); ?>";
        wapp = "<?php echo App\Trade::forIndexChart($date,"ETC/BTC"); ?>";
        heyo = "<?php echo App\Trade::forIndexChart($date,"XMR/BTC"); ?>";
        metro = "<?php echo App\Trade::forIndexChart($date,"IOTA/BTC"); ?>";
        music = "<?php echo App\Trade::forIndexChart($date,"BTG/BTC"); ?>";
        marvel = "<?php echo App\Trade::forIndexChart($date,"DASH/BTC"); ?>";
        viller = "<?php echo App\Trade::forIndexChart($date,"BTC/ETH"); ?>";
        vamp = "<?php echo App\Trade::forIndexChart($date,"LTC/ETH"); ?>";
        xmas = "<?php echo App\Trade::forIndexChart($date,"XRP/ETH"); ?>";
        bajal = "<?php echo App\Trade::forIndexChart($date,"DOGE/ETH"); ?>";
        forse = "<?php echo App\Trade::forIndexChart($date,"ETC/ETH"); ?>";
         dorge = "<?php echo App\Trade::forIndexChart($date,"XMR/ETH"); ?>";
        joge = "<?php echo App\Trade::forIndexChart($date,"IOTA/ETH"); ?>";
        caty = "<?php echo App\Trade::forIndexChart($date,"BTG/ETH"); ?>";
        dogi = "<?php echo App\Trade::forIndexChart($date,"DASH/ETH"); ?>";


        chartData.push({
            date: newDate,
            visits: visits,
            hits: hits,
            views: views,
            high: high,
            long: long,
            short: short,
            aero: aero,
            crazy: crazy,
            want: want,
            buy: buy,
            went: went,
            dragon: dragon,
            pulsar: pulsar,
            visits: visits,
            yum: yum,
            wapp: wapp,
            heyo: heyo,
            metro: metro,
            music: music,
            marvel: marvel,
            viller: viller,
            vamp: vamp,
            xmas: xmas,
            bajal: bajal,
            forse: forse,
            dorge: dorge,
            joge: joge,
            caty: caty,
            dogi: dogi,
        });






    <?php } ?>
    return chartData;
}

function zoomChart(){
    chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
}

</script>


<script type="text/javascript">
           

            function change_pwd(val)
            {
            //alert(val);
               $(".pwd_hide").hide();
               $("."+val).show();

               // active set
               $(".change_pwd_list").removeClass("active");
               $(".set_"+val).addClass("active");


            }



</script>

<style type="text/css">
  div#chrting {
    height: 351px;
}
</style>




