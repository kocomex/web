

<div class="inner_top"></div>


<section class="dash_sec">
<div class="container-fluid pl_n pr_n">


<div class="col-md-9 col-sm-10 col-xs-12  pr_n">
<div class="cms_left">

  {!! Form::open(array('id'=>'paypal', 'url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', 'method' => 'POST')) !!}

  	<input type="hidden" name="business" value="<?php echo $paypal['paypalID']; ?>">
  	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="item_name" value="<?php echo $paypal['type']; ?>">
	<input type="hidden" name="item_number" value="<?php echo $paypal['user_id']; ?>">
  <input type="hidden" name="custom" value="<?php echo $paypal['secret_id']; ?>">
	<input type="hidden" name="amount" value="<?php echo $paypal['price']; ?>">
	<input type="hidden" name="currency_code" value="<?php echo "USD"; ?>">
  <input type='hidden' name='cancel_return' value="{{ URL::to('paypalCancel/'.$paypal['secret_id'].'/'.$paypal['type']) }}">
  <input type='hidden' name='return' value="{{ URL::to('paypalSuccess') }}">
    
    <input type="image" name="submit" border="0"
    src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">
    <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript">$('#paypal').submit();</script>
{!! Form::close() !!}
</div>
</div>


<div class="clearfix mar-bot-30"></div>

</div>

<div class="dash_footer">
©  2017 All Rights Reserved.
</div>

</section>

