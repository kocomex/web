@include('layouts.front_header')
<?php $country =  Helper::get_country();  
?>


<!-- Banner section starts here -->
<section class="alBannerSec">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="innerMidSec">
          <ul class="nav profile_full justify-content-between">
            <li class="nav-item">
              <a class="nav-link pro_active set_tab1 active" href="javascript:profile_click('tab1')">
                <span class="fa fa-user"></span>Personal Information</a>
            </li>
            <li class="nav-item">
              <a class="nav-link pro_active set_tab2" href="javascript:profile_click('tab2')">
                <span class="fa fa-file-text-o"></span>Verification</a>
            </li>
            <li class="nav-item">
              <a class="nav-link pro_active set_tab3" href="javascript:profile_click('tab3')">
                <span class="fa fa-qrcode"></span>Two Factor Authentication</a>
            </li>
            <!--Mobile verification start here-->
            <li class="nav-item">
              <a class="nav-link pro_active set_tab6" href="javascript:profile_click('tab6')">
                <span class="fa fa-file-text-o"></span>Mobile Verification</a>
            </li>
            <!--Mobile verification end here-->
            <li class="nav-item">
              <a class="nav-link pro_active set_tab4" href="javascript:profile_click('tab4')">
                <span class="fa fa-briefcase"></span>Account Balance</a>
            </li>
            <li class="nav-item">
              <a class="nav-link pro_active set_tab5" href="javascript:profile_click('tab5')">
                <span class="fa fa-cogs"></span>Settings</a>
            </li>
          </ul>
          <div class=" mt-30">

            <div class="tab1 ph-20 hide_profile">
            <div class="row">
                          <div class="col-md-12 inside_tab_list">
                            <ul class="nav justify-content-center">
                              <li class="nav-item">
                                <a class="nav-link pro_active_profile set_profile1 active" href="javascript:bank_det('profile1')" >Personal Information</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link pro_active_profile set_profile2" href="javascript:bank_det('profile2')">Bank Details</a>
                              </li>
                            </ul>
                            <div class="mt-30">
                              <div class="inner_hide profile1">
                                <form class="form-horizontal" name="profile_form" id="profile_form"  enctype="multipart/form-data">

                                  {!! csrf_field() !!}

                                   <div class="form-group row">
                                    <div class="col-md-6">
                               <label>Username :{{$userdetails->device_token}}</label>
                                      
                                    </div>
                                    </div>
                                  <div class="form-group row">
                                    <div class="col-md-6">
                                      <label>First Name *</label>
                                      <input type="text" class="form-control formLg" aria-label="" placeholder="First" name="fname" value="{{$profie->first_name}}">
                                    </div>
                                    <div class="col-md-6">
                                      <label>Last Name *</label>
                                      <input type="text" class="form-control formLg" aria-label="" placeholder="Last name" name="lname" value="{{$profie->last_name}}">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-md-6">
                                      <label>Email Id *</label>
                                      <input type="text" name="email" class="form-control formLg" aria-label="" placeholder="Email Id" readonly="readonly" value="{{$final}}">
                                    </div>
                                    <div class="col-md-6">
                                      <label>Address Line 1 *</label>
                                      <input type="text" class="form-control formLg" aria-label="" placeholder="Address line1" name="address1"  value="{{$profie->address1}}">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-md-6">
                                      <label>Address Line 2 </label>
                                      <input type="text" class="form-control formLg" aria-label="" placeholder="Address line2" name="address2" value="{{$profie->address2}}">
                                    </div>
                                    <div class="col-md-6">
                                      <label>City *</label>
                                      <input type="text" class="form-control formLg" aria-label="" placeholder="City" name="city" value="{{$profie->city}}">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-md-6">
                                      <label>Country *</label>
                                      <select class="custom-select form-control selectLg" name="country">
                                        <?php

                                        foreach($country as $count) 
                                         {?>
                                       <option value="{{$count->name}}">{{$count->name}}</option>
                                       <?php }

                                       ?>
                                     </select>
                                   </div>
                                   <div class="col-md-6">
                                    <label>Postal / Zip Code *</label>
                                    <input type="text" class="form-control formLg" aria-label="" placeholder="Postal / Zip Code" name="postal" value="{{$profie->postal_code}}">
                                  </div>
                                </div>
                                 <div class="form-group row">
                                 <div class="col-md-6">
                                    <label>Profile Pic *</label>
                                    <input type="file" class="form-control formLg" aria-label=""  name="prof_pic" id="prof_pic">
                                  </div>
                                  Notes: Upload only jpeg,jpg,gif,png.
<div class="col-md-6">
                                    <div class="upldImg">
                             <?php $prof_proof    = $profie->prof_image; 

                             if ($prof_proof)

                             {  

                              $profie_proof=$profie->prof_image;

                            }
                            else{
                              $profie_proof="noimage.jpg";
                            }
                            ?>

                            <img src="{{ URL::asset('public/verify/document/'.$profie_proof) }}" alt="" class="img-fluid" height="120" width="150" id="profileprf">
                          </div>
                          </div>
                                  </div>

                                <div class="form-group row mt-30 mb-30">
                                  <div class="col-md-12 text-center">
                                    <!--   <button type="button" class="btn btnSaveSm">Save</button> -->
                                    <button type="submit" class="btn btnSaveSm" id="profilebtn" name="profie_submit">Save</button>
                                  </div>
                                </div>

                                <div id="loader2" style="display:none;margin: 0px 45px; ">

                                  <div class="loadinh_bg">
                                    <div align="center">
                                      <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
                                    </div>
                                  </div>
                                </div>
                                <div id="profile_error" style="color:red;font-size: 100%;text-align:center;"></div>
                              </form>
                            </div>


                   <div class="inner_hide profile2"  style="display: none;">
                      <form class="form-horizontal" id="bank_form">
                        {!! csrf_field() !!}
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label>Bank Name *</label>
                            <input type="text" class="form-control formLg" aria-label="" placeholder="Bank name" name="bankname" value="{{$bank_det->bank_name}}">
                          </div>
                          <div class="col-md-6">
                            <label>Account Holder Name *</label>
                            <input type="text" class="form-control formLg" aria-label="" placeholder="Account holder" name="accholder" value="{{$bank_det->holder_name}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label>Account Number *</label>
                            <input type="text" class="form-control formLg" aria-label="" placeholder="Account number" name="accno" value="{{$bank_det->account_number}}">
                          </div>
                          <div class="col-md-6">
                            <label>Bank City / Branch *</label>
                            <input type="text" class="form-control formLg" aria-label="" placeholder="Bank City / Branch" name="bankcity" value="{{$bank_det->branch}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label>Swift Code *</label>
                            <input type="text" class="form-control formLg" aria-label="" placeholder="Swift Code" name="swiftcode" value="{{$bank_det->iban}}">
                          </div>
                          <div class="col-md-6">
                            <label>Bank Country *</label>
                            <select class="custom-select form-control selectLg" name="bankcountry">
                              <?php

                                foreach($country as $count) 
                         {?>
                       <option value="{{$count->name}}">{{$count->name}}</option>
                       <?php }

                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row mt-30">
                          <div class="col-md-12 text-center">
                            <button type="submit" class="btn btnSaveSm" name="bank_submit">Save</button>
                           
                          </div>
                        </div>

                         <div id="loader3" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            </div>
            <div id="bank_error" style="color:red;font-size: 100%;text-align:center;"></div>
                      </form>
                    </div>


                </div>
              </div>
            </div>             
          </div>
          <div class="tab2 hide_profile" style="display: none;">
            <div class="row">
              <div class="col-md-12">
                <form class="form-horizontal" id="verify_form" enctype="multipart/form-data">
                  {!! csrf_field() !!}
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <h4 class="inTitLg mb-20">Verification</h4>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      {!! $verify_content->page_content !!}
                        </div>
                     
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <h4 class="f-18">ID Proof</h4>

                          <?php if((($profie->photo_id_status == 0 ) && ($profie->photo_id == '') ) || (($profie->photo_id_status == 2)))

                          { ?>
                          <div class="form-group">
                            <label class="w-100">Upload ID Proof *</label>
                            <label class="custom-file">
                              <input type="file" class="custom-file-input" name="id_proof"  id="id_proof" accept="image/*">
                              <span class="custom-file-control">Eg: JPG, PNG, JPEG</span>
                            </label>
                          </div>

                          <?php } ?>
                          <label id="id_proof-error" class="error" for="id_proof" style="display: none;">ID proof required.</label>
                          <div class="form-group">
                            <div class="upldImg">
                             <?php $pan_proof  = $profie->photo_id; 

                             if ($pan_proof)

                             {  

                              $id_proof=$profie->photo_id;

                            }
                            else{
                              $id_proof="noimage.jpg";
                            }
                            ?>

                            <img src="{{ $id_proof }}" alt="" class="img-fluid" height="120" width="150" id="idprf">
                          </div>
                          <span class="verLbl">
                           ID proof status: 


                           <?php


                           if($profie->photo_id_status == '0' && $profie->photo_id != NULL)
                           {
                            echo '<span class="label label-warning">Pending</span>';
                          } 
                          else if($profie->photo_id_status == '1')
                          {
                           echo '<span class="label label-success">Verified</span>'; 
                         }
                          else if($profie->photo_id_status == '0' && $profie->photo_id == NULL)
                          {
                           echo '<span class="label label-info">UnVerified</span>'; 
                         }

                         else {
                          echo '<span class="label label-danger">Rejected</span>';
                        }



                        ?>
                      </span>
                    </div>
                  </div>

                  <?php $address_prf    = $profie->address_prof; 

                  if ($address_prf)

                  {  

                    $add_proof=$profie->address_prof;

                  }
                  else{
                    $add_proof="noimage.jpg";
                  }
                  ?>
                  <div class="col-md-6">
                    <h4 class="f-16">Address Proof</h4>

                    <?php if((($profie->address_prof_status == 0 ) && ($profie->address_prof == '') ) || (($profie->address_prof_status == 2)))

                    { ?>
                    <div class="form-group">
                      <label class="w-100">Upload address Proof *</label>
                      <label class="custom-file">
                        <input type="file" id="add_proof" class="custom-file-input" name="add_proof"  accept="image/*" >
                        <span class="custom-file-control">Eg: JPG, PNG, JPEG</span>
                      </label>
                    </div>

                    <?php } ?>

                    <label id="add_proof-error" class="error" for="add_proof" style="display: none;">Address proof required.</label>
                    <div class="form-group">
                      <div class="upldImg">
                       <img src="{{ $add_proof }}" alt="" class="img-fluid" height="120" width="150" id="addimg">
                     </div>
                     <span class="verLbl">
                       Address proof status:  <?php


                       if($profie->address_prof_status == '0' && $profie->address_prof != NULL)
                       {
                        echo '<span class="label label-warning">Pending</span>';
                      } 
                      else if($profie->address_prof_status == '1')
                      {
                       echo '<span class="label label-success">Verified</span>'; 
                     }
                      else if($profie->address_prof_status == '0' && $profie->address_prof == NULL)
                          {
                           echo '<span class="label label-info">UnVerified</span>'; 
                         }

                     else {
                      echo '<span class="label label-danger">Rejected</span>';
                    }



                    ?>
                  </span>
                </div>
              </div>
            </div>

            <?php if((($profie->address_prof_status == 0 ) && ($profie->address_prof == '') ) || (($profie->address_prof_status == 2))  || (($profie->photo_id_status == 0 ) && ($profie->photo_id == '') ) || (($profie->photo_id_status == 2)))

            { ?>
            <div class="form-group row mt-30 mb-30">
              <div class="col-md-12 text-center">
                <button type="submit" class="btn btnSaveSm" name="verifybtn" id="verifybtn">Save</button>

              </div>
            </div>
            <?php } ?>


            <div id="loader4" style="display:none;margin: 0px 45px; ">

              <div class="loadinh_bg">
                <div align="center">
                  <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
                </div>
              </div>
            </div>
            <div id="verify_error" style="color:red;font-size: 100%;text-align:center;"></div>
          </form>
        </div>
      </div>
    </div>

    <div class="tab3 tfaSec hide_profile" style="display: none;">
      <div class="row">
        <div class="col-md-12">
          <form  class="form-horizontal" id="google_from" method="post" action="" >
           {!! csrf_field() !!}
           <div class="row">
            <div class="col-md-12 text-center">
              <h4 class="inTitLg mb-20">Two Factor Authentication</h4>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <h4 class="f-18 text-uppercase">Enable two factor authentication</h4>
              <p>
                Enter your secret code into the google authentication app on your phone or tablet...
              </p>
              <p class="m-0">Secret Code :</p>
              <div class="qrCodeSec">
                <p>{{$secret_code}}</p>
                <img src="{{$auth_url}}"> TFA Status: <span style="color: orange"><b> {{($userdetails->randcode == 'enable')?'Enable':'Disable'}}</b></span>
              </div>
            </div>
            <div class="col-md-6">
              <h4 class="f-16 mt-30 text-uppercase">Now get the code generated from your app!</h4>
              <P>To active 2-step verification please enter the code generated by yourGoogle Authenticator App...</P>
              <div class="form-group">
                <label>Enter Code</label>
                <input type="text" name="onecode" id="onecode1" class="form-control formLg" aria-label="" placeholder="Enter code">
              </div>  
              <div class="form-group">

                <input type="hidden" name="secret_code" value="{{$secret_code}}" />      
                <input type="hidden" name="auth_url" value="{{$auth_url}}" /> 
              </div>




              <div class="form-group mt-30 mb-30">
                <button type="submit" id="tfabtn" class="btn btnSaveSm">  {{($userdetails->randcode == 'enable')?'Disable':'Enable'}}  {{' '.'security'}}</button>
              </div>

              <div id="loader5" style="display:none;margin: 0px 45px; ">

                <div class="loadinh_bg">
                  <div align="center">
                    <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
                  </div>
                </div>
              </div>
              <div id="tfa_error" style="color:red;font-size: 100%;text-align:center;"></div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="tab4 ph-20 hide_profile" style="display: none;" >
    <div class="row">
      <div class="col-md-12">
        <h4 class="lftTit">Currency Balance</h4>
        <div class="currBlncTbl mt-30 table-responsive">
          <table class="table m-0">
            <thead>
              <th>Coin</th>
              <th>Name</th>
              <th>Balance</th>
              <th>Action</th>
            </thead>
            <tbody>
              <tr>
                <td>BTC</td>
                <td>Bitcoin </td>
                <td>{{$wallet->BTC}} BTC</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/BTC') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/BTC') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
              <tr>
                <td>ETH</td>
                <td>Ethereum</td>
                <td>{{$wallet->ETH}} ETH</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/ETH') }}">Deposit</a>
                  </span>
                  <span>                              <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/ETH') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
              <tr>
                <td>LTC</td>
                <td>Litecoin</td>
                <td>{{$wallet->LTC}} LTC</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/LTC') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/LTC') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
              <tr>
                <td>XRP</td>
                <td>Ripple</td>
                <td>{{$wallet->XRP}} XRP</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/XRP') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/XRP') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
              <tr>
                <td>DOGE</td>
                <td>Dogecoin</td>
                <td>{{$wallet->DOGE}} DOGE</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/DOGE') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/DOGE') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
              <tr>
                <td>ETC</td>
                <td>Ethereum Classic</td>
                <td>{{$wallet->ETC}} ETC</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/ETC') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/ETC') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
              <tr>
                <td>XMR</td>
                <td>Monero</td>
                <td>{{$wallet->XMR}} XMR</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/XMR') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/XMR') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
              <tr>
                <td>IOTA</td>
                <td>IOTA</td>
                <td>{{$wallet->IOTA}} IOTA</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/IOTA') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/IOTA') }}">Withdraw</a>
                  </span>
                </td>
              </tr>

              <tr>
                <td>BTG</td>
                <td>Bitcoin Gold</td>
                <td>{{$wallet->BTG}} BTG</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/BTG') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/BTG') }}">Withdraw</a>                  </span>
                </td>
              </tr>
              <tr>
                <td>DASH</td>
                <td>DASH</td>
                <td>{{$wallet->DASH}} DASH</td>
                <td class="acAction">
                  <span>  <i class="fa fa-download"></i><a href="{{ url('/payment/DASH') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/DASH') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
              <tr>
                <td>KRW</td>
                <td>Korean won</td>
                <td>{{$wallet->KRW}} KRW</td>
                <td class="acAction">
                  <span>
                    <i class="fa fa-download"></i><a href="{{ url('/payment/KRW') }}">Deposit</a>
                  </span>
                  <span>
                    <i class="fa fa-upload"></i><a href="{{ url('/userwithdraw/KRW') }}">Withdraw</a>
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


  <div class="tab5 hide_profile ph-20" style="display: none;">
    <div class="row">
      <div class="col-md-12 inside_tab_list">
        <ul class="nav justify-content-center">
          <li class="nav-item">
            <a class="nav-link change_pwd_list set_pwd1 active" href="javascript:change_pwd('pwd1')">Change Password</a>
          </li>
          <li class="nav-item">
            <a class="nav-link change_pwd_list set_pwd2" href="javascript:change_pwd('pwd2')">Notification</a>
          </li>
        </ul>
        <div class="mt-30">
                    <div class="ph-20 pwd_hide pwd1">
                     <form method="post" action="" id="changepassword_form">
                      {!! csrf_field() !!}
                      <div class="form-group row">
                        <div class="col-md-6">
                          <label>Old Password *</label>
                          <input name="old_password" id="old_password" value=""  type="password" class="form-control formLg">
                        </div>
                        <div class="col-md-6">
                          <label>New Password *</label>
                          <input name="password" id="password" value="" type="password" class="form-control formLg">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-md-6">
                          <label>Confirm Password *</label>
                          <input name="new_password" id="new_password" value="" type="password" class="form-control formLg">
                        </div>
                        <div class="col-md-6">

                        </div>
                      </div>
                      <div class="form-group row mt-30 mb-30">
                        <div class="col-md-12 text-center">
                          <button type="submit" id="pwdbtn" class="btn btnSaveSm">Change Password</button>

                        </div>
                      </div>
                      <div id="loader6" style="display:none;margin: 0px 45px; ">

                        <div class="loadinh_bg">
                          <div align="center">
                            <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
                          </div>
                        </div>
                      </div>
                      <div id="pwd_error" style="color:red;font-size: 100%;text-align:center;"></div>
                    </form>
                  </div>
          <div  class="ph-20 pwd_hide pwd2" style="display: none;">
           <form role="form" method="post" id="notifyform" action="{{ url('/notification') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}


              <div class="form-group row">
                <div class="col-md-6">
                  <p>1. When login from new device alert</p>
                </div>
                <div class="col-md-4">
               
                @if($get_notify->loginalert == 1)
                 

                   <input type="checkbox" name="login_check" checked data-toggle="toggle" value="1">
                 
                  </label>

                 
                   @else

                        <input type="checkbox"  name="login_check"  data-toggle="toggle" value="1">
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6">
                  <p>2. Send/Receive</p>
                </div>
                <div class="col-md-4">


@if($get_notify->trade == 1)
                   <input type="checkbox" checked data-toggle="toggle"  name="trade_check"  value="1">
                  @else
                     <input type="checkbox"  data-toggle="toggle"  name="trade_check"  value="1">
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6">
                  <p>3. Deposit / Withdraw</p>
                </div>
                <div class="col-md-4">
                @if($get_notify->transaction == 1)
                  <input type="checkbox" checked data-toggle="toggle"  name="trans_check"  value="1">
                  @else
                 <input type="checkbox"  data-toggle="toggle"  name="trans_check"  value="1">
                  @endif
                </div>
              </div>

              <div class="form-group row mt-30">
                <div class="col-md-12 text-center">
                  <button type="submit" class="btn btnSaveSm">Save</button>
                 
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="tab6 hide_profile ph-20" style="display: none;">
    <div class="row">
      <div class="col-md-12 inside_tab_list">
        <ul class="nav justify-content-center">
          <!-- li class="nav-item">
            <a class="nav-link change_pwd_list set_pwd1 active" href="javascript:change_pwd('pwd1')">Change Password</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link change_pwd_list set_pwd2 active">Mobile Verification</a>
          </li>
        </ul>
        <div class="mt-30">
                    
                    <div  class="ph-20 pwd_hide pwd2" >
                    <!-- <form role="form" method="post" id="mobile_verify" action="{{ url('/notification') }}" enctype="multipart/form-data"> -->
                    <form method="post" action="" id="mobile_verify">
                    {!! csrf_field() !!}

                            @if($userdetails->mobile_verify == 1)
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <p>Enter your mobile number :</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p>{{$profie->mobile}}</p>
                                    </div>
                                </div>

                                <div class="form-group row mt-30">
                                    <div class="col-md-12 text-center">
                                        <a class="btn btnSaveSm" >Your mobile number verified</a>
                                    </div>
                                </div>
                            @else
                              <div class="form-group row">
                                    <div class="col-md-6">
                                        <p>Enter your mobile number :</p>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="input-group-addon flag_sel"> <input class="" id="register_countrycode" name="register_countrycode" type="hidden"> </span>
                                        <input name="mobilenumber" id="mobilenumber" value=""  type="text" onkeypress="number_only(event,this.value)" placeholder="+919750344341" class="form-control formLg">
                                        <!-- <button type="button" class="btn btnSaveSm" id="otpbutton">Send OTP</button> -->
                                        <a class="btn btnSaveSm" id="otpbutton">Send OTP</a>
                                        
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <p>Enter your OTP code :</p>
                                    </div>
                                    <div class="col-md-4">
                                        <input name="otp" id="otp" value=""  type="text" class="form-control formLg">
                                    </div>
                                </div>




                              <div class="form-group row mt-30">
                                  <div class="col-md-12 text-center">
                                      <button type="submit" class="btn btnSaveSm" id="butnotp">Submit</button>
                                  </div>
                              </div>
                            @endif


                          <div id="loader_otp" style="display:none;margin: 0px 45px; ">
                              <div class="loadinh_bg">
                                <div align="center">
                                  <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
                                </div>
                              </div>
                          </div>

                          <div id="mobver_error" style="color:red;font-size: 100%;text-align:center;"></div>
                    
                    </form>
                    </div>
        </div>
      </div>
    </div>
  </div>



  
</div>
</div>
</div>
</div>
</div>
</section>
<!-- Banner section end here -->

<script type="text/javascript">


 $('#profile_form').validate({

  rules:{
    fname:{
      required:true,
      // minlength:3,
      // maxlength:15,
      // lettersonly:true,

    },
    lname:{
      required:true,
      // minlength:3,
      // maxlength:15,
      // lettersonly:true,

    },
    
    
    address1:{
      required:true,
    },
    
    city:{
      required:true,
      // lettersonly:true,

    },
    country:{
      required:true,

    },
    postal:{
      required:true,

    },
  },
  submitHandler:function(form,e)
  {


    e.preventDefault();

      var formdata = new FormData(form);
    formdata.append('_token','{{ csrf_token() }}');

    $.ajax({  
      url:"{{ url('/profile') }}",
      data :formdata,
      cache: false,
      processData: false, 
      contentType: false,
      type:'post', 
      dataType : 'json',
      enctype: 'multipart/form-data',
      beforeSend:function(){
        $('#loader2').show();
        $('#profilebtn').hide();
        
      },

      success: function(data){
        $('#loader2').hide();
        $('#profilebtn').show();

        if((data.status))
        {
         location.href="{{ url('/profile') }}";

       }
       else{
        $('#prof_error').css('display', '');
        $('#prof_error').html(data.title);
        $('#prof_error').delay(2000).fadeOut(1400); 


      }



      
    }        
  }); 
  }
})
 


 jQuery.validator.addMethod("lettersonly", function(value, element) 
 {
  return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please"); 
</script>


<script type="text/javascript">


 $('#bank_form').validate({

  rules:{
    bankname:{
      required:true,

      // lettersonly:true,

    },
    accholder:{
      required:true,
      
    },
    
    
    accno:{
      required:true,
      number:true,

    },
    
    bankcity:{
      required:true,

    },
    bankcountry:{
      required:true,

    },
    swiftcode:{
      required:true,

    },

  },
  submitHandler:function(form,e)
  {
    e.preventDefault();

    $.ajax({  
      url:"{{ url('/bank_details') }}",
      data: $('#bank_form').serialize(),
      type:'post', 
      dataType : 'json',
      beforeSend:function(){
        $('#loader3').show();
        $('#bankbtn').hide();
        
      },

      success: function(data){
        $('#loader3').hide();
        $('#profilebtn').show();

        if((data.status))
        {
         location.href="{{ url('/profile') }}";

       }
       else{
        $('#prof_error').css('display', '');
        $('#prof_error').html(data.title);
        $('#prof_error').delay(2000).fadeOut(1400); 


      }



      
    }        
  }); 
  }
})
 


 jQuery.validator.addMethod("lettersonly", function(value, element) 
 {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please"); 
</script>


<script type="text/javascript">


 $('#verify_form').validate({

  rules:{
    add_proof:{
      required:true,


    },
    id_proof:{
      required:true,
      
    },
    
    


  },
  submitHandler:function(form,e)
  {
    e.preventDefault();
    var formdata = new FormData(form);
    formdata.append('_token','{{ csrf_token() }}');

    $.ajax({  
      url:"{{ url('/verification') }}",
      data :formdata,
      cache: false,
      processData: false, 
      contentType: false,
      type:'post', 
      dataType : 'json',
      enctype: 'multipart/form-data',
      beforeSend:function(){
        $('#loader4').show();
        $('#verifybtn').hide();
        
      },

      success: function(data){
        $('#loader4').hide();
        $('#verifybtn').show();

        if((data.status))
        {
         location.href="{{ url('/profile') }}";

       }
       else{
        $('#verify_error').css('display', '');
        $('#verify_error').html(data.title);
        $('#verify_error').delay(2000).fadeOut(1400); 


      }



      
    }        
  }); 
  }
})



 $('#google_from').validate({

  rules:{


    onecode:{
      required:true,
      maxlength:6,
    },



  },
  submitHandler:function(form,e)
  {
    e.preventDefault();

    $.ajax({  
      url:"{{ url('/tfa') }}",
      data: $('#google_from').serialize(),
      type:'post', 
      dataType : 'json',
      beforeSend:function(){
        $('#loader5').show();
        $('#tfabtn').hide();
        
      },

      success: function(data){
        $('#loader5').hide();
        $('#tfabtn').show();


        if((data.status))
        {
         location.href="{{ url('/profile') }}";

       }
       else{
        $('#tfa_error').css('display', '');
        $('#tfa_error').html(data.title);
        $('#tfa_error').delay(2000).fadeOut(1400); 


      }



      
    }        
  }); 
  }
})


 $('#id_proof').change(function () {

   readURL(this);
   var ext = this.value.match(/\.(.+)$/)[1];
   switch (ext) {
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'gif':
    $('#uploadButton').attr('disabled', false);
    break;
    default:
    alert('This is not an allowed file type.');
    this.value = '';
  }
});

 $('#add_proof').change(function () {

   readURL1(this);
   var ext = this.value.match(/\.(.+)$/)[1];
   switch (ext) {
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'gif':
    $('#uploadButton').attr('disabled', false);
    break;
    default:
    alert('This is not an allowed file type.');
    this.value = '';
  }
});


  $('#prof_pic').change(function () {

   readURL3(this);
   var ext = this.value.match(/\.(.+)$/)[1];
   switch (ext) {
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'gif':
    $('#uploadButton').attr('disabled', false);
    break;
    default:
    alert('This is not an allowed file type.');
    this.value = '';
  }
});

 $('#changepassword_form').validate({

  rules:{


    old_password:{
      required:true,

    },
    password:{
      required:true,
      minlength:5,
      maxlength:64,
      noSpace:true,
      one_upper: true,
      one_lower : true,
      one_numeric:true,
      specialcharacter:true,
      one_numeric : true,

    },
    new_password:{
      required:true,
      equalTo: "#password",

    },



  },
  submitHandler:function(form,e)
  {
    e.preventDefault();

    $.ajax({  
      url:"{{ url('/changepassword') }}",
      data: $('#changepassword_form').serialize(),
      type:'post', 
      dataType : 'json',
      beforeSend:function(){
        $('#loader6').show();
        $('#pwdbtn').hide();
        
      },

      success: function(data){
        $('#loader6').hide();
        $('#pwdbtn').show();

        if((data.status))
        {
         location.href="{{ url('/') }}";

       }
       else{
        $('#pwd_error').css('display', '');
        $('#pwd_error').html(data.title);
        $('#pwd_error').delay(2000).fadeOut(1400); 


      }



      
    }        
  }); 
  }
  
});


// mobile verify 

$('#mobile_verify').validate({
  rules:{
    otp:{
      required:true,
    },
  },submitHandler:function(form,e){
    e.preventDefault();
    $.ajax({  
      url:"{{ url('/checkotp') }}",
      data: $('#mobile_verify').serialize(),
      type:'post', 
      dataType : 'json',
      beforeSend:function(){
        $('#loader_otp').show();
        $('#butnotp').hide();
      },
      success: function(data){
        $('#loader_otp').hide();
        $('#butnotp').show();
        // alert(data);
        if((data.status=='success')){
          // alert('in to success');
          location.href="{{ url('/profile') }}";
        }else{
          // alert('in to fail');
          // alert(data.title);
          $('#mobver_error').css('display', '');
          $('#mobver_error').html(data.title);
          $('#mobver_error').delay(2000).fadeOut(1400); 
        }
      }        
    }); 
  }
});

// send otp 
$( "#otpbutton" ).click(function() {
    // alert( "you called 1232" );
    // e.preventDefault();
    $.ajax({  
      url:"{{ url('/sendotp') }}",
      data: $('#mobile_verify').serialize(),
      type:'post', 
      dataType : 'json',
      beforeSend:function(){
        $('#loader_otp').show();
        $('#butnotp').hide();
      },
      success: function(data){
        $('#loader_otp').hide();
        $('#butnotp').show();
        // alert(data);
        if((data.status=='success')){
          // alert('in to success');
          // location.href="{{ url('/profile') }}";
          alert('OTP is sent to your mobile number . Please check your mobile number');
        }else{
          // alert('in to fail');
          // alert(data.title);
          $('#mobver_error').css('display', '');
          $('#mobver_error').html(data.title);
          $('#mobver_error').delay(2000).fadeOut(1400); 
        }
      }        
    }); 
});

 jQuery.validator.addMethod("one_upper", function(value, element) {
  return this.optional( element ) || /[A-Z]+/.test( value );
}, 'Minimum one upper case letter required');
 jQuery.validator.addMethod("one_lower", function(value, element) {
  return this.optional( element ) || /[a-z]+/.test( value );
}, 'Minimum one lower case letter required');

 jQuery.validator.addMethod("one_numeric", function(value, element) {
  return this.optional( element ) || /[0-9]+/.test( value );
}, 'Minimum one numeric required');

 jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "Space not allowed");
 jQuery.validator.addMethod("specialcharacter", function(value, element) 
 {
  return this.optional(element) || /[!@#$%^&*()]+/i.test(value);
}, "Must have at least one symbol"); 
 jQuery.validator.addMethod("lettersonly", function(value, element) 
 {
  return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please"); 
</script>

<script type="text/javascript">
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#idprf').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }
  function readURL3(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#profileprf').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }


  function readURL1(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#addimg').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

</script>
<!-- <script type="text/javascript">
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
        $('#myTab a[href="' + activeTab + '"]').tab('show');
    }
});
</script> -->
<script type="text/javascript">
  function profile_click(val)
            {
              //alert(val);
               $(".hide_profile").hide();
               $("."+val).show();

               // active set
               $(".pro_active").removeClass("active");
               $(".set_"+val).addClass("active");


            }


            function bank_det(val)
            {
              //alert(val);
               $(".inner_hide").hide();
               $("."+val).show();

               // active set
               $(".pro_active_profile").removeClass("active");
               $(".set_"+val).addClass("active");


            }

            function change_pwd(val)
            {
              //alert(val);
               $(".pwd_hide").hide();
               $("."+val).show();

               // active set
               $(".change_pwd_list").removeClass("active");
               $(".set_"+val).addClass("active");


            }



</script>
<script>
  $('#myTab a').click(function (e) {
    e.preventDefault()
    $(this).pill('show')
  })
</script>

<style type="text/css">
  span.label.label-success {

    color: green;
  }

  span.label.label-danger {
    color: red;
  }

  span.label.label-warning {
    color: orange;
  }
  span.label.label-info {
    color: blue;
}

.innerMidSec .profile_full li a {
    background: #f4f2f2;
    border: none;
    font-size: 18px;
    color: #323c46;
    padding: .75rem .75rem;
    position: relative;
}

.innerMidSec .profile_full li a span {
    margin-right: 10px;
}

.innerMidSec .profile_full li a:hover::after, .innerMidSec .profile_full li a.active::after {
    background: #2484c6;
    height: 4px;
    content: "";
    width: 100%;
    bottom: 0;
    position: absolute;
    left: 0;
}
.innerMidSec .profile_full li a:hover, .innerMidSec .profile_full li a.active {
    color: #2484c6;
  }
  .inside_tab_list ul li a:hover, .inside_tab_list ul li a.active {
    color: #2484c6;
}
.inside_tab_list ul li a:hover::after, .inside_tab_list ul li a.active::after {
    background: #2484c6;
    height: 2px;
    content: "";
    width: 100%;
    bottom: 0;
    position: absolute;
    left: 0;
}
.inside_tab_list ul li a {
    background: transparent;
    font-size: 24px;
    padding: .5rem;
}
.inside_tab_list ul li a {
    background: transparent;
    font-size: 24px;
    padding: .5rem;
}
.inside_tab_list ul li a {
    background: #f4f2f2;
    border: none;
    font-size: 18px;
    color: #323c46;
    padding: .75rem .75rem;
    position: relative;
}
</style>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@include('layouts.front_footer')
<script src="{{ URL::asset('theme/front_users/js/flags/intlTelInput.js') }}"></script> 
<script type="text/javascript">
$("#register_countrycode").intlTelInput();

function number_only(event,value) {
    if ((event.which != 8) && (event.which != 0) && (event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
    // if(type == "8") {
    //   digits = 8; 
    // } else {
    //   digits = 2;
    // }
  }
</script>