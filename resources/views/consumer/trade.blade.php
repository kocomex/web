
@include('layouts.header_trade')
<!-- Trade section starts here -->
<section class="tradeWrapper">
  <div class="container-fluid">
    <div class="row no-gutters">
      <div class="col-md-3">
        <div class="tradePair pt-10">
            
                <div class="form-group row m-0 justify-content-around">
                    <div class="col-5">
                        <label class="trdPairLbl">Trading Pair</label>
                    </div>
                    <div class="col-6">
                        <select class="custom-select form-control selectMd" name="cur_pair" onchange="javascript:pair_click(this.value)">
                        @foreach($getpairs as $pair)
                            <option value="{{$pair->cur_display}}">{{$pair->pair_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row m-0">
                    <div class="col-6 pairValClr"><span id="f_curname">BTC </span> Balance:
                        <label class="m-0" id="f_curs"></label>
                        
                    </div>
                    <div class="col-6 pairValClr2"><span id="s_curname">KRW </span> Balance:
                        <label class="m-0" id="s_curs"></label>
                       
                    </div>
                </div>

                <div class="w3-bar w3-black">
  <button class="w3-bar-item w3-button" onclick="openCity('London')">Sell</button>
  <button class="w3-bar-item w3-button" onclick="openCity('Paris')">Buy</button>
  
</div>
<div id="London" class="w3-container city">
               
                <div class="row order_list">
                    <div class="col-12">
                            <ul class="list-inline" id="sell_selection">
              <li data-order="instant" class="list-inline-item set_sell_instant sell_active_link active">
                  <a href="javascript:sell_click('sell_instant',0)">Instant Order </a>
              </li>
              <li data-order="limit" class="list-inline-item set_sell_limit sell_active_link">
                <a href="javascript:sell_click('sell_limit',0)">Limit Order</a>
            </li>
            <li data-order="stop" class="list-inline-item set_sell_stop sell_active_link">
                <a href="javascript:sell_click('sell_stop',0)">Stop Order</a>
            </li>
          </ul>
                         @include('consumer.trade.sellTradeForm')        
                    </div>
                </div>
            
            </div>
            <div id="Paris" class="w3-container city" style="display:none">
             
                <div class="row order_list">
                    <div class="col-12">
                        <ul class="list-inline" id="buy_selection">
                      <li data-order="instant" class="list-inline-item buy_active_link set_buy_instant active">
                          <a href="javascript:buy_click('buy_instant',0)">Instant Order </a>
                      </li>
                      <li data-order="limit" class="list-inline-item buy_active_link set_buy_limit">
                        <a href="javascript:buy_click('buy_limit',0)">Limit Order</a>
                    </li>
                    <li data-order="stop" class="list-inline-item buy_active_link set_buy_stop ">
                        <a href="javascript:buy_click('buy_stop',0)">Stop Order</a>
                    </li>
                  </ul>
                           @include('consumer.trade.buyTradeForm')     
                    </div>
                </div>
           
            </div>
        </div>
        @include('consumer.trade.tradeHistory')
      </div>
      <div class="col-md-6 trdMidSec">
        <div class="trGraph">
            <div class="trdGraphTop">
                <div id="chartdiv" style="height: 400px; min-width: 310px">
            </div>
            
        </div>

        <div>

         <ul class="nav nav-tabs">


<li class="list-inline-item acti_stp_link set_active_ord_2 active">
                    <a href="javascript:clickfn_active_stop('active_ord_2')">Active Orders</a>
                </li>
                <li class="list-inline-item acti_stp_link set_stop_ord_2">
                  <a href="javascript:clickfn_active_stop('stop_ord_2')"> Stop  Order   </a>
              </li>



    
   
  </ul>
  
   <div class="active_ord_2 act_stp_hide">
            <div class="trdTit">
                <h4>Active Trade Order</h4>
            </div>
        @include('consumer.trade.activeOrders')
        </div>
                <div class="stop_ord_2 act_stp_hide" style="display:none;">

         <div class="trdTit">
                <h4>Stop Order</h4>
            </div>
            @include('consumer.trade.stopOrders')
          
            </div>
        </div>
      </div>
      </div>


      <div class="col-md-3">
        <div>
            <div class="trdTit">
                <h4>Buy</h4>
            </div>
            @include('consumer.trade.buyOrder')
        </div>
        
        <div>
            <div class="trdTit">
                <h4>Sell</h4>
                 @include('consumer.trade.sellOrder')
            </div>
 
        </div>

      </div>
      
    </div>
  </div>
</section>

<!-- Ticket alert Modal -->
  <div id="ticketAlert" class="modal fade modalPop" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Cancel Trade Order</h4>
          <button type="button" class="close" data-dismiss="modal"><img src="{{asset('/').('theme/front_users/images/lock-ic.png')}}"></button>
        </div>
        <div class="modal-body">
          <p>Are you sure want to cancel this order?</p>
        </div>
        {!! Form::open(array('id'=>'cancel_order_form')) !!}
        
        
        <div class="modal-footer text-center">
        <input type="hidden" id="tradeid" name="tradeid">
        <input type="hidden" id="type" name="type">
          <button type="submit" id="close_yes" class="btn btnRoundBw">Yes</button>
          <button type="button" id="close_no" class="btn btnRoundBw">No</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@include('consumer.validations.trade_validation')
<!-- Trade section end here -->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script> -->
<!-- <script src="jquery-3.2.1.slim.min.js"></script>-->
 <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="{{ URL::asset('theme/front_users/js/popover.js') }}"></script>  

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="{{ URL::asset('theme/front_users/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script>
(function($){
  $(window).on("load",function(){
    var width = $(window).width();

      if(width>1024){
      $(".tradeHisTblScroll, .buySellTableScroll, .orderTableScroll").mCustomScrollbar({
        scrollButtons:{
        enable:false
        },

        scrollbarPosition: 'inside',
        autoExpandScrollbar:true,
        theme: 'minimal-dark',
        axis:"y",
        setWidth: "auto"
        });

      }else{
      $(".tradeHisTblScroll, .buySellTableScroll, .orderTableScroll").mCustomScrollbar({
      scrollButtons:{
      enable:false
      },

      scrollbarPosition: 'inside',
      autoExpandScrollbar:true,
      theme: 'minimal-dark',
      axis:"x",
      setWidth: "auto"
      });

  }
  });
})(jQuery);

</script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="{{ URL::asset('theme/front_users/js/owl.carousel.js') }}"></script>
<script type="text/javascript">
  var owl = jQuery('.owl-carousel1');
  owl.owlCarousel({
    loop:true,
    autoplay:false,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    responsive: {
  0: {
  items:1,
  nav: true
  },
  600: {
  items: 2,
  nav: true,
  margin: 10
  },
  1150: {
  items: 3,
  nav: true,
  loop: true,
  margin:10
  }
  }
  });
  </script>
  <script>
function openCity(cityName) {
    var i;
    var x = document.getElementsByClassName("city");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    document.getElementById(cityName).style.display = "block";  
}
</script>