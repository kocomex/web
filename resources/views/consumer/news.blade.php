@include('layouts.front_header')

<!-- Banner section starts here -->
<section class="alBannerSec">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="innerMidSec cmsMidSec">
          <h4 class="secTit text-center">News</h4>
            <?php $i = 0;?>
           @if($news)
        @foreach ($news as $key => $value) <?php $i++; ?>
          <div class="newsRow">
            <div class="row">
                <div class="col-md-4">
                    <div class="newsImg">
                        <span>{{$value->last_updated}}</span>
                        <img src="{{ URL::asset('public/verify/news_image/'.$value->news_image) }}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="newsHead">
                        <p>{!! $value->news_title !!}</p>
                    </div>
                    <p class="newsDesc">
                    {!! $value->news_content !!}
                    </p>
                </div>
            </div>
          </div>
     


      @endforeach
            @else
            {{"No News found!!!"}}
            @endif
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner section end here -->


@include('layouts.front_footer')