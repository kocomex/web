@include('layouts.front_header')
<section class="alBannerSec">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="innerMidSec cmsMidSec">
          <h4 class="secTit text-center">Fees</h4>
          <div class="cmsBlock">
            <p>
                {!! $fees->page_content !!}
            </p>
            <div class="row">
                <div class="col-md-5">
                    <div class="feesTable">
                        <table class="table m-0">
                            <thead>
                                <tr>
                                    <th width="50%">Pairs</th>
                                    <th width="50%">% Fee</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($trade_fees as $fees)
                                <tr>
                                    <td>{{$fees->currency_pair}}</td>
                                    <td>{{$fees->fee}} %</td>
                                </tr>
                                @endforeach
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@include('layouts.front_footer')