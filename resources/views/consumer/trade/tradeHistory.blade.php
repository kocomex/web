<div>
            <div class="trdTit">
                <h4>Trade History</h4>
            </div>
            <div class="orders_table currActOrderTbl flip-scroll">
                <table class="table m-0">
                    <thead class="clsBlue">
                        <tr>
                            <th>Type</th>
        <th>Date & time</th>
        <th><span class="table_heading_first_currency">BTC</span> Amount</th>
        <th><span class="table_heading_second_currency">USD</span> Price</th>
        <th>Total</th>
                        </tr>
                    </thead>
                    <tbody class="tradeHisTblScroll trade_hist"  id="histroy_content">
                       
                    </tbody>
                </table>
            </div>
        </div>