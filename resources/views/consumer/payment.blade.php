@include('layouts.front_header')

<!-- Banner section starts here -->
<!-- <section class="alBannerSec" id="paypal_method" >
</section> -->
<section class="alBannerSec" id="bankwire_method" >
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="innerMidSec">
                    <ul class="nav  profile_full justify-content-start" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link pro_active set_tab1 <?php echo (Request::segment(1)=='payment')?"active":""; ?>" href="javascript:profile_click('tab1')">
                                <span class="fa fa-download"></span>Deposit</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pro_active set_tab2 <?php echo (Request::segment(1)=='userwithdraw')?"active":""; ?>" href="javascript:profile_click('tab2')">
                                <span class="fa fa-upload"></span>Withdraw</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pro_active set_tab3" href="javascript:profile_click('tab3')">
                                <span class="fa fa-newspaper-o"></span>History</a>
                        </li>
                    </ul>
                    <div class=" mt-30">
                        <div  class="tab1 ph-20 hide_profile" <?php echo (Request::segment(1)=='payment')?"":"style='display: none'"; ?>   >
                            <div class="row">
                                <div class="col-md-6 inside_tab_list">
                                    <form class="form-horizontal" action="{{ url('/deposit') }}" id="deposit_form">
                                    {!! csrf_field() !!}
                                        <div class="form-group" id="sel_box">
                                            <label>Select Currency</label>
                                            <select class="custom-select form-control selectLg" onchange="depositcur(this.value);" name="dep_cur">
                                              <option value="">Select Currency</option>
                                                @foreach($getpairs as $pairs)
                                                    <option value="{{$pairs->pairs}}" <?php echo ($pairs->pairs==$currency)?'selected="selected"':''; ?>>{{$pairs->pairs}}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                         <div id="loader" style="display:none;margin: 0px 45px;">

                                  <div class="loadinh_bg">
                                    <div align="center">
                                      <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
                                    </div>
                                  </div>
                                </div>


                                   <div class="form-group" id="payment_method" style="display: none;">
                      <label class="form-control-label">Payment method <span class="required">*</span></label>
                      <div class="select_style1">
                      <select class="custom-select form-control selectLg" onchange="paymentoption(this.value);" name="payment_method">


        <option value="">Select method</option>
   
       <option value="paypal">2Checkout</option>
       <option value="bankwire">Bank wire</option>

    
    </select>
                      </div>
                    </div>
                           
                          <div class="form-group" id="checkout_div" style="display: none;">
                            <input id="token" name="token" type="hidden" value="">
                            <input type='hidden' name='currency_code' value='EUR'>
            <div>
                <label>
                    <span>Card Number</span>
                </label>
                <input id="ccNo" name="ccNo" type="text" size="20" value="" autocomplete="off" required />
            </div>
            <div>
                <label>
                    <span>Expiration Date (MM/YYYY)</span>
                </label>
                <input type="text" size="2" id="expMonth" name="expMonth" required />
                <span> / </span>
                <input type="text" size="2" id="expYear" name="expYear" required />
            </div>
            <div>
                <label>
                    <span>CVC</span>
                </label>
                <input id="cvv" size="4" type="text" value="" name="cvv" autocomplete="off" required />
            </div>
                            

                          </div>  

                      <div class="form-group" id="bank_admindiv" style="display: none;">
                      <label class="form-control-label">Admin Bank Name <span class="required">*</span></label>
                      <div class="select_style1">
                      <select name="adminbankdet" class="custom-select form-control selectLg" name="admin_bank" onchange="adminbank(this.value);">
   
       <option value="1"><?php echo $bank1->bank_name1;?></option>
       <option value="2"><?php echo $bank2->bank_name2;?></option>
       <option value="3"><?php echo $bank3->bank_name3;?></option>
    
    </select>
                      </div>
                    </div>
                                        <div class="form-group" id="amt_div" style="display: none;">
                                            <label>Amount *</label>
                                            <input type="text"  id="depamount" name="depamount" class="form-control formLg" aria-label="" placeholder="">
                                        </div>

                                           <div class="form-group" id="li_0_price_div" style="display: none;">
                                            <label>Amount *</label>
                                            <input type="text"  id="li_0_price" name="li_0_price" class="form-control formLg" aria-label="" placeholder="">
                                        </div>
                                                          <div class="col-md-1 hidden-xs-down"></div>
                                <div class="text-center f-16">
                                <!-- Start -->
                                <div id="crypto_div">
                                    


                                    </div>
                                    <!-- End -->


                                   <!--  <p class="mt-10"><a href="#">Get New Address</a></p> -->
                                </div>
                                        <div class="form-group mt-30 mb-30">
                                            <button type="submit" class="btn btnSaveSm" id="depbtn" style="display: none;">Deposit</button>
                                        </div>
                                          <div id="loader1" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            </div>
            <div id="dep_error" style="color:red;font-size: 100%;text-align:center;"></div>

    
                                    </form>
                                </div>

   <div class="col-md-5 col-sm-12 col-xs-12 cls_rsipd50" id="admin_bank" style="display: none;">
                     
                     <div class="cm_panb1">
                     
                     <div class="cm_panbd">
                        Admin Bank Details
                     </div>

                      <div class="fbd1" id="bank1">
  
  <table class="table">
      
<tr>
<td>Bank Name</td>
<td><?php echo $bank1->bank_name1;?></td>
</tr>

<tr>
<td>Account Number</td>
<td><?php echo $bank1->acc_no1;?></td>
</tr>
<tr>
<td>Account Name</td>
<td><?php echo $bank1->acc_name1;?></td>
</tr>

<tr>
<td>Branch</td>
<td><?php echo $bank1->branch1;?></td>
</tr>

<tr>
<td>IFSC/Swift Code</td>
<td><?php echo $bank1->swift1;?></td>
</tr>




  </table>
  
  </div>
  <div class="fbd1" id="bank2" style="display: none">
  
  <table class="table">
      
<tr>
<td>Bank Name</td>
<td><?php echo $bank1->bank_name2;?></td>
</tr>

<tr>
<td>Account Number</td>
<td><?php echo $bank1->acc_no2;?></td>
</tr>
<tr>
<td>Account Name</td>
<td><?php echo $bank1->acc_name2;?></td>
</tr>
<tr>
<td>Branch</td>
<td><?php echo $bank1->branch2;?></td>
</tr>

<tr>
<td>IFSC/Swift Code</td>
<td><?php echo $bank1->swift2;?></td>
</tr>




  </table>
  
  </div>
  
    <div class="fbd1" id="bank3" style="display: none">
  
  <table class="table">
      
<tr>
<td>Bank Name</td>
<td><?php echo $bank1->bank_name3;?></td>
</tr>

<tr>
<td>Account Number</td>
<td><?php echo $bank1->acc_no3;?></td>
</tr>
<tr>
<td>Account Name</td>
<td><?php echo $bank3->acc_name3;?></td>
</tr>
<tr>
<td>Branch</td>
<td><?php echo $bank1->branch3;?></td>
</tr>

<tr>
<td>IFSC/Swift Code</td>
<td><?php echo $bank1->swift3;?></td>
</tr>




  </table>
  
  </div>
                     </div>
                     
                     </div>
                     
                            </div>
                        </div>


                        <div class="tab2 ph-20 hide_profile" <?php echo (Request::segment(1)=='userwithdraw')?"":"style='display: none;'"; ?>> 
                            <div class="row">
                                <div class="col-md-6 inside_tab_list">
                                @if((($getverdoc->photo_id_status == 1) && ($getverdoc->address_prof_status == 1)) && $getbnkdoc->status == 3)
                                    <form class="form-horizontal" id="withdraw_fiat">
                                     {!! csrf_field() !!}
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label>Select Currency</label>
                                                <select class="custom-select form-control selectLg" name="with_curren" onchange="currency_change(this.value)" id="with_curren">
                                                 <option value="">Select Currency</option>
                                                 @foreach($getpairs as $pairs)
                                                    <option value="{{$pairs->pairs}}" <?php echo ($pairs->pairs==$currency)?'selected="selected"':''; ?> >{{$pairs->pairs}}</option>
                                              @endforeach
                                                </select>
                                            </div>
                                            </div>
                                            <div class="form-group row">
                                            <div class="col-md-12" style="display: none;" id="withcrypto_address">
                                                <label><span id='currency_name'></span> Address *</label>
                                                <input type="text" class="form-control formLg" aria-label="" placeholder="Address" name="crypto_add" value="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                         <div class="col-md-12" id="with_method" style="display: none;">
                                                <label>payment method</label>
                                                <select class="custom-select form-control selectLg" name="method" onchange="w_payment(this.value)">
                                               
                                                    <option value="">Select payment method</option>
                                                     <option value="paypal">Paypal</option>
                                                      <option value="bankwire">Bankwire</option>
                                             
                                                </select>
                                            </div>
                                           
           </div>


                                        <div class="form-group row">
                                            <div class="col-md-12" id="withamt_div" style="display: none;">
                                                <label>Amount *</label>
                                                <input type="text" name="w_amount" id="w_amount" class="form-control formLg" aria-label="" placeholder="Withdraw amount" onkeyup="calculate_amount(this.value)">
                                            </div>

                                            </div>
                                            <div class="form-group row">

                                            <div class="col-md-12" style="display: none;" id="with_feesdiv">
                                                <label>Fees (<span id="fee_count"></span>%)</label>
                                                <input type="text" class="form-control formLg" aria-label="" placeholder=""  disabled name="w_fees" id="w_fees">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12"  id="with_receivediv" style="display: none;"> 
                                                <label>Total</label>
                                                <input type="text" class="form-control formLg" aria-label="" placeholder="" name="w_receive" id="w_receive" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row mt-30 mb-30">
                                            <div class="col-md-12">
                                                <button type="submit" name="with_button" id="with_button" class="btn btnSaveSm" style="display: none;">Withdraw</button>
                                            </div>
                                        </div>

                                         <div id="loader_with" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            </div>
            <div id="with_error" style="color:red;font-size: 100%;text-align:center;"></div>
                                    </form>

                                    @else
                                    <div class="alert">
  
  <strong>Please verify your KYC document and bank details .<a href="{{ url('/profile') }}">click here </a> to verify</strong>
</div>

                                    @endif
                                </div>
                                <div class="col-md-5">
                                   <div id="with_userbank" style="display: none;">
          


                             <div class="slect_div">
                              <?php $i=1; ?>
                              @if($bankdetails) 

                                <div class="fbd1" >
  
                                  <table class="table">
                                      
                                <tr>
                                <td>Bank Name</td>
                                <td><?php echo $bankdetails->bank_name;?></td>
                                </tr>

                                <tr>
                                <td>Account holder</td>
                                <td><?php echo $bankdetails->holder_name;?></td>
                                </tr>
                                <tr>
                                <td>Account Name</td>
                                <td><?php echo $bankdetails->account_number;?></td>
                                </tr>
                                <tr>
                                <td>Branch</td>
                                <td><?php echo $bankdetails->branch;?></td>
                                </tr>

                                <tr>
                                <td>IFSC/Swift Code</td>
                                <td><?php echo $bankdetails->iban;?></td>
                                </tr>
                                  <tr>
                                <td>Country</td>
                                <td><?php echo $bankdetails->country;?></td>
                                </tr>




                                  </table>
                                  
                                  </div>
                            
                             </div>
                             <?php $i++; ?>
                             @endif
                           </div> 
                                </div>
                            </div>
                        </div>



                        </div>
                        <div class="tab3 ph-20 hide_profile" style="display: none;">
                            <div class="row">
                                <div class="col-md-12 inside_tab_list">
                                    <ul class="nav justify-content-center" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link  change_pwd_list set_pwd1 active"  href="javascript:change_pwd('pwd1')">Deposit</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link  change_pwd_list set_pwd2 "  href="javascript:change_pwd('pwd2')">Withdraw</a>
                                        </li>
                                      <!--   <li class="nav-item">
                                            <a class="nav-link  change_pwd_list set_pwd3 "  href="javascript:change_pwd('pwd3')">Trade History</a>
                                        </li> -->
                                    </ul>
                                    <div class=" mt-30">
                                        <div class="ph-20 pwd_hide pwd1">
                                            <form class="form-horizontal">
                                                <table id="depHisTbl" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                        <th>S.No</th>
                                                            <th>Transaction id</th>
                                                            <th>Date & Time</th>
                                                            <th>Currency</th>
                                                            <th>Amount</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($deps_details)
                                                    <?php $i=1;?>
                                                    @foreach($deps_details as $deposit)

                                                    <?php
    if($deposit->dep_status == '0')
    {
      $sta=   '<span class="" style="color:orange">Pending</span>';
    }
    else  if($deposit->dep_status == '1')
    {
      $sta= '<span class="" style="color:green">Approved</span>';
    }
    else  if($deposit->dep_status == '2')
    {
      $sta= '<span class="" style="color:red">Rejected</span>';
    }
    else  if($deposit->dep_status == '3')
    {
      $sta= '<span class="" style="color:grey">Cancelled</span>';
    }
    else  if($deposit->dep_status == '4')
    {
      $sta= '<span class=""  style="color:blue">Waiting for admin mail confirmation</span>';
    }
    else  if($deposit->dep_status == '5')
    {
      $sta= '<span class=""  style="color:pink">Completed</span>';
    }
    else{
      $sta='';
    }

                                                     ?>
                                                        <tr>

                                                 
                                                         <td>{{$i++}}</td>
                                                            <td class="clrBlue">{{$deposit->txn_id}}</td>
                                                            <td>{{$deposit->updated_at}}</td>
                                                            <td>{{$deposit->dep_currency}}</td>
                                                            <td>{{$deposit->dep_amount}}</td>
                                                            <td><span class="clrAprv">{!! $sta !!}</span></td>
                                                        </tr>
                                                       
                                                        @endforeach
                                                        @endif
                                                       
                                                    </tbody>
                                                </table>
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tph-20 pwd_hide pwd2" style="display: none;">
                                            <form class="form-horizontal">
                                                <table id="wdwHisTbl" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                           <th>S.No</th>
                                                            <th>Transaction id</th>
                                                            <th>Date & Time</th>
                                                            <th>Currency</th>
                                                             <th>Amount</th>
                                                            <th>Fee</th>
                                                           
                                                            <th>Final amount</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($with_details)
                                                    <?php $i=1;?>
                                                    @foreach($with_details as $withdraw)

                                                    <?php
         if($withdraw->wit_status == '0')
        {
          $sta=   '<span class=""  style="color:orange">Pending</span>';
        }
        else  if($withdraw->wit_status == '1')
        {
          $sta= '<span class=""  style="color:green">Approved</span>';
        }
        else  if($withdraw->wit_status == '2')
        {
          $sta= '<span class=""  style="color:red">User cancelled</span>';
        }
        else  if($withdraw->wit_status == '3')
        {
          $sta= '<span class=""  style="color:grey">waiting</span>';
        }
        else  if($withdraw->wit_status == '4')
        {
          $sta= '<span class=""  style="color:blue">Waiting for admin mail confirmation</span>';
        }
        else  if($withdraw->wit_status == '5')
        {
          $sta= '<span class=""  style="color:red">Admin cancelled</span>';
        }
        else{
          $sta='';
        }
                                                    ?>

                                                     
                                                        <tr>
                                                        
                                                        <td>{{$i++}}</td>
                                                            <td class="clrBlue">{{$withdraw->txn_id}}</td>
                                                            <td>{{$withdraw->with_created_date}}</td>
                                                            <td>{{$withdraw->wit_currency}}</td>
                                                             <td>{{$withdraw->wit_amount}}</td>
                                                            <td>{{$withdraw->wit_fee}}</td>
                                                           
                                                             <td>{{$withdraw->wil_get}}</td>
                                                             
                                                            <td><span class="clrAprv">{!! $sta !!}</span></td>
                                                        </tr>
                                                        @endforeach
                                                      @endif
                                                    </tbody>
                                                </table>
                                            </form>
                                        </div>
                                    <div role="tabpanel" class="ph-20 pwd_hide pwd3" style="display: none">
                                            <form class="form-horizontal">
                                                <table id="tradeHisTbl" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Date & Time</th>
                                                            <th>Pair</th>
                                                            <th>Type</th>
                                                            <th>Price</th>
                                                            <th>Amount</th>
                                                            <th>Total</th>
                                                            <th>Fees</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Nov 7, 2017 05:45:59 pm</td>
                                                            <td>BTC-ETH</td>
                                                            <td>Buy</td>
                                                            <td>100</td>
                                                            <td>2</td>
                                                            <td>200</td>
                                                            <td>0.2</td>
                                                        </tr>
                                                       

                                                    </tbody>
                                                </table>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    .cm_panbd {
    width: 212px;
}
</style>
<script type="text/javascript">

// atm start here 
$( document ).ready(function() {
    var curren = "<?php echo $currency; ?>";
      
      // for deposit section start here
      $.ajax({  
          url:"{{ url('/dep_curren') }}",
          data: { '_token':'{{ csrf_token() }}',cur: curren} ,
          type:'post', 

          beforeSend:function(){
              show_loader();
              $('#sel_box').hide();
          },
          success: function(data){
                hide_loader();
                $('#sel_box').show();
                $("#crypto_div").html(data);
                if(curren != "KRW")
                {
                    $("#bank_admindiv").hide();
                    $("#amt_div").hide();
                    $("#admin_bank").hide();
                    $("#depbtn").hide();
                    $("#payment_method").hide();
                    $("#checkout_div").hide();   
                    $("#li_0_price_div").hide();
                }else{
                    $("#payment_method").show();
                    $("#bank_admindiv").hide();
                    $("#amt_div").hide();
                    $("#admin_bank").hide();
                    $("#checkout_div").hide();   
                    $("#li_0_price_div").hide();
                    $("#depbtn").hide();
                    //$("#admin_bank").show();
                }
          }        
      });
      // for deposit section end here
      
      // for withdraw section start here
            if(curren == "KRW"){
            $("#with_method").show();
            $("#withcrypto_address").hide();
            $("#with_feesdiv").hide();
            $("#with_receivediv").hide();
            $("#with_userbank").hide();
            $("#withamt_div").hide();
            $("#with_button").hide();
            }else if(curren == ""){
            $("#with_method").hide();
            $("#withcrypto_address").hide();
            $("#with_feesdiv").hide();
            $("#with_receivediv").hide();
            $("#with_userbank").hide();
            $("#withamt_div").hide();
            $("#with_button").hide();
            }else{
            $("#with_method").hide();
            $("#withcrypto_address").show();
            $("#with_feesdiv").show();
            $("#with_receivediv").show();
            $("#with_userbank").hide();
            $("#withamt_div").show();
            $("#with_button").show();
            }

            if(curren == "BTC")
            {
                var commission = <?php echo $comm->btc_fee; ?>;
            }
            else if(curren == "LTC")
            {
                var commission = <?php echo $comm->ltc_fee; ?>;
            }
            else if(curren == "ETH")
            {
                var commission = <?php echo $comm->eth_fee; ?>;
            }
              if(curren == "XRP")
            {
                var commission = <?php echo $comm->xrp_fee; ?>;
            }
            else if(curren == "DOGE")
            {
                var commission = <?php echo $comm->doge_fee; ?>;
            }
            else if(curren == "ETC")
            {
                var commission = <?php echo $comm->etc_fee; ?>;
            }
              if(curren == "XMR")
            {
                var commission = <?php echo $comm->xmr_fee; ?>;
            }
            else if(curren == "IOTA")
            {
                var commission = <?php echo $comm->iota_fee; ?>;
            }
            else if(curren == "BTG")
            {
                var commission = <?php echo $comm->btg_fee; ?>;
            }
              if(curren == "DASH")
            {
                var commission = <?php echo $comm->dash_fee; ?>;
            }
            else if(curren == "KRW")
            {
                var commission = <?php echo $comm->krw_fee; ?>;
            }

              $("#fee_count").text(commission);
              $("#w_amount").val('');
              $("#w_receive").val('');
              $("#w_fees").val('');
              $("#currency_name").html('');
              $("#currency_name").html(curren);
              
      // for withdraw section end here 

});
// atm end  here

  
  function adminbank(val)
  {
if(val == '1')
{
$("#bank1").show();
$("#bank2").hide();
$("#bank3").hide();


  }
  else if(val == '2')
{
  $("#bank1").hide();
$("#bank2").show();
$("#bank3").hide();


  }
    else if(val == '3')
{
   $("#bank1").hide();
$("#bank2").hide();
$("#bank3").show();


  }

}
function paymentoption(method)
{

if(method == 'paypal')
{
    $("#payment_method").show();
        $("#bank_admindiv").hide();
        $("#amt_div").hide();
        $("#admin_bank").hide();
        $("#depbtn").show();
        $("#checkout_div").show();
        $("#li_0_price_div").show();
}
else if(method == 'bankwire'){
    $("#payment_method").show();
        $("#bank_admindiv").show();
        $("#amt_div").show();
        $("#admin_bank").show();
        $("#depbtn").show();
        $("#checkout_div").hide();
        $("#li_0_price_div").hide();
}
else{
 $("#payment_method").show();
        $("#bank_admindiv").hide();
        $("#amt_div").hide();
        $("#admin_bank").hide();
        $("#depbtn").hide();
        $("#checkout_div").hide();   
        $("#li_0_price_div").hide();

}
}
function depositcur(curren)
{

      $.ajax({  
      url:"{{ url('/dep_curren') }}",
      data: { '_token':'{{ csrf_token() }}',cur: curren} ,
      type:'post', 
   
      beforeSend:function(){
       show_loader();
 $('#sel_box').hide();
        
      },

      success: function(data){
        hide_loader();
      $('#sel_box').show();

$("#crypto_div").html(data);
        if(curren != "KRW")
        {
        $("#bank_admindiv").hide();
        $("#amt_div").hide();
        $("#admin_bank").hide();
         $("#depbtn").hide();
       $("#payment_method").hide();
        $("#checkout_div").hide();   
        $("#li_0_price_div").hide();

        }
        else{
            $("#payment_method").show();
 $("#bank_admindiv").hide();
        $("#amt_div").hide();
 $("#admin_bank").hide();
  $("#checkout_div").hide();   
        $("#li_0_price_div").hide();
     
       
        $("#depbtn").hide();
        //$("#admin_bank").show();
        }
       


      
    }        
  }); 



}
</script>

<script type="text/javascript">


 $('#deposit_form').validate({

  rules:{
    depamount:{
      required:true,
      number:true,
      min:0.0001,
    

    },
    ccNo:{
      number:true,
      maxlength:20,
      required:true,

    },
     expMonth:{
      number:true,
      maxlength:2,
      required:true,

    },
     expYear:{
      number:true,
      maxlength:4,
      required:true,

    },
     cvv:{
      number:true,
      maxlength:5,
      required:true,

    },
     li_0_price:{
      required:true,
      number:true,
      min:0.0001,
    

    },
    dep_cur:{
      required:true,
     
    },
    
  },
  

   
})
</script>
<script type="text/javascript">
  $( "#withdraw_fiat" ).validate({
    rules: {


      w_amount:{required: true,number: true,maxi: true,w_max:true,w_min:true},
    
     
      

    },
    messages:{


      w_amount :{
        required : 'Amount is required',

      },
       
      


    },submitHandler:function(form,e)
    {

      e.preventDefault();
      //  var formdata = new FormData(form);
      // formdata.append(csrf_token,csrf_hash);

      $.ajax({
        url:"{{ url('/withdraw') }}",
        data:  $( "#withdraw_fiat" ).serialize(),
        type:'post', 
        dataType : 'json',
       
        beforeSend:function(){
          $('#loader_with').show();
          $('#with_button').hide();




        // $('#loading-regcircle-overlay-security').show();
        // $('#load_security').hide();
        //$("#login_form")[0].reset();
    },

    success: function(data){


      if(data.status)
      {
        location.reload();
      }
      else
      {
        $('#with_error').css('display', '');
        $('#with_error').html(data.title);
        $('#with_error').delay(2000).fadeOut(900);
        $('#loader_with').hide();
        $('#with_button').show();

      }

    }        
}); 
    }

  });

</script>
<script type="text/javascript">
  var samp1="",nzd1="",max_w1="",up_bal1=""; 
  jQuery.validator.addMethod("maxi", function (value, element, params) {


    var bal_value1=$('#with_curren').val(); 


    var BTC_balance = '<?php echo $mywallet->BTC; ?>';
    var ETH_balance = '<?php echo $mywallet->ETH; ?>';
    var LTC_balance = '<?php echo $mywallet->LTC; ?>';
     var XRP_balance = '<?php echo $mywallet->XRP; ?>';
    var DOGE_balance = '<?php echo $mywallet->DOGE; ?>';
    var ETC_balance = '<?php echo $mywallet->ETC; ?>';
     var XMR_balance = '<?php echo $mywallet->XMR; ?>';
    var IOTA_balance = '<?php echo $mywallet->IOTA; ?>';
    var BTG_balance = '<?php echo $mywallet->BTG; ?>';
     var DASH_balance = '<?php echo $mywallet->DASH; ?>';
    var KRW_balance = '<?php echo $mywallet->KRW; ?>';
    


//alert(BTC_balance);


if(bal_value1 == 'BTC'){

  up_bal1 = BTC_balance;
}
else if(bal_value1 == 'ETH'){
  up_bal1 = ETH_balance;
}
else if(bal_value1 == 'LTC'){
  up_bal1 = LTC_balance;
}
if(bal_value1 == 'XRP'){

  up_bal1 = XRP_balance;
}
else if(bal_value1 == 'DOGE'){
  up_bal1 = DOGE_balance;
}
else if(bal_value1 == 'ETC'){
  up_bal1 = ETC_balance;
}
if(bal_value1 == 'XMR'){

  up_bal1 = XMR_balance;
}
else if(bal_value1 == 'IOTA'){
  up_bal1 = IOTA_balance;
}
else if(bal_value1 == 'BTG'){
  up_bal1 = BTG_balance;
}
if(bal_value1 == 'DASH'){

  up_bal1 = DASH_balance;
}
else if(bal_value1 == 'KRW'){
  up_bal1 = KRW_balance;
}



 var update = up_bal1;

 if(value)
 {
  return parseFloat(value) <= update;
 }
}, "Withdraw amount exceed your current balance");


  var maxi_w1 ="";
  var nzd_validation1 = function(){ return 'Maximum '+maxi_w1+' '+'should be transfer'; }
  jQuery.validator.addMethod("w_max", function (value, element, params) {


    var bal_value=$('#with_curren').val(); 

    var max_BTC='<?php echo $w_max->btc_wmax; ?>';
    var max_ETH='<?php echo $w_max->eth_wmax; ?>';
    var max_LTC='<?php echo $w_max->ltc_wmax; ?>';
     var max_XRP='<?php echo $w_max->xrp_wmax; ?>';
    var max_DOGE='<?php echo $w_max->doge_wmax; ?>';
    var max_ETC='<?php echo $w_max->etc_wmax; ?>';
     var max_XMR='<?php echo $w_max->xmr_wmax; ?>';
    var max_IOTA='<?php echo $w_max->iota_wmax; ?>';
    var max_BTG='<?php echo $w_max->btg_wmax; ?>';
     var max_DASH='<?php echo $w_max->dash_wmax; ?>';
    var max_KRW='<?php echo $w_max->krw_wmax; ?>';

    


//alert(bal_value);
// console.log("bal_value");
// console.log(bal_value);

if(bal_value == 'BTC'){

  maxi_w1=max_BTC;

}
else if(bal_value == 'ETH'){
  maxi_w1=max_ETH;

}
else if(bal_value == 'LTC'){
  maxi_w1=max_LTC;

}
if(bal_value == 'XRP'){

  maxi_w1=max_XRP;

}
else if(bal_value == 'DOGE'){
  maxi_w1=max_DOGE;

}
else if(bal_value == 'ETC'){
  maxi_w1=max_ETC;

}

if(bal_value == 'XMR'){

  maxi_w1=max_XMR;

}
else if(bal_value == 'IOTA'){
  maxi_w1=max_IOTA;

}
else if(bal_value == 'BTG'){
  maxi_w1=max_BTG;

}

if(bal_value == 'DASH'){

  maxi_w1=max_DASH;

}
else if(bal_value == 'KRW'){
  maxi_w1=max_KRW;

}



else{

}






if(value)
{
  return parseFloat(value) <= maxi_w1;
}
},nzd_validation1);

  var samp_validation1 = function(){ return 'Minimum '+samp1+' '+'should be transfer'; }

  jQuery.validator.addMethod("w_min", function (value, element, params) {


    var bal_value=$('#with_curren').val(); 


       var min_BTC='<?php echo $w_min->btc_wmin; ?>';
    var min_ETH='<?php echo $w_min->eth_wmin; ?>';
    var min_LTC='<?php echo $w_min->ltc_wmin; ?>';
     var min_XRP='<?php echo $w_min->xrp_wmin; ?>';
    var min_DOGE='<?php echo $w_min->doge_wmin; ?>';
    var min_ETC='<?php echo $w_min->etc_wmin; ?>';
     var min_XMR='<?php echo $w_min->xmr_wmin; ?>';
    var min_IOTA='<?php echo $w_min->iota_wmin; ?>';
    var min_BTG='<?php echo $w_min->btg_wmin; ?>';
     var min_DASH='<?php echo $w_min->dash_wmin; ?>';
    var min_KRW='<?php echo $w_min->krw_wmin; ?>';

    


//alert(bal_value);
// console.log("bal_value");
// console.log(bal_value);

if(bal_value == 'BTC'){

  samp1=min_BTC;

}
else if(bal_value == 'ETH'){
  samp1=min_ETH;

}
else if(bal_value == 'LTC'){
  samp1=min_LTC;

}
if(bal_value == 'XRP'){

  samp1=min_XRP;

}
else if(bal_value == 'DOGE'){
  samp1=min_DOGE;

}
else if(bal_value == 'ETC'){
  samp1=min_ETC;

}

if(bal_value == 'XMR'){

  samp1=min_XMR;

}
else if(bal_value == 'IOTA'){
  samp1=min_IOTA;

}
else if(bal_value == 'BTG'){
  samp1=min_BTG;

}

if(bal_value == 'DASH'){

  samp1=min_DASH;

}
else if(bal_value == 'KRW'){
  samp1=min_KRW;

}


if(value)
{
  return parseFloat(value) >= samp1;
}
},samp_validation1);

</script>

<script type="text/javascript">

function currency_change(curren)
{
if(curren == "KRW")
{
$("#with_method").show();
$("#withcrypto_address").hide();
$("#with_feesdiv").hide();
$("#with_receivediv").hide();
$("#with_userbank").hide();
$("#withamt_div").hide();
$("#with_button").hide();

}
else if(curren == "")
{
$("#with_method").hide();
$("#withcrypto_address").hide();
$("#with_feesdiv").hide();
$("#with_receivediv").hide();
$("#with_userbank").hide();
$("#withamt_div").hide();
$("#with_button").hide();

}
else{
$("#with_method").hide();
$("#withcrypto_address").show();
$("#with_feesdiv").show();
$("#with_receivediv").show();
$("#with_userbank").hide();
$("#withamt_div").show();
$("#with_button").show();
}

     if(curren == "BTC")
      {
 var commission = <?php echo $comm->btc_fee; ?>;
      }
      else if(curren == "LTC")
      {
 var commission = <?php echo $comm->ltc_fee; ?>;
      }
       else if(curren == "ETH")
      {
 var commission = <?php echo $comm->eth_fee; ?>;
      }
            if(curren == "XRP")
      {
 var commission = <?php echo $comm->xrp_fee; ?>;
      }
      else if(curren == "DOGE")
      {
 var commission = <?php echo $comm->doge_fee; ?>;
      }
       else if(curren == "ETC")
      {
 var commission = <?php echo $comm->etc_fee; ?>;
      }
            if(curren == "XMR")
      {
 var commission = <?php echo $comm->xmr_fee; ?>;
      }
      else if(curren == "IOTA")
      {
 var commission = <?php echo $comm->iota_fee; ?>;
      }
       else if(curren == "BTG")
      {
 var commission = <?php echo $comm->btg_fee; ?>;
      }
            if(curren == "DASH")
      {
 var commission = <?php echo $comm->dash_fee; ?>;
      }
      else if(curren == "KRW")
      {
 var commission = <?php echo $comm->krw_fee; ?>;
      }
$("#fee_count").text(commission);
$("#w_amount").val('');
$("#w_receive").val('');
$("#w_fees").val('');
//atm by 
// alert(curren);
$("#currency_name").html('');
$("#currency_name").html(curren);
}
function w_payment(method)
{
  $("#w_amount").val('');
$("#w_receive").val('');
$("#w_fees").val('');

if(method == "paypal")
{
$("#with_method").show();
$("#withcrypto_address").hide();
$("#with_feesdiv").show();
$("#with_receivediv").show();
$("#with_userbank").hide();
$("#withamt_div").show();
$("#with_button").show();
}
else{
$("#with_method").show();
$("#withcrypto_address").hide();
$("#with_feesdiv").show();
$("#with_receivediv").show();
$("#with_userbank").show();
$("#withamt_div").show();
$("#with_button").show();
}
}   
function calculate_amount(amount)
{

    var form_type = $('#with_curren').val();
    

    if(amount.trim() == '' || isNaN(amount)){
      var fee = '';
      var paid_amount = '';           
    }else{ 

      if(form_type == "BTC")
      {
 var commission = <?php echo $comm->btc_fee; ?>;
      }
      else if(form_type == "LTC")
      {
 var commission = <?php echo $comm->ltc_fee; ?>;
      }
       else if(form_type == "ETH")
      {
 var commission = <?php echo $comm->eth_fee; ?>;
      }
            if(form_type == "XRP")
      {
 var commission = <?php echo $comm->xrp_fee; ?>;
      }
      else if(form_type == "DOGE")
      {
 var commission = <?php echo $comm->doge_fee; ?>;
      }
       else if(form_type == "ETC")
      {
 var commission = <?php echo $comm->etc_fee; ?>;
      }
            if(form_type == "XMR")
      {
 var commission = <?php echo $comm->xmr_fee; ?>;
      }
      else if(form_type == "IOTA")
      {
 var commission = <?php echo $comm->iota_fee; ?>;
      }
       else if(form_type == "BTG")
      {
 var commission = <?php echo $comm->btg_fee; ?>;
      }
            if(form_type == "DASH")
      {
 var commission = <?php echo $comm->dash_fee; ?>;
      }
      else if(form_type == "KRW")
      {
 var commission = <?php echo $comm->krw_fee; ?>;
      }
      
var fee = (((amount)*(commission))/100);
      var paid_amount = (amount)-fee;
     

    }

if(form_type == "KRW")
{
 var update_fee = fee.toFixed(2);

    var update_paid_amount = paid_amount.toFixed(2);
}
else{
  var update_fee = fee.toFixed(8);

    var update_paid_amount = paid_amount.toFixed(8);
}
    
$("#fee_count").text(commission);
    $('#w_fees').val(update_fee);
    $('#w_receive').val(update_paid_amount);  
}    


</script>

<script type="text/javascript" src="https://www.2checkout.com/checkout/api/2co.min.js"></script>
        <script>
            // Called when token created successfully.
            var successCallback = function(data) {
                var myForm = document.getElementById('deposit_form');

                // Set the token as the value for the token input
                myForm.token.value = data.response.token.token;

                // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
                myForm.submit();
            };

            // Called when token creation fails.
            var errorCallback = function(data) {
                if (data.errorCode === 200) {
                    tokenRequest();
                } else {
                    
                }
            };

            var tokenRequest = function() {
                // Setup token request arguments
                var args = {
                    sellerId: "901365957",
            publishableKey: "8F7DC1F5-3AB6-4610-82C5-1FB060FBB5E5",
                    ccNo: $("#ccNo").val(),
                    cvv: $("#cvv").val(),
                    expMonth: $("#expMonth").val(),
                    expYear: $("#expYear").val()
                };

                // Make the token request
                TCO.requestToken(successCallback, errorCallback, args);
            };

            $(function() {
                // Pull in the public encryption key for our environment
                TCO.loadPubKey('sandbox');

                $("#deposit_form").submit(function(e) {
                    // Call our token request function
                    tokenRequest();

                    // Prevent form from submitting
                    
                });
            });
        </script>

        <script type="text/javascript">
  function profile_click(val)
            {
              //alert(val);
               $(".hide_profile").hide();
               $("."+val).show();

               // active set
               $(".pro_active").removeClass("active");
               $(".set_"+val).addClass("active");


            }


            function bank_det(val)
            {
              //alert(val);
               $(".inner_hide").hide();
               $("."+val).show();

               // active set
               $(".pro_active_profile").removeClass("active");
               $(".set_"+val).addClass("active");


            }

            function change_pwd(val)
            {
              //alert(val);
               $(".pwd_hide").hide();
               $("."+val).show();

               // active set
               $(".change_pwd_list").removeClass("active");
               $(".set_"+val).addClass("active");


            }



</script>
</script>

<style type="text/css">
.alert {
    padding: 20px;
    background-color: #f44336;
    color: white;
}
  span.label.label-success {

    color: green;
  }

  span.label.label-danger {
    color: red;
  }

  span.label.label-warning {
    color: orange;
  }

.innerMidSec .profile_full li a {
    background: #f4f2f2;
    border: none;
    font-size: 18px;
    color: #323c46;
    padding: .75rem .75rem;
    position: relative;
}

.innerMidSec .profile_full li a span {
    margin-right: 10px;
}

.innerMidSec .profile_full li a:hover::after, .innerMidSec .profile_full li a.active::after {
    background: #2484c6;
    height: 4px;
    content: "";
    width: 100%;
    bottom: 0;
    position: absolute;
    left: 0;
}
.innerMidSec .profile_full li a:hover, .innerMidSec .profile_full li a.active {
    color: #2484c6;
  }
  .inside_tab_list ul li a:hover, .inside_tab_list ul li a.active {
    color: #2484c6;
}
.inside_tab_list ul li a:hover::after, .inside_tab_list ul li a.active::after {
    background: #2484c6;
    height: 2px;
    content: "";
    width: 100%;
    bottom: 0;
    position: absolute;
    left: 0;
}
.inside_tab_list ul li a {
    background: transparent;
    font-size: 24px;
    padding: .5rem;
}
.inside_tab_list ul li a {
    background: transparent;
    font-size: 24px;
    padding: .5rem;
}
.inside_tab_list ul li a {
    background: #f4f2f2;
    border: none;
    font-size: 18px;
    color: #323c46;
    padding: .75rem .75rem;
    position: relative;
}
</style>

<!-- Banner section end here -->
@include('layouts.front_footer')