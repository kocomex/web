@include('layouts.front_header')
<section class="bannerSec fgtPwdBg">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-4">
        <div class="loginForm">
          <h4 class="midInTit mb-20">Reset your password</h4>
         
          <form method="post" class="form-horizontal" action="{{ url('/resetpassword/'.$forgetcode) }}" id="resetpassword_form">
    {!! csrf_field() !!}
               
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  <img src="{{ URL::asset('theme/front_users/images/lock-ic.png') }}">
                </span>
                <input type="password"   id="res_pwd" name="res_pwd" class="form-control" aria-label="" placeholder="New password">
              </div>
            </div>
<label id="res_pwd-error" class="error" for="res_pwd" style="display: none;">This field is required.</label>
               <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon">
                  <img src="{{ URL::asset('theme/front_users/images/lock-ic.png') }}">
                </span>
                <input type="password"  id="res_cpwd" name="res_cpwd" class="form-control" aria-label="" placeholder="Confirm password">
              </div>
            </div>
           
           <label id="res_cpwd-error" class="error" for="res_cpwd" style="display: none;">This field is required.</label>
            <div class="form-group mt-20">
              <button type="submit" class="btn btnSignIn btn-block" id="restbtn">Submit</button>
            </div>

                     <div id="loader" style="display:none;margin: 0px 45px;">

                                  <div class="loadinh_bg">
                                    <div align="center">
                                      <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
                                    </div>
                                  </div>
                                </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
 <script type="text/javascript">
      
       $('#resetpassword_form').validate({
    
    rules:{

    
      res_pwd:{
        required:true,
           required:true,
        minlength:8,
        maxlength:64,
        noSpace:true,
        one_upper: true,
      one_lower : true,
      one_numeric:true,
     specialcharacter:true,
      one_numeric : true,
        
      },
      res_cpwd:{
        required:true,
        equalTo: "#res_pwd",
      },
     
       
     
     
    },
  
});
       
         jQuery.validator.addMethod("one_upper", function(value, element) {
return this.optional( element ) || /[A-Z]+/.test( value );
}, 'Minimum one upper case letter required');
jQuery.validator.addMethod("one_lower", function(value, element) {
return this.optional( element ) || /[a-z]+/.test( value );
}, 'Minimum one lower case letter required');

jQuery.validator.addMethod("one_numeric", function(value, element) {
return this.optional( element ) || /[0-9]+/.test( value );
}, 'Minimum one numeric required');

 jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "Space not allowed");
  jQuery.validator.addMethod("specialcharacter", function(value, element) 
 {
  return this.optional(element) || /[!@#$%^&*()]+/i.test(value);
}, "Must have at least one symbol"); 
   jQuery.validator.addMethod("lettersonly", function(value, element) 
 {
  return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please"); 


       $( "#resetpassword_form" ).submit(function( event ) {

  

    if($("#resetpassword_form").valid())
    {
 $('#loader').show();
 $('#restbtn').hide();
}else{
$('#loader').hide();
 $('#restbtn').show();
}

});
    </script>