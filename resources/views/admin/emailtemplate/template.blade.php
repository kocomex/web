
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>WIIX Admin panel | List All templates </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  @include('layouts.styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::asset('theme/datatables/dataTables.bootstrap.css') }}"> 
  
</head>

<body>
  <div class="main-wrapper">
    <div class="app" id="app">
      @include('layouts.header')
      
      
      @include('layouts.sidebar')
      
      <div class="sidebar-overlay" id="sidebar-overlay"></div>
      
      <article class="content dashboard-page">
       <section class="section">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-block">
                <section class="example">
                 
                 <div class="table-flip-scroll">
                  <table id="example1" class="table table-striped table-bordered table-hover flip-content">
                    <thead class="flip-header">
                      <tr>
                        <th class="text-center">S.No</th>
                        <th class="text-center">Template Name</th>
                        <!--	<th class="min-desktop text-center">Status</th> -->
                        <th class="min-desktop text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      @if ($emailTemplates) 
                      <?php $i = 1; ?>
                      @foreach($emailTemplates as $email) 
                      <tr>
                        <td>{{$i}}</td>
                        <td>{{$email->title}}</td>
                        
                        
                        <td>
                          

                          <a href="{{ url('/WiPlytaIIX2/template_edit/' . $email->id) }}"><i class="fa fa-pencil"></i></a> 
                          
                        </td>
                      </tr>
                      <?php $i++; ?>

                      @endforeach					
                      @endif
                    </tbody>
                  </table>
                  
                  
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
  </article>
  @include('layouts.footer')
</div>
</div>

@include('layouts.scripts')

<!-- DataTables -->

<script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
  $(function () {
   $("#example1").DataTable({
     "pagingType": "full_numbers"
   });
 });
</script>

</body>

</html>
