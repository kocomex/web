
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>WIIX Admin panel | Template </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	@include('layouts.styles')
</head>
<body>
	<div class="main-wrapper">
		<div class="app" id="app">
			@include('layouts.header')
			
			
			@include('layouts.sidebar')
			<div class="sidebar-overlay" id="sidebar-overlay"></div>
			<article class="content dashboard-page">
				<section class="section">
					

					<form role="form" id="edit_page" name="edit_page" action="{{ url('/WiPlytaIIX2/template_edit/'.$template->id) }}" method="post">
						
						{!! csrf_field() !!}
						
						
						<div class="form-group ">
							<label for="exampleInputEmail1">Title<span style="color:red">*</span></label>
							<input class="form-control " id="exampleInputEmail1" placeholder="Enter Page name" type="text" name="name" required value="<?php echo $template->title; ?>">
						</div>
						
						<div class="form-group ">
							<label for="exampleInputEmail1">Subject<span style="color:red">*</span></label>
							<input class="form-control " id="exampleInputEmail1" placeholder="Enter Page Heading" type="text" name="subject" required value="<?php echo $template->subject; ?>">
						</div>
						
						
						
						
						<div class="form-group">
							<label for="exampleInputPassword1">Message</label>
							<textarea  id="content" name="content" rows="10" cols="80"><?php echo $template->message; ?></textarea>
						</div>
						
						
						
						
						
						<!-- /.box-body -->
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
					
					
				</section>
			</article>
		</div>
	</div>
	@include('layouts.scripts')
	
	<!-- CK Editor -->
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	<script>
		$(function () {
			
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('content');
    
});
</script>

</body>
</html>


