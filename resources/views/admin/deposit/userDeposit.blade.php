@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewDeposit') }}">Manage Deposit</a></li>
  <li><a href="#">Deposit Order Info</a></li>
</ul>
<div class="inn_content">
  {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'id'=>'sample_form')) !!}
    <div class="cm_head1">
      <h3>Deposit Order Info</h3>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Username</label>
        <label class="form-control">
          <a href="{{ URL::to($redirectUrl.'/userDetail/'.App\Model\User::endecryption(1,$deposit->user_id)) }}"><?php $getProfile = App\Model\User::getProfile(strip_tags($deposit->user_id)); echo $getProfile['user']->consumer_name; ?></a>
        </label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Ip Address</label>
        <label class="form-control"><?php echo strip_tags($deposit->ip_addr); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Amount:</label>
        <label class="form-control"><?php echo strip_tags($deposit->amount)." ".strip_tags($deposit->currency); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Payment Method</label>
        <label class="form-control"><?php echo strip_tags($deposit->payment_method); ?></label>
      </div>
    </div>

    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Reference No:</label>
        <label class="form-control"><?php echo strip_tags($deposit->reference_no); ?></label>
      </div>
      <?php if($deposit->proof != "") { ?>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Proof :</label>
        <div class="portrait">
         <img src="<?php echo strip_tags($deposit->proof); ?>">
       </div>
      </div>
      <?php } ?>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">status</label>
       <label class="form-control"><?php echo ucfirst(strip_tags($deposit->status)); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Deposit Date</label>
        <label class="form-control"><?php echo strip_tags($deposit->created_at); ?></label>
      </div>
    </div>
    <?php if(strip_tags($deposit->payment_method) == "bank") { ?>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Approve Date</label>
       <label class="form-control"><?php echo strip_tags($deposit->approve_date); ?></label>
      </div>
    </div>  <?php } ?>
   

  <?php if($deposit->currency != "USD") { ?>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Deposit Address</label>
        <label class="form-control"><?php echo strip_tags($deposit->address_info); ?></label>
      </div>
    </div>
  <?php } ?>

  {!! Form::close() !!}

  <?php
  if(isset($secure)) {
    if($secure == "confirm") { ?>
    {!! Form::open(array('url'=>$redirectUrl.'/updateConfirm', 'id'=>'confirm_form', 'onsubmit'=>'loader_show()')) !!}
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Enter OTP Code :</label>
        <input class="form-control" type="text" name="confirm_code" id="confirm_code">
      </div>
      <input type="hidden"  name="ucode" id="ucode" value="<?php echo App\Model\User::endecryption(1,strip_tags($deposit->id)); ?>">
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <button type="submit" class="btn cm_blacbtn" id="submit_button">Confirm</button>
         <img src="{{asset('/').('public/admin_assets/images/dot_loader.gif')}}" id="submit_loader" style="display: none;">
      </div>
    </div>
    {!! Form::close() !!}
  <?php } } ?>

  <?php
  if(isset($secure)) {
    if($secure == "cancel") { ?>
    {!! Form::open(array('url'=>'DaHni2elORaexdClchiFFe/updateReject', 'id'=>'confirm_form', 'onsubmit'=>'loader_show()')) !!}
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Reason :</label>
        <textarea name="reason" class="form-control" style="height: 70px;"></textarea>
        <label id="reason-error" class="error" for="reason"></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <input type="hidden"  name="ucode" id="ucode" value="<?php echo App\Model\User::endecryption(1,strip_tags($deposit->id)); ?>">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <button type="submit" class="btn cm_blacbtn" id="submit_button">Reject</button>
        <img src="{{asset('/').('public/admin_assets/images/dot_loader.gif')}}" id="submit_loader" style="display: none;">
      </div>
    </div>
    {!! Form::close() !!}
  <?php } } ?>
</div>

<style type="text/css">
.portrait {
  width: 300px;
  overflow: hidden;
}

.portrait img {
  width: 100%;
  height: 100%;
  max-height: 100%;
}
</style>

<script>
function loader_show() {
  if($('#confirm_form').valid() == true) {
    $('#submit_button').hide();
    $('#submit_loader').show();
  } else {
    $('#submit_loader').hide();
    $('#submit_button').show();
  }
}
//goto
</script>
<?php if(isset($secure)) { if($secure == "confirm") { ?>
<script>
  $('#confirm_form').validate({
    // ignore:"",
    rules:{
      confirm_code:{
        required:true,
        remote:{
            url:"{{ URL::to($redirectUrl.'/checkConfirmCode') }}",
            type: 'GET',
            data: {
              confirm_code : function() {
                return $('#confirm_form #confirm_code').val();
              },
              id : function() {
                return $('#ucode').val();
              }
            },
            dataFilter: function(data) {
              if(data != "true") {
                $('#submit_loader').hide();
                $('#submit_button').show();
                return "false";
              } else {
                return "true";
              }
            }
          }
        }
      }
    },
    messages:{
      confirm_code:{
        required:"Enter confirmation code",
        remote:"Enter correct code",
      }
    }
  })
</script>
<?php } } ?>

<?php if(isset($secure)) { if($secure == "cancel") { ?>
<script>
$('#confirm_form').validate({
  rules:{
    reason:{
      required:true,
    }
  },
  messages:{
    reason:{
      required:"Enter valid reason"
    }
  }
})
</script>
<?php } } ?>

@stop