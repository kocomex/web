@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
	<li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
	<li><a href="{{ URL::to($redirectUrl.'/viewDeposit') }}">Manage Deposits</a></li>
	<li><a href="#">Deposit Lists</a></li>
</ul>
<div class="inn_content">
	<form class="cm_frm1 verti_frm1">
	  <div class="cm_head1">
	    <h3>User Deposit List</h3>
	  </div>
	  <?php if(Session::has('success')) { ?>
	  <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
	  <?php } ?>

	  <?php if(Session::has('error')) { ?>
	  <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
	  <?php } ?>
	  
	  <div class="cm_tablesc1 dep_tablesc mb-20">
	    <div class="dataTables_wrapper form-inline dt-bootstrap">
	      <div class="row">
	        <div class="col-sm-12">
	          <div class="cm_tableh3 table-responsive">
	            <table class="table m-0" id="data_table_">
	              <thead>
	                <tr>
	                  <th>S.No.<span class="fa fa-sort"></span></th>
	                  <th>Deposited On<span class="fa fa-sort"></span></th>
                      <th>User <span class="fa fa-sort"></span></th>
                      <th>Amount<span class="fa fa-sort"></span></th>
                      <th>Currency<span class="fa fa-sort"></span></th>
                      <th>Method<span class="fa fa-sort"></span></th>
                      <th>Reference No<span class="fa fa-sort"></span></th>
                      <th>Status<span class="fa fa-sort"></span></th>
                      <th>Action<span class="fa fa-sort"></span></th>
	                </tr>
	              </thead>
	              <tbody>
	              <?php if($deposit) { $ii = 1; foreach($deposit as $dep) {  
	              	$getProfile = App\Model\User::getProfile(strip_tags($dep->user_id));
	              	$depositId = App\Model\User::endecryption(1,strip_tags($dep->id));
	              	?>
	                <tr>
	                  <td><?php echo $ii; ?></td>
	                  <td><?php echo strip_tags($dep->created_at); ?></td>
                      <td><?php echo $getProfile['user']->consumer_name; ?></td>
                      <td><?php echo strip_tags($dep->amount); ?></td>
                      <td><?php echo strip_tags($dep->currency); ?></td>
                      <td><?php echo ucfirst(strip_tags($dep->payment_method)); ?></td>
                      <td>
                      	<?php if($dep->reference_no != "") { echo strip_tags($dep->reference_no); } elseif($dep->address_info != "") { echo strip_tags($dep->address_info); } else { echo "--"; } ?>

                      </td>
                      <td><span class="clsCtlr <?php if($dep->status == "completed") { echo "clsActive"; } elseif($dep->status == "pending") { echo "clsNotVerify"; } else { echo "clsDeactive"; } ?>"><?php echo ucfirst(strip_tags($dep->status)); ?></span></td>
                      <td style="text-align: center;">
                      	<a href="{{ URL::to($redirectUrl.'/viewUserDeposit/'.$depositId) }}" class="editUser">
				          <span class="glyphicon glyphicon-eye-open" style="color: #4f5259; vertical-align: middle;" title="View"></span>
				        </a>
                      </td>
	                </tr>
	               <?php $ii++; } } ?>
	              </tbody>
	            </table>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</form>
</div>

<script>
$(document).ready(function(){
    $('#data_table_').DataTable();
});
</script>

@stop