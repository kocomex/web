@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="#">Admin Profit</a></li>
  <li><a href="#">Profit List</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>Profit List</h3>
    </div>
    <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php } ?>

    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="mb-20">
        <button type="button" class="btn cm_blacbtn" id="view_admin_profit">View Admin Profit</button>
      </div>
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S-No<span class="fa fa-sort"></span></th>
                    <th>User Name<span class="fa fa-sort"></span></th>
                    <th>Profit amount<span class="fa fa-sort"></span></th>
                    <th>Currency<span class="fa fa-sort"></span></th>
                    <th>Type<span class="fa fa-sort"></span></th>
                    <th>Date<span class="fa fa-sort"></span></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if($result) { $ii=1; foreach($result as $res) {  
                    $getUser = App\Model\User::getProfile($res['user_id']);
                    $username = strip_tags($getUser['user']['consumer_name']);
                    $profitCurrency = strip_tags($res['theftCurrency']);
                  ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo $username ?></td>
                    <td><?php echo number_format(strip_tags($res['theftAmount']),$digits, '.', '') ?></td>
                    <td><?php echo $profitCurrency; ?></td>
                    <td><?php echo strip_tags($res['type']); ?></td>
                    <td><?php echo strip_tags(date('Y-m-d',strtotime($res['created_at'])))." ".strip_tags($res['time']); ?></td>
                  </tr>
                  <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>

<div id="ticketAlert" class="modal fade modalPop" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Admin Profit</h4>
        <button type="button" class="close" data-dismiss="modal"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></button>
      </div>

    </div>
  </div>
</div>
<script>
$(document).ready(function(){
    $('#data_table_').DataTable();
});
$('#view_admin_profit').click(function(){
    $('#ticketAlert').modal('show');
})
</script>

@stop