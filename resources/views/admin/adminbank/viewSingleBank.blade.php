@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewAdminBank') }}">Manage Admin Bank</a></li>
  <li><a href="#">Admin Bank Info</a></li>
</ul>
<div class="inn_content">
  {!! Form::open(array('url' => $redirectUrl.'/updateBank', 'class'=>'cm_frm1 verti_frm1', 'id'=>'bank_form')) !!}
    <div class="cm_head1">
      <h3>Admin Bank Info</h3>
    </div>
    <input type="hidden" name="id" value="<?php if($bank) { echo App\Model\User::endecryption(1,strip_tags($bank->id)); } else { echo ""; }?>" >
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Bank Name :</label>
        <input type="text" class="form-control" name="bank_name" id="bank_name" value="<?php if($bank) { echo strip_tags($bank->bank_name); } else { echo ""; } ?>">  
      </div>
    </div> 
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Account Name :</label>
         <input type="text" class="form-control" name="acc_name" id="acc_name" value="<?php if($bank) { echo strip_tags($bank->acc_name); } else { echo ""; } ?>">
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Account Number:</label>
         <input type="text" class="form-control" name="acc_number" id="acc_number" value="<?php if($bank) { echo strip_tags($bank->acc_number); } else { echo ""; } ?>">
      </div>
    </div> 
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Bank IBAN / SWIFT Code:</label>
         <input type="text" class="form-control" name="bank_code" id="bank_code" value="<?php if($bank) { echo strip_tags($bank->bank_code); } else { echo ""; } ?>">
      </div>
    </div> 

    <ul class="list-inline">
      <li>
        <button type="submit" class="cm_blacbtn1">Submit</button>
      </li>
    </ul>
    {!! Form::close() !!}
</div>

<script>
  jQuery.validator.addMethod("alphanumeric1", function(value, element) {
    return this.optional(element) || /^\w+$/i.test(value);
  }, "Please enter valid code");

  jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9\s]+$/i.test(value);
  }, "Please enter only letters.");


  $('#bank_form').validate({
    rules:{
      acc_name:{
        required:true,
        lettersonly:true
      },
      acc_number:{
        required:true,
        number:true,
        minlength:10,
        maxlength:20,
      },
      bank_code:{
        required:true,
         alphanumeric1:true,
      },
      bank_name:{
        required:true,
        lettersonly:true,
      }
    },
    messages:{
      acc_name:{
        required:"Please enter account name",
      },
      acc_number:{
        required:"Please enter account number",
        number:"Please enter valid account number",
        minlength:"Account number should be 10 digits",
        maxlength:"Account number should not exceed over 20",
      },
      bank_code:{
        required:"Please enter bank IBAN / SWIFT code",
        alphanumeric1:"Please enter valid IBAN / SWIFT code",
      },
      bank_name:{
        required:"Please enter bank  name",
      }
    },
  })
</script>

@stop