@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="#">Manage Admin Bank</a></li>
  <li><a href="#">Admin Bank List</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>Admin Bank List</h3>
    </div>

      <?php if(Session::has('success')) { ?>
        <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong></strong><?php echo Session::get('success'); ?> </div>
        <?php } ?>

        <?php if(Session::has('error')) { ?>
        <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Sorry! </strong><?php echo Session::get('error'); ?> </div>
        <?php } ?>

    
    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S.No.<span class="fa fa-sort"></span></th>
                    <th>Bank Name<span class="fa fa-sort"></span></th>
                    <th>Account Name<span class="fa fa-sort"></span></th>
                    <th>Account Number<span class="fa fa-sort"></span></th>
                    <th>Bank Code<span class="fa fa-sort"></span></th>
                    <th>Action<span class="fa fa-sort"></span></th>
                  </tr>
                </thead>
                <tbody>
                <?php if($banks) { $ii = 1; foreach($banks as $bank) 
                  { $bankId = App\Model\User::endecryption(1,$bank->id); ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo strip_tags($bank->bank_name); ?></td>
                    <td><?php echo strip_tags($bank->acc_name); ?></td>
                    <td><?php echo strip_tags($bank->acc_number); ?></td>
                    <td><?php echo strip_tags($bank->bank_code); ?></td>
                    <td>
                      <a href="{{ URL::to($redirectUrl.'/bankEdit/'.$bankId) }}" class="editUser"><img src="{{ asset('/').('public/admin_assets/images/edit-icon.png') }}" title="Edit"  /></a>
                    </td>
                  </tr>
                  <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>

<script>
$(document).ready(function(){
    $('#data_table_').DataTable();
});
</script>

@stop