@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewContactUs') }}">Manage Contact</a></li>
  <li><a href="#">Contact Us List</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>Contacted User List</h3>
    </div>
    <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php } ?>
    
    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S.No.<span class="fa fa-sort"></span></th>
                    <th>Username<span class="fa fa-sort"></span></th>
                    <th>Email<span class="fa fa-sort"></span></th>
                    <th>View Status</th> 
                    <th>Reply Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php if($contact_list) { $ii = 1; foreach($contact_list as $contact) { 
                  $contactId = App\Model\User::endecryption(1,$contact->id); ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo strip_tags($contact->user_name); ?></td>
                    <td><?php echo strip_tags($contact->email); ?></td>
                    <td><a class="clsCtlr <?php if($contact->read_status == "read") { echo "clsActive"; } else { echo "clsDeactive"; } ?>"><?php echo ucfirst(strip_tags($contact->read_status)); ?></a></td>
                    <td style="text-align: center;">
                      <?php if($contact->status == "0") {?>
                      <a href="{{ URL::to($redirectUrl.'/contactReply/'.$contactId) }}" class="glyphicon glyphicon-share-alt"> </a>
                      <?php } else {?> <i class="glyphicon glyphicon-saved">replied</i><?php  } ?>
                    </td>
                    <td style="text-align: center;">
                      <a href="{{ URL::to($redirectUrl.'/contactReply/'.$contactId) }}" class="editUser">
                      <span class="glyphicon glyphicon-eye-open" style="color: #4f5259; vertical-align: middle;" title="View"></span>
                      </a>
                    </td>
                  </tr>
                <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<script>
$(document).ready(function(){
    $('#data_table_').DataTable();
});
</script>

  @stop