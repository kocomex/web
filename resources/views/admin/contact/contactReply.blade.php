@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewContactUs') }}">Manage contact</a></li>
  <li><a href="#">contact Info</a></li>
</ul>
<div class="inn_content">
  <?php if(Session::has('success')) { ?>
  <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
  <?php } ?>

  <?php if(Session::has('error')) { ?>
  <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
  <?php } ?>

  {!! Form::open(array('url' => $redirectUrl.'/updateContact', 'class'=>'cm_frm1 verti_frm1', 'id'=>'contact_form')) !!}
    <div class="cm_head1">
      <h3>Contact Info</h3>
    </div>
    <input type="hidden" name="id" value="<?php if($contact) { echo App\Model\User::endecryption(1,strip_tags($contact->id)); } else { echo ""; }?>" >

    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">username :</label>
         <label class="form-control"><?php if($contact) { echo strip_tags($contact->user_name); } else { echo ""; } ?></label>
         <input type="hidden" name="username" id="username" value="<?php echo strip_tags($contact->user_name);?>"> 
      </div>
    </div>  
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Email id :</label>
         <label class="form-control"><?php if($contact) { echo strip_tags($contact->email); } else { echo ""; } ?></label>
         <input type="hidden" name="email" id="email" value="<?php echo strip_tags($contact->email);?>"> 
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Phone :</label>
         <label class="form-control"><?php if($contact->phone != "") { echo strip_tags($contact->phone); } else { echo "Nil"; } ?></label>
         <input type="hidden" name="phone" id="phone" value="<?php echo strip_tags($contact->phone);?>"> 
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Subject :</label>
         <label class="form-control"><?php if($contact) { echo strip_tags($contact->subject); } else { echo ""; } ?></label>
      </div>
    </div> 

    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Message :</label>
         <label class="form-control"><?php if($contact) { echo strip_tags($contact->message); } else { echo ""; } ?></label>
         <input type="hidden" name="user_msg" id="user_msg" value="<?php echo strip_tags($contact->message);?>"> 
      </div>
    </div> 

    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Submitted On :</label>
         <label class="form-control"><?php if($contact) { echo strip_tags($contact->created_at); } else { echo ""; } ?></label> 
      </div>
    </div> 

    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Reply Status :</label>
         <label class="form-control"><?php if($contact) { if($contact->status == "unread") { echo "Not Replied"; } else { echo "Replied"; } } ?></label> 
      </div>
    </div> 
    <div class="cm_head1">
      <h3>Reply</h3>
    </div>
    <?php if($contact->status == 0) { ?>
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Reply : </label>
        <textarea class="form-control" id="reply" name="reply" cols="100" rows="5" class="form-control"><?php if($contact) { echo strip_tags($contact->replay); } else { echo ""; } ?></textarea> 
      </div>
    </div>
    <ul class="list-inline">
      <li>
        <button type="submit" class="cm_blacbtn1">Send</button>
      </li>
    </ul>
    <?php } else { ?>
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Reply :</label>
        <label class="form-control"><?php if($contact->reply !="") { echo strip_tags($contact->reply); } else { echo ""; } ?></label> 
      </div>
    </div> 
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Replied Date :</label>
        <label class="form-control"><?php if($contact) { echo strip_tags($contact->reply_date); } else { echo ""; } ?></label> 
      </div>
    </div> 
    <?php } ?>
  {!! Form::close() !!}
</div>

<script>
  $('#contact_form').validate({
    ignore:"",
    rules:{
      reply:{
        required: true
      },
    },
    messages:{
      reply:{
        required:"Enter reply message",
      },
    }
  })
</script>
@stop