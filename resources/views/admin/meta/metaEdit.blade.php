@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewmeta') }}">Manage Meta Tags</a></li>
  <li><a href="#">Meta Tag Info</a></li>
</ul>

<div class="inn_content">
  {!! Form::open(array('url' => $redirectUrl.'/updateMeta', 'class'=>'cm_frm1 verti_frm1', 'id'=>'meta_form')) !!}
    <div class="cm_head1">
      <h3>Meta Tag Info</h3>
    </div>
    <input type="hidden" name="id" value="<?php if($meta) { echo App\Model\User::endecryption(1,strip_tags($meta->id)); } else { echo ""; }?>" >

    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Title :</label>
         <input type="text" class="form-control" name="title" id="title" value="<?php if($meta) { echo strip_tags($meta->title); } else { echo ""; } ?>">
      </div>
    </div> 
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Meta Key words : </label>
       <textarea class="form-control" id="meta_keywords" name="meta_keywords" cols="100" rows="1"><?php if($meta) { echo strip_tags($meta->meta_keywords); } else { echo ""; } ?></textarea> 
      </div>
    </div>

     <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Meta Description : </label>
       <textarea class="form-control" id="meta_description" name="meta_description" cols="100" rows="3"><?php if($meta) { echo strip_tags($meta->meta_description); } else { echo ""; } ?></textarea> 
      </div>
    </div>

    
    <ul class="list-inline">
      <li>
        <button type="submit" class="cm_blacbtn1">Submit</button>
      </li>
    </ul>
  {!! Form::close() !!}
</div>

<script src="{{ asset('/').('public/admin_assets/ckeditor/ckeditor.js') }}"> </script>
<script>
  $('#meta_form').validate({
    ignore:"",
    rules:{
      title:{
        required:true,
      },
      meta_keywords:{
        required: true
      },
      meta_description:{
        required: true
      }
    },
    messages:{
       title:{
        required:"Enter Tilte",
      },
      meta_keywords:{
        required:"Enter Meta Key Word",
      },
      meta_description:{
        required:"Enter Meta Description",
      }
    }
  })
</script>

@stop