
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>WIIX Admin panel | Dashboard </title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		  @include('layouts.styles')
	</head>
	<body>
		<div class="main-wrapper">
			<div class="app" id="app">
				@include('layouts.header')
               
               
            @include('layouts.sidebar')
				<div class="sidebar-overlay" id="sidebar-overlay"></div>
				<article class="content dashboard-page">
					<section class="section">
						
			
		     <form id="edit_faq" name="edit_faq" action="{{ url('/WiPlytaIIX2/editFaq/'.$faq_data->faq_id) }}" method="POST" novalidate="">

                       {!! csrf_field() !!}
				
		      <div class="form-group ">
		        <label for="exampleInputEmail1">Question<span style="color:red">*</span></label>
		        <input class="form-control " id="exampleInputEmail1" placeholder="Enter question" type="text" name="question" required value="{{$faq_data->faq_question}}" >
		      </div>
                
		      <div class="form-group">
		        <label for="exampleInputPassword1">Answer</label>
			<textarea class=""  id="answer" name="answer" rows="10" cols="80">{{$faq_data->faq_answer}}</textarea>
		      </div>
		      
		         <div class="form-group ">
		        <label for="exampleInputEmail1">Status</label>
		        <select name="status" >
			        <option value="1" <?php echo ($faq_data->status==1)?'selected="selected"':''; ?>>Active</option>
			        <option value="0" <?php echo ($faq_data->status==0)?'selected="selected"':''; ?>>De-active</option>
		        </select>
		       </div>
		      
              
              <!-- /.box-body -->
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
	
	
            </section>
				</article>
			</div>
		</div>
@include('layouts.scripts')
		
		

<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
  $(function () {
	  
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('answer');
  });
</script>

	
	</body>
</html>


