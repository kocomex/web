@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewWithdraw') }}">Manage Withdraw</a></li>
  <li><a href="#">Withdraw Order Info</a></li>
</ul>
<div class="inn_content">
  {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'id'=>'sample_form')) !!}
  
    <div class="cm_head1">
      <h3>Withdraw Order Info</h3>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Username</label>
        <label class="form-control"><a href="{{ URL::to($redirectUrl.'/userDetail/'.App\Model\User::endecryption(1,$withdraw->user_id)) }}"><?php $getProfile = App\Model\User::getProfile(strip_tags($withdraw->user_id)); echo $getProfile['user']->consumer_name; ?></a>
        </label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Amount:</label>
        <label class="form-control"><?php echo strip_tags($withdraw->amount)." ".strip_tags($withdraw->currency); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Fee Amount:</label>
        <label class="form-control"><?php echo strip_tags($withdraw->fees_amt)." ".strip_tags($withdraw->currency); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Total</label>
        <label class="form-control"><?php echo strip_tags($withdraw->total)." ".strip_tags($withdraw->currency); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Payment Method</label>
        <label class="form-control"><?php echo strip_tags($withdraw->payment_method); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Reference No:</label>
        <label class="form-control"><?php echo strip_tags($withdraw->reference_no); ?></label>
      </div>
    </div>
    
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">status</label>
       <label class="form-control"><?php echo ucfirst(strip_tags($withdraw->status)); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Request Date</label>
        <label class="form-control"><?php echo strip_tags($withdraw->created_at); ?></label>
      </div>
    </div>
    <?php if($withdraw->status == "completed") { ?>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Approve On</label>
       <label class="form-control"><?php echo strip_tags($withdraw->approve_date); ?></label>
      </div>
    </div>  <?php } ?>
   

  <?php if($withdraw->payment_method == "bank") { 
    $bank = json_decode($withdraw->withdraw_bank_info);
  ?>
    <h4>Withdraw Bank Info</h4>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Bank Name</label>
       <label class="form-control"><?php echo strip_tags($bank->bank_name); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Account Name</label>
        <label class="form-control"><?php echo strip_tags($bank->acc_name); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Account Number</label>
       <label class="form-control"><?php echo strip_tags($bank->acc_number); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">IBAN /SWIFT Code</label>
        <label class="form-control"><?php echo strip_tags($bank->bank_code); ?></label>
      </div>
    </div>

  <?php } elseif($withdraw->currency != "USD") { ?>
  <h4>Withdraw Address</h4>
   <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Withdraw Address</label>
       <label class="form-control"><?php echo strip_tags($withdraw->address_info); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Description</label>
       <label class="form-control"><?php echo strip_tags($withdraw->description); ?></label>
      </div>
    </div>
  <?php } ?>

  {!! Form::close() !!}

  <?php
    if(isset($secure)) {
   if($secure == "confirm") { ?>

  {!! Form::open(array('url'=>$redirectUrl.'/updateConfirmWithdraw', 'id'=>'confirm_form', 'onsubmit'=>'loader_show()')) !!}
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Enter OTP Code :</label>
        <input class="form-control" type="text" name="confirm_code" id="confirm_code">
      </div>
      <input type="hidden"  name="ucode" id="ucode" value="<?php echo App\Model\User::endecryption(1,strip_tags($withdraw->id)); ?>">
    </div>
    <?php if($withdraw->payment_method == "bank") { ?>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Enter Reference No :</label>
        <input class="form-control" type="text" name="reference_no" id="reference_no">
      </div>
    </div>
    <?php } ?>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <button type="submit" class="btn cm_blacbtn" id="submit_button">Confirm</button>
         <img src="{{asset('/').('public/admin_assets/images/dot_loader.gif')}}" id="submit_loader" style="display: none;">
      </div>
    </div>

    {!! Form::close() !!}
  <?php } } ?>

  <?php
  if(isset($secure)) {
   if($secure == "cancel") { ?>

  {!! Form::open(array('url'=>$redirectUrl.'/updateRejectWithdraw', 'id'=>'confirm_form', 'onsubmit'=>'loader_show()')) !!}
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Reason :</label>
        <textarea name="reason" id="reason" class="form-control" style="height: 70px;"></textarea>
      </div>
    </div>
    <div class="form-group row clearfix">
      <input type="hidden"  name="ucode" id="ucode" value="<?php echo App\Model\User::endecryption(1,strip_tags($withdraw->id)); ?>">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <button type="submit" class="btn cm_blacbtn" id="submit_button">Reject</button>
        <img src="{{asset('/').('public/admin_assets/images/dot_loader.gif')}}" id="submit_loader" style="display: none;">
      </div>
    </div>
    {!! Form::close() !!}
  <?php } } ?>
</div>

<script>
function hide_show(id) {
  $('#reject_div_'+id).show();
  $('#reject_button_'+id).hide();
  $('#reject_submit_'+id).show();
}

function update_status(status,id) {
  $('#'+id+'_status').val(status);
}

function loader_show() {
  if($('#confirm_form').valid() == true) {
    $('#submit_button').hide();
    $('#submit_loader').show();
  } else {
    $('#submit_button').show();
    $('#submit_loader').hide();
  }
}
</script>

<?php if(isset($secure)) { if($secure == "confirm") { ?>
<script>
  jQuery.validator.addMethod("alphanumeric", function(value, element) {
      return this.optional(element) || /^\w+$/i.test(value);
  }, "Please enter valid reference number");

  $('#confirm_form').validate({
    rules:{
      confirm_code:{
        required:true,
        remote:{
          url:"{{ URL::to($redirectUrl.'/checkConfirmCodeWithdraw') }}",
          type:'GET',
          data: {
            confirm_code : function() {
              return $('#confirm_form #confirm_code').val();
            },
            id : function() {
              return $('#ucode').val();
            }
          },
          dataFilter: function(data) {
            if(data != "true") {
              $('#submit_loader').hide();
              $('#submit_button').show();
              return "false";
            } else {
              return "true";
            }
          }
        }
      },
      reference_no: {
        required:true,
        alphanumeric:true
      }
    },
    messages:{
      confirm_code:{
        required:"Enter Confirmation Code",
        remote:"Enter correct code",
      },
      reference_no: {
        required:"Enter reference number"
      }
    }
  })
</script>
<?php } } ?>

<?php if(isset($secure)) { if($secure == "cancel") { ?>
<script>
$('#confirm_form').validate({
  rules:{
    reason:{
      required:true,
    }
  },
  messages:{
    reason:{
      required:"Enter valid reason"
    }
  }
})
</script>
<?php } } ?>

@stop