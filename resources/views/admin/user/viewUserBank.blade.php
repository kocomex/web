@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewUserBank') }}">Manage User Bank</a></li>
  <li><a href="#">User Bank Info</a></li>
</ul>
<div class="inn_content">
  <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo ' '.Session::get('success'); ?> </div>
  <?php } ?>

  <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo ' '.Session::get('error'); ?> </div>
  <?php } ?>

 {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'id'=>'userbank_form')) !!}
    <div class="cm_head1">
      <h3>User Bank Info</h3>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Account holder name</label>
        <label class="form-control"><?php echo strip_tags($bank->acc_name); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Bank Name</label>
        <label class="form-control"><?php echo strip_tags($bank->bank_name); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Account number</label>
         <label class="form-control"><?php echo strip_tags($bank->acc_number); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Bank IBAN / SWIFT Code:</label>
        <label class="form-control"><?php echo strip_tags($bank->bank_code); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Country</label>
        <label class="form-control"><?php echo strip_tags($bank->country); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Status</label>
         <label class="form-control"><?php echo ucfirst(strip_tags($bank->status)); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Created on</label>
       <label class="form-control"><?php echo strip_tags($bank->created_at); ?></label>
      </div>
    </div>

  {!! Form::close() !!}

@stop