@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewuser') }}">Manage Users</a></li>
  <li><a href="#">User Balance List</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>User Balance List</h3>
    </div>

    <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php }
      if(isset($user->wallet)) {
        $wallet = $user->wallet;
      } else {
        $wallet = array();
      }
    ?>
    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S.No.<span class="fa fa-sort"></span></th>
                    <th>User Name<span class="fa fa-sort"></span></th>
                    <?php
                      if(!empty($wallet)) {
                        foreach($wallet as $wal) {
                          for($j=0; $j<count($currency); $j++) { ?>
                    <th>{{$wal[$j]->symbol}}</th>
                    <?php      
                          }
                        }
                      }
                    ?>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($wallet)) { $ii = 1; foreach($wallet as $bal) { 
                    $getUser = App\Model\User::getProfile(strip_tags($bal->user_id));
                    $username = $user->consumer_name;
                  ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo strip_tags($username); ?></td>

                  </tr>
                  <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>

<script>
$(document).ready(function(){
    $('#data_table_').DataTable();
});
</script>

@stop