@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewuser') }}">Manage Users</a></li>
  <li><a href="#">User Info</a></li>
</ul>
<div class="inn_content">
  <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
  <?php } ?>

  <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo ' '.Session::get('error'); ?> </div>
  <?php }
      $kyc = $userinfo->verification;
  ?>

 {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'id'=>'userinfo_form')) !!}
  <!-- <form class="cm_frm1 verti_frm1"> -->
    <div class="cm_head1">
      <h3>User Info</h3>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Username</label>
        <label class="form-control"><?php echo strip_tags($userinfo->consumer_name); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Email</label>
        <label class="form-control"><?php echo App\Model\User::endecryption(2,strip_tags($userinfo->wiix_content)).App\Model\User::endecryption(2,strip_tags($userinfo->unusual_user_key)); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">DOB</label>
         <label class="form-control"><?php echo strip_tags($userinfo->dob); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Mobile number</label>
         <label class="form-control"><?php echo strip_tags($userinfo->phone); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Address </label>
        <label class="form-control"><?php echo strip_tags($userinfo->address); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">City</label>
        <label class="form-control"><?php echo strip_tags($userinfo->city); ?></label>
      </div>
    </div>

    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Country</label>
        <label class="form-control"><?php echo strip_tags($userinfo->country); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Status</label>
         <label class="form-control"><?php echo ucfirst(strip_tags($userinfo->status)); ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">TFA Status</label>
       <label class="form-control"><?php echo strip_tags($userinfo->tfa_status); ?></label>
      </div>
      <?php 
      $v_status = array(''=>'Not Verified','0'=>'Not Verified','1'=>'Pending','2'=>'Rejected','3'=>'Verified'); ?>

      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">KYC Status</label>
        <label class="form-control"><?php echo $v_status[$userinfo->verified_status]; ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">IP Address</label>
        <label class="form-control"><?php echo strip_tags($userinfo->ip_address); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Registered At</label>
        <label class="form-control"><?php echo strip_tags($userinfo->created_at); ?></label>
      </div>
    </div>


  {!! Form::close() !!}
  <!-- </form> -->

  <br><br>
  <?php
    if($kyc->id_status == "") { $kyc->id_status = 0; }
    if($kyc->id_status1 == "") { $kyc->id_status1 = 0; }
    if($kyc->selfie_status == "") { $kyc->selfie_status = 0; }
    if($kyc->bank_status == "") { $kyc->bank_status = 0; }
  ?>

  {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'url' => 'DaHni2elORaexdClchiFFe/verifyUserStatus',  'id'=>'identity_form')) !!}
    <div class="cm_head1">
      <h4 style="font-weight: bold;">User Identity Info</h4>
    </div>

    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">First Name</label>
        <label class="form-control"><?php echo strip_tags($userinfo->first_name); ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Last Name</label>
        <label class="form-control"><?php echo strip_tags($userinfo->last_name); ?></label>
      </div>
    </div>

    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Id Proof Type</label>
        <label class="form-control"><?php echo $kyc->id_type; ?></label>
      </div>

      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Id Proof Number</label>
        <label class="form-control"><?php echo $kyc->id_proof_no; ?></label>
      </div>
    </div>

    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">ID Proof front side</label>
        <div class="portrait">
          <?php if($kyc->id_proof != "") { ?>
          <img src="<?php echo strip_tags($kyc->id_proof); ?>">
          <?php } else { echo "No files uploaded"; } ?>
        </div>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Id Proof front Status</label>
        <label class="form-control"><?php echo $v_status[$kyc->id_status]; ?></label>
      </div>
    </div>
    <input type="hidden" name="status" value="" id="identity_status">
    <input type="hidden" name="user_id" value="<?php echo App\Model\User::endecryption(1,strip_tags($userinfo->id)); ?>">
    <input type="hidden" name="type" value="ID Proof Front side">
    <input type="hidden" name="type1" value="id_status">
    <div class="form-group row clearfix" id="reject_div_id" style="display:none;">
      <div class="col-sm-12 col-xs-12 cls_resp50">
        <label class="form-control-label">Reject Reason : </label>
        <textarea class="form-control" name="reject_reason"></textarea>
      </div>
    </div>

    <?php if($kyc->id_status == "1") { ?>
      <ul class="list-inline">
        <li>
          <button type="submit" id="verify_identity" class="cm_blacbtn1" onclick="update_status('3','identity')">Verify</button>
        </li>
        <li>
          <button type="button" class="cm_blacbtn1" onclick="hide_show('id');" id="reject_button_id">Reject</button>
          <button type="submit" class="cm_blacbtn1" style="display: none;" id="reject_submit_id" onclick="update_status('2','identity')">Reject</button>
        </li> 
      </ul>
    <?php } ?>

    {!! Form::close() !!}

    <br><br>

    {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'url' => 'DaHni2elORaexdClchiFFe/verifyUserStatus',  'id'=>'address_form')) !!}
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">ID Proof Back side</label>
        <div class="portrait">
          <?php if($kyc->id_proof1 != "") { ?>
          <img src="<?php echo strip_tags($kyc->id_proof1); ?>">
          <?php } else { echo "No files uploaded"; } ?>
        </div>
      </div>

      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Id Proof back Status</label>
        <label class="form-control"><?php echo $v_status[$kyc->id_status1]; ?></label>
      </div>
    </div>

    <input type="hidden" name="status" value="" id="address_status">
    <input type="hidden" name="user_id" value="<?php echo App\Model\User::endecryption(1,strip_tags($userinfo->id)); ?>">
    <input type="hidden" name="type" value="ID Proof Back side">
    <input type="hidden" name="type1" value="id_status1">
    <div class="form-group row clearfix" id="reject_div_addr" style="display:none;">
      <div class="col-sm-12 col-xs-12 cls_resp50">
        <label class="form-control-label">Reject Reason : </label>
        <textarea class="form-control" name="reject_reason"></textarea>
      </div>
    </div>

    <?php if($kyc->id_status1 == "1") { ?>
      <ul class="list-inline">
        <li>
          <button type="submit" id="verify_address" class="cm_blacbtn1" onclick="update_status('3','address')">Verify</button>
        </li>
        <li>
          <button type="button" class="cm_blacbtn1" onclick="hide_show('addr');" id="reject_button_addr">Reject</button>
          <button type="submit" class="cm_blacbtn1" style="display: none;" id="reject_submit_addr" onclick="update_status('2','address')">Reject</button>
        </li> 
      </ul>
    <?php } ?>

  {!! Form::close() !!}

  <br><br>

  {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'url' => 'DaHni2elORaexdClchiFFe/verifyUserStatus',  'id'=>'selfie_form')) !!}
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Selfie ID Proof</label>
        <div class="portrait">
          <?php if($kyc->selfie_proof != "") { ?>
          <img src="<?php echo strip_tags($kyc->selfie_proof); ?>">
          <?php } else { echo "No files uploaded"; } ?>
        </div>
      </div>

      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Selfie ID proof Status</label>
        <label class="form-control"><?php echo $v_status[$kyc->selfie_status]; ?></label>
      </div>
    </div>

    <input type="hidden" name="status" value="" id="selfie_status">
    <input type="hidden" name="user_id" value="<?php echo App\Model\User::endecryption(1,strip_tags($userinfo->id)); ?>">
    <input type="hidden" name="type" value="Selfie ID proof">
    <input type="hidden" name="type1" value="selfie_status">
    <div class="form-group row clearfix" id="reject_div_selfie" style="display:none;">
      <div class="col-sm-12 col-xs-12 cls_resp50">
        <label class="form-control-label">Reject Reason : </label>
        <textarea class="form-control" name="reject_reason"></textarea>
      </div>
    </div>

    <?php if($kyc->selfie_status == "1") { ?>
      <ul class="list-inline">
        <li>
          <button type="submit" id="verify_selfie" class="cm_blacbtn1" onclick="update_status('3','selfie')">Verify</button>
        </li>
        <li>
          <button type="button" class="cm_blacbtn1" onclick="hide_show('selfie');" id="reject_button_selfie">Reject</button>
          <button type="submit" class="cm_blacbtn1" style="display: none;" id="reject_submit_selfie" onclick="update_status('2','selfie')">Reject</button>
        </li> 
      </ul>
    <?php } ?>

  {!! Form::close() !!}

  <br><br>

  {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'url' => 'DaHni2elORaexdClchiFFe/verifyUserStatus',  'id'=>'bank_form')) !!}
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Bank Proof</label>
        <div class="portrait">
          <?php if($kyc->bank_proof != "") { ?>
          <img src="<?php echo strip_tags($kyc->bank_proof); ?>">
          <?php } else { echo "No files uploaded"; } ?>
        </div>
      </div>

      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Bank proof Status</label>
        <label class="form-control"><?php echo $v_status[$kyc->bank_status]; ?></label>
      </div>
    </div>

    <input type="hidden" name="status" value="" id="bank_status">
    <input type="hidden" name="user_id" value="<?php echo App\Model\User::endecryption(1,strip_tags($userinfo->id)); ?>">
    <input type="hidden" name="type" value="Bank proof">
    <input type="hidden" name="type1" value="bank_status">
    <div class="form-group row clearfix" id="reject_div_bank" style="display:none;">
      <div class="col-sm-12 col-xs-12 cls_resp50">
        <label class="form-control-label">Reject Reason : </label>
        <textarea class="form-control" name="reject_reason"></textarea>
      </div>
    </div>

    <?php if($kyc->bank_status == "1") { ?>
      <ul class="list-inline">
        <li>
          <button type="submit" id="verify_bank" class="cm_blacbtn1" onclick="update_status('3','bank')">Verify</button>
        </li>
        <li>
          <button type="button" class="cm_blacbtn1" onclick="hide_show('bank');" id="reject_button_bank">Reject</button>
          <button type="submit" class="cm_blacbtn1" style="display: none;" id="reject_submit_bank" onclick="update_status('2','bank')">Reject</button>
        </li> 
      </ul>
    <?php } ?>

  {!! Form::close() !!}

</div>

<style type="text/css">
  .portrait {
  width: 300px;
  overflow: hidden;
}

.portrait img {
  width: 100%;
  height: 100%;
  max-height: 100%;
}
</style>

<script>
function hide_show(id)
{
   $('#reject_div_'+id).show();
   $('#reject_button_'+id).hide();
   $('#reject_submit_'+id).show();
   
}

function update_status(status,id)
{
  $('#'+id+'_status').val(status);
}

$('#verify_identity').click(function(e) {
  $('[name=reject_reason]').val(' ');
  $("#identity_form").submit();
});

$('#identity_form').validate({
  ignore:"",
  rules:{
    reject_reason:{
      required:true
    }
  },
  messages:{
    reject_reason:{
      required:"Please enter reject reason."
    }
  }
});

$('#verify_address').click(function(e) {
  $('[name=reject_reason]').val(' ');
  $("#address_form").submit();
});

$('#address_form').validate({
  ignore:"",
  rules:{
    reject_reason:{
      required:true
    }
  },
  messages:{
    reject_reason:{
      required:"Please enter reject reason."
    }
  }
});

$('#verify_selfie').click(function(e) {
  $('[name=reject_reason]').val(' ');
  $("#selfie_form").submit();
});

$('#selfie_form').validate({
  ignore:"",
  rules:{
    reject_reason:{
      required:true
    }
  },
  messages:{
    reject_reason:{
      required:"Please enter reject reason."
    }
  }
});

$('#verify_bank').click(function(e) {
  $('[name=reject_reason]').val(' ');
  $("#bank_form").submit();
});

$('#bank_form').validate({
  ignore:"",
  rules:{
    reject_reason:{
      required:true
    }
  },
  messages:{
    reject_reason:{
      required:"Please enter reject reason."
    }
  }
});
//goto
</script>

@stop