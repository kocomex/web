<div class="cm_tableh3 table-responsive">
<table class="table m-0" id="data_table_">
  <thead>
    <tr>
      <th>S.No.<span class="fa fa-sort"></span></th>
      <th>Username<span class="fa fa-sort"></span></th>
      <th>Email Address<span class="fa fa-sort"></span></th>
      <th>Registered On<span class="fa fa-sort"></span></th>
      <th>Account Status<span class="fa fa-sort"></span></th>
      <th>TFA Status<span class="fa fa-sort"></span></th>
      <th>KYC Status<span class="fa fa-sort"></span></th>
      
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php 

  $v_status = array('0'=>'Not Verified','1'=>'Pending','2'=>'Rejected','3'=>'Verified');

  if($userlist) { $ii=1; foreach($userlist as $user) { 
    $userId = App\Model\User::endecryption(1,$user->id);?>
    <tr>
      <td><?php echo $ii; ?></td>
      <td><?php echo strip_tags($user->consumer_name); ?></td>
      <td><?php echo App\Model\User::endecryption(2,strip_tags($user->wiix_content)).App\Model\User::endecryption(2,strip_tags($user->unusual_user_key)); ?></td>
      <td><?php echo $user->created_at; ?></td>

      <td><a class="clsCtlr <?php if($user->status == "active") { echo "clsActive"; } else { echo "clsDeactive"; } ?>"><?php echo ucfirst(strip_tags($user->status)); ?></span></td>

      <td><a href="{{ URL::to($redirectUrl.'/userTfaStatus/'.$userId) }}" class="clsCtlr <?php if($user->tfa_status == "enable") { echo "clsActive"; } else { echo "clsDeactive"; } ?>"><?php echo ucfirst(strip_tags($user->tfa_status)); ?></span></td>

      <td><a class="clsCtlr <?php if($user->verified_status == "3") { echo "clsActive"; } else if($user->verified_status == "1" || $user->verified_status == "0") { echo "clsNotVerify"; } else { echo "clsDeactive"; } ?> clsActive"><?php echo $v_status[$user->verified_status]; ?></span></td>
    
      <td>
        <a class="userRemove" href="{{ URL::to($redirectUrl.'/userStatus/'.$userId) }}"><img src="{{asset('/').('public/admin_assets/images/remove-user-icon.png')}}" title="Remove" /></a>

        <a href="{{ URL::to($redirectUrl.'/userDetail/'.$userId) }}" class="editUser">
          <span class="glyphicon glyphicon-eye-open" style="color: #4f5259; vertical-align: middle;" title="View"></span>
        </a>
      </td>
    </tr>
    <?php $ii++; } } else { ?>
    <tr>
      <td colspan="7" style="text-align: center;">
        No Users Found!
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>