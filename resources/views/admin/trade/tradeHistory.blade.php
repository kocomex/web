@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="#">Manage Trade History</a></li>
  <li><a href="#">Trade History</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>Trade History</h3>
    </div>
    <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php } ?>
    
    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S.No<span class="fa fa-sort"></span></th>
                    <th>Registered On<span class="fa fa-sort"></span></th>
                    <th>Pair<span class="fa fa-sort"></span></th>
                    <th>User<span class="fa fa-sort"></span></th>
                    <th>Type<span class="fa fa-sort"></span></th>
                    <th>Price<span class="fa fa-sort"></span></th>
                    <th>Amount<span class="fa fa-sort"></span></th>
                    <th>Fees<span class="fa fa-sort"></span></th>
                    <th>Total<span class="fa fa-sort"></span></th>
                    <th>Status<span class="fa fa-sort"></span></th>
                  </tr>
                </thead>
                <tbody>
                  <?php  if($orders) { $ii=1; foreach($orders as $trade) {  
                    $pair = strip_tags($trade['pair']);
                    $pair1 = explode('/',$pair);
                    $from_symbol = $pair1[0];
                    $to_symbol = $pair1[1];
                    if($to_symbol == "USD") {
                      $digits = 2;
                    } else {
                      $digits = 8;
                    }
                  ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo strip_tags($trade['tradetime']); ?></td>
                    <td><?php echo $pair; ?></td>
                    <td><?php echo strip_tags($trade['user']); ?></td>
                    <td><?php echo strip_tags($trade['tradetype']); ?></td>
                    <td><?php echo number_format((float)strip_tags($trade['tradeprice']), $digits, '.', ''); ?></td>
                    <td><?php echo number_format((float)strip_tags($trade['amount']), 8, '.', ''); ?></td>
                    <td><?php echo number_format((float)strip_tags($trade['fee']), $digits, '.', ''); ?></td>
                    <td><?php echo number_format((float)strip_tags($trade['total']), $digits, '.', ''); ?></td>
                    <td><span class="clsCtlr <?php if($trade['status'] == "cancelled") { echo "clsDeactive"; } else { echo "clsActive"; } ?>" ><?php echo ucfirst(strip_tags($trade['status'])); ?></span></td>
                  </tr>
                <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<script>
$(document).ready(function(){
  $('#data_table_').DataTable();
});
</script>

@stop