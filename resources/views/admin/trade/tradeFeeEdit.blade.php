@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewTradeFee') }}">Manage Trade Fee</a></li>
  <li><a href="#">Fee info</a></li>
</ul>
<div class="inn_content">
  {!! Form::open(array('url' => $redirectUrl.'/updateTradeFee', 'class'=>'cm_frm1 verti_frm1', 'id'=>'fee_form')) !!}
    <div class="cm_head1">
      <h3>Trade Fee Info</h3>
    </div>
    <input type="hidden" name="id" value="<?php if($fees) { echo App\Model\User::endecryption(1,strip_tags($fees->id)); } else { echo ""; }?>" >

    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Currency Pair :</label>
         <label class="form-control"><?php if($fees) { echo strip_tags($fees->currency_pair); } ?></label>
      </div>
    </div> 
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">From :</label>
         <input type="text" class="form-control" name="from_amt" id="from_amt" value="<?php if($fees) { echo strip_tags($fees->from_amt); } else { echo ""; } ?>">
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">To :</label>
         <input type="text" class="form-control" name="to_amt" id="to_amt" value="<?php if($fees) { echo strip_tags($fees->to_amt); } else { echo ""; } ?>">
      </div>
    </div> 
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Fee :</label>
         <input type="text" class="form-control" name="fee" id="fee" value="<?php if($fees) { echo strip_tags($fees->fee); } else { echo ""; } ?>">
      </div>
    </div> 

    <ul class="list-inline">
      <li>
        <button type="submit" class="cm_blacbtn1">Submit</button>
      </li>
    </ul>
    {!! Form::close() !!}

</div>

<script>
  $.validator.addMethod("greaterThan",function (value, element, param) {
    var $otherElement = $(param);
    if(value != '>') {
      return parseInt(value, 10) > parseInt($otherElement.val(), 10);
    } else {
      return true;
    }
  },'Must be Greater Than Min Amount'); 

  $.validator.addMethod("lessThan",function (value, element, param) {
    var $otherElement = $(param);
    if($otherElement.val() != '>') {
      return parseInt(value, 10) < parseInt($otherElement.val(), 10);
    } else {
      return true;
    }
  },'Must be less Than Max Amount'); 

  $('#fee_form').validate({
    rules:{
      from_amt:{
        required:true,
        lessThan:'#to_amt'
      },
      to_amt:{
        required:true,
        greaterThan:'#from_amt',
      },
      fee:{
        required:true,
        number:true,
      }
    },
    messages:{
       from_amt:{
        required:"Enter from amount",
      },
      to_amt:{
        required:"Enter to amount",
      },
      fee:{
        required:"Enter fee",
      }
    }
  })
</script>

@stop