@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewTradePairs') }}">Manage Trade Pairs</a></li>
  <li><a href="#">Pair info</a></li>
</ul>
<div class="inn_content">
  {!! Form::open(array('url' => $redirectUrl.'/updateTradePair', 'class'=>'cm_frm1 verti_frm1', 'id'=>'pair_form')) !!}
    <div class="cm_head1">
      <h3>Trade Pair Info</h3>
    </div>
    <input type="hidden" name="id" value="<?php if($pairs) { echo App\Model\User::endecryption(1,strip_tags($pairs->id)); } else { echo ""; }?>" >

    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Currency Pair :</label>
         <label class="form-control"><?php if($pairs) { echo strip_tags($pairs->pair_name); } ?></label>
      </div>
    </div> 
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Buy Rate (For 1 {{ $pairs->from_symbol }}) :</label>
         <input type="text" class="form-control" name="sell_rate" id="sell_rate" value="<?php if($pairs) { echo strip_tags($pairs->sell_rate); } else { echo ""; } ?>">
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Sell Rate (For 1 {{ $pairs->from_symbol }}) :</label>
         <input type="text" class="form-control" name="buy_rate" id="buy_rate" value="<?php if($pairs) { echo strip_tags($pairs->buy_rate); } else { echo ""; } ?>">
      </div>
    </div>  

    <ul class="list-inline">
      <li>
        <button type="submit" class="cm_blacbtn1">Submit</button>
      </li>
    </ul>
  {!! Form::close() !!}
</div>

<script>

  jQuery.validator.addMethod("fiatCurrency", function(value, element) {
      return this.optional(element) || /^\d{0,50}(\.\d{0,2})?$/i.test(value);
  }, "Only 2 decimals allowed after decimal point.");

  $('#pair_form').validate({
    rules:{
      buy_rate:{
        required:true,
        fiatCurrency:true
      },
      sell_rate:{
        required:true,
        fiatCurrency:true
      }
    },
    messages:{
       buy_rate:{
        required:"Enter from amount",
      },
      sell_rate:{
        required:"Enter to amount",
      }
    }
  })
</script>

@stop