@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="#">Manage Trade Pairs</a></li>
  <li><a href="#">Trade Pairs</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>Trade Pairs</h3>
    </div>
    <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php } ?>
    
    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S.No.<span class="fa fa-sort"></span></th>
                    <th>Pair<span class="fa fa-sort"></span></th>
                    <th>From Currency<span class="fa fa-sort"></span></th>
                    <th>To Currency<span class="fa fa-sort"></span></th>
                    <th>Buy Rate<span class="fa fa-sort"></span></th>
                    <th>Sell Rate<span class="fa fa-sort"></span></th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                <?php  if($pairs) { $ii=1; foreach($pairs as $pair) { 
                $tradeId = App\Model\User::endecryption(1,$pair->id); ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo strip_tags($pair->pair_name); ?></td>
                    <td><?php echo strip_tags($pair->from_symbol); ?></td>
                    <td><?php echo strip_tags($pair->to_symbol); ?></td>
                    <td><?php echo strip_tags($pair->sell_rate); ?></td>
                    <td><?php echo strip_tags($pair->buy_rate); ?></td>
                    <td style="text-align: center;">
                      <a href="{{ URL::to($redirectUrl.'/tradePairEdit/'.$tradeId) }}" class="editUser"><img src="{{asset('/').('public/admin_assets/images/edit-icon.png')}}" title="Edit"  /></a>
                    </td>
                  </tr>
                  <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<script>
$(document).ready(function(){
  $('#data_table_').DataTable();
});
</script>

@stop