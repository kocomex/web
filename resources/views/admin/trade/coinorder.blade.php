<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>WIIX Admin panel | Trade history </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
       @include('layouts.styles')
           <!-- DataTables -->
<link rel="stylesheet" href="{{ URL::asset('theme/datatables/dataTables.bootstrap.css') }}"> 
  
    </head>

    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
		  @include('layouts.header')
               
               
            @include('layouts.sidebar')
            
            
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                
                <article class="content dashboard-page">
                     <section class="section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block">
                                        <section class="example">
					
			          <div class="table-flip-scroll">
                             <table id="example2" class="table table-striped table-bordered table-hover flip-content">
					     <thead class="flip-header">
						<tr>
						<th>S.No</th>
						<th>User email</th>
						         <th> Currency pairs</th>
                          <th>Amount</th>
                          <th>Price</th>
                         
                          <th>Fee</th>
                          <th>Total</th>
                          <th> Type</th>
                          <th>Status</th>
						</tr>
					</thead>
					<tbody>
						
						@if($coinorder)
							<?php $i=1; ?>
							@foreach($coinorder as $det)


                     
					
						<tr>
						        <td><?php echo $i++; ?></td>

	<?php


	 $get_userid=$det->user_id;


		$get_users1=DB::table('wiix_consumers')->where('user_id',$get_userid)->get();
		$get_users=$get_users1[0];

		$email[0] = Helper::encrypt_decrypt("decrypt",$get_users->secret_key);
		$email[1] = Helper::encrypt_decrypt("decrypt",$get_users->display);
		$secure_email=$email[0]."@".$email[1];

		
		?>

						        <td>{{$secure_email}}</td>
						        <td>{{$det->firstCurrency."/".$det->secondCurrency}}</td>
                    <td>{{$det->Amount}}</td>
                     <td>{{$det->Price}}</td>
                    <td>{{$det->Fee}}</td>
                    <td>{{$det->Total}}</td>
                     <td>{{$det->Type}}</td>
                      <td style="color: #52C2B8">{{$det->status}}</td>
                  
					
					</tr>
					@endforeach
					@endif
					</tbody>
				</table>
			
                         
                      </div>

                                            </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>
             @include('layouts.footer')
             </div>
        </div>

     @include('layouts.scripts')
 <script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
 <script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>


 <script type="text/javascript">
  $(function () {
  $("#example2").DataTable({
     "pagingType": "full_numbers"
    });
   });
</script>