
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>WIIX Admin panel | List All users </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	@include('layouts.styles')
	<!-- DataTables -->
	<link rel="stylesheet" href="{{ URL::asset('theme/datatables/dataTables.bootstrap.css') }}">
	
</head>

<body>
	<div class="main-wrapper">
		<div class="app" id="app">
			@include('layouts.header')
			
			@include('layouts.sidebar')
			
			<div class="sidebar-overlay" id="sidebar-overlay"></div>
			
			<article class="content dashboard-page">
				<section class="section">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-block">
									<section class="example">
										<div class="table-flip-scroll">
											
											<table id="example1" class="table table-striped table-bordered table-hover flip-content">
												<thead>
													<tr>
														<th>S.no</th>
														<th>Page name</th>
														<th>Last updated</th>
														<th>Created</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													
													@if($cms_pages)
													<?php $i=0;  ?>
													@foreach($cms_pages as $cms_data)
													
													<tr>
														<td><?php echo ++$i; ?></td>
														<td> {{$cms_data->page_name}}</td>
														<td>{{$cms_data->last_updated}}</td>
														<td>{{$cms_data->created_on}}</td>
														<td><?php echo ($cms_data->status==1)?'active':'de-active'; ?></td>   
														<td>

															<a  href="{{ url('/WiPlytaIIX2/editPage/' . $cms_data->page_id) }}" title="Edit"><i class="fa fa-fw fa-edit"></i></a> 

															<!-- <a  href="{{ url('/WiPlytaIIX2/deletePage/' . $cms_data->page_id) }}" title="View"><i class="fa fa-fw fa-trash" onclick="return confirm('Are you sure?')"></i></a>  -->


															
														</td>
													</tr>
													@endforeach
													@else
													
													<tr>
														<td colspan="4">No Pages Found</td>
													</tr>
													@endif
													
												</tbody>
											</table>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>
				</section>
			</article>
			@include('layouts.footer')
		</div>
	</div>
	
	@include('layouts.scripts')
	
	<!-- DataTables -->
	<script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>

	<script>
		$(function () {
			$("#example1").DataTable({
				"pagingType": "full_numbers"
			});
		});
	</script>
	
</body>

</html>
