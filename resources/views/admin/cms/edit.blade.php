
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>WIIX Admin panel | Dashboard </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	@include('layouts.styles')
</head>
<body>
	<div class="main-wrapper">
		<div class="app" id="app">
			@include('layouts.header')
			
			@include('layouts.sidebar')
			<div class="sidebar-overlay" id="sidebar-overlay"></div>
			<article class="content dashboard-page">
				<section class="section">
					
					
					<form id="edit_page" name="edit_page" action="{{ url('/WiPlytaIIX2/editPage/'.$cms_id) }}" method="POST" novalidate="">

						{!! csrf_field() !!}
						
						
						<div class="form-group ">
							<label for="exampleInputEmail1">CMS Title<span style="color:red">*</span></label>
							<input class="form-control " id="exampleInputEmail1" placeholder="Enter Page name" type="text" name="name" required value="{{$cms_data->page_name}}" readonly="readonly">
						</div>
						
						
						<div class="form-group">
							<label for="exampleInputPassword1">CMS Content</label>
							<textarea  id="content" name="content" rows="10" cols="80"><?php echo $cms_data->page_content; ?></textarea>
						</div>
						
						
						<div class="form-group">
							<label for="exampleInputPassword1">Meta Title</label>
							<input type="text"  id="meta_title" name="meta_title" rows="10" cols="80" value="<?php echo $cms_data->meta_title; ?>">
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1">Meta Keyword</label>
							<textarea  id="meta_keyword" name="meta_keyword" rows="10" cols="80">{{$cms_data->meta_keyword}}</textarea>
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">Meta Description</label>
							<textarea  id="meta_description" name="meta_description" rows="10" cols="80">{{$cms_data->meta_description}}</textarea>
						</div>
						
						
						<!-- /.box-body -->
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
					
					
				</section>
			</article>
		</div>
	</div>
	@include('layouts.scripts')
	
	<!-- CK Editor -->
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	<script>
		$(function () {
			
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('content');
   
    CKEDITOR.replace('meta_keyword');
    CKEDITOR.replace('meta_description');
});
</script>

</body>
</html>


