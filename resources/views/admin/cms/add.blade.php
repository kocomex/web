<?php  defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>WIIX Admin panel | CMS </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<?php $this->load->view('admin/styles'); ?>
</head>
<body>
	<div class="main-wrapper">
		<div class="app" id="app">
			<?php $this->load->view('admin/header'); ?>
			<?php $this->load->view('admin/sidebar'); ?>
			<div class="sidebar-overlay" id="sidebar-overlay"></div>
			<article class="content dashboard-page">
				<section class="section">
					
					
					
					<form role="form" id="add_faq" name="add_page" action="<?php echo base_url().'Ara95chatatal/content/addPage'; ?>" method="post">
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
						
						<div class="form-group ">
							<label for="exampleInputEmail1">Page name<span style="color:red">*</span></label>
							<input class="form-control " id="exampleInputEmail1" placeholder="Enter Page name" type="text" name="name" required>
						</div>
						
						<div class="form-group ">
							<label for="exampleInputEmail1">Page Heading<span style="color:red">*</span></label>
							<input class="form-control " id="exampleInputEmail1" placeholder="Enter Page Heading" type="text" name="heading" required>
						</div>
						
						
						<div class="form-group">
							<label for="exampleInputPassword1">Content</label>
							<textarea  id="content" name="content" rows="10" cols="80"></textarea>
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1">Meta Title</label>
							<textarea  id="meta_title" name="meta_title" rows="10" cols="80"></textarea>
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1">Meta Keyword</label>
							<textarea  id="meta_keyword" name="meta_keyword" rows="10" cols="80"></textarea>
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1">Meta Description</label>
							<textarea  id="meta_description" name="meta_description" rows="10" cols="80"></textarea>
						</div>
						
						
						<div class="form-group ">
							<label for="exampleInputEmail1">Status</label>
							<select name="status" >
								<option value="1">Active</option>
								<option value="0">De-active</option>
							</select>
						</div>
						
						
						<!-- /.box-body -->
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</section>
			</article>
		</div>
	</div>
	<?php $this->load->view('admin/scripts'); ?>
	
	<!-- CK Editor -->
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	<script>
		$(function () {
			
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('content');
    CKEDITOR.replace("meta_title");
    CKEDITOR.replace('meta_keyword');
    CKEDITOR.replace('meta_description');
});
</script>

</body>
</html>

