@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewcms/'.$cms->type) }}">Manage CMS</a></li>
  <li><a href="#">CMS Info</a></li>
</ul>

<div class="inn_content">
  <?php if(Session::has('success')) { ?>
  <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
  <?php } ?>

  <?php if(Session::has('error')) { ?>
  <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
  <?php } ?>
  <!-- <form class="cm_frm1 verti_frm1"> -->
  {!! Form::open(array('url' => $redirectUrl.'/updateCms', 'class'=>'cm_frm1 verti_frm1', 'id'=>'cms_form')) !!}

  <div class="cm_head1">
    <h3>CMS Info</h3>
  </div>
  <input type="hidden" name="id" value="<?php echo App\Model\User::endecryption(1,strip_tags($cms->id)); ?>" >

  <div class="form-group row clearfix">
    <div class="col-sm-9 col-xs-12">
      <label class="form-control-label">CMS Heading :</label>
      <input type="text" class="form-control" name="title" id="title" value="<?php echo strip_tags($cms->title); ?>">
    </div>
  </div>

  <div class="form-group clearfix">
    <label class="form-control-label">CMS Content :</label>
    <textarea class="img-responsive" id="content" name="content"><?php echo $cms->content; ?></textarea> 
  </div>

  <ul class="list-inline">
    <li>
      <button type="submit" class="cm_blacbtn1">Submit</button>
    </li>
  </ul>
  {!! Form::close() !!}
  <!-- </form> -->
</div>


<script src="{{ asset('/').('public/admin_assets/ckeditor/ckeditor.js') }}"> </script>
<script>
  CKEDITOR.replace('content');

  $('#cms_form').validate({
    ignore: [],
    rules:{
      title:{
        required:true,
      },
      content:{
        required: function(textarea) {
           CKEDITOR.instances[textarea.id].updateElement(); // update textarea
           var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
           return editorcontent.length === 0;
        }
      },
    },
    messages:{
       title:{
        required:"Enter Template Name",
      },
      content:{
        required:"Enter content",
      },
    }
  })
</script>
@stop