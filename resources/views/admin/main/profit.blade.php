
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>WIIX Admin panel | List All users </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
      @include('layouts.styles')
           <!-- DataTables -->
	<link rel="stylesheet" href="{{ URL::asset('theme/datatables/dataTables.bootstrap.css') }}"> 
  
    </head>

    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
		  @include('layouts.header')
               
               
            @include('layouts.sidebar')
            
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                
                <article class="content dashboard-page">
                     <section class="section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block">
                                        <section class="example">
					
			          <div class="table-flip-scroll">
                            <table id="example1" class="table table-striped table-bordered table-hover flip-content">
					     <thead class="flip-header">
						<tr>
						        <th>S.no</th>
						        <th>User Email</th>
						        <th>Type</th>
						        <th>Currency</th>
						        <th>Amount</th>
						        <th>Date</th>
						      
						</tr>
					</thead>
					<tbody>
						
						@if($profit)
							<?php $i=0; ?>
							@foreach($profit as $user)
				
						<tr>
						        <td><?php echo ++$i; ?></td>
						        	      
						<td><?php
								$email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
								$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
								$final_mail=$email[0]."@".$email[1];
							?>

							{{$final_mail}}
						</td>      
						  <td>{{$user->Type}}</td>
						  <td>{{$user->theftCurrency}}</td>
						  <td>{{$user->theftAmount}}</td>
						  <td>{{$user->coin_date}}</td>
						   
					
					</tr>
					@endforeach
					@endif
					</tbody>
				</table>
			
                         
                      </div>
                      </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>
                @include('layouts.footer')
             </div>
        </div>
        
       @include('layouts.scripts')
     
     <!-- DataTables -->

             <script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
 <script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
  $(function () {
	$("#example1").DataTable({
		 "pagingType": "full_numbers",
		 "ordering": false
		});
   });
</script>
    
    </body>

</html>
