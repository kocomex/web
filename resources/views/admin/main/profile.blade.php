
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>WIIX Admin panel | Profile </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
      @include('layouts.styles')
    </head>

    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
		  @include('layouts.header')
               
               
            @include('layouts.sidebar')
            
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                
                <article class="content dashboard-page">
                    <section class="section">
                        <div class="row sameheight-container">
                                 <div class="col-md-6">
				  <div class="auth-container">
                <div class="card">
                    <div class="auth-content">
                        <form id="profile_form" name="profile_form" action="{{ url('/WiPlytaIIX2/profile') }}" method="POST" novalidate="">

                       {!! csrf_field() !!}
                           

                            <div class="form-group"> <label for="firstname">Username</label>
                                <input type="text" class="form-control underlined" name="username" id="username" placeholder="Enter username"> 
                            </div>
                            
                        
                            
                            <div class="form-group"> <label for="password">Password</label>
                                <div class="row">
                                    <div class="col-sm-6"> <input type="password" class="form-control underlined" name="password" id="password" placeholder="Enter password"> </div>
                                   
                                </div>
                            </div>
                            
                               <div class="form-group"> <label for="email">Security Phrase</label> <input type="text" class="form-control underlined" name="lock_word" id="lock_word" placeholder="Enter Security Phrase"> </div>
                               
                            <div class="form-group"> <button type="submit" class="btn btn-block btn-primary">Submit</button> </div>
                        </form>
                    </div>
                </div>
            </div>
	
			
			</div>
                             <?php
				$admin_data = $admin;
                             ?>
                             <div class="col-md-6">
                                <div class="card">
                                    <div class="card-block">
                                        <section class="example">
                                            <table class="table table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td>Admin User Name</td>
                                                        <td>{!! Helper::encrypt_decrypt("decrypt",$admin_data->admin_username) !!} </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Password</td>
                                                        <td>{!! Helper::encrypt_decrypt("decrypt",$admin_data->admin_password) !!} </td>
                                                    </tr>
                                                  
                                                    <tr>
                                                        <td>Security Phrase</td>
                                                        <td>
						{!! Helper::encrypt_decrypt("decrypt",$admin_data->lock_word) !!}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            
                             
                                </div>
                    </section>
                  
                </article>
               
             
            </div>
        </div>
        
  @include('layouts.scripts')
      
<script type="text/javascript">
      
//SignupForm validation
jQuery.validator.addMethod("pattern_check", function(value, element) {
  // allow any non-whitespace characters as the host part
  return this.optional( element ) || /^\S*$/.test( value );
}, 'No Spaces Allowed');


      
                var signupValidationSettings = {
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                        },
                        username: {
                            required: true,
                        },
                        lock_word:{
                            required: true,
                        },
                        
                        
                    },
                    messages: {
                        email: {
                            email: "Please enter a valid email address"
                        },
                        
                    },
                }
                $('#profile_form').validate(signupValidationSettings);
      
      </script>

      <style type="text/css">
          label#username-error {
    color: red;
}
        label#email-error {
    color: red;
}
        label#password-error {
    color: red;
}
        label#lock_word-error {
    color: red;
}
.auth-content {
    margin-top: 443px;
}
      </style>

    </body>

</html>
