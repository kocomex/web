
<!DOCTYPE html>
<html>
    <head>
        <title>WIIX Admin Panel | Forget</title>
        <link rel="stylesheet" href="{{ URL::asset('theme/css/vendor.css') }}">
        <link rel="stylesheet"  href="{{ URL::asset('theme/css/app-green.css') }}">
        <style>
	        
	        
        </style>
    </head>

    <body>
       
        <div class="auth">
            <div class="auth-container">
       
                <div class="card">
                    <header class="auth-header">
                        <h1 class="auth-title">
                            <div class="logo"> <span class="l l1"></span> <span class="l l2"></span> <span class="l l3"></span> <span class="l l4"></span> <span class="l l5"></span> </div> WIIX Admin </h1>
                    </header>
                    <div class="auth-content">
                        <p class="text-xs-center">PASSWORD RECOVER</p>
                        <p class="text-muted text-xs-center"><small>Enter your email address to recover your password.</small></p>
                        <form id="reset-form" action="{{ url('/WiPlytaIIX2/forgot_password') }}" method="POST" novalidate="">

                        {!! csrf_field() !!}
                        
                            <div class="form-group"> <label for="email1">Email</label> <input type="email" class="form-control underlined" name="email" id="email" placeholder="Your email address" required> </div>
                            <div class="form-group"> <button type="submit" class="btn btn-block btn-primary">Reset</button> </div>
                            <div class="form-group clearfix"> <a class="pull-left" href="{{ url('/WiPlytaIIX2/login') }}">return to Login</a></div>
                        </form>
                    </div>
                </div>
                   </div>
        </div>
     
     
          <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
         <script src="{{ URL::asset('theme/js/vendor.js') }}"></script>
        <script src="{{ URL::asset('theme/js/app.js') }}"></script>
	   <script type="text/javascript" src="{{ URL::asset('theme/front_users/js/notifIt.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ URL::asset('theme/front_users/css/notifIt.css') }}">

<script type="text/javascript">
  $( document ).ready(function() {
 <?php if(session()->has('success')) { ?>

  var sucess= '{!! session('success') !!}';
  notif({

msg: '<img src="{{ URL::asset("theme/front_users/images/sucesssmile.png") }}" height="50px" width="50px"/>'+' '+' '+' '+sucess,
 width: 700,
height: 60,

type: "success"

});
  <?php } ?>

   <?php if(session()->has('error')) { ?>

  var error= '{!! session('error') !!}';
 notif({
        type: "error",
        msg: '<img src="{{ URL::asset("theme/front_users/images/sadsmile.png") }}" height="50px" width="50px"/>'+' '+' '+' '+error,
        
        width: 700,
        height: 60,
        autohide: true
      });
  <?php } ?>
});

</script>
    </body>

</html>
