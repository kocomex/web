
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>WIIX Admin panel | Settings </title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		 @include('layouts.styles')
	</head>
	<body>
		<div class="main-wrapper">
			<div class="app" id="app">
				 @include('layouts.header')
               
               
            @include('layouts.sidebar')
				<div class="sidebar-overlay" id="sidebar-overlay"></div>
				<article class="content dashboard-page">
					<section class="section">
						<form id="profile-form" action="{{ url('/WiPlytaIIX2/settings') }}" method="POST" enctype="multipart/form-data" >
							 {!! csrf_field() !!}
						<div class="row sameheight-container">
							<div class="col-md-6">
								<div class="auth-container">
									<div class="card">
										<div class="auth-content">
											
											<div class="form-group row"> <label for="firstname" class="col-sm-2 form-control-label">Site name</label><div class="col-sm-10"><input type="text" class="form-control" name="site_name" id="site_name" value="<?php echo $setting->site_name; ?>"></div></div>

											<div class="form-group row"> <label for="email" class="col-sm-2 form-control-label">Contact email </label><div class="col-sm-10"><input type="text" class="form-control " name="email" id="email"  value="<?php echo $setting->email; ?>"> </div> </div> 

											

											<div class="form-group row"> <label for="password" class="col-sm-2 form-control-label">Phone</label><div class="col-sm-10"><input type="text" class="form-control" name="phone" id="phone" value="<?php echo $setting->phone; ?>"> </div></div>

											<div class="form-group row"> <label for="password" class="col-sm-2 form-control-label">Address</label><div class="col-sm-10"><textarea  class="form-control" name="address"><?php echo $setting->address; ?></textarea>  </div></div>
											
											
											<div class="form-group row"> <label for="password" class="col-sm-2 form-control-label">Site logo</label><div class="col-sm-10"><input type="file" class="form-control" name="logo" > </div></div>
											
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="card card-block " >
									
									<div class="form-group row"><label for="inputEmail3" class="col-sm-2 form-control-label">Facebook</label><div class="col-sm-10"><input type="text" class="form-control" id="inputEmail3" name="facebook_url" id="facebook_url" value="<?php echo $setting->facebook_url; ?>"> </div></div>
									
									<div class="form-group row"><label for="inputEmail3" class="col-sm-2 form-control-label">Twitter </label><div class="col-sm-10"><input type="text" class="form-control" id="inputEmail3" name="twitter_url" value="<?php echo $setting->twitter_url; ?>"> </div></div>
									
									<div class="form-group row"><label for="inputEmail3" class="col-sm-2 form-control-label">Linkedin </label><div class="col-sm-10"><input type="text" class="form-control" id="inputEmail3" name="linkedin_url" value="<?php echo $setting->linkedin_url; ?>"> </div></div>
									
									<div class="form-group row"><label for="inputPassword3" class="col-sm-2 form-control-label">Google </label><div class="col-sm-10"><input type="text" class="form-control" id="inputPassword3" name="google_url" value="<?php echo $setting->google_url; ?>"> </div></div>

									<div class="form-group row"><div class="col-sm-offset-2 col-sm-10 "><button type="submit" class="btn btn-success">Save</button></div></div>

								</div>
							</div>
							
							
						</form>
					</section>
				</article>
			</div>
		</div>
	@include('layouts.scripts')
		
		<script>
			$(function() {
				
				if (!$('#profile-form').length) {
					return false;
				}

				var signupValidationSettings = {
					rules: {
						email: {
							email: true
						},
						retype_password: {
							equalTo: "#password"
						},
						address: {
						required: true,
						},
						agree: {
							required: true,
						},
						facebook_url:{
							url: true	
						},
						twitter_url:{
							url: true	
						},
						linkedin_url:{
							url: true	
						},
						google_url:{
							url: true	
						},
						
					},
					messages: {
						email: {
							email: "Please enter a valid email address"
						},
						facebook_url: {
							url: "Please enter valid URL.",
						},
						twitter_url: {
							url: "Please enter valid URL.",
						},
						linkedin_url: {
							url: "Please enter valid URL.",
						},
						google_url: {
							url: "Please enter valid URL.",
						}
					},
				}
				$('#profile-form').validate(signupValidationSettings);
			});
		</script>

		<style type="text/css">
			.auth-content {
    margin-top: 264px;
}
		</style>
			</body>
</html>
