
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>WIIX Admin panel | Dashboard </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    @include('layouts.styles')
    <style>
        .row .col {
          padding-left: 2px !important;
          padding-right: 2px !important;
          float: left !important;
      }
  </style>
</head>

<body>
    <div class="main-wrapper">
        <div class="app" id="app">
          @include('layouts.header')


          @include('layouts.sidebar')

          <div class="sidebar-overlay" id="sidebar-overlay"></div>

          <article class="content dashboard-page">
            <section class="section">
                <div class="row sameheight-container">


                    <div class="col col-xs-4 col-sm-4 col-md-4">
                        <div class="card sameheight-item sales-breakdown" data-exclude="xs,sm,lg">
                            <div class="card-header">
                                <div class="header-block">
                                    <strong>Pending details:</strong>
                                </div>
                            </div>
                            <div class="card-block">
                                <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col col-xs-12 col-sm-12 col-md-8">
                       <div class="card sameheight-item stats" data-exclude="xs">
                        <div class="card-block">
                            <div class="title-block">
                                <h4 class="title">Quick view</h4>
                            </div>
                            <div class="row row-sm stats-container">
                            <a href="{{ url('/WiPlytaIIX2/users') }}">
                                <div class="col-xs-12 col-sm-4 stat-col">
                                    <div class="stat-icon"> <i class="fa fa-users"></i> </div>
                                    <div class="stat">
                                        <div class="value"> {{$users_count}} </div>
                                        <div class="name"> Active users </div>
                                    </div>  </div></a>

                                    <a href="{{ url('/WiPlytaIIX2/enquires') }}">
                                    <div class="col-xs-12 col-sm-4  stat-col">
                                        <div class="stat-icon"> <i class="fa fa-list-alt"></i> </div>
                                        <div class="stat">
                                            <div class="value"> {{$tickets_count}} </div>
                                            <div class="name"> Total Tickets  </div>
                                        </div> </div></a>

                                        <a href="{{ url('/WiPlytaIIX2/Subscribers') }}">
                                        <div class="col-xs-12 col-sm-4  stat-col">
                                            <div class="stat-icon"> <i class="fa fa-user"></i> </div>
                                            <div class="stat">
                                                <div class="value"> {{$subscribers}} </div>
                                                <div class="name">Subscribers</div>
                                            </div> </div>

                                            </a>


                                            <a href="{{ url('/WiPlytaIIX2/dephistory') }}">
                                            <div class="col-xs-12 col-sm-4  stat-col">
                                                <div class="stat-icon"> <i class="fa fa-line-chart"></i> </div>
                                                <div class="stat">
                                                    <div class="value"> {{$dep_count}}  </div>
                                                    <div class="name"> Total Deposit </div>
                                                </div> </div>
                                                </a>
                                                <a href="{{ url('/WiPlytaIIX2/withhistory') }}">
                                                <div class="col-xs-12 col-sm-4  stat-col">
                                                    <div class="stat-icon"> <i class="fa fa-money"></i> </div>
                                                    <div class="stat">
                                                        <div class="value"> {{$with_count}} </div>
                                                        <div class="name"> Total Withdraw </div>
                                                    </div></div>
                                                    </a>

                                              

<a href="{{ url('/WiPlytaIIX2/tradehistory') }}">

                                                        <div class="col-xs-12 col-sm-4 stat-col">
                                                            <div class="stat-icon"> <i class="fa fa-exchange"></i> </div>
                                                            <div class="stat">
                                                                <div class="value"> {{$trade_count}} </div>
                                                                <div class="name"> Total Trades </div>
                                                            </div>  </div>
                                                            </a>

                                                       <div class="col-xl-6"> <div class="card card-info"> <div class="card-header"> <div class="header-block"> <p class="title"> Fiat Currency Profit </p> </div> </div> <div class="card-block" id="blcks"> <div class="item-img rounded" > <span id="fiat_spn"><i class="fa fa-money fa-5x"></span></i><span class="countings">

                                                       {{ number_format($fiat, 2) }} </span></div>  </div>  </div> </div>
                                                     <div class="col-xl-6"> <div class="card card-success"> <div class="card-header"> <div class="header-block"> <p class="title"> Crypto Currency Profit </p> </div> </div> <div class="card-block" id="blcks_is"> <div class="item-img rounded" > <span id="cryp_spn"><i class="fa fa-money fa-5x"></span></i><span class="countings">



                                                     {{ number_format($crypto, 8) }} </span></div>  </div>  </div> </div>
                                                            </div>

                                                        </div>
                                                    </div>



                                                </div>






                                            </div>




                                            <div class="row sameheight-container">



                                             <div class="col col-xs-12 col-sm-12 col-md-12">
                                                <div class="card sameheight-item" data-exclude="xs">
                                                    <div class="card-header card-header-sm bordered">
                                                        <div class="header-block">
                                                            <strong>Fee Settings</strong>
                                                        </div>
                                                        <ul class="nav nav-tabs pull-right" role="tablist">
                                                         <li class="nav-item"> <a class="nav-link active" href="#visits" role="tab"  data-toggle="tab">Withdraw Limit</a> </li>

                                                         <li class="nav-item"> <a class="nav-link" href="#settings" role="tab" data-target="#settings" data-toggle="tab">Withdraw Fees</a> </li>

                                                         <li class="nav-item"> <a class="nav-link" href="#downloads" role="tab" data-toggle="tab">Trade fee</a> </li>

                                                         <li class="nav-item"> <a class="nav-link" href="#traderate" role="tab" data-toggle="tab">Trade Buy rate</a> </li>

                                                          <li class="nav-item"> <a class="nav-link" href="#tradesellrate" role="tab" data-toggle="tab">Trade Sell rate</a> </li>
                                                     </ul>
                                                 </div>
                                                 <div class="card-block">
                                                    <div class="tab-content">

                                                        <div role="tabpanel" class="tab-pane active fade in" id="visits">
                                                         <div class="col-md-12">
                                                          <div class="card card-block " >

    <form id="withdraw_limit" action="{{ url('/WiPlytaIIX2/dashboard') }}" method="POST" novalidate="" name="withdraw_limit">

{!! csrf_field() !!}

 <div class="form-group row"><label><b>Minimum limit</b></label></div>

<div class="form-group col-md-6" ><label for="" class="form-control-label">BTC</label><input type="text" class="form-control" name="btc_wmin" id="btc_wmin" value="{{$get_settings->btc_wmin}}" pattern="[0-9]" required="required"></div>

   <div class="form-group col-md-6"><label for="" class="form-control-label">ETH </label><input type="text" class="form-control" id="eth_wmin" name="eth_wmin" value="{{$get_settings->eth_wmin}}" pattern="[0-9]" required="required"> </div>

 <div class="form-group col-md-6"><label for="" class="form-control-label">LTC </label><input type="text" class="form-control" id="ltc_wmin" name="ltc_wmin" value="{{$get_settings->ltc_wmin}}" pattern="[0-9]" required="required"> </div>
   
     <div class="form-group col-md-6"><label for="" class="form-control-label">XRP </label><input type="text" class="form-control" id="xrp_wmin" name="xrp_wmin" value="{{$get_settings->xrp_wmin}}" pattern="[0-9]" required="required"> </div>
 
 <div class="form-group col-md-6" ><label for="" class="form-control-label">DOGE</label><input type="text" class="form-control" id="doge_wmin" name="doge_wmin" id="btc" value="{{$get_settings->doge_wmin}}" pattern="[0-9]" required="required"> </div>

 <div class="form-group col-md-6"><label for="" class="form-control-label">ETC </label><input type="text" class="form-control" id="etc_wmin" name="etc_wmin" value="{{$get_settings->etc_wmin}}" pattern="[0-9]" required="required"> </div>
    
    <div class="form-group col-md-6"><label for="" class="form-control-label">XMR </label><input type="text" class="form-control" id="xmr_wmin" name="xmr_wmin" value="{{$get_settings->xmr_wmin}}" pattern="[0-9]" required="required"> </div>

<div class="form-group col-md-6"><label for="" class="form-control-label">IOTA </label><input type="text" class="form-control" id="iota_wmin" name="iota_wmin" value="{{$get_settings->iota_wmin}}" pattern="[0-9]" required="required"> </div>

<div class="form-group col-md-6" ><label for="" class="form-control-label">BTG</label><input type="text" class="form-control" name="btg_wmin" id="btg_wmin" value="{{$get_settings->btg_wmin}}" pattern="[0-9]" required="required"> </div>
 <div class="form-group col-md-6"><label for="" class="form-control-label">DASH </label><input type="text" class="form-control" id="dash_wmin" name="dash_wmin" value="{{$get_settings->dash_wmin}}" pattern="[0-9]" required="required"> </div>
  
   <div class="form-group col-md-6"><label for="" class="col-sm-2 form-control-label">KRW </label><input type="text" class="form-control" id="krw_wmin" name="krw_wmin" value="{{$get_settings->krw_wmin}}" pattern="[0-9]" required="required"> </div>


  
   <!-- Maximum withdraw -->

  <div class="form-group row col-md-12"><label><b>Maximum limit</b></label></div>
  
  <div class="form-group col-md-6" ><label for="" class="form-control-label">BTC</label><input type="text" class="form-control" name="btc_wmax" id="btc_wmax" value="{{$get_settings->btc_wmax}}" pattern="[0-9]" required="required"></div>
    
     <div class="form-group col-md-6"><label for="inputEmail3" class="col-sm-2 form-control-label">ETH </label><input type="text" class="form-control" id="eth_wmax" name="eth_wmax" value="{{$get_settings->eth_wmax}}" pattern="[0-9]" required="required"> </div>

  <div class="form-group col-md-6"><label for="inputEmail3" class="col-sm-2 form-control-label">LTC </label><input type="text" class="form-control" id="ltc_wmax" name="ltc_wmax" value="{{$get_settings->ltc_wmax}}" pattern="[0-9]" required="required"> </div>
    
     <div class="form-group col-md-6"><label for="inputPassword3" class="col-sm-2 form-control-label">XRP </label><input type="text" class="form-control" id="xrp_wmax" name="xrp_wmax" value="{{$get_settings->xrp_wmax}}" pattern="[0-9]" required="required"> </div>
   
   <div class="form-group col-md-6" ><label for="inputEmail3" class="col-sm-2 form-control-label">DOGE</label><input type="text" class="form-control" name="doge_wmax" id="doge_wmax" value="{{$get_settings->doge_wmax}}" pattern="[0-9]" required="required"> </div>

 <div class="form-group col-md-6"><label for="inputEmail3" class="col-sm-2 form-control-label">ETC </label><input type="text" class="form-control" id="etc_wmax" name="etc_wmax" value="{{$get_settings->etc_wmax}}" pattern="[0-9]" required="required"> </div>
  
   <div class="form-group col-md-6"><label for="inputEmail3" class="col-sm-2 form-control-label">XMR </label><input type="text" class="form-control" id="xmr_wmax" name="xmr_wmax" value="{{$get_settings->xmr_wmax}}" pattern="[0-9]" required="required"> </div>

<div class="form-group col-md-6"><label for="inputPassword3" class="col-sm-2 form-control-label">IOTA </label><input type="text" class="form-control" id="iota_wmax" name="iota_wmax" value="{{$get_settings->iota_wmax}}" pattern="[0-9]" required="required"> </div>

<div class="form-group col-md-6"><label for="inputEmail3" class="col-sm-2 form-control-label">BTG</label><input type="text" class="form-control" id="inputEmail3" name="btg_wmax" id="btg_wmax" value="{{$get_settings->btg_wmax}}" pattern="[0-9]" required="required"> </div>
 
  <div class="form-group col-md-6"><label for="inputEmail3" class="col-sm-2 form-control-label">DASH </label><input type="text" class="form-control" id="dash_wmax" name="dash_wmax" value="{{$get_settings->dash_wmax}}" pattern="[0-9]" required="required"> </div>

 <div class="form-group col-md-6"><label for="inputEmail3" class="col-sm-2 form-control-label">KRW </label><input type="text" class="form-control" id="krw_wmax" name="krw_wmax" value="{{$get_settings->krw_wmax}}" pattern="[0-9]" required="required"> </div>

 
  <div class="form-group row"><div class="col-sm-offset-2 col-sm-12 "><center><input name="withdraw" type="submit" class="btn btn-success" value="Update"></center></div></div>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>


                                            <div role="tabpanel" class="tab-pane fade" id="settings">
                                                <div class="col-md-12">
                                                    <div class="card card-block " >
           <form id="withdraw_fees" action="{{ url('/WiPlytaIIX2/dashboard') }}" method="POST" novalidate="" name="withdraw_fees">

{!! csrf_field() !!}

          
   <div class="form-group col-md-6">
<label class="control-label">BTC</label>
            <div class="input-group">

 <input type="number" id="btc_fee" class="form-control" placeholder="BTC fees" name="btc_fee" value="{{$get_settings->btc_fee}}" pattern="[0-9]" required="required">
     <span class="input-group-addon">%</span></div>
                                                                </div>

      
        <div class="form-group col-md-6">
 <label class="control-label">ETH</label>
     <div class="input-group">


     <input type="number" id="eth_fee" class="form-control" placeholder="ETH fees" name="eth_fee" value="{{$get_settings->eth_fee}}" pattern="[0-9]" required="required">
      <span class="input-group-addon">%</span></div>
                                                                    </div>

     
      <div class="form-group col-md-6">
<label class="control-label">LTC</label>
         <div class="input-group">


          <input type="number" id="ltc_fee" class="form-control" placeholder="LTC amount" name="ltc_fee" value="{{$get_settings->ltc_fee}}" pattern="[0-9]" required="required">
          <span class="input-group-addon">%</span></div>
                                                                        </div>

             
              <div class="form-group col-md-6">
<label class="control-label">XRP</label>
                <div class="input-group">


              <input type="number" id="xrp_fee" class="form-control" placeholder="XRP fees" name="xrp_fee" value="{{$get_settings->xrp_fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                                                                            </div>


            
              <div class="form-group col-md-6">
  <label class="control-label">DOGE</label>
                <div class="input-group">


              <input type="number" id="doge_fee" class="form-control" placeholder="DOGE fees" name="doge_fee" value="{{$get_settings->doge_fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                  </div>

                   
              <div class="form-group col-md-6">
<label class="control-label">ETC</label>
                <div class="input-group">


              <input type="number" id="etc_fee" class="form-control" placeholder="ETC fees" name="etc_fee" value="{{$get_settings->etc_fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                  </div>

                   
              <div class="form-group col-md-6">
<label class="control-label">XMR</label>
                <div class="input-group">


              <input type="number" id="xmr_fee" class="form-control" placeholder="XMR fees" name="xmr_fee" value="{{$get_settings->xmr_fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                  </div>



              <div class="form-group col-md-6">
 <label class="control-label">IOTA</label>
                <div class="input-group">


              <input type="number" id="iota_fee" class="form-control" placeholder="IOTA fees" name="iota_fee" value="{{$get_settings->iota_fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                  </div>

                   
              <div class="form-group col-md-6">
<label class="control-label">BTG</label>
                <div class="input-group">


              <input type="number" id="btg_fee" class="form-control" placeholder="BTG amount" name="btg_fee" value="{{$get_settings->btg_fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                  </div>

                  
              <div class="form-group col-md-6">
 <label class="control-label">DASH</label>
                <div class="input-group">


              <input type="number" id="dash_fee" class="form-control" placeholder="DASH fees" name="dash_fee" value="{{$get_settings->dash_fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                  </div>

                   <div class="form-group col-md-6">
 <label class="control-label">KRW</label>
                <div class="input-group">


              <input type="number" id="krw_fee" class="form-control" placeholder="KRW fees" name="krw_fee" value="{{$get_settings->krw_fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                  </div>


                 <div class="form-group row"><div class="col-sm-offset-2 col-sm-12 "><center><input name="withdraw_fee" type="submit" class="btn btn-success" value="Update"></center></div></div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div role="tabpanel" class="tab-pane fade" id="downloads">
        <form id="buy_sell_percentage" action="{{ url('/WiPlytaIIX2/fees_settings') }}" method="POST" novalidate="" name="buy_sell_percentage">
{!! csrf_field() !!}

@foreach($get_fees as $fees)

 <div class="form-group col-md-4">
 <label class="control-label">{{$fees->currency_pair}}</label>
                <div class="input-group">


              <input type="number" id="{{$fees->currency_pair}}" class="form-control" placeholder="{{$fees->currency_pair}} fees" name="{{$fees->currency_pair}}" value="{{$fees->fee}}" pattern="[0-9]" required="required">
                 <span class="input-group-addon">%</span></div>
                  </div>

@endforeach

 


                                                                <div class="form-group row"><div class="col-sm-offset-2 col-sm-12 "><center><input type="submit" class="btn btn-success" value="Update"></center></div></div>
                                                            </form>
                                                        </div>

    <div role="tabpanel" class="tab-pane fade" id="traderate">
        <form id="traderateform" action="{{ url('/WiPlytaIIX2/trade_rate') }}" method="POST" novalidate="" name="traderateform">
{!! csrf_field() !!}

@foreach($get_tradefees as $pairs)

 <div class="form-group col-md-3">
 <label class="control-label">Buy rate: {{$pairs->pair_name}}</label>
   
              <input type="number" id="{{$pairs->pair_name}}" class="form-control" placeholder="{{$pairs->pair_name}} buy rate" name="{{$pairs->pair_name}}" value="{{$pairs->buy_rate}}" required>
               
                  </div>

     

@endforeach

 


  <div class="form-group row"><div class="col-sm-offset-2 col-sm-12 "><center><input type="submit" class="btn btn-success" value="Update"></center></div></div>
                                                            </form>
                                                        </div>


 <div role="tabpanel" class="tab-pane fade" id="tradesellrate">
        <form id="tradesellform" action="{{ url('/WiPlytaIIX2/trade_sellrate') }}" method="POST" novalidate="" name="tradesellform">
{!! csrf_field() !!}

@foreach($get_tradefees as $pairs)



                   <div class="form-group col-md-3">
 <label class="control-label">Sell rate: {{$pairs->pair_name}}</label>
   
              <input type="number" id="{{$pairs->pair_name}}" class="form-control" placeholder="{{$pairs->pair_name}} sell rate" name="{{$pairs->pair_name}}" value="{{$pairs->sell_rate}}">
               
                  </div>

@endforeach

 


                                                                <div class="form-group row"><div class="col-sm-offset-2 col-sm-12 "><center><input type="submit" class="btn btn-success" value="Update"></center></div></div>
                                                            </form>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>








                                    </div>


                                    <div class="row sameheight-container">

                                    </div>  

                                </section>
                            </article>
                            @include('layouts.footer')
                        </div>
                    </div>
                    @include('layouts.scripts')

                    <script>

                    </script>




                    <!-- DataTables -->
                    <script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
                    <script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>
                    <script>
                      $(function () {
                         $("#example1").DataTable({
                           "pagingType": "full_numbers"
                       });
                     });


                      $(function() {

                        var $dashboardSalesBreakdownChart = $('#dashboard-sales-breakdown-chart');

                        if (!$dashboardSalesBreakdownChart.length) {
                            return false;
                        } 

                        function drawSalesChart(){

                            $dashboardSalesBreakdownChart.empty();

                            Morris.Donut({
                                element: 'dashboard-sales-breakdown-chart',
                                data: [{ label: "BTC", value: 12 },
                                { label: "ETH", value: 30 },
                                { label: "LTC", value: 20 } , { label: "KRW", value: 38 } ],
                                resize: true,
                                colors: [
                                tinycolor(config.chart.colorPrimary.toString()).lighten(10).toString(),
                                tinycolor(config.chart.colorPrimary.toString()).darken(8).toString(),
                                config.chart.colorPrimary.toString()
                                ],
                            });

                            var $sameheightContainer = $dashboardSalesBreakdownChart.closest(".sameheight-container");

                            setSameHeights($sameheightContainer);
                        }

                        drawSalesChart();

                        $(document).on("themechange", function(){
                           drawSalesChart();
                       });

                    });

                </script>
                <script>
                    window.onload = function() {


var cha1=<?php echo $withdraws; ?>;
var cha2=<?php echo $deposits; ?>;
var cha3=<?php echo $consumers; ?>;

                        var chart = new CanvasJS.Chart("chartContainer", {
    theme: "light2", // "light1", "light2", "dark1", "dark2"
    exportEnabled: true,
    animationEnabled: true,
    title: {
        text: "Pending details"
    },
    data: [{
        type: "pie",
        startAngle: 25,
      
        showInLegend: "true",
        legendText: "{label}",
        indexLabelFontSize: 16,
        indexLabel: "{label} - {y}",
        dataPoints: [
        { y: cha1, label: "Withdraw Pending" },
        { y: cha2, label: "Deposit Pending" },
        { y: cha3, label: "Active users" }
        ]
    }]
});
                        chart.render();

                    }
                </script>
                <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

                <script type="text/javascript">
                  $( "#withdraw_limit" ).validate({
                    rules: {


                      btc_wmin:{required: true,number: true},
                      eth_wmin:{required: true,number: true},
                      ltc_wmin:{required: true,number: true},
                      xrp_wmin:{required: true,number: true},
                      doge_wmin:{required: true,number: true},
                      etc_wmin:{required: true,number: true},
                      xmr_wmin:{required: true,number: true},
                      iota_wmin:{required: true,number: true},
                      btg_wmin:{required: true,number: true},
                      dash_wmin:{required: true,number: true},
                      krw_wmin:{required: true,number: true},

                      btc_wmax:{required: true,number: true},
                      eth_wmax:{required: true,number: true},
                      ltc_wmax:{required: true,number: true},
                      xrp_wmax:{required: true,number: true},
                      doge_wmax:{required: true,number: true},
                      etc_wmax:{required: true,number: true},
                      xmr_wmax:{required: true,number: true},
                      iota_wmax:{required: true,number: true},
                      btg_wmax:{required: true,number: true},
                      dash_wmax:{required: true,number: true},
                      krw_wmax:{required: true,number: true},

                  },


              });
          </script>

                    <script type="text/javascript">
                  $( "#withdraw_fees" ).validate({
                    rules: {


                      btc_fee:{required: true,number: true},
                      eth_fee:{required: true,number: true},
                      ltc_fee:{required: true,number: true},
                      xrp_fee:{required: true,number: true},
                      doge_fee:{required: true,number: true},
                      etc_fee:{required: true,number: true},
                      xmr_fee:{required: true,number: true},
                      iota_fee:{required: true,number: true},
                      btg_fee:{required: true,number: true},
                      dash_fee:{required: true,number: true},
                      krw_fee:{required: true,number: true},

                  },


              });
          </script>
<style type="text/css">
    .error {
    color: red;
}
span#fiat_spn {
    color: #76D4F5;
}
span#cryp_spn {
    color: #4bcf99;
}
span.countings {
    font-size: 29px;
    font-family: fantasy;
    font-weight: bold;
        margin-left: 50px;
}
</style>

      </body>

      </html>
