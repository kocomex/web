
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>WIIX Admin panel | Dashboard </title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		 @include('layouts.styles')
	</head>
	<body>
		<div class="main-wrapper">
			<div class="app" id="app">
				 @include('layouts.header')
               
            @include('layouts.sidebar')
				<div class="sidebar-overlay" id="sidebar-overlay"></div>
				<article class="content dashboard-page">
					<section class="section">
						<form id="profile-form" action="{{ url('/WiPlytaIIX2/adminbank') }}" method="post" novalidate="">
							{!! csrf_field() !!}
						
						<div class="row sameheight-container">
							<div class="col-md-6">
								<div class="auth-container">
									<div class="card">
										<div class="auth-content">
											<label class="bank_lab">Bank details1</label>
											<div class="form-group row"> <label for="acc_no1" class="col-sm-2 form-control-label">Account number1</label><div class="col-sm-10"><input type="text" class="form-control" name="acc_no1" id="acc_no1" value="<?php echo $setting->acc_no1; ?>"></div></div>

											<div class="form-group row"> <label for="acc_no1" class="col-sm-2 form-control-label">Account name1</label><div class="col-sm-10"><input type="text" class="form-control" name="acc_name1" id="acc_name1" value="<?php echo $setting->acc_name1; ?>"></div></div>

											<div class="form-group row"> <label for="bank_name1" class="col-sm-2 form-control-label">Bank name1 </label><div class="col-sm-10"><input type="text" class="form-control " name="bank_name1" id="bank_name1"  value="<?php echo $setting->bank_name1; ?>"> </div> </div> 

											

											<div class="form-group row"> <label for="branch1" class="col-sm-2 form-control-label">Bank branch1</label><div class="col-sm-10"><input type="text" class="form-control" name="branch1" id="branch1" value="<?php echo $setting->branch1; ?>"> </div></div>
											
											<div class="form-group row"> <label for="password" class="col-sm-2 form-control-label">Swift code1</label><div class="col-sm-10"><input type="text" class="form-control" name="swift1" value="<?php echo $setting->swift1; ?>" > </div></div>

											
											
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="card card-block " >
								<label class="bank_lab">Bank details3</label>
									
										<div class="form-group row"> <label for="acc_no3" class="col-sm-2 form-control-label">Account number3</label><div class="col-sm-10"><input type="text" class="form-control" name="acc_no3" id="acc_no3" value="<?php echo $setting->acc_no3; ?>"></div></div>

											<div class="form-group row"> <label for="acc_no1" class="col-sm-2 form-control-label">Account name3</label><div class="col-sm-10"><input type="text" class="form-control" name="acc_name3" id="acc_name3" value="<?php echo $setting->acc_name3; ?>"></div></div>

											<div class="form-group row"> <label for="bank_name3" class="col-sm-2 form-control-label">Bank name3 </label><div class="col-sm-10"><input type="text" class="form-control " name="bank_name3" id="bank_name3"  value="<?php echo $setting->bank_name3; ?>"> </div> </div> 

											

											<div class="form-group row"> <label for="branch3" class="col-sm-2 form-control-label">Bank branch3</label><div class="col-sm-10"><input type="text" class="form-control" name="branch3" id="branch3" value="<?php echo $setting->branch3; ?>"> </div></div>
											
											<div class="form-group row"> <label for="swift3" class="col-sm-2 form-control-label">Swift code3</label><div class="col-sm-10"><input type="text" class="form-control" name="swift3" value="<?php echo $setting->swift3; ?>" > </div></div>
									

								</div>
							</div>
							<div class="col-md-6">
<div class="card card-block " >
<label class="bank_lab">Bank details2</label>
							<div class="form-group row"> <label for="acc_no2" class="col-sm-2 form-control-label">Account number2</label><div class="col-sm-10"><input type="text" class="form-control" name="acc_no2" id="acc_no2" value="<?php echo $setting->acc_no2; ?>"></div></div>

							<div class="form-group row"> <label for="acc_no1" class="col-sm-2 form-control-label">Account name2</label><div class="col-sm-10"><input type="text" class="form-control" name="acc_name2" id="acc_name2" value="<?php echo $setting->acc_name2; ?>"></div></div>

											<div class="form-group row"> <label for="bank_name2" class="col-sm-2 form-control-label">Bank name2 </label><div class="col-sm-10"><input type="text" class="form-control " name="bank_name2" id="bank_name2"  value="<?php echo $setting->bank_name2; ?>"> </div> </div> 

											

											<div class="form-group row"> <label for="branch2" class="col-sm-2 form-control-label">Bank branch2</label><div class="col-sm-10"><input type="text" class="form-control" name="branch2" id="branch2" value="<?php echo $setting->branch2; ?>"> </div></div>
											
											<div class="form-group row"> <label for="password" class="col-sm-2 form-control-label">Swift code2</label><div class="col-sm-10"><input type="text" class="form-control" name="swift2" value="<?php echo $setting->swift2; ?>" > </div></div>
											</div>
											</div>
											<div class="col-md-12">

											<div class="form-group row"><div class="col-sm-offset-2 col-sm-10 "><button type="submit" class="btn btn-success">Update</button></div></div>
											</div>
						
						</form>
					</section>
				</article>
			</div>
		</div>
	 @include('layouts.scripts')
		
		<script>
			$(function() {
				
				if (!$('#profile-form').length) {
					return false;
				}

				var signupValidationSettings = {
					rules: {
						email: {
							email: true
						},
						retype_password: {
							equalTo: "#password"
						},
						agree: {
							required: true,
						},
						facebook_url:{
							url: true	
						},
						twitter_url:{
							url: true	
						},
						linkedin_url:{
							url: true	
						},
						google_url:{
							url: true	
						},
						
					},
					messages: {
						email: {
							email: "Please enter a valid email address"
						},
						facebook_url: {
							url: "Please enter valid URL.",
						},
						twitter_url: {
							url: "Please enter valid URL.",
						},
						linkedin_url: {
							url: "Please enter valid URL.",
						},
						google_url: {
							url: "Please enter valid URL.",
						}
					},
				}
				$('#profile-form').validate(signupValidationSettings);
			});
		</script>
		<style type="text/css">
			label.bank_lab {
    border-left-color: red;
    font-size: 22px;
}
.auth-content {
    margin-top: 351px;
}
.card.card-block {
    margin-top: 22px;
}
		</style>
	</body>
</html>
