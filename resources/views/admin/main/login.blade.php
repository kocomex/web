<html>
	<head>
		<title>WIIX Admin Panel | Login</title>

		<link rel="stylesheet" href="{{ URL::asset('theme/css/vendor.css') }}">
		<link rel="stylesheet"  href="{{ URL::asset('theme/css/app-green.css') }}">
		<style>.ls{padding:5px;border:1px solid #dedede;margin:5px}.correctText{padding:5px;border:1px solid #dedede;color:#0f0;margin:5px}</style>


	</head>

  <body>
      
        <div class="auth">
            <div class="auth-container">
       
                <div class="card">
                    <header class="auth-header">
                        <h1 class="auth-title">
                            <div class="logo"> <span class="l l1"></span> <span class="l l2"></span> <span class="l l3"></span> <span class="l l4"></span> <span class="l l5"></span> </div> WIIX  Admin </h1>
                    </header>
                    <div class="auth-content">
                        <p class="text-xs-center">LOGIN TO CONTINUE</p>
                        <form id="login-form" action="{{ url('/WiPlytaIIX2/login') }}" method="POST" novalidate="">
                         {!! csrf_field() !!}


                            <div class="form-group"> <label for="username">Username</label> 
                            <input type="text" class="form-control underlined" name="username" id="username" placeholder="Username" required> </div>
                            <div class="form-group"> <label for="password">Password</label> <input type="password" class="form-control underlined" name="password" id="password" placeholder="Your password" required> </div>
                    
                       <div id="jumbleWords" class="jumbleWordsWrap"></div>
                          <div class="form-group">
                       <input   class="form-control underlined" type="hidden" id="jumble" name="jumble" value="" required>
                      
                        </div>
                        
                            <div class="form-group"> <label for="remember">
		    <input class="checkbox" id="remember" type="checkbox"> 
            <span>Remember me</span>
          </label> <a href="{{ url('/WiPlytaIIX2/reset') }}" class="forgot-btn pull-right">Forgot password?</a> </div>
                            <div class="form-group"> <button type="submit" class="btn btn-block btn-primary">Login</button> </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="color-panel" style="display:none"><?php echo $lock_word; ?></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="{{ URL::asset('theme/js/vendor.js') }}"></script>
        <script src="{{ URL::asset('theme/js/app.js') }}"></script>
        <script src="{{ URL::asset('theme/js/captcha.min.js') }}"></script>

            <script type="text/javascript" src="{{ URL::asset('theme/front_users/js/notifIt.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ URL::asset('theme/front_users/css/notifIt.css') }}">

<script type="text/javascript">
  $( document ).ready(function() {
 <?php if(session()->has('success')) { ?>

  var sucess= '{!! session('success') !!}';
  notif({

msg: '<img src="{{ URL::asset("theme/front_users/images/sucesssmile.png") }}" height="50px" width="50px"/>'+' '+' '+' '+sucess,
 width: 700,
height: 60,

type: "success"

});
  <?php } ?>

   <?php if(session()->has('error')) { ?>

  var error= '{!! session('error') !!}';
 notif({
        type: "error",
        msg: '<img src="{{ URL::asset("theme/front_users/images/sadsmile.png") }}" height="50px" width="50px"/>'+' '+' '+' '+error,
        
        width: 700,
        height: 60,
        autohide: true
      });
  <?php } ?>
});

</script>
       
        <style type="text/css">
            #errordiv {
  -webkit-animation: seconds 1.0s forwards;
  -webkit-animation-iteration-count: 1;
  -webkit-animation-delay: 5s;
  animation: seconds 1.0s forwards;
  animation-iteration-count: 1;
  animation-delay: 5s;
  position: relative;
    
}
          #successdiv {
  -webkit-animation: seconds 1.0s forwards;
  -webkit-animation-iteration-count: 1;
  -webkit-animation-delay: 5s;
  animation: seconds 1.0s forwards;
  animation-iteration-count: 1;
  animation-delay: 5s;
  position: relative;
    
}
@-webkit-keyframes seconds {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    left: -9999px; 
    position: absolute;   
  }
}
@keyframes seconds {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    left: -9999px;
    position: absolute;     
  }
}
        </style>
    </body>
</html>
