@extends('admin.layouts/admin')

@section('content')

<div class="loader" id="myLoad">
  <div class="spinner">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
  </div>
</div>

<?php if(Session::has('success')) { ?>
<div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
<?php } ?>

<?php if(Session::has('error')) { ?>
<div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
<?php } ?>
    
<ul class="breadcrumb cm_breadcrumb">
	<li><a href="#">Home</a></li>
</ul>
<div class="mainWrapper">
	<div class="cardsTopSec mb-20">
	  <div class="row">
	    <div class="col-md-3 col-sm-6">
	      <div class="cardsBlk cardsClr1">
	        <p>New Users <?php if($data['new_users'] != 0) { ?> <a href="{{ URL::to($redirectUrl.'/viewUser/new') }}">View</a><?php } ?> </p>
	        <div class="midSec">
	          {{ $data['new_users'] }}
	        </div>
	      </div>
	    </div>
	    <div class="col-md-3 col-sm-6">
	      <div class="cardsBlk cardsClr2">
	        <p>Pending KYC <?php if($data['pending_users'] != 0) { ?> <a href="{{ URL::to($redirectUrl.'/viewUser/pending') }}">View</a><?php } ?> </p>
	        <div class="midSec">
	          {{ $data['pending_users'] }}
	        </div>
	        <div class="bottomSec">
	        </div>
	      </div>
	    </div>
	    <div class="col-md-3 col-sm-6">
	      <div class="cardsBlk cardsClr5">
	        <p>Pending Deposit <?php if($data['pending_deposit'] != 0) { ?> <a href="{{ URL::to($redirectUrl.'/viewDeposit/pending') }}">View</a><?php } ?> </p>
	        <div class="midSec">
	          {{  $data['pending_deposit'] }}
	        </div>
	        <div class="bottomSec">
	        </div>
	      </div>
	    </div>
	    <div class="col-md-3 col-sm-6">
	      <div class="cardsBlk cardsClr2">
	        <p>Pending Withdraw <?php if($data['pending_withdraw'] != 0) { ?> <a href="{{ URL::to($redirectUrl.'/viewWithdraw/pending') }}">View</a><?php } ?> </p>
	        <div class="midSec">
	          {{ $data['pending_withdraw'] }}
	        </div>
	        <div class="bottomSec">
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="row">
	    <div class="col-md-3 col-sm-6">
	      <div class="cardsBlk cardsClr3">
	        <p>Total Users</p>
	        <div class="midSec">
	          {{ $data['total_users'] }}
	        </div>
	      </div>
	    </div>
	    <div class="col-md-3 col-sm-6">
	      <div class="cardsBlk cardsClr1">
	        <p>New ContactUs<?php if($data['new_message'] != 0) { ?> <a href="{{ URL::to($redirectUrl.'/viewContactUs/new') }}">View</a><?php } ?></p>
	        <div class="midSec">
	          {{ $data['new_message'] }}
	        </div>
	        <div class="bottomSec">
	        </div>
	      </div>
	    </div>
	    <div class="col-md-3 col-sm-6">
	      <div class="cardsBlk cardsClr4">
	        <p>Support Ticket</p>
	        <div class="midSec supTick">
	          <div>{{ $data['new_support'] }}</div>
	          <div>{{ $data['unread_support'] }}</div>
	        </div>
	        <div class="bottomSec suppTickBtm">
	          <?php if($data['new_support'] != 0) { ?> <a href="{{ URL::to($redirectUrl.'/viewSupportTicket/new') }}">New</a><?php } else { ?>
	          <a href="#">New</a>
	          <?php } ?>
	          <?php if($data['unread_support'] != 0) { ?> <a href="{{ URL::to($redirectUrl.'/viewSupportTicket/pending') }}">Pending</a><?php } else {  ?>
	          <a href="#">Pending</a>
	          <?php } ?>
	        </div>
	      </div>
	    </div>
	    <div class="col-md-3 col-sm-6">
	      <div class="cardsBlk cardsClr3">
	        <p>Number of Transaction</p>
	        <div class="midSec">
	          {{ $data['total_transactions'] }}
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="fancy-collapse-panel usrsFees">
		<div class="panel-group" id="accordionUsrsFees" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
		  <div class="panel-heading" role="tab" id="headingOne">
		      <h4 class="panel-title">
		          <div class="panelLft">
		            <img src="{{asset('/').('public/admin_assets/images/users-icon.png')}}" /><span>Profit</span>
		          </div>
		          <div class="panelRight">
	             	<select onchange="set_year_chart2()" id="dep_with_chart_year2">
			            <option value="2018">2018</option>
			            <option value="2019">2019</option>
			            <option value="2020">2020</option>
			            <option value="2021">2021</option>
			            <option value="2022">2022</option>
		            </select>
		            <select onchange="set_curr_chart2()" id="dep_with_chart_curr2">
			            <option value="BTC">BTC</option>
		            </select>
		            <a data-toggle="collapse" data-parent="#accordionUsrsFees" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		              <span class=""><img src="{{asset('/').('public/admin_assets/images/bar-icon.png')}}" /></span>
		            </a>
		          </div>
		      </h4>
		  </div>
		  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      <div class="panel-body panelMinHgt">
		      <div id="chartdiv2"></div>
		         
		      </div>
		  </div>
		</div>
		</div>
	</div>

	<div class="fancy-collapse-panel totDepWdw">
		<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingThree">
		        <h4 class="panel-title">
		            <div class="panelLft">
		              <img src="{{asset('/').('public/admin_assets/images/db-icon.png')}}" /><span>Total Deposit and Withdraw</span>
		            </div>
		            <div class="panelRight">
		            <select onchange="set_year_chart()" id="dep_with_chart_year">
			            <option value="2018">2018</option>
			            <option value="2019">2019</option>
			            <option value="2020">2020</option>
			            <option value="2021">2021</option>
			            <option value="2022">2022</option>
		            </select>
		            <select onchange="set_curr_chart()" id="dep_with_chart_curr">
			            <option value="BTC">BTC</option>
		            </select>
		              
		              <a data-toggle="collapse" data-parent="#accordion3" href="#collapseTotDepWdw" aria-expanded="true" aria-controls="collapseTotDepWdw">
		                <span class=""><img src="{{asset('/').('public/admin_assets/images/bar-icon.png')}}" /></span>
		              </a>
		            </div>
		        </h4>
		    </div>
		    <div id="collapseTotDepWdw" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
		        <div class="panel-body panelMinHgt">
		        
		        <div id="chartdiv1"></div>
		            
		        </div>
		    </div>
		</div>
		</div>
	</div>

	<div class="fancy-collapse-panel usrsFrom">
		<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="usersFrom">
		      <h4 class="panel-title">
		        <div class="panelLft">
		          <img src="{{asset('/').('public/admin_assets/images/users-icon.png')}}" /><span>Users From</span>
		        </div>
		        <div class="panelRight">
		          
		          <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
		            <span class="">
		            
		             <img src="{{asset('/').('public/admin_assets/images/bar-icon.png')}}" /> 
		            </span>
		          </a>
		        </div>
		      </h4>
		    </div>
		    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="usersFrom">
		      <div class="panel-body panelMinHgt">
		      <div id="chartdiv"></div>
		       
		      </div>
		    </div>
		  </div>
		</div>
	</div>

	<ul id="draggablePanelList" class="list-unstyled">
        <li class="panel">
          
        </li>

        <li class="panel">
          
        </li>

        <li class="panel">
          
        </li>
    </ul>

</div>

<script type="text/javascript">
/*search menu click jquery starts*/
$('.showresult').click(function(){
$('.searc_drop').css('display','block');
});

$(document).mouseup(function(e)
{
var container = $(".searc_drop");

if (!container.is(e.target) && container.has(e.target).length === 0)
{
 $('.searc_drop').css('display','none');
}
});


/*notification checkbox starts*/
$(document).ready(function(e) {
$('.setting_drp input').lc_switch();

// triggered each time a field changes status
$('body').delegate('.lcs_check', 'lcs-statuschange', function() {
var status = ($(this).is(':checked')) ? 'checked' : 'unchecked';
console.log('field changed status: '+ status );
});

// triggered each time a field is checked
$('body').delegate('.lcs_check', 'lcs-on', function() {
console.log('field is checked');
});

// triggered each time a is unchecked
$('body').delegate('.lcs_check', 'lcs-off', function() {
console.log('field is unchecked');
});
});
/*notification checkbox ends*/
</script>
<script>
jQuery(function($) {
    var panelList = $('#draggablePanelList');

    panelList.sortable({
       
        handle: '.panel-heading',
        update: function() {
            $('.panel', panelList).each(function(index, elem) {
                 var $listItem = $(elem),
                     newIndex = $listItem.index();
            });
        }
    });
});
</script>
<script>
(function () {
    var Message;
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
    $(function () {
        var getMessageText, message_side, sendMessage;
        message_side = 'right';
        getMessageText = function () {
            var $message_input;
            $message_input = $('.message_input');
            return $message_input.val();
        };
        sendMessage = function (text) {
            var $messages, message;
            if (text.trim() === '') {
                return;
            }
            $('.message_input').val('');
            $messages = $('.messages');
            message_side = message_side === 'left' ? 'right' : 'left';
            message = new Message({
                text: text,
                message_side: message_side
            });
            message.draw();
            return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        };
        $('.send_message').click(function (e) {
            return sendMessage(getMessageText());
        });
        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                return sendMessage(getMessageText());
            }
        });
        sendMessage('Hello Philip! :)');
        setTimeout(function () {
            return sendMessage('Hi Sandy! How are you?');
        }, 1000);
        return setTimeout(function () {
            return sendMessage('I\'m fine, thank you!');
        }, 2000);
    });
}.call(this));
</script>
<script src="{{asset('/').('public/admin_assets/js/moment.min.js')}}"> </script>

<script type = "text/javascript">
  setTimeout(function(){
     document.getElementById("myLoad").style.display="none";
  }, 3000);
</script>

<script src="{{asset('/').('public/admin_assets/js/amcharts.js')}}"></script>
<script src="{{asset('/').('public/admin_assets/js/serial.js')}}"></script>
<script src="{{asset('/').('public/admin_assets/js/export.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('/').('public/admin_assets/css/export.css')}}" type="text/css" media="all" />
<script src="{{asset('/').('public/admin_assets/js/light.js')}}"></script>
<script src="{{asset('/').('public/admin_assets/js/pie.js')}}"></script>
<style>
#chartdiv {
  width   : 100%;
  height    : 350px;
  font-size : 11px;
} 

#chartdiv1 {
  width   : 100%;
  height    : 500px;
  font-size : 11px;
} 

#chartdiv2 {
  width   : 100%;
  height    : 350px;
  font-size : 11px;
}           
</style>

<!-- User from chart -->
<script>
$('document').ready(function(){
  setTimeout(function(){ $('[title="JavaScript charts"]').hide(); }, 3000);
  
})

$.getJSON("{{ URL::to($redirectUrl.'/userFromChart') }}", function (data) {

var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "none",
  "dataProvider": data,
  "valueField": "value",
  "titleField": "users",
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 30,
  "export": {
    "enabled": true
  }
});
});
</script>

<!-- //deposit withdraw chart -->
<script>

function get_dep_with_chart(year,currency) {
	$.getJSON("{{ URL::to($redirectUrl.'/depWithChart?year=') }}"+year+'&currency='+currency, function (data) {

		var chart = AmCharts.makeChart("chartdiv1", {
			"hideCredits":true,
		  	"type": "serial",
		 	"theme": "light",
			"categoryField": "Month",
			"rotate": true,
			"startDuration": 1,
			"categoryAxis": {
		    "gridPosition": "start",
		    "position": "left"
		  },
		  "trendLines": [],
		  "graphs": [
		    {
		      "balloonText": "Withdraw:[[value]]",
		      "fillAlphas": 0.8,
		      "id": "AmGraph-1",
		      "lineAlpha": 0.2,
		      "title": "Withdraw",
		      "type": "column",
		      "valueField": "Withdraw"
		    },
		    {
		      "balloonText": "Deposit:[[value]]",
		      "fillAlphas": 0.8,
		      "id": "AmGraph-2",
		      "lineAlpha": 0.2,
		      "title": "Deposit",
		      "type": "column",
		      "valueField": "Deposit"
		    }
		  ],
			"guides": [],
			"valueAxes": [
		    {
		      "id": "ValueAxis-1",
		      "position": "top",
		      "axisAlpha": 0
		    }
		  ],
			"allLabels": [],
			"balloon": {},
			"titles": [],
			"dataProvider": data,
		    "export": {
		      "enabled": true
	     	}

		});

	});
}

$(function () {
  var dt = new Date();
  year = dt.getFullYear();
  $('#dep_with_chart_year').val(year);
  get_dep_with_chart(year,'BTC');
  $('#dep_with_chart_curr').val('BTC');
});

function set_year_chart() {
  year = $('#dep_with_chart_year').val();
  currency = $('#dep_with_chart_curr').val();
  get_dep_with_chart(year,currency);
}
function set_curr_chart() {
  currency = $('#dep_with_chart_curr').val();
  year = $('#dep_with_chart_year').val();
  get_dep_with_chart(year,currency);
}
</script>

<script>
function getprofit_chart(year,currency) {
	$.getJSON("{{ URL::to($redirectUrl.'/profitChart?year=') }}"+year+'&currency='+currency, function (data) {

		var chart = AmCharts.makeChart("chartdiv2", {
			"hideCredits":true,
		    "theme": "none",
		    "type": "serial",
		    "startDuration": 2,
		    "dataProvider": data,
		    "valueAxes": [{
		        "position": "left",
		        "axisAlpha":0,
		        "gridAlpha":0
		    }],
		    "graphs": [{
		        "balloonText": "[[category]]: <b>[[value]]</b>",
		        "colorField": "color",
		        "fillAlphas": 0.85,
		        "lineAlpha": 0.1,
		        "type": "column",
		        "topRadius":1,
		        "valueField": "profit"
		    }],
		    "depth3D": 40,
		  	"angle": 30,
		    "chartCursor": {
		        "categoryBalloonEnabled": false,
		        "cursorAlpha": 0,
		        "zoomable": false
		    },
		    "categoryField": "Month",
		    "categoryAxis": {
		        "gridPosition": "start",
		        "axisAlpha":0,
		        "gridAlpha":0

		    },
		    "export": {
		      "enabled": true
	     	}
		});
	});
}

$(function () {
  var dt = new Date();
  year = dt.getFullYear();
  $('#dep_with_chart_year2').val(year);
  getprofit_chart(year,'BTC');
  $('#dep_with_chart_curr2').val('BTC');
});

function set_year_chart2() {
  year = $('#dep_with_chart_year2').val();
  currency = $('#dep_with_chart_curr2').val();
  getprofit_chart(year,currency);
}
function set_curr_chart2() {
  currency = $('#dep_with_chart_curr2').val();
  year = $('#dep_with_chart_year2').val();
  getprofit_chart(year,currency);
}
</script>

@stop