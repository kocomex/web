@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="#">Manage Email Template</a></li>
  <li><a href="#">Template List</a></li>
</ul>

<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>Template List</h3>
    </div>
    <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php } ?>

    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S.No.<span class="fa fa-sort"></span></th>
                    <th>Template Name<span class="fa fa-sort"></span></th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                <?php if($email_list) { $ii =1; foreach($email_list as $email) { 
                  $emailId = App\Model\User::endecryption(1,strip_tags($email->id)); ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo strip_tags($email->name); ?></td>
                    <td style="text-align: center;">
                      <a href="{{ URL::to($redirectUrl.'/emailEdit/'.$emailId) }}" class="editUser"><img src="{{asset('/').('public/admin_assets/images/edit-icon.png')}}" title="Edit"  /></a>
                    </td>
                  </tr>
                 <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<script>
$(document).ready(function(){
  $('#data_table_').DataTable();
});
</script>
@stop