@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewemail') }}">Email Template</a></li>
  <li><a href="#">Edit Template</a></li>
</ul>
<div class="inn_content">
  {!! Form::open(array('url' => $redirectUrl.'/updateEmail', 'class'=>'cm_frm1 verti_frm1', 'id'=>'templ_form')) !!}

    <div class="cm_head1">
      <h3>Edit Template</h3>
    </div>
    <input type="hidden" name="id" value="<?php echo App\Model\User::endecryption(1,$email->id); ?>" >

    <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Template Name :</label>
         <input type="text" class="form-control" name="name" id="name" value="<?php echo $email->name; ?>">
      </div>
    </div> 
     <div class="form-group row clearfix">
      <div class="col-sm-9 col-xs-12">
        <label class="form-control-label">Email Subject : </label>
        <input type="text" class="form-control" name="subject" id="subject" value="<?php echo strip_tags($email->subject); ?>">
      </div>
    </div>

    <div class="form-group clearfix">
      <label class="form-control-label">Email Content :</label>
      <textarea class="img-responsive" id="template" name="template"><?php echo $email->template; ?></textarea> 
    </div>
    <ul class="list-inline">
      <li>
        <button type="submit" class="cm_blacbtn1">Submit</button>
      </li>
    </ul>
  {!! Form::close() !!}
</div>

<script src="{{ asset('/').('public/admin_assets/ckeditor/ckeditor.js') }}"> </script>
<script>
  CKEDITOR.replace('template');

  $('#templ_form').validate({
    ignore: [],
    rules:{
      name:{
        required:true,
      },
      subject:{
        required:true,
      },
      template:{
        required: function(textarea) {
           CKEDITOR.instances[textarea.id].updateElement(); // update textarea
           var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
           return editorcontent.length === 0;
        }
      },
    },
    messages:{
       name:{
        required:"Enter Template Name",
      },
      subject:{
        required:"Enter Subject",
      },
      template:{
        required:"Enter content",
      },
    }
  })
</script>

@stop