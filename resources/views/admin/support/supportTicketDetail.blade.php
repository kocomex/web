@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewSupportTicket') }}">Support Tickets</a></li>
  <li><a href="#">Ticket Info</a></li>
</ul>
<div class="inn_content">
	<?php if(Session::has('success')) { ?>
  <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
  <?php } ?>

  <?php if(Session::has('error')) { ?>
  <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
  <?php } ?>

  {!! Form::open(array('url' => $redirectUrl.'/updateTicket', 'class'=>'cm_frm1 verti_frm1', 'enctype' => "multipart/form-data", 'id'=>'ticket_form')) !!}

  <div class="cm_head1">
    <h3>Ticket Info</h3>
  </div>
  <input type="hidden" name="id" value="<?php if($ticket) { echo App\Model\User::endecryption(1,strip_tags($ticket->id)); } else { echo ""; }?>" >
  <input type="hidden" name="reference_no" value="<?php if($ticket) { echo App\Model\User::endecryption(1,strip_tags($ticket->reference_no)); } else { echo ""; }?>" >

  <div class="form-group row clearfix">
    <div class="col-sm-6 col-xs-12 cls_resp50">
      <label class="form-control-label">User Name :</label>
      <label class="form-control"><?php if($ticket) { $userName = App\Model\User::getProfile(strip_tags($ticket->user_id)); echo $userName['user']->consumer_name; } else { echo ""; } ?></label>
    </div>
    <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
    	<label class="form-control-label">Reference ID :</label>
      <label class="form-control"><?php if($ticket) { echo $ticket->reference_no; } else { echo ""; } ?></label>
    </div>
  </div>  

  <div class="form-group row clearfix">
    <div class="col-sm-6 col-xs-12 cls_resp50">
      <label class="form-control-label">Subject :</label>
      <label class="form-control"><?php if($ticket->subject != "") { echo strip_tags($ticket->subject); } else { echo ""; } ?></label>
      <input type="hidden" name="subject" id="subject" value="<?php echo strip_tags($ticket->subject);?>"> 
    </div>
    <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
      <label class="form-control-label">Last updated :</label>
      <label class="form-control"><?php if($ticket) { echo strip_tags($ticket->updated_at); } else { echo ""; } ?></label> 
    </div>
  </div>

  <div class="form-group row clearfix">
    <div class="col-sm-9 col-xs-12">
      <label class="form-control-label">Description :</label>
      <label class="form-control"><?php if($ticket) { echo $ticket->description; } else { echo ""; } ?></label>
    </div>
  </div>

  <?php if($ticket->image != "") { ?>
  <div class="form-group row clearfix">
    <div class="col-sm-9 col-xs-12">
      <label class="form-control-label">Reference image :</label>
      <div class="portrait">
        <img src="<?php echo strip_tags($ticket->image); ?>">
      </div>
    </div>
  </div>
  <?php } ?>

  <div class="cm_head1">
    <h3>Ticket Details</h3>
  </div>

  <div class="row">
  	<div class="col-md-12">
      <div class="ticket_table" id="ticketList">
      	<?php  if($lists) {  foreach($lists as $list) {  
      		if($list->user_id == 0) {
      	?>
    		  <h3>Admin</h3>
      		Message :  <?php echo $list->description; ?><br>
      		image : <?php if($list->image!='') { ?> <img style="width:6%; " src="<?php echo $list->image; ?>"> <?php }else { echo "--"; } ?> 
                <br>
        <?php } else { $userName = App\Model\User::getProfile($list->user_id); ?>
        	<h3><?php echo $userName['user']->consumer_name; ?></h3>
      		Message :  <?php echo $list->description; ?><br>
      		image : <?php if($list->image!='') { ?> <img style="width:6%; " src="<?php echo $list->image; ?>"> <?php }else { echo "--"; } ?> 
                <br>
      	<?php } } } ?>
      </div>
    </div>
  </div>

  <div class="clearfix">
    <h3></h3>
  </div>
  <div class="clearfix">
    <h3></h3>
  </div>

  <?php if($ticket->ticket_status=='active') { ?>
  <div class="cm_head1">
    <h3>Reply</h3>
  </div>

	<div class="form-group clearfix">
		<div class="col-sm-9 col-xs-12">
      <label class="form-control-label">Reply to this message</label>
      <textarea class="form-control " id="content" name="content" placeholder="Message"></textarea> 
  	</div>
	</div>

	<div class="form-group clearfix">
		<div class="col-sm-9 col-xs-12">
      <ul class="list-inline stng_lis1">
      	<li class="sfd1">
      		<label class="form-control-label">Upload Image</label>
      		<div class="input-group file-upload">
      		  <input id="supportTicket" class="form-control" name="supportimage" placeholder="Upload image" disabled="disabled">
      		  <div class="input-group-addon">
      		    <div class="fileUpload btn btn-primary"> <span> Upload </span>
      		      <input id="supportimage" class="upload" name="supportimage" type="file">
      		    </div>
      		  </div>
      		</div>
      	</li>
      	<li class="sfd2">
      		<div class="portrait">
      			<img class="img-responsive" id="support_image">
      		</div>
      	</li>
      </ul>
    </div>
	</div>

 	<div class="form-group row clearfix">
    <div class="col-md-2 col-sm-3 col-xs-12">
    	<label class="form-control-label">Ticket Status</label>
			<div class="select_style1">
				<select name="status">
					<option value="active">Active</option>
					<option value="close">Close</option>              
				</select>
			</div>
    </div>
	</div>

  <ul class="list-inline">
    <li>
      <button type="submit" class="cm_blacbtn1">Send</button>
    </li>
    <li></li>
  </ul>
  {!! Form::close() !!}
  <?php } ?>
</div>

<style type="text/css">
.portrait {
  width: 300px;
  overflow: hidden;
}

.portrait img {
  width: 100%;
  height: 100%;
  max-height: 100%;
}

.ticket_table {
	max-height: 320px;
	overflow: hidden;
}
</style>
<!-- scrollbar scripts -->
<script src="{{asset('/').('public/frontend/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<link href="{{asset('/').('public/frontend/css/jquery.mCustomScrollbar.css')}}" rel="stylesheet">
<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#support_image').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#supportimage").change(function(){
  readURL(this);
});

(function ($) {
  $(window).on("load", function () {
    var width = $(window).width();
    if (width > 1024) {
      //    big screen 
      $(".ticket_table").mCustomScrollbar({
        scrollButtons: {
          enable: false
        },
        scrollbarPosition: 'inside',
        autoExpandScrollbar: true,
        theme: 'minimal-dark',
        axis: "y",
        setWidth: "auto"
      });
    } else {
      $(".ticket_table").mCustomScrollbar({
        scrollButtons: {
          enable: false
        },
        scrollbarPosition: 'inside',
        autoExpandScrollbar: true,
        theme: 'minimal-dark',
        axis: "x",
        setWidth: "auto"
      });
    }
  });
})(jQuery);

$(document).ready(function(){
  $(".ticket_table").mCustomScrollbar({
	}).mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
})
</script>
<script>
  $('#ticket_form').validate({
    ignore:"",
    rules:{
      content:{
        required: true,
      },
    },
    messages:{
      content:{
        required:"Enter reply message",
      },
    }
  })
</script>

@stop