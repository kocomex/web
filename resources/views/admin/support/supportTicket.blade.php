@extends('admin.layouts/admin')
@section('content')

<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewSupportTicket') }}">Support Ticket</a></li>
  <li><a href="#">Ticket List</a></li>
</ul>
<div class="inn_content">
  <form class="cm_frm1 verti_frm1">
    <div class="cm_head1">
      <h3>Support Ticket List</h3>
    </div>
    
    <?php if(Session::has('success')) { ?>
    <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
    <?php } ?>

    <?php if(Session::has('error')) { ?>
    <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
    <?php } ?>
    
    <div class="cm_tablesc1 dep_tablesc mb-20">
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
          <div class="col-sm-12">
            <div class="cm_tableh3 table-responsive">
              <table class="table m-0" id="data_table_">
                <thead>
                  <tr>
                    <th>S.No.<span class="fa fa-sort"></span></th>
                    <th>Username<span class="fa fa-sort"></span></th>
                    <th>Subject<span class="fa fa-sort"></span></th> 
                    <th>Ticket Status<span class="fa fa-sort"></span></th> 
                    <th>View</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  if($tickets) { $ii = 1; foreach($tickets as $ticket) { 
                    $tId = App\Model\User::endecryption(1,strip_tags($ticket->id));
                    $uName = App\Model\User::getProfile(strip_tags($ticket->user_id));
                  ?>
                  <tr>
                    <td><?php echo $ii; ?></td>
                    <td><?php echo $uName['user']->consumer_name; ?></td>
                    <td><?php echo strip_tags($ticket->subject); ?></td>
                    <td><span class="clsCtlr <?php if($ticket->ticket_status == "active") { echo "clsActive"; } else { echo "clsDeactive"; } ?>"><?php echo strip_tags($ticket->ticket_status); ?></span>
                    </td>
                    <td style="text-align: center;">
                      <a href="{{ URL::to($redirectUrl.'/ticketReply/'.$tId) }}" class="editUser">
                      <span class="glyphicon glyphicon-eye-open" style="color: #4f5259; vertical-align: middle;" title="View"></span>
                      </a>
                    </td>
                  </tr>
                <?php $ii++; } } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<script>
$(document).ready(function(){
    $('#data_table_').DataTable();
});
</script>

  @stop