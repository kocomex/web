
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>WIIX Admin panel | Dashboard </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  @include('layouts.styles')
  <style>
    .alert {
      padding: 20px;
      background-color: #ECF0F5;
      color: #000;
    }

    .closebtn {
      margin-left: 15px;
      color: #000;
      font-weight: bold;
      float: right;
      font-size: 22px;
      line-height: 20px;
      cursor: pointer;
      transition: 0.3s;
    }

    .closebtn:hover {
      color: #ECF0F5;
    }
  </style>
</head>

<body>
  <div class="main-wrapper">
    <div class="app" id="app">
     @include('layouts.header')
     
     
     @include('layouts.sidebar')
     
     <div class="sidebar-overlay" id="sidebar-overlay"></div>
     
     <article class="content dashboard-page">
      <section class="section">
       <a href="{{ url('/WiPlytaIIX2/enquires') }}" class="btn btn-pill-left btn-danger pull-right">Back to Messages</a><br><br><br>
       <div class="row">
         <?php if($enquiry_details){ 
          

           ?>
           <div class="col-md-6">
            <div class="card sameheight-item stats" data-exclude="xs">
             <div class="card-block">
              <div class="title-block"><h4 class="title">Mail details </h4>
              </div>
              
              <div class="box-body">
               
               <table class="table table-striped">
                 <tbody>
                  <tr>
                    <th scope="row">Name</th>
                    <td><?php echo $enquiry_details->user_name; ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Email</th>
                    <td><?php echo $enquiry_details->user_mail; ?></td>
                  </tr>
             
                  <tr>
                    <th scope="row">Subject</th>
                    <td><?php echo $enquiry_details->user_subject; ?></td>
                    
                  </tr>
                  <tr>
                    <th scope="row">Message</th>
                    <td><?php echo $enquiry_details->user_message; ?></td>
                    
                  </tr>
                </tbody>
              </table>
              
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
      <!-- /.col (left) -->
      <?php if($enquiry_details->status!=1){ ?>
      <div class="col-md-6">
        <div class="card sameheight-item stats" data-exclude="xs">
         <div class="card-block">
          <div class="title-block"><h4 class="title">Reply</h4>
          </div>
          <div class="box-body">
            <form role="form" id="settings" name="settings" action="{{ url('/WiPlytaIIX2/view_enquiry/'.$id) }}" method="post">

             {!! csrf_field() !!}
             
             <div class="form-group">
              <label for="exampleInputPassword1">Reply Message</label>
              <textarea  class="form-control " id="address" name="reply" rows="3" cols="50"></textarea>
            </div>
            <!-- /.box-body -->
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
  
  
  
  <?php }else{ ?>
  
  <div class="col-md-6">
    <div class="card sameheight-item stats" data-exclude="xs">
     <div class="card-block">
      <div class="title-block"><h4 class="title">Replied Message</h4>
      </div>
      <div class="box-body">
       <div class="alert">
        
        <?php echo $enquiry_details->reply_msg; ?>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>	
</div>	

<?php }  } ?>



<!-- /.col (right) -->
</div>


</section>
</article>
@include('layouts.footer')
</div>
</div>

@include('layouts.scripts')

</body>
</html>
