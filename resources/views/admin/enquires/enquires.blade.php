
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>WIIX Admin panel | Dashboard </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	@include('layouts.styles')
	<style>
		.row .col {
			padding-left: 2px !important;
			padding-right: 2px !important;
			float: left !important;
		}
	</style>
</head>


<body>
	<div class="main-wrapper">
		<div class="app" id="app">
			@include('layouts.header')
			
			
			@include('layouts.sidebar')
			
			<div class="sidebar-overlay" id="sidebar-overlay"></div>
			
			<article class="content dashboard-page">
				<section class="section">
					
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>S.no</th>
								<th>Email Id</th>
								<th>Enquiry Date</th>
								<th>Subject</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							
							@if($enquires)
							<?php $i=0; ?>
							@foreach($enquires as $enquiry)
							
							<tr>
								<td><?php echo ++$i;?> </td>
								<td>{{$enquiry->user_mail}}</td>
								<td>{{$enquiry->created_on}}</td>
								<td>{{$enquiry->user_subject}}</td>
								<td><a href="{{ url('/WiPlytaIIX2/view_enquiry/' . $enquiry->id) }}" class="btn btn-block btn-info btn-sm"><i class="fa <?php echo ($enquiry->status==1)?"fa-envelope":"fa-fw fa-mail-reply"; ?>"></i>  <?php echo ($enquiry->status==1)?"View":"Reply"; ?></a></td>
							</tr>
							@endforeach
							@else
							
							<tr>
								<td colspan="3">No Enquired Found</td>
							</tr>
							@endif
						</tbody>
					</table>
					
				</section>
			</article>
			@include('layouts.footer')
		</div>
	</div>
	
	@include('layouts.scripts')
	<!-- DataTables -->
	<script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>

	<script>
		$(function () {
			$("#example1").DataTable();
		});
	</script>

	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	<script>
		$(function () {
			
			CKEDITOR.replace('content');

		});
	</script>
</body>
</html>
