
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>WIIX Admin panel | List All users </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
       @include('layouts.styles')
           <!-- DataTables -->
<link rel="stylesheet" href="{{ URL::asset('theme/datatables/dataTables.bootstrap.css') }}"> 
  
    </head>

    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
		  @include('layouts.header')
               
               
            @include('layouts.sidebar')
            
            
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                
                <article class="content dashboard-page">
                     <section class="section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block">
                                        <section class="example">
					
			          <div class="table-flip-scroll">
                            <table id="example1" class="table table-striped table-bordered table-hover flip-content">
					     <thead class="flip-header">
						<tr>
						<th>S.No</th>
						<th>User email</th>
						         <th>Date and Time</th>
                         
                          <th>Address / Transaction ID</th>
                         
                          <th>Amount</th>
                          <th>Fee</th>
                          <th>Final amount</th>
                          <th>Status</th>
                          <th>Action</th>
						</tr>
					</thead>
					<tbody>
						 
						@if($details)
							<?php $i=1; ?>
							@foreach($details as $det)


                      <?php  if($det->wit_status == '0')
                        {
                          $sta=   '<span class="tx_orng2">Pending</span>';
                        }
                        else  if($det->wit_status == '1')
                        {
                          $sta= '<span class="tx_orng2">Approved</span>';
                        }
                           else  if($det->wit_status == '2')
                        {
                          $sta= '<span class="tx_orng2">User cancelled</span>';
                        }
                           else  if($det->wit_status == '3')
                        {
                          $sta= '<span class="tx_orng2">waiting</span>';
                        }
                            else  if($det->wit_status == '4')
                        {
                          $sta= '<span class="tx_orng2">Waiting for admin mail confirmation</span>';
                        }
                         else  if($det->wit_status == '5')
                        {
                          $sta= '<span class="tx_orng2">Admin cancelled</span>';
                        }
                        else{
                          $sta='';
                        }


                      
					?>
						<tr>
						        <td><?php echo $i++; ?></td>

		<?php


	 $get_userid=$det->user_id;


		$get_users1=DB::table('wiix_consumers')->where('user_id',$get_userid)->get();
		$get_users=$get_users1[0];

		$email[0] = Helper::encrypt_decrypt("decrypt",$get_users->secret_key);
		$email[1] = Helper::encrypt_decrypt("decrypt",$get_users->display);
		$secure_email=$email[0]."@".$email[1];

		
		?>

						        <td>{{$secure_email}}</td>
						        <td>{{$det->with_created_date}}</td>
   
                          
                          @if($det->wit_currency == "KRW")
                          
                          <td>{{$det->txn_id}}</td>
                          @else
                      
                        <td>{{$det->crypto_address}}
                       @endif
                       </td>

                       @if($det->wit_currency == "KRW")
                        
                          <td>{{ number_format($det->wit_amount,2).$det->wit_currency}}</td>
                           <td>{{ number_format($det->wit_fee,2)}}</td>
                            <td>{{ number_format($det->wil_get,2)}}</td>
                            @else
                             <td>{{ number_format($det->wit_amount,8).$det->wit_currency}}</td>
                           <td>{{ number_format($det->wit_fee,8)}}</td>
                            <td>{{ number_format($det->wil_get,8)}}</td>
                            @endif
                          <td><span class="tx_orng2"> {!! $sta !!}</span></td>
						        	      
						     
						
						    <td><a class="btn btn-pill-right btn-warning" href="javascript:dep_popup(<?php echo $det->id; ?>);"><i class="fa fa-fw fa-user"></i> View</a>


						   
						  
                                     
						    
						    </td>
					
					
					</tr>
					@endforeach
					@endif
					</tbody>
				</table>
			
                         
                      </div>
                      <div class="modal fade" tabindex="-1" role="dialog" id="dep_popup" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Withdraw Details</h4>
			</div>
			<div class="modal-body" id="dep_details">
				<p>One fine body&hellip;</p>
			</div>
			                  <div id="loader1" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- OTP model -->
                      <div class="modal fade" tabindex="-1" role="dialog" id="with_otpacceptpopup">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header suc">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Withdraw OTP -Approve</h4>
			</div><div class="modal-body" id="with_otpacceptdetails">
				<p>One fine body&hellip;</p>
			</div>
			                       <div id="loaderotp" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            </div>
            <div id="with_accerror" style="color:red;font-size: 100%;text-align:center;"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


                    <div class="modal fade" tabindex="-1" role="dialog" id="with_otprejectdetails" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header rej">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Withdraw OTP -Reject</h4>
			</div>
			<div class="modal-body" id="with_rejectdetails">
				<p>One fine body&hellip;</p>
			</div>
			                       <div id="loaderrejotp" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            </div>
            <div id="with_rejecterror" style="color:red;font-size: 100%;text-align:center;"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

                      </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>
              @include('layouts.footer')
             </div>
        </div>

     @include('layouts.scripts')
     
     <!-- DataTables -->
<script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
 <script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
	$("#example1").DataTable({
		 "pagingType": "full_numbers"
		});
   });
</script>
<script type="text/javascript">
	
	function dep_popup(id){
		
		$.ajax({
			type: 'GET',
			url: "{{ url('/WiPlytaIIX2/viewWithhistory') }}"+'/'+id,
			success: function (data) {
				$("#dep_details").html(data);
				$('#dep_popup').modal('show');
			}
		});
	}

	function rejectdeposit(id){
		

		
		$.ajax({
			type: $('#dep_approval').attr('method'),
			 dataType : 'json',
			url: "{{ url('/WiPlytaIIX2/withdrawApproval') }}"+"/"+id+"-reject",
			data: {
        "_token": "{{ csrf_token() }}",
        "comment": $("#comment").val(),
        "otp_accept": $("#otp_accept").val()
        },
         beforeSend:function(){
       
         $('#loaderrejotp').show();
        $('#withacceptotp').hide();
        $('#withrejectotp').hide();
        
      },
			success: function (data) {
				
					$('#loaderrejotp').hide();
					 $('#withacceptotp').show();
        $('#withrejectotp').show();


				if(data.status)
				{
					
				location.reload();
				}
				else{
					
 $('#with_rejecterror').css('display', '');
        $('#with_rejecterror').html("Enter a vaild OTP code");
        $('#with_rejecterror').delay(2000).fadeOut(1400); 
				}
			}
		});
	}
	
	function acceptdeposit(id){
		
		$.ajax({
			type: $('#dep_approval').attr('method'),
			dataType : 'json',
			url: "{{ url('/WiPlytaIIX2/withdrawApproval') }}"+"/"+id+"-accept",
			data: {
        "_token": "{{ csrf_token() }}",
        "comment": $("#comment").val(),
        "otp_accept": $("#otp_accept").val()
        },
         beforeSend:function(){
       
         $('#loaderotp').show();

        
                $('#withacceptotp').hide();
        $('#withrejectotp').hide();
        
      },
			success: function (data) {
				
				 $('#loaderotp').hide();
				 					 $('#withacceptotp').show();
        $('#withrejectotp').show();
				if(data.status)
				{
				location.reload();
				}
				else{
 $('#with_accerror').css('display', '');
        $('#with_accerror').html("Enter a valid OTP");
        $('#with_accerror').delay(2000).fadeOut(1400); 
				}
			}
		});
	}
	function withacceptotp(id){

		
		$.ajax({
			type: 'post',
			url: "{{ url('/WiPlytaIIX2/withacceptotp') }}"+"/"+id,
			data: {
        "_token": "{{ csrf_token() }}",
       
        },
          beforeSend:function(){
        $('#loader1').show();
        
        
      },
			success: function (data) {
				$('#loader1').hide();
				$("#with_otpacceptdetails").html(data);
				$('#with_otpacceptpopup').modal('show');
			}
		});
	}
function withrejectotp(id){

		
		$.ajax({
			type: 'post',
			url: "{{ url('/WiPlytaIIX2/withrejectotp') }}"+"/"+id,
			data: {
        "_token": "{{ csrf_token() }}",
       
        },
          beforeSend:function(){
        $('#loader1').show();
        
        
      },
			success: function (data) {
				$('#loader1').hide();
				$("#with_rejectdetails").html(data);
				$('#with_otprejectdetails').modal('show');
			}
		});
	}
	
	function sendOTP(){

		
		$.ajax({
			type: 'post',
			 dataType : 'json',
			url: "{{ url('/WiPlytaIIX2/generate_transid') }}",
			data: {
        "_token": "{{ csrf_token() }}",
       
        },
          beforeSend:function(){
       $('#loaderotp').show();
       $('#loaderrejotp').show();
        
        
      },
			success: function (data) {
				$('#loaderotp').hide();
				$('#loaderrejotp').hide();

				alert("OTP send to your mobile.Kindly check it");
			}
		});
	}
					    
</script>
    <style type="text/css">
.modal-header.suc {
    background-color: #3A89B4;
}
.modal-header.rej {
    background-color: #F44A4A;
}
a.btn.btn-oval.btn-info {
    background-color: #3A89B4;
    border-color: #3A89B4;
}
</style>
    </body>

</html>
