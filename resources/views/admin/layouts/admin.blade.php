<!doctype html>
<html>
<head>
    @include('admin.layouts.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    @include('admin.layouts.header')

    @include('admin.layouts.sidebar')
    
    <div class="content-wrapper">

            @yield('content')

    </div>

    <footer>
        @include('admin.layouts.footer')
    </footer>

</div>
</body>
</html>

