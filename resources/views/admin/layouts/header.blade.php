<header class="main-header">
<?php 
  $getSite = App\Model\User::getSiteLogo();
?>
  <div class="tp_layer1">
    <a href="{{ URL::to($redirectUrl) }}" class="logo">
      <span class="logo-lg"><img src="{{ $getSite->site_logo }}" alt="Wiix"></span>
    </a> 
  </div>
  <nav class="navbar navbar-static-top">
    <div class="mn_righ">
      <div class="mn_rightp fd_rw">
        <div class="tp_sear1">
        </div>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-right">
            <li class="dropdown" id="decreasecount"> <a data-toggle="dropdown" class="dropdown-toggle drop_icocn"><span class="mn_ico1 mn_imbel"><img src="{{asset('/').('public/admin_assets/images/mn_imbel.png')}}"> <sup id="re_count"><?php echo App\Model\SubAdmin::getNotificationCount() ?></sup> </span></a>
              <?php $notify = App\Model\SubAdmin::getAdminNotifcation(); ?>
              <ul class="dropdown-menu anti_dropdown aler_drop notscroll">

                <?php
                  $url = array(
                    'Withdraw'=>URL::to($redirectUrl.'/viewWithdraw'),
                    'Deposit'=>URL::to($redirectUrl.'/viewDeposit'),
                    'New-user'=>URL::to($redirectUrl.'/viewUser'),
                    'KYC'=>URL::to($redirectUrl.'/viewUser'),
                    'Contact'=>URL::to($redirectUrl.'/viewContactUs'),
                    'Support'=>URL::to($redirectUrl.'/viewHelpCentre'),
                    'BANK'=>URL::to($redirectUrl.'/viewUserBank')
                  );
                  if($notify != "") { foreach($notify as $not) {
                ?>
                <li class="<?php if($not->status == "unread"){ echo "succ_nrw"; } else { echo "prim_nrw"; } ?> noti_rw1">
                  <a href="<?php echo $url[$not->type]; ?>">
                    <div class="fd_rw">
                      <div class="notification_desc">
                        <p>{{$not->message}}</p>
                        <p class="hr_tx"><?php echo App\Model\SubAdmin::getTimeAgo($not->created_at) ?></p>
                      </div>
                    </div>
                  </a>
                </li>
                <?php } } else { ?>
                <li class="noti_rw1">
                  <div class="fd_rw">
                    <div class="notification_desc">
                      <p>No notifications found!</p>
                    </div>
                  </div>
                </li>
                <?php } ?>
              </ul>
            </li>
            <?php
              $getProfile = App\Model\SubAdmin::getProfile(Session::get('adminId'));
            ?>
            <!-- user -->
            <li class="dropdown dropdown-user"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <div class="user-image"><span class="hidden-xs">Hi, {{ $getProfile['admin']->username }}</span> <img src="{{asset('/').('public/admin_assets/images/mn_imusr.png')}}" class="img-responsive" alt="User"> </div>
              </a>
              <ul class="dropdown-menu usr_drpmn">
                <!-- User image -->
                <li class="user-header">
                  <div class="usr_mask">
                    <p> {{ $getProfile['admin']->username }} </p>
                  </div>
                  <div class="admin_profile_icon">
                    <?php if($getProfile['admin']->profile == "") { ?>
                    <img src="{{asset('/').('public/admin_assets/images/default-avatar.png')}}" class="img-responsive" alt="User Image">
                    <?php } else { ?>
                    <img src="{{ $getProfile['admin']->profile }}" class="img-responsive" alt="User Image">
                    <?php } ?>
                  </div>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="">
                    <div class="col-xs-6"> <a href="{{ URL::to($redirectUrl.'/profile') }}" class="active">Profile</a> </div>
                    <?php if(Session::get('adminRole') == "admin") { ?>
                    <div class="col-xs-6"> <a href="{{ URL::to($redirectUrl.'/settings') }}" class="active">Settings</a> </div>
                    <?php } ?>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer text-center"> <a href="{{ URL::to($redirectUrl.'/logout') }}" class="btn btn-flat center-block">Logout</a> </li>
              </ul>
            </li>
            
          </ul>
        </div>
      </div>
    </div>
  </nav>
</header>