<script src="{{asset('/').('public/admin_assets/js/jquery-ui.js')}}"> </script>
<script src="{{asset('/').('public/admin_assets/js/bootstrap.min.js')}}"> </script>
<script src="{{asset('/').('public/admin_assets/js/dashboard.js')}}"> </script>
<script src="{{asset('/').('public/admin_assets/js/lc_switch.js')}}"> </script>
<link rel="stylesheet" href="{{asset('/').('public/admin_assets/css/lc_switch.css')}}">
<link rel="stylesheet" href="{{asset('/').('public/admin_assets/css/jquery.dataTables.min.css')}}">

<script src="{{asset('/').('public/admin_assets/js/jquery.dataTables.min.js')}}"> </script>

<script>
jQuery.validator.addMethod("validEmail", function(value, element) {
  return this.optional( element ) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test( value );
}, 'Please enter a valid email address.');

$(document).ajaxSuccess(function(event, jqXHR, settings) { 
    resetAjaxToken(jqXHR);
});

function resetAjaxToken(jqXHR) {  
  var token = jqXHR.getResponseHeader("CI-CSRF-Token"); 
  $('input[name="csrf_test_name"]').val(token);
}

$(document).ready(function(){
  setTimeout(function() {
    $('.alert').fadeOut('slow');
  }, 4000); // <-- time in milliseconds
});

$(document).ready(function() {
  (function(seconds) {
    var refresh,       
    intvrefresh = function() {
      clearInterval(refresh);
      refresh = setTimeout(function() {
         location.href = "{{ URL::to($redirectUrl.'/logout') }}";
      }, seconds * 60000);
    };
  $(document).on('keypress click mousemove', function() { intvrefresh() });
  intvrefresh();
  }(5)); 

  window.onbeforeunload = function () {
    location.href = "{{ URL::to($redirectUrl.'/logout') }}";
  };
});
</script>
<link rel="stylesheet" href="{{asset('/').('public/admin_assets/css/jquery.mCustomScrollbar.css')}}">
<script src="{{asset('/').('public/admin_assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script>
(function($){
  $(window).on("load",function(){
    var height = $(window).height();
    $(".notscroll").mCustomScrollbar({
      scrollButtons:{
        enable:false
      },
      scrollbarPosition: 'inside',
      autoExpandScrollbar:true,
      theme: 'minimal-dark',
      axis:"y",
      setWidth: "auto"
    });
  });
})(jQuery);
</script>