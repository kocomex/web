<aside class="main-sidebar">
    <div class="sidebar">
      <!-- sidebar menu -->
      <ul class="sidebar-menu" id="accord_side">
        <li class="hidden-xs"> 
            <a class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="fa fa-bars hdt_cnt">Menu</span> </a>
        </li>
        <li> 
            <a href="{{ URL::to($redirectUrl) }}" class="mn_catgcur fa fa-tachometer"><span>Dashboard</span></a> 
        </li>
        <?php 
          $currentRoute = \Route::getCurrentRoute()->getActionName();
          $explodeRoute = explode('@', $currentRoute);
          $uri = $explodeRoute[1];
        ?>
       
       <?php if(Session::get('adminId') == "1" && Session::get('adminRole') == "admin") { ?> 
        <li class="childcateg <?php if($uri == "userList" || $uri == "viewUserBank" || $uri == "viewUserBalance") { echo "active"; } ?>"> <a class="mn_sib fa fa-users" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn2" aria-expanded="true"><span>Manage Users</span></a>
          <ul id="sid_mn2" class="mnsub_catglis collapse <?php if($uri == "userList" || $uri == "viewUserBank" || $uri == "viewUserBalance") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewuser') }}" class="mnsub_catg fa fa-list-ul">User List</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewUserBank') }}" class="mnsub_catg fa fa-university">User Bank Info</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewUserBalance') }}" class="mnsub_catg fa fa-credit-card">User Balance</a></li>
          </ul>
        </li>

        <li class="childcateg <?php if($uri == "viewSubadmin" || $uri == "loginHistory") { echo "active"; } ?>"> <a class="mn_sib fa fa-user" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn1" aria-expanded="true"><span>Manage SubAdmin</span></a>
          <ul id="sid_mn1" class="mnsub_catglis collapse <?php if($uri == "viewSubadmin" || $uri == "addSubadmin" || $uri == "loginHistory") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewSubadmin') }}" class="mnsub_catg fa fa-user-secret">View Subadmin</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/loginHistory') }}" class="mnsub_catg fa fa-history">Admin Log history</a></li>
          </ul>
        </li> 

        <li class="childcateg <?php if($uri == "viewDeposit" || $uri == "viewWithdraw" || $uri == "viewWithdrawSettings") { echo "active"; } ?>">
            <a class="mn_sib fa fa-exchange" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn4" aria-expanded="true"> <span>Manage Transactions</span> </a>
            <ul id="sid_mn4" class="mnsub_catglis collapse <?php if($uri == "viewDeposit" || $uri == "viewWithdraw" || $uri == "viewWithdrawSettings") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewDeposit') }}" class="mnsub_catg fa fa-cloud-upload">Deposit History</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewWithdraw') }}" class="mnsub_catg fa fa-cloud-download">Withdraw History</a></li>
          </ul>
        </li>

        <li class="childcateg <?php if($uri == "viewTradePairs" || $uri == "viewTradeFee" || $uri == "viewOrderHistory" || $uri == "viewTradeHistory") { echo "active"; } ?>">
            <a class="mn_sib fa fa-line-chart" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn6" aria-expanded="true"> <span>Trade Management</span> </a>
            <ul id="sid_mn6" class="mnsub_catglis collapse <?php if($uri == "viewTradePairs" || $uri == "viewTradeFee" || $uri == "viewOrderHistory" || $uri == "viewTradeHistory") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewTradePairs') }}" class="mnsub_catg fa fa-hand-peace-o">Trading Pairs</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewTradeFee') }}" class="mnsub_catg fa fa-money">Trading Fees Structure</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewOrderHistory') }}" class="mnsub_catg fa fa-history">Orders History</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewTradeHistory') }}" class="mnsub_catg fa fa-bar-chart">Trade History</a></li>
          </ul>
        </li>

        <li class="<?php if($uri == "viewAdminProfit") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewAdminProfit') }}" class="mn_catgcur fa fa-cart-plus"><span>Admin Profit</span> </a>
        </li>

        <li class="<?php if($uri == "viewAdminBank") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewAdminBank') }}" class="mn_catgcur fa fa-university"><span>Admin Bank Info</span> </a>
        </li>

        <li class="<?php if($uri == "viewBlockIp") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewBlockIp') }}" class="mn_catgcur fa fa-user-times"><span>Block IP Address</span> </a>
        </li>

        <li class="<?php if($uri == "viewSupportTicket") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewSupportTicket') }}" class="mn_catgcur fa fa-ticket"><span>Support Ticket</span> </a>
        </li>

        <li class="<?php if($uri == "viewContactUs") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewContactUs') }}" class="mn_catgcur fa fa-comment-o"><span>Contact US</span> </a>
        </li>

        <li class="<?php if($uri == "viewFaq") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewfaq') }}" class="mn_catgcur fa fa-question"><span>Manage FAQ</span> </a>
        </li>

        <li class="<?php if($uri == "viewEmail") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewemail') }}" class="mn_catgcur fa fa-envelope"><span>Email Template</span> </a>
        </li>

        <li class="childcateg <?php if($uri == "viewCms") { echo "active"; } ?>">
            <a class="mn_sib fa fa-files-o" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn3" aria-expanded="true"> <span>CMS Management</span> </a>
            <ul id="sid_mn3" class="mnsub_catglis collapse <?php if($uri == "viewCms") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewcms/page') }}" class="mnsub_catg fa fa-clone">CMS Pages</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewcms/content') }}" class="mnsub_catg fa fa-folder-open-o">CMS Static content</a></li>
          </ul>
        </li>

        <li class="<?php if($uri == "viewMeta") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewmeta') }}" class="mn_catgcur fa fa-code"><span>Meta Tags</span> </a>
        </li>

        <?php } else { 
            $permis = App\Model\SubAdmin::getPermission(Session::get('adminId'));
            $permis = explode(',',$permis);
        ?>

        <?php if(in_array("1", $permis)) { ?>
        <li class="childcateg <?php if($uri == "userList" || $uri == "viewUserBank" || $uri == "viewUserBalance") { echo "active"; } ?>"> <a class="mn_sib fa fa-users" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn2" aria-expanded="true"><span>Manage Users</span></a>
          <ul id="sid_mn2" class="mnsub_catglis collapse <?php if($uri == "userList" || $uri == "viewUserBank" || $uri == "viewUserBalance") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewuser') }}" class="mnsub_catg fa fa-list-ul">User List</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewUserBank') }}" class="mnsub_catg fa fa-university">User Bank Info</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewUserBalance') }}" class="mnsub_catg fa fa-credit-card">User Balance</a></li>
          </ul>
        </li>
        <?php } ?>

        <?php if(in_array("11", $permis)) { ?>
        <li class="childcateg <?php if($uri == "viewDeposit" || $uri == "viewWithdraw" || $uri == "viewWithdrawSettings") { echo "active"; } ?>">
            <a class="mn_sib fa fa-exchange" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn4" aria-expanded="true"> <span>Manage Transactions</span> </a>
            <ul id="sid_mn4" class="mnsub_catglis collapse <?php if($uri == "viewDeposit" || $uri == "viewWithdraw" || $uri == "viewWithdrawSettings") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewDeposit') }}" class="mnsub_catg fa fa-cloud-upload">Deposit History</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewWithdraw') }}" class="mnsub_catg fa fa-cloud-download">Withdraw History</a></li>
          </ul>
        </li>
        <?php } ?>

        <?php if(in_array("10", $permis)) { ?>
        <li class="childcateg <?php if($uri == "viewTradePairs" || $uri == "viewTradeFee" || $uri == "viewOrderHistory" || $uri == "viewTradeHistory") { echo "active"; } ?>">
            <a class="mn_sib fa fa-line-chart" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn6" aria-expanded="true"> <span>Trade Management</span> </a>
            <ul id="sid_mn6" class="mnsub_catglis collapse <?php if($uri == "viewTradePairs" || $uri == "viewTradeFee" || $uri == "viewOrderHistory" || $uri == "viewTradeHistory") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewTradePairs') }}" class="mnsub_catg fa fa-hand-peace-o">Trading Pairs</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewTradeFee') }}" class="mnsub_catg fa fa-money">Trading Fees Structure</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewOrderHistory') }}" class="mnsub_catg fa fa-history">Orders History</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewTradeHistory') }}" class="mnsub_catg fa fa-bar-chart">Trade History</a></li>
          </ul>
        </li>
        <?php } ?>

        <?php if(in_array("9", $permis)) { ?>
        <li class="<?php if($uri == "viewAdminProfit") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewAdminProfit') }}" class="mn_catgcur fa fa-cart-plus"><span>Admin Profit</span> </a>
        </li>

        <li class="<?php if($uri == "viewAdminBank") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewAdminBank') }}" class="mn_catgcur fa fa-university"><span>Admin Bank Info</span> </a>
        </li>
        <?php } ?>

        <?php if(in_array("8", $permis)) { ?>
        <li class="<?php if($uri == "viewBlockIp") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewBlockIp') }}" class="mn_catgcur fa fa-user-times"><span>Block IP Address</span> </a>
        </li>
        <?php } ?>

        <?php if(in_array("4", $permis)) { ?>
        <li class="<?php if($uri == "viewFaq") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewfaq') }}" class="mn_catgcur fa fa-question"><span>Manage FAQ</span> </a>
        </li>
        <?php } ?>

        <?php if(in_array("2", $permis)) { ?>
        <li class="<?php if($uri == "viewContactUs") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewContactUs') }}" class="mn_catgcur fa fa-comment-o"><span>Contact US</span> </a>
        </li>
        <?php } ?>

        <?php if(in_array("5", $permis)) { ?>
        <li class="<?php if($uri == "viewEmail") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewemail') }}" class="mn_catgcur fa fa-envelope"><span>Email Template</span> </a>
        </li>
        <?php } ?>

        <?php if(in_array("6", $permis)) { ?>
        <li class="childcateg <?php if($uri == "viewCms") { echo "active"; } ?>">
            <a class="mn_sib fa fa-files-o" data-toggle="collapse" data-parent="#accord_side" href="#sid_mn3" aria-expanded="true"> <span>CMS Management</span> </a>
            <ul id="sid_mn3" class="mnsub_catglis collapse <?php if($uri == "viewCms") { echo "in"; } ?>" aria-expanded="true" style="">
            <li><a href="{{ URL::to($redirectUrl.'/viewcms/page') }}" class="mnsub_catg fa fa-clone">CMS Pages</a></li>
            <li><a href="{{ URL::to($redirectUrl.'/viewcms/content') }}" class="mnsub_catg fa fa-folder-open-o">CMS Static content</a></li>
          </ul>
        </li>
        <?php } ?>

        <?php if(in_array("7", $permis)) { ?>
        <li class="<?php if($uri == "viewMeta") { echo "active"; } ?>"> 
            <a href="{{ URL::to($redirectUrl.'/viewmeta') }}" class="mn_catgcur fa fa-code"><span>Meta Tags</span> </a>
        </li>
        <?php } ?>

        <?php } ?>
      </ul>
    </div>
</aside>