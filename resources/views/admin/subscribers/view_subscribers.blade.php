
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>WIIX Admin panel | Subcribers </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
         @include('layouts.styles')
       <style>
	       .row .col {
		    padding-left: 2px !important;
		    padding-right: 2px !important;
		    float: left !important;
		}
       </style>
    </head>

    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
		  @include('layouts.header')
               
               
            @include('layouts.sidebar')
            
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                
                <article class="content dashboard-page">
                    <section class="section">
				<form role="form" id="email_subscribers" name="email_subscribers" action="{{ url('/WiPlytaIIX2/Subscribers') }}" method="post"  enctype="multipart/form-data" >

				{!! csrf_field() !!}

				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
						       <!--  <th><input name="select_all" value="1" type="checkbox" id="select_all" class="checkbox_check"></th> -->
						       <th>Check</th>
						        <th>Email Id</th>
						        <th>Subscribed Date</th>
						</tr>
					</thead>
					<tbody>
					
						@if($subscribers)
							<?php $i=0;  ?>
							@foreach($subscribers as $subscriber)
				
						<tr>
							<td><input  type="checkbox" name="id[]" value="<?php echo $subscriber->email; ?>" > </td>

							<td>{{$subscriber->email}}</td>
							<td>{{$subscriber->created_on}}</td>
						</tr>
					@endforeach
						@else
						<tr>
						        <td colspan="3">{{"No Subscribers Found"}}</td>
						</tr>
					@endif
					</tbody>
				</table>
			
		
		      
			
			  <div class="form-group">
		        <label for="exampleInputPassword1">Content</label>
			<textarea  id="content" name="content" rows="10" cols="80"></textarea>
		      </div>
		      
			  <div class="form-group">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			
			
            </form>
			
</section>
                </article>
                @include('layouts.footer')
             </div>
        </div>
        
      @include('layouts.scripts')
     <!-- DataTables -->
 <script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
 <script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
	$("#example1").DataTable({
		 "pagingType": "full_numbers"
		});
   });
</script>

<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
   $(function () {
	  
 	 CKEDITOR.replace('content');
	      
	});
	

$("#email_subscribers").submit(function(e) {  
	var atLeastOneIsChecked = false;  
	$('input:checkbox').each(function (){    
		if ($(this).is(':checked')) 
		{
      		atLeastOneIsChecked = true;      
        } 
    });  
    if(atLeastOneIsChecked == false)
    {
    	$.iaoAlert({msg: "Please Select atleast one subscribed email", type: "error",mode: "dark",});
    	e.preventDefault();
 			return false; 
    }
    else
    {
    	//alert('poda dai');
    }
});

</script>
</body>
</html>
