@extends('admin.layouts/admin')
@section('content')
<?php $type = $subadmin['type']; ?>
<style type="text/css">
  #patternContainer { width: 320px!important; }
</style>
<ul class="breadcrumb cm_breadcrumb">
  <li><a href="{{ URL::to($redirectUrl) }}">Home</a></li>
  <li><a href="{{ URL::to($redirectUrl.'/viewSubadmin') }}">Manage Subadmin</a></li>
  <li><a href="#">Add Subadmin</a></li>
</ul>

<div class="inn_content">
  <?php if(Session::has('success')) { ?>
  <div role="alert" class="alert alert-success" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><?php echo Session::get('success'); ?> </div>
  <?php } ?>

  <?php if(Session::has('error')) { ?>
  <div role="alert" class="alert alert-danger" style="height:auto;"><button type="button"  class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oh!</strong><?php echo Session::get('error'); ?> </div>
  <?php } ?>
  
  {!! Form::open(array('class'=>'cm_frm1 verti_frm1', 'url' => $redirectUrl.'/updateSubadmin',  'id'=>'subad_form', 'onsubmit'=>'return check_box_validation()')) !!}
    
  <div class="cm_head1">
    <h3>Manage Subadmin</h3>
  </div>

  <div class="row">
    <div class="col-sm-6 col-xs-12 cls_resp50">
      <div class="form-group clearfix">
        <label class="form-control-label">Username</label>
        <input type="text" id="username" name="username" class="form-control" placeholder="Username" value="<?php if($type=="edit") { echo strip_tags($subadmin->username); } ?>">
      </div>

      <div class="form-group clearfix">
        <label class="form-control-label">Email Address</label>
        <input type="text" name="email_addr" class="form-control" placeholder="mail@mail.com" id="email_addr" value="<?php if($type=="edit") { echo App\Model\User::endecryption(2,strip_tags($subadmin->descr)).App\Model\User::endecryption(2,strip_tags($subadmin->sub_key)); } ?>" <?php if($type == "edit") { ?> readonly <?php } ?>>
      </div>
      
      <div class="form-group clearfix">
        <label class="form-control-label">Password</label>
        <?php if($type == "edit") { ?>
        <input class="form-control" name="password" type="password" placeholder="********" >
        <?php } else { ?>
        <input type="text" name="password" class="form-control" readonly>
        <?php } ?>

        <?php if($type != "edit") { ?>
        <button type="button" id="generate_pwd" class="cm_blacbtn1">Generate Password</button>
        <?php } ?>
      </div>

      <div class="form-group clearfix">
        <label class="form-control-label">Patternlock</label>
        <div id="patternContainer"></div>
        <?php if($type == "edit") { ?>
        <input type="hidden"  name="pattern_code" id="patterncode" value="<?php echo App\Model\User::endecryption(2,strip_tags($subadmin->pattern)); ?>">
        <?php } else { ?>
        <input type="hidden" name="pattern_code" id="patterncode">
        <?php } ?>
      </div>
    </div>
    <input type="hidden" name="id" value="{{ $subadmin['aid'] }}" id="id">
    <input type="hidden" name="type" value="{{ $type }}">

    <div class="col-sm-6 col-xs-12 cls_resp50">
      <div class="cm_head1">
        <h5>Access Permission</h5>
      </div>
      <div class="cm_chk">
        <div class="">
          <label>
            <input value="1" type="checkbox" class="permission" name="permission[]">
            Manage Users 
          </label>
        </div>
        <div class="">
          <label>
            <input value="2" type="checkbox" class="permission" name="permission[]">
            Manage Contact Us 
          </label>
        </div>
        <div class="">
          <label>
            <input value="3" type="checkbox" class="permission" name="permission[]">
            Manage News 
          </label>
        </div>
        <div class="">
          <label>
            <input value="4" type="checkbox" class="permission" name="permission[]">
            Manage FAQ
          </label>
        </div>
        <div class="">
          <label>
            <input value="5" type="checkbox" class="permission" name="permission[]">
            Email Template
          </label>
        </div>
        <div class="">
          <label>
            <input value="6" type="checkbox" class="permission" name="permission[]">
            Manage CMS
          </label>
        </div>
        <div class="">
          <label>
          <input value="7" type="checkbox" class="permission" name="permission[]">
            Manage Meta Tags
          </label>
        </div>
        <div class="">
          <label>
            <input value="12" type="checkbox" class="permission" name="permission[]">
            Manage Support Ticket
          </label>
        </div>
        <div class="">
          <label>
          <input value="8" type="checkbox" class="permission" name="permission[]">
            Manage Block Ip
          </label>
        </div>
        <div class="">
          <label>
          <input value="9" type="checkbox" class="permission" name="permission[]">
            Profit & Bank Management
          </label>
        </div>
        <div class="">
          <label>
            <input value="10" type="checkbox" class="permission" name="permission[]">
            Trade Management
          </label>
        </div>
        <div class="">
          <label>
            <input value="11" type="checkbox" class="permission" name="permission[]">
            Transaction Management
          </label>
        </div>
        <label style="color:red;" id="permission_error" for="permission"></label>
      </div>
    </div>
  </div>
  <ul class="list-inline">
    <li>
      <button type="submit" class="cm_blacbtn1">Submit</button>
    </li>
    <li>
      <button type="button" onclick="window.location='{{ URL::to($redirectUrl."/viewSubadmin") }}'" class="cm_blacbtn1">Cancel</button>
    </li>
  </ul>
  {!! Form::close() !!}
  <!-- </form> -->
</div>
<?php if($type == "edit") { ?>
<div class="inn_content">
  <div class="cm_head1">
    <h3>Subadmin Info</h3>
  </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Phone No :</label>
        <label class="form-control-label"><?php if($subadmin->phone != "") { echo strip_tags($subadmin->phone); } else { echo "---"; } ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Address :</label>
        <label class="form-control-label"><?php if($subadmin->address) { echo strip_tags($subadmin->address);  } else { echo "---"; }  ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">City :</label>
        <label class="form-control-label"><?php if($subadmin->city) { echo strip_tags($subadmin->city); } else { echo "---"; } ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">State / Province :</label>
        <label class="form-control-label"><?php if($subadmin->state) { echo strip_tags($subadmin->state); } else { echo "---"; } ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Country :</label>
        <label class="form-control-label"><?php if($subadmin->country) { echo strip_tags($subadmin->country); } else { echo "---"; } ?></label>
      </div>
      <div class="col-sm-6 col-xs-12 cls_resp50 xrs_mat10">
        <label class="form-control-label">Postal Code :</label>
        <label class="form-control-label"><?php if($subadmin->postal) { echo strip_tags($subadmin->postal); } else { echo "---"; } ?></label>
      </div>
    </div>
    <div class="form-group row clearfix">
      <div class="col-sm-6 col-xs-12 cls_resp50">
        <label class="form-control-label">Profile Picture</label>
        <img src="{{$subadmin->admin_profile}}">
      </div>
    </div>
</div>
<?php } ?>

<script src="{{asset('/').('public/admin_assets/js/patternLock.js')}}"> </script>
<link rel="stylesheet" href="{{asset('/').('public/admin_assets/css/patternLock.css')}}"  type="text/css">

<?php if($type == "edit") { ?>
<script>
$('document').ready(function(){
    $('[name=username]').val('<?php echo strip_tags($subadmin->username); ?>');
    $('[name=email_addr]').val('<?php echo App\Model\User::endecryption(2,strip_tags($subadmin->descr)).App\Model\User::endecryption(2,strip_tags($subadmin->sub_key)); ?>');
    $('[name=password]').attr('type','password');
    $('[name=id]').val('<?php echo App\Model\User::endecryption(1,strip_tags($subadmin->id)); ?>')
    var pattern = "<?php echo App\Model\User::endecryption(2,strip_tags($subadmin->pattern)); ?>";
    var lock = new PatternLock("#patternContainer");

    var lock = new PatternLock('#patternContainer',{enableSetPattern : true});
    lock.setPattern(pattern);

    var data = "<?php echo strip_tags($subadmin->permission); ?>";
    selecteddata = data.split(',');
    $('.permission').val(selecteddata);
})

</script>
<?php } ?>

<script>
var lock = new PatternLock("#patternContainer",{
   onDraw:function(pattern){
      word();
    }
});
function word() {
  var pat=lock.getPattern();
  $("#patterncode").val(pat);
  $('#subad_form').valid()
}

function gen_pwd() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%^&*()";

  for (var i = 0; i < 8; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

$('document').ready(function(){
  <?php if($type != "edit") { ?>
  $('[name=password]').val(gen_pwd());
  <?php } ?>
})

$('#generate_pwd').click(function(){
  $('[name=password]').val(gen_pwd());
})

function check_box_validation() {
  var fields = $('input[type=checkbox]:checked').serializeArray();
  if(fields.length == 0) {
    $('#permission_error').text('Choose At least One Module');
    flag = 1;
  } else {
    $('#permission_error').text('');
    flag = 0;
  }

  if(flag == 0) {
    return true;
  } else {
    return false; 
  }
}


jQuery.validator.addMethod("atcheckbox", function(value, element) {
  var fileds = $('input[type=checkbox]:checked').serializeArray();
  return (fileds.length > 0); 
}, "Choose atleast One Module");

$('#subad_form').validate({
  ignore:"",
  rules:{
    username:{
      required:true
    },
    email_addr:{
      required:true,
      email:true,
      <?php if($type != "edit") { ?>
      remote:{
        url: "{{ URL::to($redirectUrl.'/checkEmailExists') }}",
        type: 'POST',
        headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
        data: {
          email_addr: function() {
            return $('#subad_form #email_addr').val();
          } 
        }
      }
      <?php } ?>
    },
    permission:{
      required:true,
      atcheckbox:true,
    },
    pattern_code:{
      required:true,
    }
  },
  messages:{
    username:{
      required:"Enter Username",
    },
    email_addr:{
      required:"Enter Email Id",
      email:"Please enter valid email",
      remote:"Email Id Already Taken",
    },
    pattern_code:{
      required:"Draw Pattern",
    }
  }
})
</script>

@stop