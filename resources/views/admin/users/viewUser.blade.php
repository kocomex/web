
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>WIIX Admin panel | List All users </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
       @include('layouts.styles')
       
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap-theme.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css" />
  <style>
  .prod_det {
    align-items: center;
    display: flex;
    height: 90px;
    justify-content: center;
}

.bootstrap-switch-large{
    width: 200px;
}
  </style>
 
        <!-- DataTables -->
	<link rel="stylesheet" href="{{ URL::asset('theme/datatables/dataTables.bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" media="all" href="{{ URL::asset('theme/light_box/css/styles.css') }}">
  <link rel="stylesheet" type="text/css" media="all" href="{{ URL::asset('theme/light_box/css/jquery.lightbox-0.5.css') }}">
  
   </head>

    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
		 @include('layouts.header')
               
               
            @include('layouts.sidebar')
            
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
             
               <article class="content cards-page">
                    <section class="section">
                        <div class="row">
			    
			      <div class="col-xl-12">
                                <div class="card card-primary">
                                    <div class="card-block">
				            <div class="col-xl-8">   
						  
						     <div class="card card-warning">
                                    <div class="card-header">
                                        <div class="header-block">
                                            <p class="title"> Profile </p>
                                        </div>
                                    </div>
                                    <div class="card-block">
				      
                                         <table class="table">
                                                <tbody>
				  

 <?php $profe=Helper::displayprof_pic($profile->user_id);?>
							 <tr>
								  <td><img src="{{ $profe }}" height="100"></td>
							<td>	 
								   <table class="table">
									   
									    <tr>
							      <th scope="row">{{$profile->first_name." ".$profile->last_name}}</th>
							  </tr>
							  
									   
								    <tr>
								      <th scope="row"><?php

								      $email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
								$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
								$final_mail=$email[0]."@".$email[1];
											
										?>
											{{$final_mail}}
										</th>
								  </tr>
                                                    
                                                    
							
							   
							  </table>
							  </td>
							 
							  </tr>
<?php 

/*user status*/

if($user->status == 0)
{
$acc_status= '<span class="label label-danger" style="color:red"><b>Inactive</b></span>';
}
else
{
	$acc_status= '<span class="label label-success" style="color:green"><b>Active</b></span>';
}
/*Bank status*/
if($user_bank1->status == 0)
{
	$bank_status= '<span style="color:blue"><b>Unverified</b></span>';
}
else if($user_bank1->status == 1)
{
	$bank_status= '<span style="color:orange"><b>Pending</b></span>';
}
else if($user_bank1->status == 2)
{
	$bank_status= '<span style="color:red"><b>Rejected</b></span>';
}
else if($user_bank1->status == 3)
{
	$bank_status= '<span style="color:green"><b>Verified</b></span>';
}

/*KYC status*/

      if(($profile->photo_id_status == '0' && $profile->photo_id == NULL) || ($profile->address_prof_status == '0' && $profile->address_prof_status == NULL))
                          {
                           $kyc_status=  '<span class="label label-info" style="color:blue"><b>UnVerified</b></span>'; 
                         }


else if($profile->address_prof_status == 0 || $profile->photo_id_status == 0)
{
	$kyc_status= '<span style="color:orange"><b>Pending</b></span>';
}
else if($profile->address_prof_status == 2 || $profile->photo_id_status == 2)
{
	$kyc_status= '<span style="color:red"><b>Rejected</b></span>';
}

else if($profile->address_prof_status == 1 && $profile->photo_id_status == 1)
{
	$kyc_status= '<span style="color:green"><b>Verified</b></span>';
}
else if($profile->address_prof_status == 1 || $profile->photo_id_status == 1)
{
	$kyc_status= '<span style="color:grey"><b>UnVerified</b></span>';
}
else{
	$kyc_status='';
}
?>
							  <tr>
							  <th>{{$profile->first_name}} Account status</th>
							  <td>
							  	{!! $acc_status !!}
							  </td></tr>

							   <tr>
							  <th>{{$profile->first_name}} Bank status</th>
							  <td>
							  	{!! $bank_status !!}
							  </td></tr>
							
							   <tr>
							  <th>{{$profile->first_name}} KYC status</th>
							  <td>
							  {!! $kyc_status !!}
							  </td></tr>
							  
							  <tr>
							      <th scope="row">First name </th>
							      <td>{{$profile->first_name}}</td>
							  </tr>
							  
							   <tr>
							      <th scope="row">Last name </th>
							      <td>{{$profile->last_name}}</td>
							  </tr>
							  
							   <tr>
							      <th scope="row">Address1 </th>
							      <td>{{$profile->address1}}</td>
							  </tr>

							  <tr>
							      <th scope="row">Address2 </th>
							      <td>{{$profile->address2}}</td>
							  </tr>
							  
							     <tr>
							      <th scope="row">Country </th>
							      <td>{{$profile->country}}</td>
							  </tr>
							  
							   
							  
							     <tr>
							      <th scope="row">City </th>
							      <td>{{ $profile->city}}</td>
							  </tr>
							  
							     <tr>
							      <th scope="row">Postal code </th>
							      <td>{{ $profile->postal_code}}</td>
							  </tr>
                                                    
                                         
                                                      
                                                      
                                                    <tr>
                                                        <th scope="row">Device Type</th>
                                                        <td>{{  $user->device_type }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Created Date</th>
                                                        <td><?php echo date('d M Y',strtotime($user->created_on)); ?></td>
                                                    </tr>
                                           
                                                </tbody>
                                            </table>
                                               </div>
                                </div>
                                         </div>
                                         
                                         
                               <div class="col-xl-4 dashboard-page">          
                                      
                                      
			    <div class="col col-xs-12 col-sm-12 col-md-12">
                                <div class="card card-warning sameheight-item stats" data-exclude="xs">
				  
				  <div class="card-header">
                                        <div class="header-block">
                                            <p class="title"> Wallet </p>
                                        </div>
                                    </div>
                                    
                                    <div class="card-block">
                                      
                                        <div class="row row-sm stats-container">
                                            <div class="col-xs-12 col-sm-12 stat-col">
                                                <div class="stat-icon"><img src="{{ URL::asset('theme/icons/c1.png') }}" ></div>
                                                <div class="stat">
                                                    <div class="value">{{ $wallet->BTC}}</div>
                                                    <div class="name"> BTC </div>
                                                </div>  </div>
                                            <div class="col-xs-12 col-sm-12 stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c2.png') }}" > </div>
                                                <div class="stat">
                                                    <div class="value"> {{$wallet->ETH}}</div>
                                                    <div class="name">ETH</div>
                                                </div></div>
                                            <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"><img src="{{ URL::asset('theme/icons/c3.png') }}" ></div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->LTC}} </div>
                                                    <div class="name">LTC </div>
                                                </div> </div>
                                                 <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c4.png') }}" height="40" width="40"> </div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->XRP}} </div>
                                                    <div class="name"> XRP </div>
                                                </div></div>

                                                 <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c5.png') }}" height="40" width="40"> </div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->DOGE}} </div>
                                                    <div class="name"> DOGE </div>
                                                </div></div>
                                                 <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c6.png') }}" height="40" width="40"> </div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->ETC}} </div>
                                                    <div class="name"> ETC </div>
                                                </div></div>

                                                 <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c7.jpg') }}" height="40" width="40"> </div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->XMR}} </div>
                                                    <div class="name"> XMR </div>
                                                </div></div>

                                                    <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c8.png') }}" height="40" width="40"> </div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->IOTA}} </div>
                                                    <div class="name"> IOTA </div>
                                                </div></div>

                                                    <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c11.jpg') }}" height="45" width="60"> </div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->BTG}} </div>
                                                    <div class="name"> BTG </div>
                                                </div></div>

                                                 <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c9.png') }}" height="40" width="40"> </div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->DASH}} </div>
                                                    <div class="name"> DASH </div>
                                                </div></div>

                                            <div class="col-xs-12 col-sm-12  stat-col">
                                                <div class="stat-icon"> <img src="{{ URL::asset('theme/icons/c10.png') }}" height="40" width="40"> </div>
                                                <div class="stat">
                                                    <div class="value">  {{$wallet->KRW}} </div>
                                                    <div class="name"> KRW </div>
                                                </div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
              
                 
                 
                            <!-- /.col-xl-4 -->
                            </div>
                         
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-xl-4 -->
                            
                      
                          
                           
                        </div>
                        <!-- /.row -->
                        
                         <div class="row">
                            <div class="col-xl-12">
                                <div class="card sameheight-item">
                                    <div class="card-block">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-tabs-bordered">
				     <li class="nav-item"> <a href="javascript:void(0);" class="nav-link active" data-target="#bank" aria-controls="messages" data-toggle="tab" role="tab">Bank Details</a> </li>
				     <li class="nav-item"> <a href="javascript:void(0);" class="nav-link" data-target="#kyc" aria-controls="messages" data-toggle="tab" role="tab" onClick="init();" >KYC Documents</a> </li>
				     
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content tabs-bordered">
					<div class="tab-pane fade in active" id="bank">
						<div class="table-flip-scroll">
							<table id="banktable" class="table table-striped table-bordered table-hover flip-content">
								<thead class="flip-header">
									<tr>
									        <th>S.no</th>
									        <th>Bank Name</th>
									        <th>Account Number</th>
									        <th>IBAN</th>
									        <th>Status</th>
									        <th></th>
									</tr>
								</thead>
								<tbody>
									<?php $sno=0; ?>
										@if($user_bank)
										@foreach($user_bank as $bank)
											<tr>
												<td><?php echo ++$sno; ?></td>
												<td>{{$bank->bank_name}}</td>
												<td>{{$bank->account_number}}</td>
												<td>{{$bank->iban}}</td>
												<td><?php 
														switch($bank->status){
															case 1:
																echo "Pending";
															break;
															case 3:
																echo "Verified";
															break;
															case 2:
																echo "Rejected";
															break;
														}
												?></td>
											        <td>
											        <a class="btn btn-pill-left btn-warning" href="javascript:bank_popup(<?php echo $bank->id; ?>);" title="view/edit bank details"><em class="fa fa-gears"></em></a>
											        </td>
											</tr>
											
										@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
                                      
                                      
                                         <div class="tab-pane fade in " id="kyc">
					<div class="table-flip-scroll">
						<form id="proof_approval"  name="proof_approval" action="{{ url('/WiPlytaIIX2/proofApproval/'.$user_id) }}" method="post" novalidate="">
						{!! csrf_field() !!}
							<div id="thumbnails">
																																																
			<ul>
			
				  <li> 
					  
					  <div class="col-xl-12 row">
						 
						   <div class="col-sm-3">
						    <label><h5>Photo ID Proof</h5></label>
						    <?php if($profile->photo_id) 
						    {?>

						  <a href="{{ $profile->photo_id }}" title="Photo proof"><img src="{{ $profile->photo_id }}" alt="" style="max-width:100px; "></a>
						  <?php } 
						  else { ?>
<img src="http://design-ec.com/d/e_others_50/l_e_others_500.png" alt="" style="max-width:100px; ">

						 <?php }?>

						  </div>
						  
						   <div class="col-sm-3">
						<div class="prod_det">
							<input <?php if($profile->photo_id_status == 1 ){ ?>checked disabled<?php } ?>  type="checkbox" name="pan_status" value="1" id="pan_status" onclick="ShowHideDiv('pan_')">     
					   	</div>
					   </div>
					   
						  
						    <div class="col-sm-6  pull-right" id="pan_comment" >
						<?php echo ($profile->photo_id_status==1)?'':' <label>Comment</label>
							<textarea class="validate[required] form-control" rows="2" placeholder="Enter ..." name="pan_comment"></textarea>
						'; ?>
					</div>
					
					</div>
					  
					  
					    
					   
					<hr>
				  </li>
				  
				    <li> 
					      <div class="col-xl-12 row">
					       <div class="col-sm-3">
					       <label><h5>Address Proof</h5></label>

					         <?php if($profile->address_prof) 
						    {?>
					    <a href="{{ $profile->address_prof }}" title="Bank Passbook"><img src="{{ $profile->address_prof }}" alt="" style="max-width:100px; "></a>

					     <?php } 
						  else { ?>
<img src="http://design-ec.com/d/e_others_50/l_e_others_500.png" alt="" style="max-width:100px; ">

						 <?php }?>
					     </div>
					     
					    
					      <div class="col-sm-3">
						<div class="prod_det">
							<input <?php if($profile->address_prof_status == 1 ){ ?>checked disabled<?php } ?>  type="checkbox" name="address_prof_status" value="1" id="address_prof_status" >     
					   	</div>
					   </div>
					   
					   <div class="col-sm-6  pull-right" id="address_prof_comment" >
						<?php echo ($profile->address_prof_status==1)?'':' <label>Comments</label>
							<textarea class="validate[required] form-control" rows="2" placeholder="Enter ..." name="address_prof_comment" ></textarea>
						'; ?>
					</div>
					
					    
					    
					  </div>
						  
					<hr>
				  </li>
				 
				  @if($profile->address_prof_status == 1 && $profile->photo_id_status==1)
				 <center style="display: none;"> <button type="submit" class="btn btn-default" data-dismiss="modal" id="proof_submission">Submit</button></center>
                 @else
                 <center> <button type="submit" class="btn btn-default" data-dismiss="modal" id="proof_submission">Submit</button></center>
                 @endif
			</ul>		
			
			
				
				
			
				</div>
				
				
			</form>
			
					      
					</div>
				</div>
                                      
                                      
                                 
                                    </div>
                                    <!-- /.card-block -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col-xl-6 -->
                        </div>
                        
                      </section>
                </article>
             
             
<div class="modal fade" tabindex="-1" role="dialog" id="history_popup"  >
	<div class="modal-dialog" role="document" style="max-width:75%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Personal Wallet Transactions</h4>
			</div>
			<div class="modal-body">
				<table id="transactions" class="table table-striped table-bordered table-hover flip-content">
					<thead class="flip-header">
						<tr>
						        <th>S.no</th>
						        <th>Title</th>
						        <th>Amount</th>
						        <th>Created Date</th>
						</tr>
					</thead>
					<tbody id="view_pw_history"></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" tabindex="-1" role="dialog" id="bank_modal" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Bank Details</h4>
			</div>
			<div class="modal-body" id="bank_details">
				<p>One fine body&hellip;</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
                                 <div id="loadering" style="display:none;margin: 0px 45px; ">

            <div class="loadinh_bg">
            <div align="center">
              <img src="{{ URL::asset('theme/front_users/images/loader.gif') }}" height="50" width="50"/>
            </div>
            </div>
            </div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


     	
		
                   <script>
		 
            </script>
            
                       @include('layouts.footer')
             </div>
        </div>
        @include('layouts.scripts')
      
         
            
            
            <script>
$(document).ready(function(){
	// binds form submission and fields to the validation engine
	$("#proof_approval").validationEngine('attach', {scroll: false});
	
});

function init(){
	$("#proof_approval").validationEngine('attach', {scroll: false});
}

</script>


     
           <!-- DataTables -->
 <script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
 <script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('theme/light_box/js/jquery.lightbox-0.5.min.js') }}"></script>

  
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js" data-turbolinks-track="true"></script>


<script type="text/javascript">

$( "#proof_approval" ).submit(function( event ) {


    $("#proof_submission").attr("disabled", true);
});
    
</script>

 <script type="text/javascript">
	$(function() {
	    $('#thumbnails a').lightBox();
	});
</script>



 <script type="text/javascript">

 if($("#pan_status").is(":checked")){
	$("#pan_comment").hide();
}else{
	$("#pan_comment").show();
}
		
 $("[name='pan_status']").bootstrapSwitch({
	onText: 'Confirm',
	offText: 'Decline',
	size: 'normal',
	onSwitchChange: function(e, state) {
		$('#pan_comment').toggle(!state);
	}
});
	 
	 
	 
 if($("#address_prof_status").is(":checked")){
	$("#address_prof_comment").hide();
}else{
	$("#address_prof_comment").show();
}
		
 $("[name='address_prof_status']").bootstrapSwitch({
	onText: 'Confirm',
	offText: 'Decline',
	size: 'normal',
	onSwitchChange: function(e, state) {
		$('#address_prof_comment').toggle(!state);
	}
});
</script>

  
<script>
  $(function () {
	  
	  $("#banktable").DataTable({
		 "pagingType": "full_numbers",
		 "iDisplayLength" : 5, 
		 "bLengthChange": false,
		});
	  $("#kyctable").DataTable({
		 "pagingType": "full_numbers",
		 "iDisplayLength" : 5, 
		 "bLengthChange": false,
		});
	$("#example1").DataTable({
		 "pagingType": "full_numbers",
		 "iDisplayLength" : 5, 
		 "bLengthChange": false,
		});
	$("#example2").DataTable({
		 "pagingType": "full_numbers",
		 "iDisplayLength" : 5, 
		 "bLengthChange": false,
		});	
	$("#example3").DataTable({
		 "pagingType": "full_numbers",
		 "iDisplayLength" : 5, 
		 "bLengthChange": false,
		});
		
			
   });
   
   
	function bank_popup(id){	
		$.ajax({
			type: 'GET',
			url: "{{ url('/WiPlytaIIX2/bankInfo/') }}"+'/'+id,
			success: function (data) {
				$("#bank_details").html(data);
				$('#bank_modal').modal('show');
			}
		});
	}
	
	
	function rejectBank(id){
		 
		$.ajax({
			type: $('#bank_approval').attr('method'),
			url: "{{ url('/WiPlytaIIX2/bankApproval') }}"+"/"+id+"-reject",

			data: {
        "_token": "{{ csrf_token() }}",
        "comment": $("#comment").val()
        },
           beforeSend:function(){
        $('#loadering').show();
        
        
      },
		
			success: function (data) {
                $('#loadering').hide();
				location.reload();
			}
		});
	}
	
	function acceptBank(id){



		$.ajax({
			type: $('#bank_approval').attr('method'),
			url: "{{ url('/WiPlytaIIX2/bankApproval') }}"+'/'+id+"-accept",
			data: {
        "_token": "{{ csrf_token() }}",
        "comment": $("#comment").val()
        },
         beforeSend:function(){
        $('#loadering').show();
        
        
      },
			success: function (data) {
                 $('#loadering').hide();
				location.reload();
			}
		});
	}
	
	
</script>
    

    </body>
</html>
