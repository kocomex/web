
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>WIIX Admin panel | List All users </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
      @include('layouts.styles')
           <!-- DataTables -->
	<link rel="stylesheet" href="{{ URL::asset('theme/datatables/dataTables.bootstrap.css') }}"> 
  
    </head>

    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
		  @include('layouts.header')
               
               
            @include('layouts.sidebar')
            
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                
                <article class="content dashboard-page">
                     <section class="section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block">
                                        <section class="example">
					
			          <div class="table-flip-scroll">
                            <table id="example1" class="table table-striped table-bordered table-hover flip-content">
					     <thead class="flip-header">
						<tr>
						        <th>S.no</th>
						        <th> Email</th>
						        <th>Username</th>
						        <th>Created Date</th>
						        <th> account status</th>
						         <th> TFA status</th>
						         <th> Mobile status</th>
						          <th> KYC status</th>
						        <th>Action</th>
						</tr>
					</thead>
					<tbody>
						
						@if($users)
							<?php $i=0; ?>
							@foreach($users as $user)
				
						<tr>
						        <td><?php echo ++$i; ?></td>
						        	      
						<td><?php
								$email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
								$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
								$final_mail=$email[0]."@".$email[1];
							?>

							{{$final_mail}}
						</td>      
						  <td>{{$user->device_token}}</td>
						    <td><?php echo date('d M Y',strtotime($user->created_on)); ?></td>


						    <?php

						    $user_status=$user->status;

						    if($user_status == 0)
						    {
						    	$us_sta="De-Activate";
						    }
						    else{
						    	$us_sta="Activate";
						    }
						    $user_tfa=$user->randcode;
						     if($user_tfa == 'disable' || $user_tfa == NULL)
						    {
						    	$tfa_sta="Disable";
						    }
						    else{
						    	$tfa_sta="Enable";
						    }

						    ?>
						     <td> {{$us_sta}}

						     <?php if($user_status == 0)
						    {?>
						    <a href="{{ url('/WiPlytaIIX2/user_sta/' . $user->user_id . '/1') }}" title="Activate user"><i class="fa fa-lock"></i></a>

						     	

						     	<?php } else {?>

						     		<a href="{{ url('/WiPlytaIIX2/user_sta/' . $user->user_id . '/0') }}" title="Deactivate user"><i class="fa fa-unlock"></i></a>

						     		<?php } ?>

						     </td>
						      <td><?php echo $tfa_sta; ?>
						      	

						      	   <?php if($user_tfa == 'disable' || $user_tfa == NULL)
						    {?>

						   

						     	<!--  <a href="{{ url('/WiPlytaIIX2/tfa_status/' . $user->user_id . '/1') }}" title="Disable TFA"><i class="fa fa-lock"></i></a> -->


						     	<?php } else {?>

						     		 <a href="{{ url('/WiPlytaIIX2/tfa_status/' . $user->user_id . '/0') }}" title="Enable TFA"><i class="fa fa-unlock"></i></a>

						     		<?php } ?>
						      </td>

						      <!--mobile start here-->
								<td><?php echo ($user->mobile_verify==1)?'Enable':'Disable'; ?>


								<?php if($user->mobile_verify==0 || $user->mobile_verify==NULL) {?>



								<!--  <a href="{{ url('/WiPlytaIIX2/tfa_status/' . $user->user_id . '/1') }}" title="Disable TFA"><i class="fa fa-lock"></i></a> -->


								<?php } else {?>

								<a href="{{ url('/WiPlytaIIX2/mobile_status/' . $user->user_id . '/0') }}" title="Disa Mobile Verification"><i class="fa fa-unlock"></i></a>

								<?php } ?>
								</td>
						      <!--mobile end here-->


						      <td><?php /*KYC status*/

						      if(($user->photo_id_status == '0' && $user->photo_id == NULL) || ($user->address_prof_status == '0' && $user->address_prof_status == NULL))
                          {
                           $kyc_status=  '<span class="label label-info" style="color:blue"><b>UnVerified</b></span>'; 
                         }

else if($user->address_prof_status == 0 || $user->photo_id_status == 0)
{
	$kyc_status= '<span style="color:orange"><b>Pending</b></span>';
}
else if($user->address_prof_status == 2 || $user->photo_id_status == 2)
{
	$kyc_status= '<span style="color:red"><b>Rejected</b></span>';
}

else if($user->address_prof_status == 1 && $user->photo_id_status == 1)
{
	$kyc_status= '<span style="color:green"><b>Verified</b></span>';
}
else if($user->address_prof_status == 1 || $user->photo_id_status == 1)
{
	$kyc_status= '<span style="color:grey"><b>UnVerified</b></span>';
}

else{
	$kyc_status='';
}?>

{!! $kyc_status !!}


</td>
						    <td>
						    
						  						    
						  <a class="btn btn-pill-right btn-warning" href="{{ url('/WiPlytaIIX2/viewusers/' . $user->user_id) }}" title="View user"><i class="fa fa-fw fa-user"></i>View</a>

						
						  
                                     
						    
						    </td>
					
					
					</tr>
					@endforeach
					@endif
					</tbody>
				</table>
			
                         
                      </div>
                      </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>
                @include('layouts.footer')
             </div>
        </div>
        
       @include('layouts.scripts')
     
     <!-- DataTables -->

             <script src="{{ URL::asset('theme/datatables/jquery.dataTables.min.js') }}"></script>
 <script src="{{ URL::asset('theme/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
  $(function () {
	$("#example1").DataTable({
		 "pagingType": "full_numbers"
		});
   });
</script>
    
    </body>

</html>
