
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>WIIX Admin panel | News </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	@include('layouts.styles')
</head>
<body>
	<div class="main-wrapper">
		<div class="app" id="app">
			@include('layouts.header')
			
			
			@include('layouts.sidebar')
			<div class="sidebar-overlay" id="sidebar-overlay"></div>
			<article class="content dashboard-page">
				<section class="section">
					
					<form id="add_faq" name="add_faq" action="{{ url('/WiPlytaIIX2/addnews') }}" method="POST" novalidate="" enctype="multipart/form-data">

						{!! csrf_field() !!}

						
						
						<div class="form-group ">
							<label for="exampleInputEmail1">Title<span style="color:red">*</span></label>
							<input class="validate[required,maxSize[200]] form-control " id="news_title" placeholder="Enter question" type="text" name="news_title" required>
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1">Content</label>
							<textarea  id="news_content" name="news_content" class="validate[required]" rows="10" cols="80"></textarea>
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">News image</label>
							<input type="file" class="form-control" name="news_image" > 
						</div>
						
						<div class="form-group ">
							<label for="exampleInputEmail1">Status</label>
							<select name="status" >
								<option value="1">Active</option>
								<option value="0">De-active</option>
							</select>
						</div>
						
						
						<!-- /.box-body -->
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
					
					
				</section>
			</article>
		</div>
	</div>
	@include('layouts.scripts')
	
	

	<!-- CK Editor -->
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	<script>
		$(function () {
			
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('news_content');
});
</script>


<script>
	$(function() {
		
		if (!$('#profile-form').length) {
			return false;
		}

		var signupValidationSettings = {
			rules: {
				email: {
					email: true
				},
				retype_password: {
					equalTo: "#password"
				},
				agree: {
					required: true,
				},
				facebook_url:{
					url: true	
				},
				twitter_url:{
					url: true	
				},
				linkedin_url:{
					url: true	
				},
				google_url:{
					url: true	
				},
				
			},
			messages: {
				email: {
					email: "Please enter a valid email address"
				},
				facebook_url: {
					url: "Please enter valid URL.",
				},
				twitter_url: {
					url: "Please enter valid URL.",
				},
				linkedin_url: {
					url: "Please enter valid URL.",
				},
				google_url: {
					url: "Please enter valid URL.",
				}
			},
		}
		$('#profile-form').validate(signupValidationSettings);
	});
</script>
</body>
</html>
