
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>WIIX Admin panel | News </title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		  @include('layouts.styles')
	</head>
	<body>
		<div class="main-wrapper">
			<div class="app" id="app">
				@include('layouts.header')
               
               
            @include('layouts.sidebar')
				<div class="sidebar-overlay" id="sidebar-overlay"></div>
				<article class="content dashboard-page">
					<section class="section">
						
			
		     <form id="edit_faq" name="edit_faq" action="{{ url('/WiPlytaIIX2/editnews/'.$news_data->news_id) }}" method="POST" novalidate="" enctype="multipart/form-data">

                       {!! csrf_field() !!}
				
		      <div class="form-group ">
		        <label for="exampleInputEmail1">Title<span style="color:red">*</span></label>
		        <input class="form-control " id="exampleInputEmail1" placeholder="Enter question" type="text" name="news_title" required value="{{$news_data->news_title}}" >
		      </div>
                
		      <div class="form-group">
		        <label for="exampleInputPassword1">Content</label>
			<textarea class=""  id="answer" name="news_content" rows="10" cols="80">{{$news_data->news_content}}</textarea>
		      </div>

		      <div class="form-group">
							<label for="exampleInputPassword1">News image</label>
							<input type="file" class="form-control" name="news_image" value="$news_data->news_image"> 
						</div>
		      
		         <div class="form-group ">
		        <label for="exampleInputEmail1">Status</label>
		        <select name="status" >
			        <option value="1" <?php echo ($news_data->status==1)?'selected="selected"':''; ?>>Active</option>
			        <option value="0" <?php echo ($news_data->status==0)?'selected="selected"':''; ?>>De-active</option>
		        </select>
		       </div>
		      
              
              <!-- /.box-body -->
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
	
	
            </section>
				</article>
			</div>
		</div>
@include('layouts.scripts')
		
		

<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
  $(function () {
	  
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('news_content');
  });
</script>

	
	</body>
</html>


