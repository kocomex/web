var from_address 	=	process.argv[2];
var to_address   	=	process.argv[3];
var amount			=	process.argv[4];
var pass			=	process.argv[5];

var Web3         	=	require('web3');
var web3 			=	new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
var send_amount  	=	web3._extend.utils.toWei(amount);

var trans_det = { from : from_address, to : to_address, value : send_amount };
/* console.log(JSON.stringify(trans_det)); */
web3.eth.getBalance(from_address,function(err, balance) 
{
	if(balance >= amount)
	{
		web3.eth.estimateGas(trans_det,function(gaslimit_err, gaslimit) 
		{
			web3.eth.getGasPrice(function(gas_err, getGasPrice) 
			{
				if(gas_err) {
					console.log(JSON.stringify({'error1':gas_err}));
				}

				web3.personal.unlockAccount(from_address, pass, 60000, function(lock_err, res)
				{
					if(lock_err) {
						console.log(JSON.stringify({'error2':lock_err}));
					}
					trans_det.value = send_amount;
					web3.eth.sendTransaction(trans_det,function(trans_err,txid)
					{
						if(trans_err) {
							console.log(JSON.stringify({'error':trans_err}));
						}
					 	if(txid && txid != "") {
					 		console.log(JSON.stringify({'tx':txid,'hash':txid}));
					 	}
					});
				});
			});
		});
	}else{
		console.log({'error':'Not Enough Balance to Transfer','balance':balance});
	}
});