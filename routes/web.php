<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Model\BlockIP;
use App\Model\SiteSettings;

// //To prevent access from users whose IP address blocked
$remote = !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
$ip = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $remote;
$checkIp = BlockIP::where('ip_addr',$ip)->where('status','active')->count();
if($checkIp > 0) {
	echo "Blocked";
	exit();
}

//To get admin URL
$getUrl = SiteSettings::where('id',1)->select('admin_redirect')->first();
$url = $getUrl->admin_redirect;


//to get encrypted key
Route::get('getkey', 'Admin@getEncrypt');


/******************* Front End routes ***********************/
Route::group(['prefix' => '', 'middleware' => 'web'], function () {
	// basic actions
	Route::get('/', 'Home@index');
	Route::get('/signup', 'Home@register');
	Route::post('/signup', 'Home@register');
	Route::post('/auth_mail', 'Home@auth_mail');
	Route::get('/auth_mail', 'Home@auth_mail');
	Route::get('/activation/{id}', 'Home@activateEmail');
	Route::get('/login', 'Home@login');
	Route::post('/login', 'Home@login');
	Route::get('/logout', 'Home@logout');
	Route::get('/refresh', 'Home@captcharefresh');

	Route::group(['middleware' => ['user_session']],function() {
		//profile actions
		Route::get('/mypage', 'Users@mypage');

		// deposit actions 
		Route::group(['prefix' => ''], function () {
			Route::get('/deposit/{id}', 'Credit@index');
			Route::post('/getCryptoInfo','Credit@getCryptoInfo');
		});

		// trade actions 
		Route::group(['prefix' => 'trade'], function () {
			Route::get('/deal', 'Trade@index');
		});
	});


});



/******************* Admin Controller routes ***********************/

//Admin panel pages
Route::group(['prefix' => $url, 'middleware' => ['web']],function(){
	Route::get('/', 'Admin@index');
	Route::post('/adminLogin', 'Admin@adminLogin');
	Route::get('/logout','Admin@logout');

	//Forgot Password
	Route::post('/forgotPassword', 'Admin@forgotPassword');
	Route::post('/checkResetEmail','Admin@checkResetEmail');
	Route::get('/resetPassword/{id}/{token}', 'Admin@resetPassword');
	Route::post('/updatePassword', 'Admin@updatePassword');

	//Profile actions
	Route::get('/profile','Admin@viewProfile');
	Route::post('/updateProfile','Admin@updateProfile');
	Route::get('/checkPassword','Admin@checkPassword');
	Route::post('/changePassword','Admin@changePassword');

	Route::group(['middleware' => ['admin_session','admin_permit']],function(){
		//Site settings actions
		Route::get('/settings','Admin@adminSettings');
		Route::post('/updateSite','Admin@updateSite');

		//dashboard chart actions
		Route::get('/userFromChart', 'Admin@userFromChart');
		Route::get('/depWithChart', 'Admin@depWithChart');
		Route::get('/profitChart', 'Admin@profitChart');

		//CMS actions
		Route::get('/viewcms/{type}','Admin@viewCms');
		Route::get('/cmsEdit/{id}','Admin@cmsEdit');
		Route::post('/updateCms', 'Admin@cmsUpdate');

		//subadmin Actions
		Route::post('/checkEmailExists','Admin@checkEmailExists');
		Route::get('/loginHistory', 'Admin@loginHistory');
		Route::get('/viewSubadmin', 'Admin@viewSubadmin');
		Route::get('/subadminStatus/{id}','Admin@subadminStatus');
		Route::post('/updateSubadmin','Admin@updateSubadmin');
		Route::get('/subadminEdit/{id}/{type}','Admin@subadminEdit');
		Route::get('/deleteSubadmin/{id}', 'Admin@deleteSubadmin');

		//cms actions
		Route::get('/viewcms/{type}','Admin@viewCms');
		Route::get('/cmsEdit/{id}','Admin@cmsEdit');
		Route::post('/updateCms', 'Admin@cmsUpdate');

		//email actions
		Route::get('/viewemail','Admin@viewEmail');
		Route::get('/emailEdit/{id}','Admin@emailEdit');
		Route::post('/updateEmail', 'Admin@emailUpdate');

		//Meta actions
		Route::get('/viewmeta','Admin@viewMeta');
		Route::get('/metaEdit/{id}','Admin@metaEdit');
		Route::post('/updateMeta', 'Admin@metaUpdate');

		//faq actions
		Route::get('/viewfaq','Admin@viewFaq');
		Route::get('/faqStatus/{id}','Admin@faqStatus');
		Route::get('/faqEdit/{id}','Admin@faqEdit');
		Route::get('/addFaq','Admin@faqEdit');
		Route::post('/updateFaq', 'Admin@faqUpdate');
		Route::get('/faqDelete/{id}', 'Admin@faqDelete');

		//Contact us actions
		Route::get('/viewContactUs','Admin@viewContactUs');
		Route::get('/viewContactUs/{type}','Admin@viewContactUs');
		Route::get('/contactReply/{id}','Admin@contactReply');
		Route::post('/updateContact', 'Admin@updateContact');

		//support actions
		Route::get('/viewSupportTicket','Admin@viewSupportTicket');
		Route::get('/viewSupportTicket/{type}','Admin@viewSupportTicket');
		Route::get('/ticketReply/{id}','Admin@ticketReply');
		Route::post('/updateTicket','Admin@updateTicket');

		//user actions
		Route::get('/viewuser','Admin@userList');
		Route::get('/viewuser/{type}','Admin@userList');
		Route::get('/userStatus/{id}','Admin@userStatus');
		Route::get('/userTfaStatus/{id}','Admin@userTfaStatus');
		Route::get('/userDetail/{id}','Admin@userDetail');
		Route::post('/verifyUserStatus','Admin@verifyUserStatus');
		Route::get('/viewUserBalance','Admin@viewUserBalance');

		//user bank actions
		Route::get('/viewUserBank','Admin@viewUserBank');
		Route::get('/viewUserBankDetail/{id}','Admin@viewUserBankDetail');

		//admin bank actions
		Route::get('/viewAdminBank','Admin@viewAdminBank');
		Route::get('/bankEdit/{id}','Admin@bankEdit');
		Route::post('/updateBank', 'Admin@bankUpdate');

		//admin profit
		Route::get('/viewAdminProfit', 'Admin@viewAdminProfit');

		//IP block actions
		Route::get('/viewBlockIp','Admin@viewBlockIp');
		Route::get('/ipAddrStatus/{id}','Admin@ipAddrStatus');
		Route::get('/ipAddrDelete/{id}','Admin@ipAddrDelete');
		Route::post('/addIpAddress','Admin@addIpAddress');

		//Deposit actions
		Route::get('/viewDeposit', 'Admin@viewDeposit');
		Route::get('/viewDeposit/{type}','Admin@viewDeposit');
		Route::get('/viewUserDeposit/{id}', 'Admin@viewUserDeposit');
		Route::get('/confirmDeposit/{tid}/{uid}', 'Admin@confirmDeposit');
		Route::get('/rejectDeposit/{tid}/{uid}', 'Admin@rejectDeposit');
		Route::get('/checkConfirmCode', 'Admin@checkConfirmCode');
		Route::post('/updateConfirm', 'Admin@updateConfirmDeposit');
		Route::post('/updateReject', 'Admin@updateRejectDeposit');

		//Withdraw actions
		Route::get('/viewWithdraw', 'Admin@viewWithdraw');
		Route::get('/viewWithdraw/{type}', 'Admin@viewWithdraw');
		Route::get('/viewUserWithdraw/{id}', 'Admin@viewUserWithdraw');
		Route::get('/confirmWithdraw/{tid}/{uid}', 'Admin@confirmWithdraw');
		Route::get('/rejectWithdraw/{tid}/{uid}', 'Admin@rejectWithdraw');
		Route::get('/checkConfirmCodeWithdraw', 'Admin@checkConfirmCodeWithdraw');
		Route::post('/updateConfirmWithdraw', 'Admin@updateConfirmWithdraw');
		Route::post('/updateRejectWithdraw', 'Admin@updateRejectWithdraw');

		//Trade actions
		Route::get('/viewTradePairs', 'Admin@viewTradePairs');
		Route::get('/tradePairEdit/{id}','Admin@tradePairEdit');
		Route::post('/updateTradePair', 'Admin@tradePairUpdate');
		Route::get('/viewTradeFee', 'Admin@viewTradeFee');
		Route::get('/tradeFeeEdit/{id}','Admin@tradeFeeEdit');
		Route::post('/updateTradeFee', 'Admin@tradeFeeUpdate');
		Route::get('/viewOrderHistory', 'Admin@viewOrderHistory');
		Route::get('/viewTradeHistory', 'Admin@viewTradeHistory');
	});
});