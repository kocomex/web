<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;

class checkUserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('wiix_userId') == "") {
            Session::flash('error','Please login to continue!');
            return Redirect::to('login');
        }
        return $next($request);
    }
}
