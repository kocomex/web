<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Cloudinary;

use App\Model\Googleauthenticator;
use App\Model\jsonRPCClient;

use Validator;
use Auth;
use Redirect;
use DB;
use URL;
use Session;
use Mail;

use App\Model\User;
use App\Model\SubAdmin;
use App\Model\SiteSettings;
use App\Model\LoginAttempt;
use App\Model\AdminActivity;
use App\Model\Transaction;
use App\Model\Support;
use App\Model\Deposit;
use App\Model\Withdraw;
use App\Model\Cms;
use App\Model\EmailTemplate;
use App\Model\Faq;
use App\Model\MetaContent;
use App\Model\Wallet;
use App\Model\AdminNotification;
use App\Model\News;
use App\Model\HelpCentre;
use App\Model\HelpIssue;
use App\Model\BlockIP;
use App\Model\ConsumerVerification;
use App\Model\UserBank;
use App\Model\AdminBank;
use App\Model\CoinProfit;
use App\Model\Currency;
use App\Model\CoinOrder;
use App\Model\OrderTemp;
use App\Model\TradingFee;
use App\Model\TradePairs;

class Admin extends Controller
{
	public function __construct() {
		// CDN credentials
		Cloudinary::config(array(
		    "cloud_name" => "dh0kb7sbx",
		    "api_key" => "578432172636126",
		    "api_secret" => "BpYGipiQuAnzS8LohSmmtadF6iE"
		));

		$getUrl = SiteSettings::where('id',1)->select('admin_redirect')->first();
		$this->Url = $getUrl->admin_redirect;
	}

	//to get static encrypted text
	public function getEncrypt(Request $request) {
		$string = "Mail@123";
		echo User::endecryption(1,$string); exit;
	}

	//To return dashboard page if admin logged in or return login page
	public function index() {
		if(session('adminId')!='') {
			$cur_date = date('Y-m-d');

			$data['pending_users'] = 0;
			$data['new_users'] = 0;
			$data['total_users'] = User::count();
			$data['total_transactions'] = Transaction::count();
			$data['pending_withdraw'] = Withdraw::where('status','pending')->count();
			$data['new_message'] = Support::where('status','unread')->count();
			$data['pending_deposit'] = Deposit::where('status','pending')->count();
			$data['new_support'] = 0;
			$data['unread_support'] = 0;

			return view('admin.home.dashboard')->with('data',$data)->with('redirectUrl',$this->Url);
		}
		return view('admin.home.login')->with('redirectUrl',$this->Url);
    }

	//To get users count according to their KYC status
	public function userFromChart() {
		$verify = User::where('verified_status',3)->count();

		$pending = User::where('verified_status', 1)->count();

		$rejected = User::where('verified_status', 2)->count();

		$unverified = User::where('verified_status', 0)->count();

		$chart = array(array('users'=>'KYC Verified','value'=>$verify),array('users'=>'KYC pending','value'=>$pending),array('users'=>'Unverified Users','value'=>$unverified),array('users'=>'KYC Rejected','value'=>$rejected));
		echo json_encode($chart);
	}

	//To get deposit and withdraw amount for every month
	public function depWithChart() {
		$year = $_GET['year'];
		$currency = $_GET['currency'];

		$data = array(
			array('Month'=>'Jan',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-01-01",$year."-01-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-01-01",$year."-01-31",$currency)),
			array('Month'=>'Feb',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-02-01",$year."-02-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-02-01",$year."-02-31",$currency)),
			array('Month'=>'Mar',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-03-01",$year."-03-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-03-01",$year."-03-31",$currency)),
			array('Month'=>'Apr',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-04-01",$year."-04-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-04-01",$year."-04-31",$currency)),
			array('Month'=>'May',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-05-01",$year."-05-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-05-01",$year."-05-31",$currency)),
			array('Month'=>'Jun',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-06-01",$year."-06-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-06-01",$year."-06-31",$currency)),
			array('Month'=>'Jul',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-07-01",$year."-07-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-07-01",$year."-07-31",$currency)),
			array('Month'=>'Aug',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-08-01",$year."-08-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-08-01",$year."-08-31",$currency)),
			array('Month'=>'Sep',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-09-01",$year."-09-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-09-01",$year."-09-31",$currency)),
			array('Month'=>'Oct',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-10-01",$year."-10-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-10-01",$year."-10-31",$currency)),
			array('Month'=>'Nov',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-11-01",$year."-11-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-11-01",$year."-11-31",$currency)),
			array('Month'=>'Dec',
			    'Withdraw'=>Controller::userTransactionDetails('Withdraw',$year."-12-01",$year."-12-31",$currency),
			    'Deposit'=>Controller::userTransactionDetails('Deposit',$year."-12-01",$year."-12-31",$currency)),
		);
		echo json_encode($data);
	}

	//To get profit amount for every month
	public function profitChart() {
		$year = $_GET['year'];
		$currency = $_GET['currency'];

		$data = array(array('Month'=>'Jan',
		          'profit'=>Controller::getProfitDetails($year."-01-01",$year."-01-31",$currency),"color"=>"#FF0F00"),
				array('Month'=>'Feb',
				  'profit'=>Controller::getProfitDetails($year."-02-01",$year."-02-31",$currency),"color"=>"#FF6600"),
				array('Month'=>'Mar',
				  'profit'=>Controller::getProfitDetails($year."-03-01",$year."-03-31",$currency),"color"=> "#FF9E01"),
				array('Month'=>'Apr',
				  'profit'=>Controller::getProfitDetails($year."-04-01",$year."-04-31",$currency),"color"=> "#FCD202"),
				array('Month'=>'May',
				  'profit'=>Controller::getProfitDetails($year."-05-01",$year."-05-31",$currency),"color"=>"#F8FF01"),
				array('Month'=>'Jun',
				  'profit'=>Controller::getProfitDetails($year."-06-01",$year."-06-31",$currency),"color"=> "#B0DE09"),
				array('Month'=>'Jul',
				  'profit'=>Controller::getProfitDetails($year."-07-01",$year."-07-31",$currency),"color"=>"#04D215"),
				array('Month'=>'Aug',
				  'profit'=>Controller::getProfitDetails($year."-08-01",$year."-08-31",$currency),"color"=> "#0D8ECF"),
				array('Month'=>'Sep',
				  'profit'=>Controller::getProfitDetails($year."-09-01",$year."-09-31",$currency),"color"=> "#0D52D1"),
				array('Month'=>'Oct',
				  'profit'=>Controller::getProfitDetails($year."-10-01",$year."-10-31",$currency),"color"=>"#2A0CD0"),
				array('Month'=>'Nov',
				  'profit'=>Controller::getProfitDetails($year."-11-01",$year."-11-31",$currency),"color"=>"#8A0CCF"),
				array('Month'=>'Dec',
				  'profit'=>Controller::getProfitDetails($year."-12-01",$year."-12-31",$currency),"color"=> "#CD0D74")
				);
		echo json_encode($data);
	}

    //check credentials and login to admin panel
    public function adminLogin() {
	  	$data = Input::all();

	  	$Validation = Validator::make($data, User::$adminLoginRule);
	  	if($Validation->fails()) {
			Session::flash('error', $Validation->messages());
			return Redirect::to($this->Url);
	  	}
	  	if($data == array_filter($data)) {
	  		$email = strip_tags($data['username']);
	  		$first = User::endecryption(1,User::firstEmail($email));
	  		$second = User::endecryption(1,User::secondEmail($email));
	  		$password = User::endecryption(1,strip_tags($data['user_pwd']));
	  		$pattern = User::endecryption(1,strip_tags($data['pattern_code']));

			$ip = Controller::getIpAddress();
			$browser = Controller::getBrowser();
			$platform = Controller::getPlatform();

			$login = SubAdmin::where('descr', $first)
			 		->where('sub_key', $second) 
			 		->where('skey', $password) 
			 		->where('pattern', $pattern)->first();

			if($login) {
				if($login->status == "deactive") {
					Session::flash('error', 'You have been deactivated by Admin.');
					return Redirect::to($this->Url);
				}
				LoginAttempt::where('ip_address',$ip)->where('status','new')->update(['status' => 'old']);

				$activity['ip_address'] = $ip;
				$activity['browser_name'] = $browser;
				$activity['os'] = $platform;
				$activity['activity'] = "Login";
				$activity['admin_id'] = $login['id'];
				AdminActivity::create($activity);

				session(['adminId'=>$login['id'], 'adminName' => $login['username'], 'adminRole'=>$login['role']]);
				Session::flash('success', 'Login Success');
				return Redirect::to($this->Url);
			} else {
				$getCount = LoginAttempt::where('ip_address',$ip)->where('status','new')->count();
				if($getCount >= 2) {
					$getBlockCount = BlockIP::where('ip_addr',$ip)->count();
					if($getBlockCount == 0) {
						$updata = array('ip_addr'=>$ip,'status'=>'active');
						BlockIP::create($updata);
					} else {
						BlockIP::where('ip_addr',$ip)->update(['status' => 'active']);
					}

					$getEmail = EmailTemplate::where('id',15)->first();
					$getSiteDetails  = Controller::getEmailTemplateDetails();
					$emaildata = array('content' =>strtr($getEmail->template,$getSiteDetails));

					$emaildata = array('content' =>strtr($getEmail->template,$replace));
					$toDetails['useremail'] = $getSiteDetails['contact_mail_id'];
					$toDetails['subject'] = $getEmail->subject;
					$toDetails['from'] = $getSiteDetails['contact_mail_id'];
				  	$toDetails['name'] = $getSiteDetails['site_name'];

					$sendEmail = Controller::sendEmail($emaildata, $toDetails);
				}
				$createAttempt = array('ip_address' =>$ip,'os' =>$platform,'browser' =>$browser,'status'=>'new','username'=>strip_tags($data['username']),'password'=>$data['user_pwd']);
				LoginAttempt::create($createAttempt);

				Session::flash('error', 'Invalid login credentials!');
				return Redirect::to($this->Url);
			}
		} else {
			Session::flash('error', 'Please fill all fields!');
			return Redirect::to($this->Url);
		}
	}

    //logout
	public function logout() {
		if(session('adminId')!='') {
			$ip = Controller::getIpAddress(); 
			$activity['ip_address'] = $ip;
			$activity['browser_name'] = Controller::getBrowser();
			$activity['os'] = Controller::getPlatform();
			$activity['activity'] = "Logout";
			$activity['admin_id'] = session('adminId');
			AdminActivity::create($activity);

			Session::flush();
			Session::flash('success', 'Logged out successfully!');
			return Redirect::to($this->Url);
		} else {
			Session::flash('error', 'Logged out!');
			return Redirect::to($this->Url);
		}
	}

	//update generated key in DB and to send reset password link to email
	public function forgotPassword() {
		$data = Input::all();
    
	    $Validation = Validator::make($data, User::$forgotRule);
	    if($Validation->fails()) {
	      Session::flash('error', $Validation->messages());
	      return Redirect::to($this->Url);
	    }

	    if($data == array_filter($data)) {
			$useremail = strip_tags($data['useremail']);
			$first = User::endecryption(1,User::firstEmail($useremail));
			$second = User::endecryption(1,User::secondEmail($useremail));

			$getDetail = SubAdmin::select('id','username')
		  			->where('descr', $first)
		  			->where('sub_key', $second)->first();
			if($getDetail->count()) {
				$id = User::endecryption(1,$getDetail->id);
				$generateCode = User::randomString(8);
				$randomCode = User::endecryption(1,$generateCode);
				$update = SubAdmin::where('id',$getDetail->id)->update(['forgot_code'=>$randomCode,'forgot_status'=>'active']);
				$securl =URL::to($this->Url.'/resetPassword/'.$id.'/'.$randomCode);

				$getEmail = EmailTemplate::where('id',2)->first();
				$getSiteDetails  = Controller::getEmailTemplateDetails();
				$info = array('###USER###'=>$getDetail->admin_username,'###LINK###'=>$securl);
				$replace = array_merge($getSiteDetails, $info);
				$emaildata = array('content' =>strtr($getEmail->template,$replace));
				$toDetails['useremail'] = $useremail;
				$toDetails['subject'] = $getEmail->subject;
				$toDetails['from'] = $getSiteDetails['contact_mail_id'];
				$toDetails['name'] = $getSiteDetails['site_name'];

				$sendEmail = Controller::sendEmail($emaildata, $toDetails);
				if(count(Mail::failures()) > 0) {
			  		$result_data = array('status'=>'0','msg'=>'Please Try Again');
				} else {
			  		$result_data = array('status'=>'1','msg'=>'Reset password Link has sent to Your Mail Id');
				}
			} else {
				$result_data = array('status'=>'0','msg'=>'Invalid Email ID');
			}
	    }
	    echo json_encode($result_data);
	}

	public function resetPassword($arg1,$arg2) {
		$id = User::endecryption(2,$arg1);
		$getDetail = SubAdmin::where('forgot_status','active')->where('id', $id)->count();
		if($getDetail == 1) {
			$data['fcode'] = $arg2;
			$data['ucode'] = $arg1;
			return view('admin.home.reset')->with('data',$data)->with('redirectUrl',$this->Url);
		} else {
			Session::flash('error', 'Invalid / Expired URL');
			return Redirect::to($this->Url);
		}
	}

	public function updatePassword() {
		$data = Input::all();
    
	    $Validation = Validator::make($data, User::$passwordRule);
	    if($Validation->fails()) {
			Session::flash('error', $Validation->messages());
			return Redirect::to($this->Url);
	    }

	    if($data == array_filter($data)) {
			$new_pwd = strip_tags($data['new_pwd']);
			$cnfirm_pwd = strip_tags($data['cnfirm_pwd']);
			$fcode = strip_tags($data['fcode']);
			$userid = User::endecryption(2,strip_tags($data['ucode']));
			if($new_pwd == $cnfirm_pwd) {
				$getDetail = SubAdmin::select('id','admin_forgot_status')
			  				->where('id', $userid)->first();
				if($getDetail->admin_forgot_status == "active") {
			  		$password = User::endecryption(1,$cnfirm_pwd);
			  		$update = SubAdmin::where('id',$userid)->update(['skey' => $password,'forgot_code'=>"", 'forgot_status'=>'deactive']);
			  		if($update) {
	    				Session::flash('success', 'Password Reset Success');
			    		return Redirect::to($this->Url);
			  		} else {
		    			Session::flash('error', 'Please Try Again');
			  		}
				} else {
			  		Session::flash('error', 'Reset Url Expired');
				}
			} else {
				Session::flash('error', 'Password Must match');
			}
	    } else {
      		Session::flash('error', 'Fill All the Field');
	    }
	}

	//check given mail exists in DB
	public function checkResetEmail(Request $request) {
		$email = explode('@',strip_tags($request['email_addr']));
		$first = User::endecryption(1,$email[0]);
		$second = User::endecryption(1,$email[1]);
		$getCount = SubAdmin::where('descr',$first)
			->where('sub_key',$second)->count();
		echo ($getCount == 1) ? "true" : "false";
	}

	//Sub Admin functions
	//check if entered email already exists in DB
	public function checkEmailExists(Request $request) {
		$email = strip_tags($request['email_addr']);
		$first = User::endecryption(1,User::firstEmail($email));
		$second = User::endecryption(1,User::secondEmail($email));
		$getCount = SubAdmin::where('descr',$first)
					->where('sub_key',$second)->count();
		echo ($getCount == 1) ? "false" : "true";
  	}

	//To list all sub admins
	public function viewSubadmin() {
		$data = SubAdmin::where('role','!=','admin')->orderBy('id','desc')->get();
      	return view('admin.sub.subadmin')->with('subadmin',$data)->with('redirectUrl',$this->Url);
	}

	//To activate or deactivate sub admin
	public function subadminStatus($id) {
		$id = User::endecryption(2,strip_tags($id));
		if($id != session('adminId')) {
			$admin = SubAdmin::where('id',$id)->select('status','role')->first();
			if($admin->role == "admin") {
				Session::flash('error', 'You can not change Admin status');
				return Redirect::back();
			}
			$new_status = ($admin->status == "active") ? "deactive" : "active";
			$update = SubAdmin::where('id',$id)->update(['status' => $new_status]);
			if($update) {
				$new_stat = ($admin->status == "active") ? "Deactivated" : "Activated";
				$msg = "Sub Admin ".$new_stat. " Successfully";
				Session::flash('success', $msg);
			} else {
				Session::flash('error', 'Please Try Again');
			}
		} else {
			Session::flash('error', 'You could not change your activity status.');
		}
		return Redirect::back();
	}

	//To return view page for add and edit sub admin details
	public function subadminEdit($id,$type) {
		$aid = User::endecryption(2,$id);
		if($aid != 0) {
			$data = SubAdmin::where('id',$aid)->first();
		}
		$data['type'] = $type;
		$data['aid'] = $id;
		return view('admin.sub.addSubadmin')->with('subadmin',$data)->with('redirectUrl',$this->Url);
	}

	//To update sub admin details in DB and to send email to the respective sub admin
  	public function updateSubadmin() {
        $data = Input::all();
        $Validation = Validator::make($data, SubAdmin::$addSubadminRule);
        if($Validation->fails()) {
      		Session::flash('error', $Validation->messages());
      		return Redirect::to($this->Url);
        }
        if($data['type'] == "add") {
      		unset($data['id']);
        }
        if($data['type'] == "edit") {
      		$id = User::endecryption(2,strip_tags($data['id']));
      		$getAdminPwd = SubAdmin::select('skey')->where('id',$id)->first();
      		$password = $getAdminPwd->skey;
        }
        if($data['password'] != "") {
      		$password = User::endecryption(1,strip_tags($data['password']));
        }
        $data['password'] = $password;
        if($data == array_filter($data)) {
			$permission = implode(',',$data['permission']);
			$email = strip_tags($data['email_addr']);
			$first = User::endecryption(1,User::firstEmail($email));
			$second = User::endecryption(1,User::secondEmail($email));

			$getSiteDetails  = Controller::getEmailTemplateDetails();
			$getEmail = EmailTemplate::where('id',13)->first();

			$securl = URL::to($this->Url);
			if(isset($id) && $id != "") {
				$result = SubAdmin::where('id',$id)->update(['permission' => $permission, 'skey' => $password, 'pattern' => User::endecryption(1,strip_tags($data['pattern_code']))]);
				$st = "Updated";
				$user_data = SubAdmin::where('id',$id)->first();

				$info = array('###USER###'=>$user_data->username,'###EMAIL###'=>$data['email_addr'],'###PWD###'=>User::endecryption(2,$user_data->skey),'###PATT###'=>User::endecryption(2,$user_data->pattern),'###URL###'=>$securl);
				$replace = array_merge($getSiteDetails, $info);
				$emaildata = array('content' =>strtr($getEmail->template,$replace));
			} else {
				$getCount = SubAdmin::where('descr',$first)
				          ->where('sub_key',$second)->count();
				if($getCount == 0) {
					$subadd['pattern'] = User::endecryption(1,$data['pattern_code']);
					$subadd['skey'] = $password;
					$subadd['descr'] = $first;
					$subadd['sub_key'] = $second;
					$subadd['username'] = $data['username'];
					$subadd['permission'] = $permission;
					$subadd['role'] = 'subadmin';
					$subadd['status'] = "active";
					$result = SubAdmin::create($subadd);
					$st = "Added";

					$info = array('###USER###'=>$data['username'],'###EMAIL###'=>$data['email_addr'],'###PWD###'=>User::endecryption(2,$password),'###PATT###'=>$data['pattern_code'],'###URL###'=>$securl);
					$replace = array_merge($getSiteDetails, $info);
					$emaildata = array('content' =>strtr($getEmail->template,$replace));
				} else {
			  		Session::flash('error', 'Email Already exists.');
			  		return Redirect::back();
				}
			}
      		if($result) {
	            $toDetails['useremail'] = strip_tags($data['email_addr']);
	            $toDetails['subject'] = $getEmail->subject;
	            $toDetails['from'] = $getSiteDetails['contact_mail_id'];
	  			$toDetails['name'] = $getSiteDetails['site_name'];

	            $sendEmail = Controller::sendEmail($emaildata, $toDetails);

	            Session::flash('success','Subadmin '.$st.' Succeesfully');
	            return Redirect::to($this->Url.'/viewSubadmin');
        	}
    	} else {
      		Session::flash('error','Failed to add Subadmin.');
          	return Redirect::back();
        }
  	}

  	//To delete subadmin
  	public function deleteSubadmin($id) {
        $id = User::endecryption(2,$id);
        if($id != session('adminId')) {
			$delete = SubAdmin::where(['id'=>$id])->delete();
			if($delete) {
				Session::flash('success', "SubAdmin deleted Successfully");
			} else {
				Session::flash('error', 'Please Try Again');
			}
        } else {
			Session::flash('error', 'You can not delete your own account.');
        }
        return Redirect::back();
  	}

  	public function loginHistory() {
      	$data = AdminActivity::orderBy('id','desc')->get();
      	return view('admin.sub.history')->with('history',$data)->with('redirectUrl',$this->Url);
  	}

	//CMS Functions
	//To list all CMS pages and static contents
	public function viewCms($type) {
		$data = Cms::where('type',$type)->get();
		return view('admin.cms.cms')->with('cms_list',$data)->with('redirectUrl',$this->Url);
	}

	//To fetch view page to edit for the selected CMS page/content
	public function cmsEdit($id) {
		$id = User::endecryption(2,strip_tags($id));
		$data = Cms::where('id',$id)->first();
   		return view('admin.cms.cmsEdit')->with('cms',$data)->with('redirectUrl',$this->Url);
	}

	//Update CMS content and title in DB to reflect in frontend
	public function cmsUpdate() {
		$data = Input::all();
		$Validation = Validator::make($data, SubAdmin::$cmsRule);
		if($Validation->fails()) {
	  		Session::flash('error', $Validation->messages());
	     	return Redirect::back();
		}
		if($data == array_filter($data)) {
			$title = strip_tags($data['title']);
			$content = $data['content'];
			$id = User::endecryption(2,strip_tags($data['id']));
			if(!empty($data['id'])) {
				$result = Cms::where('id',$id)->update(['title' => $title, 'content' => $content, 'status' => 'active']);
			} else {
				$data['id'] = $id;
				$data['title'] = $title;
				$data['content'] = $content;
				$data['status'] = 'active';
				$result = Cms::insert($data);
			}
			if($result) {
				Session::flash('success', 'CMS Updated Successfully');
			} else {
				Session::flash('error', 'Failed to update.');
			}
			return Redirect::back();
		}
	}

	//Email Actions
	//To view all email templates
  	public function viewEmail() {
		$data = EmailTemplate::all();
		return view('admin.email.email')->with('email_list',$data)->with('redirectUrl',$this->Url);
  	}

  	//To return view page to edit email template for selected template
	public function emailEdit($id) {
		$id = User::endecryption(2,strip_tags($id));
		$data = EmailTemplate::where('id',$id)->first();
       	return view('admin.email.emailEdit')->with('email',$data)->with('redirectUrl',$this->Url);
	}

	//To update email template details in DB
	public function emailUpdate() {
		$data = Input::all();
		$Validation = Validator::make($data, SubAdmin::$emailRule);
		if($Validation->fails()) {
			Session::flash('error', $Validation->messages());
		  	return Redirect::to($this->Url.'/viewemail');
		}
		if($data == array_filter($data)) {
			$email['name'] = strip_tags($data['name']);
			$email['subject'] = strip_tags($data['subject']);
			$email['template'] = $data['template'];
			$id = User::endecryption(2,strip_tags($data['id']));
			if(!empty($id)) {
				$result = EmailTemplate::where('id',$id)->update($email);
			}
			if($result) {
				Session::flash('success', 'Email Updated Successfully');
			} else {
				Session::flash('error', 'Failed to update.');
			}
			return Redirect::to($this->Url.'/viewemail');
		}
	}

	//Meta functions
	//To view all meta tags for all pages
	public function viewMeta() {
    	$data = MetaContent::all();
    	return view('admin.meta.meta')->with('meta_list',$data)->with('redirectUrl',$this->Url);
	}

	//To return view to edit meta tag information
	public function metaEdit($id = NULL) {
		$id = User::endecryption(2,strip_tags($id));
		$data = MetaContent::where('id',$id)->first();
     	return view('admin.meta.metaEdit')->with('meta',$data)->with('redirectUrl',$this->Url);
	}

	//To update meta tag details in DB
	public function metaUpdate() {
     	$data = Input::all();
     	$Validation = Validator::make($data, SubAdmin::$metaRule);
    	if($Validation->fails()) {
	        Session::flash('error', $Validation->messages());
	        return Redirect::to($this->Url.'/viewmeta');
    	}
     	if($data == array_filter($data)) {
     		$meta['title'] = strip_tags($data['title']);
     		$meta['meta_keywords'] = strip_tags($data['meta_keywords']);
     		$meta['meta_description'] = strip_tags($data['meta_description']);
     		$id = User::endecryption(2,$data['id']);
     		if(!empty($id)) {
     			$result = MetaContent::where('id',$id)->update($meta);
     		}
     		if($result) {
     			Session::flash('success', 'Meta Content Updated Successfully');
     		} else {
     			Session::flash('error', 'Failed to update.');
     		}
     		return Redirect::to($this->Url.'/viewmeta');
     	}
	}

	//contact us actions
	//To view list of contacted messages and also to fetch new and pending contact us messages
	public function viewContactUs($type = NULL) {
		$query = Support::orderBy('id', 'desc');
		if($type == "new") {
			$query = $query->where('read_status','unread');
		}
		if($type == "pending") {
			$query = $query->where('status','unread');
		}
		$data = $query->get();
	    return view('admin.contact.contactUs')->with('contact_list',$data)->with('redirectUrl',$this->Url);
	}

	//To return view to reply user's contact us message
	public function contactReply($id) {
	    $id = User::endecryption(2,strip_tags($id));
	    Support::where('id',$id)->update(['read_status' => 'read']);
	    $data = Support::where('id',$id)->first();
	    return view('admin.contact.contactReply')->with('contact',$data)->with('redirectUrl',$this->Url);
	}

	//To save admin's reply message and to send email to user
	public function updateContact() {
	    $data = Input::all();
	    $id = User::endecryption(2,strip_tags($data['id']));
	    $replymsg = strip_tags($data['reply']);
	    $username = strip_tags($data['username']);
	    $email = strip_tags($data['email']);
	    $usermsg = strip_tags($data['user_msg']);
	    if($id != "") {
			$result = Support::where('id',$id)->update(['reply' => $replymsg, 'reply_date' => date('Y-m-d H:i:s'), 'status' => 1]);
			if($result) {
				$getEmail = EmailTemplate::where('id',3)->first();
				$getSiteDetails  = Controller::getEmailTemplateDetails();
				$info = array('###USER###'=>$username,'###MESSAGE###'=>$usermsg,'###REPLY###'=>$replymsg);
				$replace = array_merge($getSiteDetails, $info);

				$emaildata = array('content' =>strtr($getEmail->template,$replace));
				$toDetails['useremail'] = $data['email'];
				$toDetails['subject'] = $getEmail->subject;
				$toDetails['from'] = $getSiteDetails['contact_mail_id'];
				$toDetails['name'] = $getSiteDetails['site_name'];

				$sendEmail = Controller::sendEmail($emaildata, $toDetails);
				if(count(Mail::failures()) > 0) {
			  		Session::flash('error', 'Email sending failed.');
				} else {
				  	Session::flash('success', 'Reply message sent to user.');
				}
			} else {
				Session::flash('error', 'Failed to update!');
			}
			return Redirect::back();
	    } else {
      		Session::flash('error', 'Failed to get user information.');
	      	return Redirect::back();
	    }
	}

	//support ticket actions
	//To view list of support ticket and also to fetch new and pending support ticket
	public function viewSupportTicket($type = NULL) {
		$query = HelpCentre::orderBy('id', 'desc')->where('user_id','!=',0)->groupBy('reference_no');
		if($type == "new") {
			$query = $query->where('status','unread');
		}
		if($type == "pending") {
			$query = $query->where('ticket_status','active');
		}
		$data = $query->get();
	    return view('admin.support.supportTicket')->with('tickets',$data)->with('redirectUrl',$this->Url);
	}

	//To return view to reply user's support ticket
	public function ticketReply($id) {
	    $id = User::endecryption(2,strip_tags($id));
        $data = HelpCentre::where('id',$id)->first();
        $getTickets = HelpCentre::where('reference_no',$data->reference_no)->get();
        return view('admin.support.supportTicketDetail')->with('ticket',$data)->with('lists',$getTickets)->with('redirectUrl',$this->Url);
  	}

  	//To save admin's reply message to the support ticket
  	public function updateTicket() {
        $data = Input::all();
        $id = User::endecryption(2,strip_tags($data['id']));
        $refId = User::endecryption(2,$data['reference_no']);

        if($_FILES['supportimage']['name'] == "") {
			$image = "";
		} elseif($_FILES['supportimage']['name'] != "") {
			$fileExtensions = ['jpeg','jpg','png'];
			$fileName = $_FILES['supportimage']['name'];
			$fileType = $_FILES['supportimage']['type'];
			$explode = explode('.',$fileName);
  			$extension = end($explode);
  			$fileExtension = strtolower($extension);
  			$mimeImage = mime_content_type($_FILES["supportimage"]['tmp_name']);
  			$explode = explode('/', $mimeImage);

  			if (!in_array($fileExtension,$fileExtensions)) {
  				Session::flash('error', 'Invalid file type. Only image files are accepted.');  
				return Redirect::back();
  			} else {
  				if($explode[0] != "image") {
  					Session::flash('error', 'Invalid file type. Only image files are accepted.');  
					return Redirect::back();
  				}
  				$cloudUpload = \Cloudinary\Uploader::upload($_FILES["supportimage"]['tmp_name']);
  				if($cloudUpload) {
  					$image = $cloudUpload['secure_url'];
  				} else {
  					Session::flash('error', $cloudUpload["error"]["message"]);
					return Redirect::back();
  				}
  			}
		}

        $help['user_id'] = 0;
        $help['subject'] = strip_tags($data['subject']);
        $help['description'] = strip_tags($data['content']);
        $help['status'] = "read";
        $help['ticket_status'] = "active";
        $help['image'] = $image;
        $help['reference_no'] = $refId;

        $createHelp = HelpCentre::create($help);

        if($createHelp) {
        	$ticketStatus = strip_tags($data['status']);
        	if($ticketStatus == "close") {
        		$ticketStatus = "close";
        	}
          	$result = HelpCentre::where('reference_no',$refId)->update(['status' => 'read', 'ticket_status' => $ticketStatus]);
          	if($result) {
          		Session::flash('success', 'Reply message updated successfully');
          	} else {
            	Session::flash('error', 'Failed to update!');
          	}
        } else {
      		Session::flash('error', 'Failed to update!');
        }
        return Redirect::back();
	}

	//User Actions
	//To list all users and to display KYC pending and new users list
  	public function userList($type = NULL) {
    	$query = User::orderBy('id', 'desc');
    	if($type == "new") {
    		$cur_date = date('Y-m-d');
			$query = $query->where(DB::raw('DATE(created_at)'),$cur_date);
		}
		if($type == "pending") {
			$query = $query->where('verified_status',1);
		}
		$data = $query->get();
        return view('admin.user.userList')->with('userlist',$data)->with('redirectUrl',$this->Url);
  	}

  	//To update user's status to active or deactive
  	public function userStatus($id) {
        $id = User::endecryption(2,strip_tags($id));
        $user = User::where('id',$id)->select('status','consumer_name')->first();
        if($user->status == "inactive") {
    		Session::flash('error', "Can't activate user before email activation");
        	return Redirect::back();
        }
        $new_status = ($user->status == "active") ? "deactive" : "active";
        $update = User::where('id',$id)->update(['status' => $new_status]);
        if($update) {
			$new_stat = ($user->status == "active") ? "Deactivated" : "Activated";
			if(session('adminId') != 1) {
				$adminNotify['admin_id'] = 1;
				$adminNotify['type'] = "KYC";
				$adminNotify['message'] = session('adminName')." has ".$new_stat." ".$user->consumer_name.".";
				$adminNotify['status'] = "unread";
				AdminNotification::create($adminNotify);
			}
			$msg = "User ".$new_stat. " Successfully";
			Session::flash('success', $msg);
        } else {
      		Session::flash('error', 'Please Try Again');
        }
        return Redirect::back();
  	}

  	//To disable TFA status
  	public function userTfaStatus($id) {
        $id = User::endecryption(2,strip_tags($id));
        $user = User::where('id',$id)->select('id','tfa_status')->first();
        if($user->tfa_status == "disable") {
          	Session::flash('error', " Admin can't enable TFA for the user.");
          	return Redirect::back();
        } else {
          	$update = User::where('id',$id)->update(['tfa_status' => 'disable']);
          	if($update) {
            	Session::flash('success', "TFA status disabled Successfully");
          	} else {
            	Session::flash('error', 'Please Try Again');
          	}
          	return Redirect::back();
        }
  	}

  	//To view selected user's full details
  	public function userDetail($id) {
		$id = User::endecryption(2,strip_tags($id));
		$data = User::where('id',$id)->with('verification')->first();
       	return view('admin.user.singleUser')->with('userinfo',$data)->with('redirectUrl',$this->Url);
  	}

  	//To approve or reject single KYC document and to send email to user
  	public function verifyUserStatus() {
		$data = Input::all(); 
		$user_id = User::endecryption(2,strip_tags($data['user_id']));
		$type = strip_tags($data['type']);
		$status = strip_tags($data['status']);
		$type1 = strip_tags($data['type1']);
		$rej = array('id_status'=>'id_reject','id_status1'=>'id_reject1','selfie_status'=>'selfie_reject','bank_status'=>'bank_reject');
		($status == "2") ? $reject = $data['reject_reason'] : $reject = "";
		if($status == "3") { $reject = ""; }

		$update = ConsumerVerification::where('user_id',$user_id)->update([$type1 => $status, $rej[$type1] => $reject]);
		if($update) {
			if($type1 == 'bank_status') {
				if($status == "3") {
					UserBank::where('user_id',$user_id)->update(['status'=>'active']);
				} else {
					UserBank::where('user_id',$user_id)->update(['status'=>'deactive']);
				}
			}
			$getDetail = User::where('id', $user_id)
		    	->select('id','consumer_name','user_mail_id', 'unusual_user_key')
		    	->with(['verification' => function($query) {
					return $query->select('user_id','id_status','id_status1','selfie_status','bank_status');
				}])->first();
			$kyc = $getDetail->verification;
			if($kyc->id_status == 3 && $kyc->id_status1 == 3 && $kyc->selfie_status == 3 && $kyc->bank_status == 3) {
		  		User::where('id',$user_id)->update(["verified_status" => 3]);
			}
			if($kyc->id_status == 2 || $kyc->id_status1 == 2 || $kyc->selfie_status == 2 || $kyc->bank_status == 2) {
			  	User::where('id',$user_id)->update(["verified_status" => 2]);
			}
			$useremail = User::endecryption(2,$getDetail->user_mail_id).'@'.User::endecryption(2,$getDetail->unusual_user_key);
			$getSiteDetails  = Controller::getEmailTemplateDetails();
			$getEmail = EmailTemplate::where('id',4)->first();
			$sts = array('2'=>'Rejected','3'=>'Verified');

			if($status == "2") {
				$info = array('###USER###'=>$getDetail->consumer_name,'###TYPE###'=>$type,'###STATUS###'=>$sts[$status],'###MSG###'=>'Please check the proof and resubmit.','###REASON###'=>"Reason : ".$data['reject_reason']);
			} else {
				$info = array('###USER###'=>$getDetail->consumer_name,'###TYPE###'=>$type,'###STATUS###'=>$sts[$status],'###MSG###'=>'You can proceed further if all proofs are verified.','###REASON###'=>"");
			}
			$replace = array_merge($getSiteDetails, $info);
			$emaildata = array('content' =>strtr($getEmail->template,$replace));

			$toDetails['useremail'] = $useremail;
			$toDetails['subject'] = $getEmail->subject;
			$toDetails['from'] = $getSiteDetails['contact_mail_id'];
			$toDetails['name'] = $getSiteDetails['site_name'];

			$sendEmail = Controller::sendEmail($emaildata, $toDetails);

			if(count(Mail::failures()) > 0) {
		  		Session::flash('error', 'Email sending failed.');
			} else {
				$add_msg = ($sts[$status] == "Rejected") ? "Submit Valid Proof" : "";
				$msg1 = $type." has been ".$sts[$status]." by Admin.".$add_msg;
				$notification['user_id'] = $user_id;
				$notification['type'] = "KYC";
				$notification['message'] = $msg1;
				$notification['status'] = "unread";
				UserNotification::create($notification);
				$msg = $type." ".$sts[$status]." successfully";
				Session::flash('success', $msg);
			}
		} else {
			Session::flash('error', 'Please try again.');
		}
		return Redirect::back();
  	}

  	//To list all user's balance
  	public function viewUserBalance() {
        $data = User::select('id','consumer_name')->with('wallet')->get();
        $currency = Currency::where('status',1)->select('id','symbol')->get();
        return view('admin.user.userBalance')->with('user',$data)->with('currency',$currency)->with('redirectUrl',$this->Url);
  	}

  	//To list all user's bank
  	public function viewUserBank() {
    	$data = UserBank::all();
        return view('admin.user.userBank')->with('banks',$data)->with('redirectUrl',$this->Url);
  	}

  	//To view selected bank details
  	public function viewUserBankDetail($id) {
    	$id = User::endecryption(2,$id);
        $data = UserBank::where('id',$id)->first();
        return view('admin.user.viewUserBank')->with('bank',$data)->with('redirectUrl',$this->Url);
  	}

  	//FAQ actions
  	//To view all FAQ added by admin
	public function viewFaq() {
		$data = Faq::orderBy('id','desc')->get();
	    return view('admin.faq.faq')->with('faq_list',$data)->with('redirectUrl',$this->Url);
	}

	//To return view page to edit FAQ details
	public function faqEdit($id = NULL) {
		if($id != "") {
			$id = User::endecryption(2,strip_tags($id));
			$data = Faq::where('id',$id)->first();
			$data['type'] = "edit";
		} else {
			$data['faq'] = "";
			$data['type'] = 'add';
		}
	   	return view('admin.faq.faqEdit')->with('faq',$data)->with('redirectUrl',$this->Url);
	}

	//To update the changes or new FAQ in DB
	public function faqUpdate() {
     	$data = Input::all();
     	$Validation = Validator::make($data, SubAdmin::$faqRule);
    	if($Validation->fails()) {
            Session::flash('error', $Validation->messages());
			return Redirect::back();
    	}
     	if(!empty($data)) {
     		$faq['question'] = strip_tags($data['question']);
     		$faq['description'] = strip_tags($data['description']);
     		$faq['status'] = 'active';
     		$id = User::endecryption(2,strip_tags($data['id']));
     		if($id != "") {
     			$result = Faq::where('id',$id)->update($faq);
     			$msg = 'FAQ Updated Successfully';
     		} else {
     			unset($faq['id']);
     			$result = Faq::create($faq);
     			$msg = 'FAQ Added Successfully';
     		}
     		if($result) {
     			Session::flash('success', $msg);
     		} else {
     			Session::flash('error', 'Failed to update.');
     		}
     		return Redirect::to($this->Url.'/viewfaq');
     	}
	}

	//To activate or deactivate particular FAQ
	public function faqStatus($id) {
		$id = User::endecryption(2,strip_tags($id));
		$faq = Faq::where('id',$id)->first();
		$new_status = ($faq->status == "active") ? "deactive" : "active";
		$update = Faq::where('id',$id)->update(['status' => $new_status]);
		if($update) {
			$new_stat = ($faq->status == "active") ? "Deactivated" : "Activated";
			$msg = "FAQ ".$new_stat. " Successfully";
			Session::flash('success', $msg);
		} else {
			Session::flash('error', 'Please Try Again');
		}
		return Redirect::back();
	}

	//To delete FAQ from DB
	public function faqDelete($id) {
		$id = User::endecryption(2,strip_tags($id));
		$faq = Faq::where(['id'=>$id])->delete();
		if($faq) {
			Session::flash('success', "FAQ deleted Successfully");
		} else {
			Session::flash('error', 'Please Try Again');
		}
		return Redirect::back();
	}

	//admin bank actions
	//To list all banks added by admin
 	public function viewAdminBank() {
    	$data = AdminBank::all();
    	return view('admin.adminbank.adminBank')->with('banks',$data)->with('redirectUrl',$this->Url);
  	}

  	//To return view page to edit admin bank details with type
  	public function bankEdit($id = NULL) {
        if($id != "") {
			$id = User::endecryption(2,strip_tags($id));
			$bank = AdminBank::where('id',$id)->first();
			$type = "edit";
        } else {
			$bank = "";
			$type = "add";
        }
        return view('admin.adminbank.viewSingleBank')->with('bank',$bank)->with('type',$type)->with('redirectUrl',$this->Url);
  	}

  	//To update bank details in DB
  	public function bankUpdate() {
        $data = Input::all();
        $Validation = Validator::make($data, SubAdmin::$bankRule);
        if($Validation->fails()) {
      		Session::flash('error', $Validation->messages());
          	return Redirect::back();
        }
        $bank_name = strip_tags($data['bank_name']);
        $acc_name = strip_tags($data['acc_name']);
        $acc_number = strip_tags($data['acc_number']);
        $bank_code = strip_tags($data['bank_code']);

        $id = User::endecryption(2,$data['id']);
        if($data['id'] != "") {
          	$updata = array('bank_name'=>$bank_name,'acc_name'=>$acc_name,'acc_number'=>$acc_number,'bank_code'=>$bank_code);
          	$result = AdminBank::where('id',$id)->update($updata);
          	$msg = 'Admin bank updated successfully';
        } 
        if($result) {
          	Session::flash('success', $msg);
        } else {
          	Session::flash('error', 'Failed to update.');
        }
        return redirect($this->Url.'/viewAdminBank');
  	}

  	//view admin profit details
  	public function viewAdminProfit() {
        $data = CoinProfit::orderBy('id','desc')->get();

        $profit = array();
        
        return view('admin.adminbank.coinProfit')->with('result',$data)->with('profit',$profit)->with('redirectUrl',$this->Url);
  	}

  	//Block IP actions
  	//To view all blocked IP addresses
  	public function viewBlockIp() {
        $data = BlockIP::all();
        return view('admin.sub.blockIp')->with('ip',$data)->with('redirectUrl',$this->Url);
  	}

  	//To change status of the blocked IP address
  	public function ipAddrStatus($id) {
        $id = User::endecryption(2,strip_tags($id));
        $ipAddr = BlockIP::where('id',$id)->first();
        $new_status = ($ipAddr->status == "active") ? "deactive" : "active";
        $update = BlockIP::where('id',$id)->update(['status' => $new_status]);
        if($update) {
          	$new_stat = ($ipAddr->status == "active") ? "Deactivated" : "Activated";
          	$msg = "IP address ".$new_stat. " Successfully";
          	Session::flash('success', $msg);
        } else {
          	Session::flash('error', 'Please Try Again');
        }
        return Redirect::back();
  	}

  	//To delete blocked IP addresses from DB
  	public function ipAddrDelete($id) {
        $id = User::endecryption(2,strip_tags($id));
        $faq = BlockIP::where(['id'=>$id])->delete();
        if($faq) {
          	Session::flash('success', "IP Address deleted Successfully");
        } else {
          	Session::flash('error', 'Please Try Again');
        }
        return Redirect::back();
  	}

  	//To add IP address to be blocked in DB
  	public function addIpAddress() {
        $data = Input::all();
        if($data == array_filter($data)) {
          	$data['ip_addr'] = strip_tags($data['ip_addr']);
          	$checkIp = BlockIP::where('ip_addr',$data['ip_addr'])->count();
          	if($checkIp == 0) {
            	$insdata = array('ip_addr'=>$data['ip_addr'],'status'=>'active');
            	$createIp = BlockIP::create($insdata);
            	if($createIp) {
              		Session::flash('success','IP Address added successfully');
            	} else {
              		Session::flash('error', 'IP address already added.');
            	}
          	} else {
            	Session::flash('error', 'Failed to add IP address.');
          	}
        } else {
          	Session::flash('error', 'Please enter IP address.');
        }
        return Redirect::back();
  	}

  	//Deposit functions
  	//To view deposit history and pending requests of all users
  	public function viewDeposit($type = NULL) {
        $query = Deposit::orderBy('id', 'desc');
        if($type == "pending") {
			$query = $query->where('status','pending');
		}
		$data = $query->get();
        return view('admin.deposit.deposit')->with('deposit',$data)->with('redirectUrl',$this->Url);
  	}

  	//To view selected deposit details
  	public function viewUserDeposit($id) {
        $id = User::endecryption(2,$id);
        $data = Deposit::where('id', $id)->first();
        return view('admin.deposit.userDeposit')->with('deposit',$data)->with('redirectUrl',$this->Url);
  	}

  	//To confirm deposit from email and send OTP to admin
  	public function confirmDeposit($tid,$uid) {
        $tid = User::endecryption(2,strip_tags($tid));
        $uid = User::endecryption(2,strip_tags($uid));
        $checkStatus = Deposit::where('id', $tid)->where('user_id',$uid)->first();
        if($checkStatus['status'] == "pending") {
          	$otp = User::randomString(8);
          	$encryptOtp = User::endecryption(1,$otp);
          	Deposit::where('id', $tid)->update(['confirm_code'=>$encryptOtp]);

          	$getDetail = User::getProfile($uid);

          	$getEmail = EmailTemplate::where('id',6)->first();
          	$getSiteDetails  = Controller::getEmailTemplateDetails();
          	$info = array('###USER###'=>$getDetail['user']->consumer_name,'###OTP###'=>$otp,'###TYPE###'=>'deposit');
          	$replace = array_merge($getSiteDetails, $info);
          
          	$emaildata = array('content' =>strtr($getEmail->template,$replace));
          	$toDetails['useremail'] = $getSiteDetails['contact_mail_id'];
          	$toDetails['subject'] = $getEmail->subject;
          	$toDetails['from'] = $getSiteDetails['contact_mail_id'];
		  	$toDetails['name'] = $getSiteDetails['site_name'];

          	$sendEmail = Controller::sendEmail($emaildata, $toDetails);

  		  	if(count(Mail::failures()) > 0) {
            	Session::flash('error', 'OTP sending failed.');
            	return Redirect::to($this->Url.'/viewDeposit');
          	} else {
            	return view('admin.deposit.userDeposit')->with('deposit',$checkStatus)->with('secure','confirm')->with('redirectUrl',$this->Url);
          	}
        } else {
          	Session::flash('error', 'Transaction already processed.');
          	return Redirect::to($this->Url.'/viewDeposit');
        }
  	}

  	//To reject deposit from email
  	public function rejectDeposit($tid,$uid) {
        $tid = User::endecryption(2,strip_tags($tid));
        $uid = User::endecryption(2,strip_tags($uid));
        $checkStatus = Deposit::where('id', $tid)->where('user_id',$uid)->first();
        if($checkStatus['status'] == "pending") {
          	return view('admin.deposit.userDeposit')->with('deposit',$checkStatus)->with('secure','cancel')->with('redirectUrl',$this->Url);
        } else {
          	Session::flash('error', 'Transaction already processed.');
          	return Redirect::to($this->Url.'/viewDeposit');
        }
  	}

  	//To update deposit and transactions table in DB and to send email to the user after rejecting deposit request
  	public function updateRejectDeposit() {
        $data = Input::all();
        $reason = strip_tags($data['reason']);

        $id = User::endecryption(2,strip_tags($data['ucode']));
        $userDeposit = Deposit::where('id',$id)->select('user_id','amount','currency','status','reference_no','address_info','payment_method')->first();

        if($userDeposit->status == "pending") {
          	$transactionData = array('user_id'=>$userDeposit->user_id,'type'=>"Deposit",'currency_name'=>$userDeposit->currency,'amount'=>$userDeposit->amount,'total'=>$userDeposit->amount,'method'=>$userDeposit->payment_method,'payment_method'=>$userDeposit->payment_method,'t_status'=>'cancelled','transaction_id'=>$userDeposit->reference_no);
          	$createTransaction = Transaction::create($transactionData);

          	$updateDeposit = Deposit::where('id',$id)->update(['status'=>'cancelled','reject_reason'=>$reason,'approve_date'=>date('Y-m-d H:i:s')]);

          	if($updateDeposit == true) {
            	$getUser = User::where('id',$userDeposit->user_id)->select('consumer_name','wiix_content','unusual_user_key','notify_deposit_withdraw')->first();
            	$usermail = User::endecryption(2,$getUser->wiix_content).User::endecryption(2,$getUser->unusual_user_key);

	            if($getUser->notify_deposit_withdraw == 1) {
	              	$msg1 = "Your Depoist request has been rejected by admin.For ".$reason;
	              	$insdata = array('user_id'=>$userDeposit->user_id,'type'=>'Deposit','message'=>$msg1,'status'=>'unread');
	              	UserNotification::create($insdata);
	            }

	            $getEmail = EmailTemplate::where('id',8)->first();
	            $getSiteDetails  = Controller::getEmailTemplateDetails();
	            $info = array('###USER###'=>$getUser->consumer_name,'###AMT###'=>$userDeposit->amount,'###REASON###' => $reason);
	            $replace = array_merge($getSiteDetails, $info);
	            
	            $emaildata = array('content' =>strtr($getEmail->template,$replace));
	            $toDetails['useremail'] = $usermail;
	            $toDetails['subject'] = $getEmail->subject;
	            $toDetails['from'] = $getSiteDetails['contact_mail_id'];
	  			$toDetails['name'] = $getSiteDetails['site_name'];

	            $sendEmail = Controller::sendEmail($emaildata, $toDetails);

	            if(count(Mail::failures()) > 0) {
	              	Session::flash('error', 'Email sending failed.');
	            } else {
	              	Session::flash('success', 'Transaction rejected successfully.');
	            }
          	}
        } else {
          	Session::flash('error', 'Transaction already processed.');
        }
        return Redirect::to($this->Url.'/viewDeposit');
  	}

  	//To check entered OTP is valid while approving deposit request
  	public function checkConfirmCode() {
	    $code = User::endecryption(1,$_GET['confirm_code']);
	    $id = User::endecryption(2,$_GET['id']);
	    $result = Deposit::where('id', $id)->where('confirm_code', $code)->where('status', 'pending')->count();
	    echo ($result == "1") ? "true" : "false";
  	}

  	//To update deposit, transactions and wallet for the user and to send email to the user after approving deposit
  	public function updateConfirmDeposit() {
        $data = Input::all();
        $code = strip_tags($data['confirm_code']);
        $id = strip_tags($data['ucode']);
        $checkConfirmation = Controller::verifyConfirmation($code,$id);
        if($checkConfirmation == true) {
          	$id = User::endecryption(2,$id);
          	$userDeposit = Deposit::where('id',$id)->select('user_id','amount','currency','status','reference_no','address_info','payment_method')->first();

          	$transactionData = array('user_id'=>$userDeposit->user_id,'type'=>"Deposit",'currency_name'=>$userDeposit->currency,'amount'=>$userDeposit->amount,'total'=>$userDeposit->amount,'method'=>$userDeposit->payment_method,'payment_method'=>$userDeposit->payment_method,'t_status'=>'completed','transaction_id'=>$userDeposit->reference_no);
          	$createTransaction = Transaction::create($transactionData);

          	if($userDeposit->status == "pending") {
				$getCurrency = $userDeposit->currency;
				$getBalance = Wallet::where('user_id',$userDeposit->user_id)->first();
				$total = $getBalance->$getCurrency + $userDeposit->amount;
				if($getCurrency == "USD") {
					$total = number_format($total,2, '.', '');
				} else {
					$total = number_format($total,8, '.', '');
				}
				$update = Wallet::where('user_id',$userDeposit->user_id)->update([$userDeposit->currency => $total]);

				$updateDeposit = Deposit::where('id',$id)->update(['status'=>'completed','confirm_code'=>'','approve_date'=>date('Y-m-d H:i:s')]);

				if($update == true && $updateDeposit == true) {
					$getUser = User::where('id',$userDeposit->user_id)->select('consumer_name','user_mail_id','unusual_user_key','notify_deposit_withdraw')->first();
					$usermail = User::endecryption(2,$getUser->user_mail_id).'@'.User::endecryption(2,$getUser->unusual_user_key);
					if($getUser->notify_deposit_withdraw == 1) {
						$msg1 = "Your Deposit request has been approved by admin.your deposited amount ".$userDeposit->amount. " ".$userDeposit->currency." has been added to your H2O exchange wallet";
						$insdata = array('user_id'=>$userDeposit->user_id,'type'=>'Deposit','message'=>$msg1,'status'=>'unread');
						UserNotification::create($insdata);
					}

					$getEmail = EmailTemplate::where('id',7)->first();
					$getSiteDetails  = Controller::getEmailTemplateDetails();
					$info = array('###USER###'=>$getUser->consumer_name,'###REF###'=>$userDeposit->reference_no,'###AMT###'=>$userDeposit->amount.' '.$userDeposit->currency,'###TOTAL###' => $total.' '.$userDeposit->currency);
					$replace = array_merge($getSiteDetails, $info);

					$emaildata = array('content' =>strtr($getEmail->template,$replace));
					$toDetails['useremail'] = $usermail;
					$toDetails['subject'] = $getEmail->subject;
					$toDetails['from'] = $getSiteDetails['contact_mail_id'];
				  	$toDetails['name'] = $getSiteDetails['site_name'];

					$sendEmail = Controller::sendEmail($emaildata, $toDetails);

					if(count(Mail::failures()) > 0) {
						Session::flash('error', 'Email sending failed.');
					} else {
						Session::flash('success', 'Transaction approved successfully.');
					}
				}
          	}
        } else {
          	Session::flash('error', 'Please verify your OTP.');
        }
        return Redirect::to($this->Url.'/viewDeposit');
  	}

  	//Withdraw functions
  	//To view withdraw history and pending requests of all users
  	public function viewWithdraw($type = NULL) {
        $query = Withdraw::orderBy('id', 'desc');
        if($type == "pending") {
			$query = $query->where('status','pending');
		}
		$data = $query->get();
        return view('admin.withdraw.withdraw')->with('withdraw',$data)->with('redirectUrl',$this->Url);
  	}

  	//To view selected withdraw details
  	public function viewUserWithdraw($id) {
        $id = User::endecryption(2,$id);
        $data = Withdraw::where('id', $id)->first();
        return view('admin.withdraw.userWithdraw')->with('withdraw',$data)->with('redirectUrl',$this->Url);
  	}

  	//To check entered OTP is valid while approving withdraw request
  	public function checkConfirmCodeWithdraw() {
	    $code = User::endecryption(1,$_GET['confirm_code']);
	    $id = User::endecryption(2,$_GET['id']);
	    $result = Withdraw::where('id', $id)->where('confirm_code', $code)->where('status', 'pending')->count();
	    echo ($result == "1") ? "true" : "false";
  	}

  	//To confirm withdraw from email and send OTP to admin
  	public function confirmWithdraw($id,$uid) {
        $id = User::endecryption(2,strip_tags($id));
        $uid = User::endecryption(2,strip_tags($uid));

        $checkStatus = Withdraw::where('id', $id)->where('user_id',$uid)->first();
        if($checkStatus->status == "pending") {
          	$otp = User::randomString(8);
          	$encryptOtp = User::endecryption(1,$otp);
          	Withdraw::where('id', $id)->update(['confirm_code'=>$encryptOtp]);

          	$getDetail = User::getProfile($uid);

          	$getEmail = EmailTemplate::where('id',6)->first();
          	$getSiteDetails  = Controller::getEmailTemplateDetails();
          	$info = array('###USER###'=>$getDetail['user']->consumer_name,'###OTP###'=>$otp,'###TYPE###'=>'withdraw');
          	$replace = array_merge($getSiteDetails, $info);
          
          	$emaildata = array('content' =>strtr($getEmail->template,$replace));
          	$toDetails['useremail'] = $getSiteDetails['contact_mail_id'];
          	$toDetails['subject'] = $getEmail->subject;
          	$toDetails['from'] = $getSiteDetails['contact_mail_id'];
		  	$toDetails['name'] = $getSiteDetails['site_name'];

          	$sendEmail = Controller::sendEmail($emaildata, $toDetails);

  		  	if(count(Mail::failures()) > 0) {
            	Session::flash('error', 'OTP sending failed.');
            	return Redirect::to($this->Url.'/viewWithdraw');
          	} else {
            	return view('admin.withdraw.userWithdraw')->with('withdraw',$checkStatus)->with('secure','confirm')->with('redirectUrl',$this->Url);
          	}
        } else {
          	Session::flash('error', 'Transaction already processed.');
          	return Redirect::to($this->Url.'/viewWithdraw');
        }
  	}

  	//To reject withdraw from email
  	public function rejectWithdraw($tid,$uid) {
        $tid = User::endecryption(2,strip_tags($tid));
        $uid = User::endecryption(2,strip_tags($uid));
        $checkStatus = Withdraw::where('id', $tid)->where('user_id',$uid)->first();
        if($checkStatus->status == "pending") {
          	return view('admin.withdraw.userWithdraw')->with('withdraw',$checkStatus)->with('secure','cancel')->with('redirectUrl',$this->Url);
        } else {
          	Session::flash('error', 'Transaction already processed.');
          	return Redirect::to($this->Url.'/viewWithdraw');
        }
  	}

  	//To update withdraw, transactions and wallet for the user and to send email to the user after approving withdraw
  	public function updateConfirmWithdraw() {
        $data = Input::all();
        $code = strip_tags($data['confirm_code']);
        $id = strip_tags($data['ucode']);

        if(isset($data['reference_no'])) {
          	$referenceNumber = strip_tags($data['reference_no']);
        }

        $checkConfirmation = Controller::verifyWithdrawConfirmation($code,$id);
        if($checkConfirmation == true) {
          	$id = User::endecryption(2,$id);

          	$userWithdraw = Withdraw::where('id',$id)->select('user_id','amount','currency','status','total','reference_no','address_info','payment_method','fees_amt')->first();

          	if($userWithdraw->status == "pending") {
          		$currency = $userWithdraw->currency;
            	if($currency != 'USD') {
              		$amount = $userWithdraw->total;
              		$toAddress = $userWithdraw->address_info;
	              	if($currency == 'BTC') {
	                	$referenceNumber = Controller::sendBtcFundToAddress($toAddress,$amount);
	              	} elseif($currency == 'ETH') {
	                	$fromAddr = User::getAddress(0,'ETH');
	                	$referenceNumber = Controller::sendEthFundToAddress($fromAddr, $amount, $toAddress);
	              	} elseif($currency == 'LTC') {
	                	$referenceNumber = Controller::sendLtcToAddress($toAddress,$amount);
	              	} else {
	                	$referenceNumber = "";
	              	}
            	}

            	$transactionData = array('user_id'=>$userWithdraw->user_id,'type'=>"Withdraw",'currency_name'=>$userWithdraw->currency,'amount'=>$userWithdraw->amount,'total'=>$userWithdraw->total,'method'=>$userWithdraw->payment_method,'payment_method'=>$userWithdraw->payment_method,'t_status'=>'completed','transaction_id' => $referenceNumber);
            	$createTransaction = Transaction::create($transactionData);

            	$updateWithdraw = Withdraw::where('id',$id)->update(['status'=>'completed','confirm_code'=>'','approve_date'=>date('Y-m-d H:i:s'),'reference_no'=>$referenceNumber]);

            	if($updateWithdraw == true) {
              		$profitData = array(
			    		'user_id'=>$userWithdraw->user_id,
			            'theftAmount'=>$userWithdraw->fees_amt,
			            'theftCurrency'=>$userWithdraw->currency,
			            'Type'=>'Withdraw Profit'
	    	  		);
	    	  		$query = CoinProfit::create($profitData);
              	
              		$getUser = User::where('id',$userWithdraw->user_id)->select('consumer_name','wiix_content','unusual_user_key','notify_deposit_withdraw')->first();
              		$usermail = User::endecryption(2,$getUser->wiix_content).User::endecryption(2,$getUser->unusual_user_key);

              		if($getUser->notify_deposit_withdraw == 1) {
                		$msg1 = "Your withdraw request has been approved by admin.your withdraw amount ".$userWithdraw->amount. " ".$userWithdraw->currency." has been debited from your wallet";
                		$insdata = array('user_id'=>$userWithdraw->user_id,'type'=>'Withdraw','message'=>$msg1,'status'=>'unread');
                		UserNotification::create($insdata);
              		}

              		$getEmail = EmailTemplate::where('id',11)->first();
              		$getSiteDetails  = Controller::getEmailTemplateDetails();
              		$info = array('###USER###'=>$getUser->consumer_name,'###REF###'=>$referenceNumber,'###AMT###'=>$userWithdraw->amount." ".$userWithdraw->currency,'###TOTAL###' => $userWithdraw->total." ".$userWithdraw->currency);
              		$replace = array_merge($getSiteDetails, $info);
              
              		$emaildata = array('content' =>strtr($getEmail->template,$replace));
              		$toDetails['useremail'] = $usermail;
              		$toDetails['subject'] = $getEmail->subject;
              		$toDetails['from'] = $getSiteDetails['contact_mail_id'];
			  		$toDetails['name'] = $getSiteDetails['site_name'];

              		$sendEmail = Controller::sendEmail($emaildata, $toDetails);

              		if(count(Mail::failures()) > 0) {
                		Session::flash('error', 'Email sending failed.');
              		} else {
                		Session::flash('success', 'Withdraw approved successfully.');
              		}
            	}
          	}
        } else {
          	Session::flash('error', 'Please verify your OTP.');
        }
        return Redirect::to($this->Url.'/viewWithdraw');
  	}

  	//To update withdraw and transactions table in DB and to send email to the user after rejecting withdraw request
  	public function updateRejectWithdraw() {
        $data = Input::all();
        $reason = strip_tags($data['reason']);

        $id = User::endecryption(2,strip_tags($data['ucode']));
        $userWithdraw = Withdraw::where('id',$id)->select('user_id','amount','currency','status','total','reference_no','address_info','payment_method')->first();

        if($userWithdraw->status == "pending") {
          	$transactionData = array('user_id'=>$userWithdraw->user_id,'type'=>"Withdraw",'currency_name'=>$userWithdraw->currency,'amount'=>$userWithdraw->amount,'total'=>$userWithdraw->total,'method'=>$userWithdraw->payment_method,'payment_method'=>$userWithdraw->payment_method,'t_status'=>'cancelled');
          	$createTransaction = Transaction::create($transactionData);

          	$currency = $userWithdraw->currency;
          	$balance = Wallet::where('user_id',$userWithdraw->user_id)->first();
          	$usrbal = $balance->$currency;
          	$updatebalance = $usrbal + $userWithdraw->amount;
          	if($currency == "USD") {
            	$updatebalance = number_format($updatebalance,2, '.', '');
          	} else {
            	$updatebalance = number_format($updatebalance,8, '.', '');
          	}

          	$updatWallet = Wallet::where('user_id',$userWithdraw->user_id)->update([$currency => $updatebalance]);

          	$updateWithdraw = Withdraw::where('id',$id)->update(['status'=>'cancelled','reject_reason'=>$reason,'approve_date'=>date('Y-m-d H:i:s')]);

          	if($updateWithdraw == true) {
            	$getUser = User::where('id',$userWithdraw->user_id)->select('consumer_name','wiix_content','unusual_user_key','notify_deposit_withdraw')->first();
            	$usermail = User::endecryption(2,$getUser->wiix_content).User::endecryption(2,$getUser->unusual_user_key);

            	if($getUser->notify_deposit_withdraw == 1) {
              		$msg1 = "Your Withdraw request has been rejected by admin.For ".$reason;
              		$insdata = array('user_id'=>$userWithdraw->user_id,'type'=>'Withdraw','message'=>$msg1,'status'=>'unread');
              		UserNotification::create($insdata);
            	}

            	$getEmail = EmailTemplate::where('id',12)->first();
            	$getSiteDetails  = Controller::getEmailTemplateDetails();
            	$info = array('###USER###'=>$getUser->consumer_name,'###AMT###'=>$userWithdraw->amount." ".$currency,'###REASON###' => $reason);
            	$replace = array_merge($getSiteDetails, $info);
            
            	$emaildata = array('content' =>strtr($getEmail->template,$replace));
            	$toDetails['useremail'] = $usermail;
            	$toDetails['subject'] = $getEmail->subject;
            	$toDetails['from'] = $getSiteDetails['contact_mail_id'];
				$toDetails['name'] = $getSiteDetails['site_name'];

            	$sendEmail = Controller::sendEmail($emaildata, $toDetails);

            	if(count(Mail::failures()) > 0) {
              		Session::flash('error', 'Email sending failed.');
        		} else {
              		Session::flash('success', 'Transaction rejected successfully.');
            	}
          	}
        } else {
          	Session::flash('error', 'Transaction already processed.');
        }
        return Redirect::to($this->Url.'/viewWithdraw');
  	}

  	//Trade actions
  	public function viewTradePairs() {
        $data = TradePairs::all();
        return view('admin.trade.tradePairs')->with('pairs',$data)->with('redirectUrl',$this->Url);
  	}

  	public function tradePairEdit($id = NULL) {
        $id = User::endecryption(2,strip_tags($id));
        $data = TradePairs::where('id',$id)->first();
        return view('admin.trade.tradePairEdit')->with('pairs',$data)->with('redirectUrl',$this->Url);
  	}

  	public function tradePairUpdate() {
        $data = Input::all();
        $Validation = Validator::make($data, SubAdmin::$tradePairRule);
        if($Validation->fails()) {
          	Session::flash('error', $Validation->messages());
          	return Redirect::to($this->Url.'/viewTradePairs');
        }
        $id = User::endecryption(2,strip_tags($data['id']));
        $updata = array('buy_rate'=>strip_tags($data['buy_rate']),'sell_rate'=>strip_tags($data['sell_rate']));
        $result = TradePairs::where('id',$id)->update($updata);
        if($result) {
          	Session::flash('success', 'Details has been updated successfully');
        } else {
          	Session::flash('error', 'Failed to update!');
        }
        return Redirect::to($this->Url.'/viewTradePairs');
  	}

  	public function viewTradeFee() {
        $data = TradingFee::all();
        return view('admin.trade.tradeFees')->with('fees',$data)->with('redirectUrl',$this->Url);
  	}

  	public function tradeFeeEdit($id = NULL) {
        $id = User::endecryption(2,strip_tags($id));
        $data = TradingFee::where('id',$id)->first();
        return view('admin.trade.tradeFeeEdit')->with('fees',$data)->with('redirectUrl',$this->Url);
  	}

  	public function tradeFeeUpdate() {
        $data = Input::all();
        $Validation = Validator::make($data, SubAdmin::$tradeFeeRule);
        if($Validation->fails()) {
          	Session::flash('error', $Validation->messages());
          	return Redirect::to($this->Url.'/viewTradeFee');
        }
        $id = User::endecryption(2,strip_tags($data['id']));
        if($data['to_amt'] == ">") {
          	$to_amt = ">";
        } else {
          	$to_amt = strip_tags($data['to_amt']);
        }
        $updata=array('from_amt'=>strip_tags($data['from_amt']),'to_amt'=>$to_amt,'fee'=>strip_tags($data['fee']));
        $result = TradingFee::where('id',$id)->update($updata);
        if($result) {
          	Session::flash('success', 'Details has been updated successfully');
        } else {
          	Session::flash('error', 'Failed to update!');
        }
        return Redirect::to($this->Url.'/viewTradeFee');
  	}

  	public function viewOrderHistory($type = NULL) {
        $names = array('active', 'partially');
        if($type != "") {
          	$orders = CoinOrder::whereIn('status',$names)
                  	->where('Type',$type)->orderBy('id','desc')->get();
        } else {
          	$orders = CoinOrder::whereIn('status',$names)
                  	->orderBy('id','desc')->get();
        }
        $orderdata = array();
        if(!$orders->isEmpty()) {
          	foreach($orders as $trade) {
	            $tradeuserId  = $trade->user_id;
	            $tradePrice   = $trade->Price;
	            $tradeAmount  = $trade->Amount;
	            $tradeType    = $trade->Type;
	            $tradeTotal   = $trade->Total;
	            $fees = $trade->Fee;
	            $firstCurrency  = $trade->firstCurrency;
	            $secondCurrency = $trade->secondCurrency;
	            $orderDate    = $trade->orderDate;
	            $orderTime    = $trade->orderTime;
	            $status    = $trade->status;
	            $ts = $orderDate." ".$orderTime;
	            $getUser = User::getProfile($tradeuserId);
	            $username = $getUser['user']->consumer_name;
	            $amount = $tradeAmount." ".$firstCurrency;
	            $total  = $tradeTotal." ".$secondCurrency;
	            $pair  = $firstCurrency."/".$secondCurrency;

            	if($status == "partially") {
              		$activefilledAmount = Trade::checkOrdertempdetails1($trade->id,$tradeType);
              		$partial_date = Trade::checkOrdertempdetails_date($trade->id,$tradeType);
              		if($activefilledAmount) {
		                $fee_per = $trade->fee_per;
		                $amount = $tradeAmount - $activefilledAmount;
		                $fees = ($amount*$tradePrice)*$fee_per/100;
		                $total = ($amount * $tradePrice);
		                $amount = $amount." ".$firstCurrency;
		                $total = $total." ".$secondCurrency;
              		}
              		$ts = $partial_date;
            	}
            	if($ts == "0000-00-00 00:00:00" || $ts == NULL || $ts == "") {
              		$ts = $trade->datetime;
            	}
            	$data = array('tradetime'=>$ts,'pair'=>$pair,'user'=>$username,'tradetype'=>$tradeType,'tradeprice'=>$tradePrice,'amount'=>$amount,'total'=>$total,'status'=>$status,'fees'=>$fees);
            	array_push($orderdata,$data);
          	}
        }
        return view('admin.trade.orderHistory')->with('orders',$orderdata)->with('redirectUrl',$this->Url);
 	}

 	public function viewTradeHistory() {
        $orders = OrderTemp::orderBy('id','desc')->get();
        $orderdata = array();
        if(!$orders->isEmpty()) {
        	foreach($orders as $row) {
				$tempPair = $row->pair;
				$buyerUserId = $row->buyerUserId;
				$sellerUserId = $row->sellerUserId;
				$firstCurrency  = $row->firstCurrency;
            	$secondCurrency = $row->secondCurrency;
            	$cancelId = $row->cancel_id;

				$datetime = $row->datetime;
				$filledAmount = $row->filledAmount;
				$askPrice = $row->askPrice;

				if($buyerUserId != "") {
		         	$getUser = User::getProfile($buyerUserId);
            		$username = $getUser['user']->consumer_name;
            		$tradeType = "Buy";
            		$fees = $row->buy_fee;
            		$total = $row->buy_total;

            		$data = array('tradetime'=>$datetime,'pair'=>$tempPair,'user'=>$username,'tradetype'=>$tradeType,'tradeprice'=>$askPrice,'amount'=>$filledAmount,'total'=>$total,'fee'=>$fees);
            		if($cancelId != null) {
            			$data['status'] = "cancelled";
            		} else {
            			$data['status'] = "filled";
            		}
            		array_push($orderdata,$data);
				}

				if($sellerUserId != "") {
					$getUser = User::getProfile($sellerUserId);
            		$username = $getUser['user']->consumer_name;
            		$tradeType = "Sell";
            		$fees = $row->sell_fee;
            		$total = $row->sell_total;

            		$data = array('tradetime'=>$datetime,'pair'=>$tempPair,'user'=>$username,'tradetype'=>$tradeType,'tradeprice'=>$askPrice,'amount'=>$filledAmount,'total'=>$total,'status'=>"filled",'fee'=>$fees);
            		if($cancelId != null) {
            			$data['status'] = "cancelled";
            		} else {
            			$data['status'] = "filled";
            		}
            		array_push($orderdata,$data);
				}
			}
        }
        return view('admin.trade.tradeHistory')->with('orders',$orderdata)->with('redirectUrl',$this->Url);
  	}

  	//Profile Actions
  	//To view admin profile
  	public function viewProfile() {
  		if(session('adminId')!='') {
  			$data = SubAdmin::where('id',session('adminId'))->first();
         	return view('admin.sub.profile')->with('profile',$data)->with('redirectUrl',$this->Url);
      	}
      	Session::flash('error', 'Please login to continue!');
      	return Redirect::to($this->Url);
  	}

  	//To update admin details in DB
  	public function updateProfile() {
  		if(session('adminId')!='') {
	  		$adminId = session('adminId');
	  		$data = Input::all();

	  		$Validation = Validator::make($data, SubAdmin::$profileRule);
	      	if($Validation->fails()) {
	          	Session::flash('error', $Validation->messages());
	  			return Redirect::back();
	      	}
			if($_FILES['admin_profile']['name'] == "") {
	  			$image = strip_tags($data['admin_profile_old']);
	  		} else {
	  			$fileExtensions = ['jpeg','jpg','png'];
	  			$fileName = $_FILES['admin_profile']['name'];
	  			$fileType = $_FILES['admin_profile']['type'];
	  			$explode = explode('.',$fileName);
	  			$extension = end($explode);
	  			$fileExtension = strtolower($extension);
	        	$mimeImage = mime_content_type($_FILES["admin_profile"]['tmp_name']);
	        	$explode = explode('/', $mimeImage);

	        	if (!in_array($fileExtension,$fileExtensions)) {
		    		Session::flash('error', 'Invalid file type. Only image files are accepted.');
	  				return Redirect::back();
	        	} else {
	          		if($explode[0] != "image") {
	            		Session::flash('error', 'Invalid file type. Only image files are accepted.');  
	            		return Redirect::back();
	          		}
	  				$cloudUpload = \Cloudinary\Uploader::upload($_FILES["admin_profile"]['tmp_name']);
	          		if($cloudUpload) {
	            		$image = $cloudUpload['secure_url'];
	          		} else {
	            		Session::flash('error', $cloudUpload["error"]["message"]);
	            		return Redirect::back();
	          		}
	  			}
	  		}
	  		$result = SubAdmin::where('id',$adminId)->update(['username'=>strip_tags($data['admin_username']),'profile'=>$image,'phone'=>strip_tags($data['admin_phno']), 'address'=>strip_tags($data['admin_address']), 'city'=>strip_tags($data['admin_city']), 'state'=>strip_tags($data['admin_state']), 'postal'=>strip_tags($data['admin_postal']), 'country'=>strip_tags($data['country'])]);
	  		if($result) {
	   			Session::flash('success', 'Profile Updated Successfully');
	   		} else {
	   			Session::flash('error', 'Failed to update.');
	   		}
	   		return Redirect::back();
      	}
      	Session::flash('error', 'Please login to continue!');
      	return Redirect::to($this->Url);
  	}

  	//To check the entered current password is valid
  	public function checkPassword() {
  		if(session('adminId')!='') {
         	$pwd = User::endecryption(1,$_GET['current_pwd']);
         	$getCount = SubAdmin::where('id',session('adminId'))
         			->where('skey',$pwd)->count();
         	echo ($getCount == 1) ? "true" : "false";
      	} else {
      		echo "false";
      	}
  	}

  	//To update new password in DB
  	public function changePassword() {
  		if(session('adminId')!='') {
	  		$data = Input::all();
	  		$adminId = session('adminId');
	     	$Validation = Validator::make($data, SubAdmin::$pwdRule);
	    	if($Validation->fails()) {
	        	Session::flash('error', $Validation->messages());
		    	return Redirect::back();
	    	}
	     	if($data == array_filter($data)) {
     			$new_pwd = strip_tags($data['new_pwd']);
		      	$confirm_pwd = strip_tags($data['confirm_pwd']);
	     		$pwd = User::endecryption(1,strip_tags($data['current_pwd']));
	     		$getCount = SubAdmin::where('id',$adminId)
	     				->where('skey',$pwd)->count();
	     		if($getCount == 1) {
	     			if($new_pwd == $confirm_pwd) {
	     				$updateData = User::endecryption(1,$confirm_pwd);
	     				$result = SubAdmin::where('id',$adminId)->update(['skey' => $updateData]);
	     			}
	     		}
	     		if($result) {
     				Session::flash('success', 'Password Updated Successfully');
	     		} else {
	     			Session::flash('error', 'Failed to update.');
	     		}
	     		return Redirect::back();
	     	}
      	}
      	Session::flash('error', 'Session Expired');
      	return Redirect::to($this->Url);
  	}

  	//site settings action
  	//To display site settings
  	public function adminSettings() {
  		$data = SiteSettings::first();
     	return view('admin.home.settings')->with('adminsettings',$data)->with('redirectUrl',$this->Url);
  	}

  	//To update site details in DB
  	public function updateSite() {
  		$data = Input::all();
  		$Validation = Validator::make($data, SubAdmin::$siteRule);
    	if($Validation->fails()) {
    		foreach($Validation->messages()->getMessages() as $field=>$message) {
				Session::flash('error', $message[0]);
				return Redirect::back();
			}
    	}

  		if($_FILES['site_logo']['name'] == "") {
			$image = strip_tags($data['site_logo_old']);
		} else if($_FILES['site_logo']['name'] != "") {
			$fileExtensions = ['jpeg','jpg','png'];
			$fileName = $_FILES['site_logo']['name'];
			$fileType = $_FILES['site_logo']['type'];
			$explode = explode('.',$fileName);
			$extension = end($explode);
			$fileExtension = strtolower($extension);
        	$mimeImage = mime_content_type($_FILES["site_logo"]['tmp_name']);
        	$explode = explode('/', $mimeImage);

	        if (!in_array($fileExtension,$fileExtensions)) {
	    		Session::flash('error', 'Invalid file type. Only image files are accepted.');
	          	return Redirect::back();
	        } else {
	          	if($explode[0] != "image") {
	            	Session::flash('error', 'Invalid file type. Only image files are accepted.');  
	          		return Redirect::back();
	          	}
				$cloudUpload = \Cloudinary\Uploader::upload($_FILES["site_logo"]['tmp_name']);
	          	if($cloudUpload) {
	            	$image = $cloudUpload['secure_url'];
	          	} else {
	            	Session::flash('error', $cloudUpload["error"]["message"]);
	            	return Redirect::back();
	          	}
			}
		}

    	if($_FILES['site_favicon']['name'] == "") {
        	$iconImage =strip_tags( $data['site_favicon_old']);
      	} else if($_FILES['site_favicon']['name'] != "") {
	        $fileExtensions = ['ico','jpeg','jpg','png'];
	        $fileName = $_FILES['site_favicon']['name'];
	        $fileType = $_FILES['site_favicon']['type'];
	        $explode = explode('.',$fileName);
	        $extension = end($explode);
	        $fileExtension = strtolower($extension);
	        $mimeImage = mime_content_type($_FILES["site_favicon"]['tmp_name']);
	        $explode = explode('/', $mimeImage);

	        if (!in_array($fileExtension,$fileExtensions)) {
	          	Session::flash('error', 'Invalid file type. Only .ico files are accepted.');
	          	return Redirect::back();
        	} else {
	          	if($explode[0] != "image") {
	            	Session::flash('error', 'Invalid file type. Only image files are accepted.');  
	            	return Redirect::back();
	          	}
	          	$cloudUploader = \Cloudinary\Uploader::upload($_FILES["site_favicon"]['tmp_name']);
	          	if($cloudUpload) {
	            	$iconImage = $cloudUploader['secure_url'];
	          	} else {
	            	Session::flash('error', $cloudUpload["error"]["message"]);
	            	return Redirect::back();
	          	}
	        }
    	}

		$result = SiteSettings::where('id',1)->update(['site_name'=>strip_tags($data['site_name']), 'site_logo'=>$image, 'site_favicon'=>$iconImage, 'contact_email'=>$data['contact_email'], 'contact_number'=>strip_tags($data['contact_no']), 'contact_address'=>strip_tags($data['contact_address']), 'city'=>strip_tags($data['city']), 'country'=>strip_tags($data['country']), 'copy_right_text'=>strip_tags($data['copy_right_text']), 'fb_url'=>strip_tags($data['facebook_url']), 'twitter_url'=>strip_tags($data['twitter_url']), 'linkedin_url'=>strip_tags($data['linkedin_url']), 'googleplus_url'=>strip_tags($data['googleplus_url']),'id_proofs'=>strip_tags($data['id_proofs'])]);

		if($result) {
 			Session::flash('success', 'Site Updated Successfully');
 		} else {
 			Session::flash('error', 'Failed to update.');
 		}
 		return Redirect::back();
  	}
}