<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Basic;
use App\profile;
use App\wallet;
use App\bank_details;
use App\Deposit;
use App\PaymentGateway;
 require public_path('lib/Twocheckout.php');

\Twocheckout::privateKey('5CAA64CC-0A93-480C-87A8-75673E5163BA'); //Private Key
\Twocheckout::sellerId('901365957'); // 2Checkout Account Number
\Twocheckout::sandbox(true);

class PaymentController extends Controller
{


	public function payment(Request $request,$cur=false)
	{

		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }
	$getusers= Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_consumers');
		$usersta=$getusers->status;

		if($usersta == 0)
		{
			AuthController::logout($request);
			return redirect('/');
		}

		$where = array("user_id"=>Session::get('user_id'));
		$order= array('id','desc');
		$data["deps_details"] = Basic::getmultiplerow($where,'wiix_mydeposits',$order);
		//print_r($data["deps_details"]);exit;
		$data["with_details"] = Basic::getmultiplerow($where,'wiix_withdraw',$order);


        $where = array();
		$order= array('id','asc');
		$data["getpairs"] = Basic::getmultiplerow($where,'wiiix_pairs',$order);
		

		$data['bank1'] = Basic::getsinglerow(array('id' => 1),'wiix_adminbank');
		$data['bank2'] = Basic::getsinglerow(array('id' => 1),'wiix_adminbank');
		$data['bank3'] = Basic::getsinglerow(array('id' => 1),'wiix_adminbank');

		$data['consumer_det'] = Basic::getsinglerow(array('user_id' => Session::get('user_id')),'wiix_consumers');

		$data['bankdetails'] =Basic::getsinglerow(array('user_id' => Session::get('user_id')),'wiix_user_bank');
		$data['mywallet'] =Basic::getsinglerow(array('user_id' => Session::get('user_id')),'wiix_user_wallet');
		$data['w_min']=Basic::getsinglerow(array('id' => 1),'wiix_settings');
		$data['w_max']=Basic::getsinglerow(array('id' => 1),'wiix_settings');
		$data['comm']=Basic::getsinglerow(array('id' => 1),'wiix_settings');



			$data['getverdoc']= Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_profile');


			$data['getbnkdoc']= Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_bank');
	
      	$where = array('page_name'=>'Payment');
		$data['prof']= Basic::getsinglerow($where,'wiix_cms');
		$data['currency']= $cur;
		$data['title'] = $data['prof']->meta_title;
		$data['meta_keyword'] = $data['prof']->meta_keyword;
		$data['meta_description'] = $data['prof']->meta_description;
		$data['settings']=HomeController::mainfunction();

		return view('consumer/payment',$data);

  }

public function dep_curren(Request $request)
{
$currency = $request['cur'];


$wherepro = array('user_id'=>Session::get('user_id'));
$profie= Basic::getsinglerow($wherepro,'wiix_consumers');

if($currency != "")
{

if($currency != "KRW")
{

	if($currency == "BTC")
	{
$address=$profie->BTC_address;

	}
	else if($currency == "ETH")
	{
$address=$profie->ETH_address;
	}
	else if($currency == "LTC")
	{
$address=$profie->LTC_address;
	}
	else if($currency == "XRP")
	{
$address=$profie->XRP_address;
	}
	else if($currency == "DOGE")
	{
$address=$profie->DOGE_address;
	}
	else if($currency == "ETC")
	{
$address=$profie->ETC_address;
	}
	else if($currency == "XMR")
	{
$address=$profie->XMR_address;
	}
	else if($currency == "IOTA")
	{
$address=$profie->IOTA_address;
	}
	else if($currency == "BTG")
	{
$address=$profie->BTG_address;
	}
	else if($currency == "DASH")
	{
		$address=$profie->DASH_address;

	}
	$result = '<p>Scan QR Code</p>
                                    <div class="depQrSec">
                                        <img src="https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=$address&choe=UTF-8&chld=L" class="img-fluid">
                                         <div class="form-group">
                                            <label>'.$currency.' Address : '.$address.'</label>
                                            
                                        </div>
                                    </div>';

	echo $result;
}
else{
	$result = '';
	echo $result;
}
}
else{
$result = '';
	echo $result;	
}
}

public function deposit(Request $request)
{

		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }

		

		$user_id = Session::get('user_id');


			$wherepro = array('user_id'=>$user_id);
		$userdetails= Basic::getsinglerow($wherepro,'wiix_consumers');


		if($request['li_0_price'] || $request['depamount']){



				$getverifydoc=Basic::getsinglerow($wherepro,'wiix_user_profile');

			

			if(($getverifydoc->photo_id_status == 1) && ($getverifydoc->address_prof_status == 1))
			{

if(strip_tags($request['payment_method']) == "paypal") {

	$userId=Session::get('user_id');
     

   
try {
    $charge = \Twocheckout_Charge::auth(array(
        "merchantOrderId" => "123",
        "token"      => $request['token'],
        "currency"   => $request['currency_code'],
        "total"      => $request['li_0_price'],
        "billingAddr" => array(
             "name" => 'Testing Tester',
            "addrLine1" => '123 Test St',
            "city" => 'Columbus',
            "state" => 'OH',
            "zipCode" => '43123',
            "country" => 'USA',
            "email" => 'example@2co.com',
            "phoneNumber" => '555-555-5555'
        )
    ));

    if ($charge['response']['responseCode'] == 'APPROVED') {
        // echo "Thanks for your Order!";
        // echo "<h3>Return Parameters:</h3>";
        // echo "<pre>";
        // print_r($charge);
        // echo "</pre>";

        $ip_address = PageHandler::get_client_ip();


$data=array('user_id' => $userId,'payment_method' => "2checkout", 'dep_amount' => $request['li_0_price'], 'dep_currency' => "KRW",'txn_id' => $charge['response']['transactionId'],'ip_addr'=>$ip_address,'dep_status' => 1);

        Basic::insertdatas($data,'wiix_mydeposits');

        $tranamt=$charge['response']['total'];
        $depcuys="KRW";
        //$request['currency_code'];

$walbal = Basic::getsinglerow(array('user_id'=>$userId),'wiix_user_wallet');
        $tottranamt=(($walbal->$depcuys) + $tranamt);


        wallet::where('user_id',$userId)->update([$depcuys=>$tottranamt]);


        $request->session()->flash('success', 'Your deposit request successfully approved from 2Checkout');
        return redirect('/payment');

    }
    else{
$request->session()->flash('error', 'Your deposit request not approved from 2Checkout');
        return redirect('/payment');
    }
} catch (Twocheckout_Error $e) {
    print_r($e->getMessage());
}


$ip_address = PageHandler::get_client_ip();
 $browser=Helper::getBrowser();

DB::table('wiix_log_history')->insert(['user_id'=>Session::get('user_id'),'ip'=>$ip_address,'browser'=>$browser,'type'=>"Deposit"]);





    
    }
else
{
			$txid = 'TD'.Session::get('user_id').time();



			$details = Basic::getsinglerow(array('txn_id'=>$txid),'wiix_mydeposits');

			if(empty($details)){
				
				if(isset($request['depamount']) && (float)$request['depamount']>0){
					//print_r("Haiii");
					//print_r("hai");
			//print_r($postdata['currency']);exit;

					$ip_address = PageHandler::get_client_ip();

					$depositdata = array('dep_amount'=>$request['depamount'],
						
						'dep_status'=>"0",
						
						'user_id'=>Session::get('user_id'), 'txn_id'=>$txid,
						'dep_currency'=>$request['dep_cur'],'bank_value'=>$request['adminbankdet'],'payment_method' => $request['payment_method'],'ip_addr'=>$ip_address

						);


					PaymentController::add_transaction($depositdata);

				

					$request->session()->flash('success', 'Your deposit request send successfully to our team');
					return redirect('/payment');
					
					
				}else{
					$request->session()->flash('error', 'Amount should be greater than zero');

					return redirect('/payment');
				}	

			}else
			{

			}

		
}

		}

		else{
		$request->session()->flash('error', 'Before deposit verify your document');

			return redirect('/payment');		
		}
		}
		else{

	     	return redirect('/payment');
		}

}

function add_transaction($data){



			$wherepro = array('user_id'=>$data['user_id']);
		$userdetails= Basic::getsinglerow($wherepro,'wiix_consumers');

   $insert=Basic::insertdatas($data,'wiix_mydeposits');


$user_prof = Basic::getsinglerow(array("user_id"=>$data['user_id']),'wiix_user_profile');

        $email=array();
        $email[0] = Helper::encrypt_decrypt("decrypt",$userdetails->secret_key);
        $email[1] = Helper::encrypt_decrypt("decrypt",$userdetails->display);
        $secure_email = $email[0]."@".$email[1];

			$wherepro = array('user_id'=>$data['user_id']);
		$noti= Basic::getsinglerow($wherepro,'wiix_notification');



if($noti->transaction == 1)
{


/*Email starts*/
          $msg=DB::table('wiix_email_templates')->where('mail_key','deposit_fiat')->get();
							$to=$secure_email;


							$message=$msg[0]->message;
							$date=date('M d, Y');
							
							$message=str_replace("##AMOUNT##",$data['dep_amount'],$message);
							$message=str_replace("##NAME##",$user_prof->first_name,$message);
							$message=str_replace("##STATUS##","Pending",$message);
							$message=str_replace("##CURRENCY##",$data['dep_currency'],$message);
							$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
							$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
							
							$email_template = 'Deposit Status';
							
							Helper::mailsend($email_template,$message,$to);

						}
         

        return $insert;
    }

     public function paypalSuccess() {
  $user_id = $_GET['item_number'];
  $amount = $_GET['amt'];
  $currency = $_GET['cc'];
  $type = $_GET['item_name'];
  $ref = $_GET['tx'];
  $status = strtolower($_GET['st']);
  $txId = $_GET['cm'];

  if($status == "pending") {
   $getBalance = wallet::select($currency)
   ->where('user_id',$user_id)->first();
   $balance = $getBalance->$currency + $amount;
  
   $result = Wallet::where('user_id',$user_id)->update([$currency => $balance]);
   Deposit::where('id',$txId)->update(['user_id'=>$user_id,'dep_currency'=>$currency,'dep_amount'=>$amount,'payment_method'=>'paypal','txn_id'=>$ref,'dep_status'=>1]);

 
  }
  Session::flash('success', 'Payment Successful');
  return Redirect('/payment');
 }

 //to display error message on payment failure
 public function paypalCancel($id,$type) {
  $userDeposit = Deposit::where('id',$id)->select('user_id','dep_amount','dep_currency','dep_status','txn_id','payment_method')->first();
  Deposit::where('id',$id)->update(['status'=> 3]);
   


  Session::flash('error', 'Payment Canceled!');
   return Redirect('/payment');
 }


 public function withdraw(Request $request)
{

	
		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }


      $withdraw_amount=$request['w_amount'];

	       if($request->isMethod('post')) 
    {

		$getverifydoc= Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_profile');



			

			if(($getverifydoc->photo_id_status == 1) && ($getverifydoc->address_prof_status == 1))
			{

			
			
			$getbankdoc= Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_bank');
				

				$getbankdoc=Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_bank');

				if($getbankdoc->status == 3)
				{


			$withdraw_amount=$request['w_amount'];


			$mywallet=Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_wallet');



			$max_min = Basic::getsinglerow(array('id'=>'1'),'wiix_settings');


			$w_minbtc=$max_min->btc_wmin;
			$w_mineth=$max_min->eth_wmin;
			$w_minltc=$max_min->ltc_wmin;
			$w_minxrp=$max_min->xrp_wmin;
			$w_mineth=$max_min->eth_wmin;
			$w_mindoge=$max_min->doge_wmin;
			$w_minetc=$max_min->etc_wmin;
			$w_minxmr=$max_min->xmr_wmin;
			$w_miniota=$max_min->iota_wmin;
			$w_minbtg=$max_min->btg_wmin;
			$w_mindash=$max_min->dash_wmin;
			$w_minkrw=$max_min->krw_wmin;
			

			$w_maxbtc=$max_min->btc_wmax;
			$w_maxeth=$max_min->eth_wmax;
			$w_maxltc=$max_min->ltc_wmax;
			$w_maxxrp=$max_min->xrp_wmax;
			$w_maxdoge=$max_min->doge_wmax;
			$w_maxetc=$max_min->etc_wmax;
			$w_maxxmr=$max_min->xmr_wmax;
			$w_maxiota=$max_min->iota_wmax;
			$w_maxbtg=$max_min->btg_wmax;
			$w_maxdash=$max_min->dash_wmax;
			$w_maxkrw=$max_min->krw_wmax;
			
			

			$btc_bal = $mywallet->BTC;
			$eth_bal = $mywallet->ETH;
			$ltc_bal = $mywallet->LTC;
			$xrp_bal = $mywallet->XRP;
			$doge_bal = $mywallet->DOGE;
			$xmr_bal = $mywallet->XMR;
			$iota_bal = $mywallet->IOTA;
			$btg_bal = $mywallet->BTG;
			$dash_bal = $mywallet->DASH;
			$krw_bal = $mywallet->KRW;
			
			

			$resp = FALSE;

			$curencys_value = $request['with_curren'];
			
			if($curencys_value == "BTC")
			{

				if($withdraw_amount < $w_minbtc)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}

				else if($withdraw_amount > $w_maxbtc)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}

				else if($withdraw_amount > $btc_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}
			if($curencys_value == "ETH")
			{
				if($withdraw_amount < $w_mineth)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxeth)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $eth_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}
			if($curencys_value == "LTC")
			{

				if($withdraw_amount < $w_maxltc)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxltc)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $ltc_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}
			if($curencys_value == "XRP")
			{

				if($withdraw_amount < $w_minxrp)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxxmr)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $xmr_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}

if($curencys_value == "DOGE")
			{

				if($withdraw_amount < $w_mindoge)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxdoge)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $doge_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}


if($curencys_value == "ETC")
			{

				if($withdraw_amount < $w_minetc)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxetc)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $etc_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}

			if($curencys_value == "XMR")
			{

				if($withdraw_amount < $w_minxmr)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxxmr)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $xmr_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}

						if($curencys_value == "IOTA")
			{

				if($withdraw_amount < $w_miniota)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxiota)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $iota_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}

							if($curencys_value == "BTG")
			{

				if($withdraw_amount < $w_minbtg)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxbtg)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $btg_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}

		if($curencys_value == "DASH")
			{

				if($withdraw_amount < $w_mindash)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxdash)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $dash_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}

						if($curencys_value == "KRW")
			{

				if($withdraw_amount < $w_minkrw)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed minimum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed minimum limit!!!');
				}
				else  if($withdraw_amount > $w_maxkrw)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed maximum limit!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed maximum limit!!!');
				}
				else if($withdraw_amount > $krw_bal)
				{
					$request->session()->flash('error', 'Your withdraw amount exceed your current balance!!!');
					$resp = array('status' => FALSE,'title' => 'Your withdraw amount exceed your current balance!!!');
				}
			}


		
			
			$max_min = Basic::getsinglerow(array('id'=>'1'),'wiix_settings');


			$txid = 'TD'.Session::get('user_id').time();

			$details = Basic::getsinglerow(array('txn_id'=>$txid),'wiix_withdraw');
			if(empty($details)){

				$commission= Basic::getsinglerow(array('id'=>'1'),'wiix_settings');


				$amount = $request['w_amount'];

				if($curencys_value == "BTC")
				{
					$feesss=$commission->btc_fee;
				}
				else if($curencys_value == "LTC")
				{
					$feesss=$commission->ltc_fee;
				}
				else if($curencys_value == "ETH")
				{
					$feesss=$commission->eth_fee;
				}
				else if($curencys_value == "XRP")
				{
					$feesss=$commission->xrp_fee;
				}
				else if($curencys_value == "DOGE")
				{
					$feesss=$commission->doge_fee;
				}
				else if($curencys_value == "ETC")
				{
					$feesss=$commission->etc_fee;
				}
				else if($curencys_value == "XMR")
				{
					$feesss=$commission->xmr_fee;
				}
				else if($curencys_value == "IOTA")
				{
					$feesss=$commission->iota_fee;
				}
				else if($curencys_value == "BTG")
				{
					$feesss=$commission->btg_fee;
				}
				else if($curencys_value == "DASH")
				{
					$feesss=$commission->dash_fee;
				}
				else if($curencys_value == "KRW")
				{
					$feesss=$commission->krw_fee;
				}


				$fee = (($amount * $feesss)/100);
				$paid_amount = $amount - $fee;



$getbankdoc=Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_bank');
$with_vls="bank_".$getbankdoc->id;


				if($amount>$fee){
					if($request['with_curren'] == 'KRW'){
						$depositdata = array('wit_amount'=>($request['w_amount']),'wit_bankaccount'=>($with_vls),'wit_fee'=>$fee,'type'=>1,'wit_status'=>3,
							'wil_get'=>$paid_amount,'wit_currency'=>($request['with_curren']),
							'user_id'=>Session::get('user_id'), 'txn_id'=>$txid
							);


					}else{
						$depositdata = array('wit_amount'=>($request['w_amount']),
							'wit_fee'=>$fee,'type'=>4,
							'wil_get'=>$paid_amount,'wit_status'=>3,'crypto_address'=>$request['crypto_add'],'wit_currency'=>($request['with_curren']),
							'user_id'=>Session::get('user_id'));

					}
					//save wallet account details for users
					

					
					$valuesd=PaymentController::add_transactionwith($depositdata);

					

					$encid=Helper::encrypt_decrypt("encrypt",$valuesd);
				
					$id=$txid;


					$res = Basic::getsinglerow(array('txn_id'=>$id),'wiix_withdraw');


					 

					$userdetails = Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_consumers');

					

					$data['mywallet'] = Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_wallet');

					$data['userdetails'] = $userdetails;


					$curency_values = $request['with_curren'];

					if(($curency_values != 'KRW'))
					{


						$data = array('wit_status' => 3);

			$where = array('id'=>$valuesd);
			Basic::updatedatas($where,$data,'wiix_withdraw');


						$userprofs = Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_profile');


						$user = Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_consumers');



						 $email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
        $email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
        $secure_email = $email[0]."@".$email[1];

        /*Withdraw balance update*/
        PaymentController::withdrawbalance($request,$encid);

        /*BAlance withdraw ends*/


/*Email starts*/
          $msg=DB::table('wiix_email_templates')->where('mail_key','crypto_withdraw')->get();
							$to=$secure_email;


							$link1=url('payment/verify_withdraw/').'/'.$encid;
							$link2=url('payment/cancel_withdraw/').'/'.$encid;


							$message=$msg[0]->message;
							$date=date('M d, Y');
							
							$message=str_replace("##NAME##",$userprofs->first_name,$message);
							$message=str_replace("##AMOUNT##",$request['w_amount'],$message);
							$message=str_replace("##CURRENCY##",$curency_values,$message);
							$message=str_replace("##STATUS##","Pending",$message);
							$message=str_replace("##LINK1##",$link1,$message);
							$message=str_replace("##LINK2##",$link2,$message);
							$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
							$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
							
							$email_template = 'Withdraw status';
							
							Helper::mailsend($email_template,$message,$to);



						$request->session()->flash('success', 'Please Confirm your Email to Complete the Withdraw Request');
						$resp['status'] = TRUE;
						$resp['title'] = 'Please Confirm your Email to Complete the Withdraw Request!!!';
						echo json_encode($resp);
						
					}
					else{




				

						Basic::updatedatas(array('id'=>$valuesd),array('wit_status'=> 3),'wiix_withdraw');
						$res = Basic::getsinglerow(array('txn_id'=>$txid),'wiix_withdraw');

 

						$userdetails = Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_consumers');



						$userprofs = Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_profile');


									$user = Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_consumers');

 /*Withdraw balance update*/
        PaymentController::withdrawbalance($request,$encid);

        /*BAlance withdraw ends*/


						 $email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
        $email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
        $secure_email = $email[0]."@".$email[1];
/*Email starts*/
          $msg=DB::table('wiix_email_templates')->where('mail_key','crypto_withdraw')->get();
							$to=$secure_email;


							$message=$msg[0]->message;
							$date=date('M d, Y');
							
							$message=str_replace("##NAME##",$userprofs->first_name,$message);
							$message=str_replace("##AMOUNT##",$request['w_amount'],$message);
							$message=str_replace("##CURRENCY##",$curency_values,$message);
							$message=str_replace("##STATUS##","Pending",$message);
							$message=str_replace("##LINK1##",url('payment/verify_withdraw/').'/'.$encid,$message);
							$message=str_replace("##LINK2##",url('payment/cancel_withdraw/').'/'.$encid,$message);
							$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
							$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
							
							$email_template = 'Withdraw status';
							
							Helper::mailsend($email_template,$message,$to);


						$request->session()->flash('success', 'Please Confirm your Email to Complete the Withdraw Request!!!');

						$resp['status'] = TRUE;
						$resp['title'] = 'Your withdraw request send successfully!!!';
						echo json_encode($resp);

					}
		           //redirect('withdraw','refresh');

					
				}else{ 


					$request->session()->flash('error', 'withdraw amount must be greater than zero');
					$resp['status'] = FALSE;
					$resp['title'] = 'withdraw amount must be greater than zero';
					echo json_encode($resp);die;


					 //redirect('withdraw','refresh');
				}	
			}
		}
		else{
$request->session()->flash('error', 'The bank you selected was not verified,Please verify bank document to proceed withdraw');
					$resp['status'] = FALSE;
					$resp['title'] = 'The bank you selected was not verified,Please verify bank document to proceed withdraw';
					echo json_encode($resp);die;

		}
			}
			else{
				$request->session()->flash('error', 'Your verification document was not verified,Please verify to proceed withdraw');
					$resp['status'] = FALSE;
					$resp['title'] = 'Your verification document was not verified,Please verify to proceed withdraw';
					echo json_encode($resp);die;
			}	

		}



		

 }

  function add_transactionwith($data){

    

$insert=Basic::insertdatas($data,'wiix_withdraw');
    


$wherepro = array('user_id'=>$data['user_id']);
		$userdetails= Basic::getsinglerow($wherepro,'wiix_consumers');

$user_prof = Basic::getsinglerow(array("user_id"=>$data['user_id']),'wiix_user_profile');
 
        
     
        $email=array();
        $email[0] = Helper::encrypt_decrypt("decrypt",$userdetails->secret_key);
        $email[1] = Helper::encrypt_decrypt("decrypt",$userdetails->display);
        $secure_email = $email[0]."@".$email[1];

			$wherepro = array('user_id'=>$data['user_id']);
		$noti= Basic::getsinglerow($wherepro,'wiix_notification');



if($noti->transaction == 1)
{


$msg=DB::table('wiix_email_templates')->where('mail_key','fiat_withdraw')->get();
							$to=$secure_email;


							$message=$msg[0]->message;
							$date=date('M d, Y');
							
							$message=str_replace("##AMOUNT##",$data['dep_amount'],$message);
							$message=str_replace("##NAME##",$user_prof->first_name,$message);
							$message=str_replace("##STATUS##","Pending",$message);
							$message=str_replace("##CURRENCY##",$data['wit_amount'],$message);
							$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
							$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
							
							$email_template = 'Withdraw status';
							
							//Helper::mailsend($email_template,$message,$to);


           
        }
        return $insert;
    }
function verify_withdraw(Request $request,$id){

		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }



		$decrid=Helper::encrypt_decrypt("decrypt",$id);
	
		$res = Basic::getsinglerow(array('id' => $decrid),'wiix_withdraw');
		
		if($res && $res->user_id == Session::get('user_id') ){

			if($res->wit_status != 3){
				$request->session()->flash('error','Amount already refund to this account');
				return redirect('/payment');
			}else{
				
				 

				$request->session()->flash('success','Your withdraw request send successfully to our team');
				
				Basic::updatedatas(array('id'=>$decrid),array('wit_status'=>'0'),'wiix_withdraw');
				
				return redirect('/payment');
				
				
			}

		}else{

			$request->session()->flash('error',"Invalid withdraw credentials");
			return redirect('/payment');
		}

	}	

	public static function withdrawbalance(Request $request,$id)
	{

				$decrid=Helper::encrypt_decrypt("decrypt",$id);
	
		$res = Basic::getsinglerow(array('id' => $decrid),'wiix_withdraw');
$userdetails = Basic::getsinglerow(array('user_id' => Session::get('user_id')),'wiix_consumers');

				$data['mywallet'] = Basic::getsinglerow(array('user_id' => Session::get('user_id')),'wiix_user_wallet');
				
				$data['userdetails'] = $userdetails;
				$data['depositdata'] = $res;
				
				



				
				
				if($res->wit_currency == 'BTC')
				{
		$transfer=1;
					
				}
				else if($res->wit_currency == 'LTC')
				{
$transfer=1;
					
				}
				else if($res->wit_currency == 'ETH')
				{
$transfer=1;
					
				}
				else if($res->wit_currency == 'XRP')
				{

	$transfer=1;
					
				}
				else if($res->wit_currency == 'DOGE')
				{
$transfer=1;
					
				}
				else if($res->wit_currency == 'ETC')
				{

				$transfer=1;
					
				}
				else if($res->wit_currency == 'XMR')
				{
$transfer=1;
					
				}
				else if($res->wit_currency == 'IOTA')
				{

					$transfer=1;
					
				}

				else if($res->wit_currency == 'BTG')
				{

					$transfer=1;
					
				}
				
				else if($res->wit_currency == 'KRW')
				{


					$transfer=1;
					
				}
				
				
				if($transfer){

					DB::select( DB::raw('UPDATE `wiix_user_wallet` SET `'.$res->wit_currency.'` = ('.$res->wit_currency.'-'.(float)$res->wit_amount.') WHERE user_id ='.$res->user_id.'' ));






					$request->session()->flash('success', 'Your withdraw request send successfully to our team!!!');
							//redirect('/','refresh');
					
				}else{
					$request->session()->flash('error', 'Wallet transaction failed. Please check your wallet balance and destination wallet address.');
				}


	}

	function cancel_withdraw(Request $request,$id){
	if(Session::get('user_id')=="")
      {
        return redirect('/');
      }
	
		$decrid=Helper::encrypt_decrypt("decrypt",$id);	

		$res = Basic::getsinglerow(array('id' => $decrid),'wiix_withdraw');

		
		if(($res)){
			if($res->user_id == Session::get('user_id') )
			{
				if($res->wit_status != 3){
					$request->session()->flash('error','Withdraw request already cancelled on this account');
					return redirect('/payment');
				}else{

DB::select( DB::raw('UPDATE `wiix_user_wallet` SET `'.$res->wit_currency.'` = ('.$res->wit_currency.'+'.(float)$res->wit_amount.') WHERE user_id ='.$res->user_id.'' ));


					Basic::updatedatas(array('id'=>$decrid),array('wit_status'=>2),'wiix_withdraw');

					
					$request->session()->flash('success','Your withdraw request successfully cancelled');
					return redirect('/payment');
				}
			}
		}else{
			$request->session()->flash('error','Invalid withdraw credentials');
			return redirect('/payment');
		}

	}

	public static function update_transaction_status($details,$status,$amount)
	{


		        $data = array('wit_status'=>$status, 'wit_amount'=>$amount);
       
        $where = array('id'=>$details->id);
            

      Basic::updatedatas($where,$data,'wiix_withdraw');


  
 
$user = Basic::getsinglerow(array('user_id' =>$details->user_id),'wiix_consumers');

$user_prof =  Basic::getsinglerow(array('user_id' =>$details->user_id),'wiix_user_profile');
 
       



 $email=array();
        $email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
        $email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
        $secure_email = $email[0]."@".$email[1];
$msg=DB::table('wiix_email_templates')->where('mail_key','fiat_withdraw')->get();
							$to=$secure_email;


							$message=$msg[0]->message;
							$date=date('M d, Y');
							
							$message=str_replace("##AMOUNT##",$details->wit_amount,$message);
							$message=str_replace("##NAME##",$user_prof->first_name,$message);
							$message=str_replace("##STATUS##","Pending",$message);
							$message=str_replace("##CURRENCY##",$details->wit_currency,$message);
							$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
							$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
							
							$email_template = 'Withdraw status';
							
							Helper::mailsend($email_template,$message,$to);


	}

}



			