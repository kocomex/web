<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\moneroController;
use App\Basic;
use App\profile;
use App\wallet;
use App\bank_details;
use jsonRPCClient;


class WithdrawCrypto extends Controller
{
	
    public function __construct()
    {

    }

public function btc_withdraws($to_address,$amount)
{
		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"bitcoin"),'wiix_cryptodetails');
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


		//Send Bitcoin Address
		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber/");
		$getifo = $bitcoin->getinfo();
		
		$transfer = $bitcoin->sendtoaddress($to_address,$amount);

		return $transfer;
				

}
public function ltc_withdraws($to_address,$amount)
{
		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"litecoin"),'wiix_cryptodetails');
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


		//Send Bitcoin Address
		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber/");
		$getifo = $bitcoin->getinfo();

		$transfer = $bitcoin->sendtoaddress($to_address,$amount);

		return $transfer;
				

}
public function doge_withdraws($to_address,$amount)
{



		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"dogecoin"),'wiix_cryptodetails');
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


		//Send Bitcoin Address
		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber/");
		$getifo = $bitcoin->getinfo();

		$transfer = $bitcoin->sendtoaddress($to_address,$amount);

		return $transfer;
				

}
public function dash_withdraws($to_address,$amount)
{



		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"dashcoin"),'wiix_cryptodetails');
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


		//Send Bitcoin Address
		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber/");
		$getifo = $bitcoin->getinfo();

		$transfer = $bitcoin->sendtoaddress($to_address,$amount);

		return $transfer;
				

}

// atm do some edit start here
public function eth_withdraws($to_address,$amount)
{
				// $address = $from_address;
				$getaddeth=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
				$eth_addr    = $getaddeth->ETH_address;
				$eth_eth_key = $getaddeth->ETH_admin_pwd;
				$address 	 = '"'.Helper::encrypt_decrypt('decrypt',$eth_addr).'"';
				$admin_key 	 = '"'.Helper::encrypt_decrypt('decrypt',$eth_eth_key).'"';
				$to 	     = '"'.trim($to_address).'"';
				$amount      = $amount; 
				$key         = $admin_key; 


				$data = array('adminaddress'=>$address,'toaddress'=>$to,'amount'=>$amount,'key'=>$key);
                return $transfer     = Helper::connecteth('ethwithdrawjson',$data);

				// $lastnumber = exec('curl -X POST --data \'{"jsonrpc":"2.0","method":"personal_unlockAccount","params":['.$address.',"password",null],"id":1}\' "http://localhost:8545"');
                // $output = exec('curl -X POST --data \'{"jsonrpc":"2.0","method":"eth_sendTransaction","params":[{"from":'.$address.',"to":'.$to.',"value":'.$amount.'}],"id":22}\' "http://localhost:8545"');


				// $abc = json_decode($output);
				// // echo '<pre>'; print_r($abc); exit;
				// $transfer = $abc->result;
}
public function etc_withdraws($to_address,$amount)
{
				// $address = $from_address;
				$getaddeth=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
				$etc_addr    = $getaddeth->ETC_address;
				$etc_etc_key = $getaddeth->ETC_admin_pwd;
				$address 	 = '"'.Helper::encrypt_decrypt('decrypt',$etc_addr).'"';
				$admin_key 	 = '"'.Helper::encrypt_decrypt('decrypt',$etc_etc_key).'"';
				$to 	     = '"'.trim($to_address).'"';
				$amount      = $amount; 
				$key         = $admin_key; 

				$data = array('adminaddress'=>$address,'toaddress'=>$to,'amount'=>$amount,'key'=>$key);
                return $transfer     = Helper::connectetc('ethwithdrawjson',$data);

				// $address = $from_address;
				// $to 	 = $to_address;
				// $amount  = $amount; 

				// $lastnumber = exec('curl -X POST --data \'{"jsonrpc":"2.0","method":"personal_unlockAccount","params":['.$address.',"password",null],"id":1}\' "http://localhost:8545"');
				// $output = exec('curl -X POST --data \'{"jsonrpc":"2.0","method":"eth_sendTransaction","params":[{"from":'.$address.',"to":'.$to.',"value":'.$amount.'}],"id":22}\' "http://localhost:8545"');

				// $abc = json_decode($output);
				// // echo '<pre>'; print_r($abc); exit;
				// $transfer = $abc->result;
}

public function btg_withdraws($to_address,$amount)
{
		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"bitcoingold"),'wiix_cryptodetails');
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


		//Send Bitcoin Address
		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber/");
		$getifo = $bitcoin->getinfo();
		
		$transfer = $bitcoin->sendtoaddress($to_address,$amount);

		return $transfer;
				

}

// atm do some edit end here

public function xrp_withdraws($to_address,$amount)
{
	$xrp_row=Basic::getsinglerow(array('coinname'=>"ripple"),'wiix_cryptodetails');

	         $xrp_addr = $xrp_row->address_ripple;
            $xrp_addr  = trim($xrp_addr);
            $xrp_secret = $xrp_row->rippple_secret;
            $xrp_secret  = trim($xrp_secret);
            $url = "https://data.ripple.com/v2/accounts/" . $xrp_addr . "/balances?currency=XRP";
            $output = file_get_contents($url);
            $result = json_decode($output,true);
            echo "<pre>";
            print_r($result);
            if($result['result'] == 'success' && count($result['balances'])){
            $value =  $result['balances'][0]['value'];
            }else{
            $value=0;
            }

           /* $output  = shell_exec('cd /var/www/html; /usr/bin/node ripple_balance.js "' . $xrp_addr . '"');
            $output1 = json_decode($output);
            if ($output1) {
                foreach ($output1 as $key => $value) {
                $value = $value->value;
                }*/
                $newvalue = $amount+21;
                if ($value > $newvalue) { 

                        $transaction = shell_exec('cd /var/www/html; /usr/bin/node ripple_sendcoins.js "' . $to_address . '" "' . $amount . '" "' . $xrp_addr . '" "' . $xrp_secret . '" '); 
                        if ($transaction) {
                        $testt   = explode('NaN', $transaction);
                        $a       = $testt[1];
                        $b       = $testt[2];
                        $aa      = json_decode($a, true);
                        $txxid1  = $aa['txid'];
                        $bb      = json_decode($b, true);
                        $messag2 = $bb['resultCode'];
                        if ($messag2 == 'tesSUCCESS' || $messag2=='terQUEUED') {
                        return $txxid1;
                        }
                        return false;
                        }

                } 
}

//xmr withdraw
public function xmr_withdraws($to_address,$amount)
{		
		$xmrcoin			=	moneroController::transfer($amount,$to_address,$mixin=4);
		return $xmrcoin;
}


}