<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Cloudinary;

use App\Model\jsonRPCClient;
use App\Model\User;


class CryptoAddress extends Controller {
	public function __construct() {
		
	}

	public static function createAddress($type,$email) {
		if($type == "BTC" || $type == "LTC" || $type == "KRW") {
			$address = User::randomString(32);
		} else {
			$address = '0x'.User::randomString(40);
		}
		return $address;
	}
}