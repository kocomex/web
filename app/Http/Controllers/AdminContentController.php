<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Illuminate\Support\Facades\Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;

use App\Basic;


class AdminContentController extends Controller
{
	public function addPage(Request $request)
	{

		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	


	}
	public function viewPages(Request $request)
	{

		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		$where = array();
		$order= array('page_id','desc');
		
		$data["cms_pages"] = Basic::getmultiplerow($where,'wiix_cms',$order);

		$data["title"] = "View CMS";
		return view('admin/cms.view_all_pages',$data);

	}

	

	public function editPage(Request $request,$id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		$data["cms_data"] = Basic::getsinglerow(array("page_id"=>$id),'wiix_cms');	
		$data["cms_id"]  = $id;

		if($request->isMethod('post'))
		{
			
			$data = array("page_name" => $request['name'],
				"page_url"  => str_replace(" ","_",strtolower($request['name'])),
				"page_content"  => $request['content'],
				"meta_title"  => $request['meta_title'],
				"meta_keyword"  => $request['meta_keyword'],
				"meta_description"  => $request['meta_description'],
				"status" => 1,
				"last_updated" => date('Y-m-d H:i:s')
				);

			$where = array('page_id'=>$id);					
			$is_update = Basic::updatedatas($where, $data,'wiix_cms');
			if($is_update){
				$request->session()->flash('success','Page updated sucessfully');
				return redirect('/WiPlytaIIX2/editPage/'.$id);
			}else{
				$request->session()->flash('error','Error in updating Page');
				return redirect('/WiPlytaIIX2/editPage/'.$id);
			}
		}else{
			$data["title"] = "Edit CMS";
			
			return view('admin/cms.edit',$data);
		}
		
		
	}
	public function viewFaqs(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	


		$where = array();
		$order= array('faq_id','desc');
		
		$data["faqs"] = Basic::getmultiplerow($where,'wiix_faq',$order);
		$data["title"] = "View FAQ";
		return view('admin/faq.view_all',$data);

	}

	public function addFaq(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		if($request->isMethod('post'))
		{
			$data = array(
				"faq_question" => $request['question'],
				"faq_answer" => $request['answer'],
				"status" => $request['status'],
				"last_updated" => date('Y-m-d H:i:s'),
				"created_on" => date('Y-m-d H:i:s')
				);
			$insert_id = Basic::insertdatas($data,'wiix_faq');
			if($insert_id){
				$request->session()->flash('success','FAQ added sucessfully');
				return redirect('WiPlytaIIX2/addFaq');
			}else{
				$request->session()->flash('error','Error in adding FAQ');
				return redirect('WiPlytaIIX2/addFaq');
			}
		}
		$data["title"] = "Add FAQ";
		return view('admin/faq.add',$data);
		
	}

	public function editFaq(Request $request,$id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		$data["faq_data"] = Basic::getsinglerow(array("faq_id"=>$id),'wiix_faq');	
		$data["faqs_id"]  = $id;

		if($request->isMethod('post'))
		{
			
			$data = array("faq_question" => $request['question'],
				
				"faq_answer"  => $request['answer'],
				"status"  => $request['status'],
				
				"last_updated" => date('Y-m-d H:i:s')
				);

			$where = array('faq_id'=>$id);					
			$is_update = Basic::updatedatas($where, $data,'wiix_faq');
			if($is_update){
				$request->session()->flash('success','FAQ updated sucessfully');
				return redirect('/WiPlytaIIX2/editFaq/'.$id);
			}else{
				$request->session()->flash('error','Error in updating FAQ');
				return redirect('/WiPlytaIIX2/editFaq/'.$id);
			}
		}else{
			$data["title"] = "Edit FAQ";
			
			return view('admin/faq.edit',$data);
		}
		

	}

	public function deleteFaq(Request $request,$id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	
		if($id)
		{
			$condition = array('id' => $id);
			$delete = DB::table('wiix_faq')->where('faq_id', $id)->delete();

			$request->session()->flash('success', 'FAQ deleted successfully');
			return redirect('/WiPlytaIIX2/viewFaqs');

		}
	}

	public function profit()
	{
		if(Session::get('admin_id')=="")
					return redirect('WiPlytaIIX2/login');


				$where = array();
				//$order= array('theft_id','desc');
				$data["title"] = "Admin profit";



				$data["profit"] = DB::table('wiix_coin_theft')->leftjoin('wiix_consumers','wiix_consumers.user_id','=','wiix_coin_theft.userId')->orderBy('theft_id', 'desc')->get();


				return view('admin/main.profit',$data);
	}

	public function viewnews(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	


		$where = array();
		$order= array('news_id','desc');
		
		$data["news"] = Basic::getmultiplerow($where,'wiix_news',$order);
		$data["title"] = "View News";
		return view('admin/news.view_all',$data);

	}

	public function addnews(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		if($request->isMethod('post'))
		{

			$path=public_path('verify/news_image');

			if ($request->hasFile('news_image')) 
			{
				$fileName =$_FILES['news_image']['name'];
				$request->file('news_image')->move($path,$fileName);
			}
			else
			{
				$fileName = $request['news_image'];
			}
			
			
			if($fileName) 
			{
				$site_log = $fileName;
			}
			else{
				$site_log =  "";
			}



			$data = array(
				"news_title" => $request['news_title'],
				"news_content" => $request['news_content'],
				"status" => $request['status'],
				"news_image" => $site_log,
				"last_updated" => date('Y-m-d H:i:s'),
				"created_on" => date('Y-m-d H:i:s')
				);
			$insert_id = Basic::insertdatas($data,'wiix_news');
			if($insert_id){
				$request->session()->flash('success','News added sucessfully');
				return redirect('WiPlytaIIX2/addFaq');
			}else{
				$request->session()->flash('error','Error in adding News');
				return redirect('WiPlytaIIX2/addFaq');
			}
		}
		$data["title"] = "Add News";
		return view('admin/news.add',$data);
		
	}

	public function editnews(Request $request,$id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		$data["news_data"] = Basic::getsinglerow(array("news_id"=>$id),'wiix_news');	
		$data["news_id"]  = $id;

		if($request->isMethod('post'))
		{


			$path=public_path('verify/news_image');



			if ($request->hasFile('news_image')) 
			{
				$fileExtensions = ['jpeg','jpg','png'];
				$fileName = $_FILES['news_image']['name'];
				$fileType = $_FILES['news_image']['type'];
				$explode = explode('.',$fileName);
	  			$extension = end($explode);
	  			$fileExtension = strtolower($extension);
	  			$mimeImage = mime_content_type($_FILES["news_image"]['tmp_name']);
	  			$explode = explode('/', $mimeImage);

	  			if (!in_array($fileExtension,$fileExtensions)) {
	  				Session::flash('error', 'Invalid file type. Only image files are accepted.');  
					return Redirect::back();
	  			} else {
	  				if($explode[0] != "image") {
	  					Session::flash('error', 'Invalid file type. Only image files are accepted.');  
						return Redirect::back();
	  				}
		$fileName =$_FILES['news_image']['name'];
				$request->file('news_image')->move($path,$fileName);
	  			}
				
				
			}
			else
			{
			
				$fileName = $data["news_data"]->news_image;
			}
			
			
			if($fileName) 
			{
				$site_log = $fileName;
			}
			else{
				$site_log =  $data["news_data"]->news_image;
			}
			
			$data = array("news_title" => $request['news_title'],
				
				"news_content"  => $request['news_content'],
				"news_image" => $site_log,
				"status"  => $request['status'],
				
				"last_updated" => date('Y-m-d H:i:s')
				);

			$where = array('news_id'=>$id);					
			$is_update = Basic::updatedatas($where, $data,'wiix_news');
			if($is_update){
				$request->session()->flash('success','News updated sucessfully');
				return redirect('/WiPlytaIIX2/editnews/'.$id);
			}else{
				$request->session()->flash('error','Error in updating News');
				return redirect('/WiPlytaIIX2/editnews/'.$id);
			}
		}else{
			$data["title"] = "Edit News";
			
			return view('admin/news.edit',$data);
		}
		

	}

	public function deletenews(Request $request,$id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	
		if($id)
		{
			
			$delete = DB::table('wiix_news')->where('news_id', $id)->delete();

			$request->session()->flash('success', 'News deleted successfully');
			return redirect('/WiPlytaIIX2/viewnews');

		}
	}

}

