<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\moneroController;
use App\Basic;
use App\profile;
use App\wallet;
use App\bank_details;
use jsonRPCClient;

class CronController extends Controller
{
   
    //
	function btc_deposit_process()
	{
		$cur_date = date('Y-m-d');
		$cur_time = date('H:i:s');

		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"bitcoin"),'wiix_cryptodetails');
		
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress);


		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");


		$bitcoin_isvalid         =     $bitcoin->listtransactions();
        //echo "<pre>";
        //print_r($bitcoin_isvalid);
        //exit;


		if($bitcoin_isvalid)
		{

			for($i=0;$i<count($bitcoin_isvalid);$i++)
			{

				$account      =      $bitcoin_isvalid[$i]['account'];
				$category     =      $bitcoin_isvalid[$i]['category'];
				$btctxid      =      $bitcoin_isvalid[$i]['txid'];
				if($category=="receive")
				{

					$isvalid = $bitcoin->gettransaction($btctxid);
					echo "<pre>";
					//print_r($isvalid);

					$det_category  =   $isvalid['details'][0]['category'];
					if($det_category=="receive")
					{
						$btcaccount            =   $isvalid['details'][0]['account'];
						$btcaddress         =     $isvalid['details'][0]['address'];
						$bitcoin_balance     =     $isvalid['details'][0]['amount']; 
						$btcconfirmations     =     $isvalid['confirmations']; 
					}
					else
					{
						$btcaccount            =   $isvalid['details'][1]['account'];
						$btcaddress         =     $isvalid['details'][1]['address'];
						$bitcoin_balance     =     $isvalid['details'][1]['amount']; 
						$btcconfirmations     =     $isvalid['confirmations']; 

					}
					$finmail=$btcaccount;

					$splitemail=explode("@",$finmail);
					$firstmailid=Helper::encrypt_decrypt("encrypt",$splitemail[0]);
					$secmailid=Helper::encrypt_decrypt("encrypt",$splitemail[1]);

					$getidsuser=Basic::getsinglerow(array('secret_key'=>$firstmailid,'display'=>$secmailid),'wiix_consumers');



					$btcuserId             = $getidsuser->user_id;
					$dep_id             = $btctxid;
					$bitcoin_balance     = $bitcoin_balance;




					if( $btcconfirmations > 0)
					{
						echo "confirm0";


						$dep_already = Basic::getsinglerow(array('txn_id'=>$dep_id),'wiix_mydeposits');

						if(!$dep_already)
						{
							echo "nottxid";
							$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');
							

							$fetchBTCbalance     = $getwallets->BTC;
							$updateBTCbalance     = $fetchBTCbalance+$bitcoin_balance;



							DB::table('wiix_mydeposits')->insert(['user_id'=>$btcuserId,'dep_currency'=>"BTC",'dep_amount'=>$bitcoin_balance,'txn_id'=>$dep_id,'confirmation'=>$btcconfirmations,'dep_status'=>0,'crypto_address'=>$btcaddress]);      
						}
						else
						{
							echo "elseconfirm3";
									

							if($btcconfirmations >= 3)
							{


				                    	//echo "dasfdf";

								$query = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$dep_id,'dep_status'=>0),'wiix_mydeposits');
								echo "confirm3";
								if($query)
								{

									$userdata = array('dep_status' => 1);

									$where = array('txn_id'=>$dep_id);
									Basic::updatedatas($where,$userdata,'wiix_mydeposits');



									$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

									$fetchBTCbalance     = $getwallets->BTC;


									$updateBTCbalance     = $fetchBTCbalance+$bitcoin_balance;

											// insert data into transaction history

									$data = array('BTC' => $updateBTCbalance);

									$where = array('user_id'=>$btcuserId);
									Basic::updatedatas($where,$data,'wiix_user_wallet');

echo "success";




								} 

							}


						}

					} 








				}

			}

		}


	}

	function ltc_deposit_process()
	{
		$cur_date = date('Y-m-d');
		$cur_time = date('H:i:s');

		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"litecoin"),'wiix_cryptodetails');
		
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress);


		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");


		$bitcoin_isvalid         =     $bitcoin->listtransactions();
       // echo "<pre>";
       // print_r($bitcoin_isvalid);
       // exit;


		if($bitcoin_isvalid)
		{

			for($i=0;$i<count($bitcoin_isvalid);$i++)
			{

				$account      =      $bitcoin_isvalid[$i]['account'];
				$category     =      $bitcoin_isvalid[$i]['category'];
				$btctxid      =      $bitcoin_isvalid[$i]['txid'];
				if($category=="receive")
				{

					$isvalid = $bitcoin->gettransaction($btctxid);
					echo "<pre>";
					print_r($isvalid);

					$det_category  =   $isvalid['details'][0]['category'];
					if($det_category=="receive")
					{
						$btcaccount            =   $isvalid['details'][0]['account'];
						$btcaddress         =     $isvalid['details'][0]['address'];
						$bitcoin_balance     =     $isvalid['details'][0]['amount']; 
						$btcconfirmations     =     $isvalid['confirmations']; 
					}
					else
					{
						$btcaccount            =   $isvalid['details'][1]['account'];
						$btcaddress         =     $isvalid['details'][1]['address'];
						$bitcoin_balance     =     $isvalid['details'][1]['amount']; 
						$btcconfirmations     =     $isvalid['confirmations']; 

					}
					$finmail=$btcaccount;
					$splitemail=explode("@",$finmail);
					$firstmailid=Helper::encrypt_decrypt("encrypt",$splitemail[0]);
					$secmailid=Helper::encrypt_decrypt("encrypt",$splitemail[1]);

					$getidsuser=Basic::getsinglerow(array('secret_key'=>$firstmailid,'display'=>$secmailid),'wiix_consumers');



					$btcuserId             = $getidsuser->user_id;
					$dep_id             = $btctxid;
					$bitcoin_balance     = $bitcoin_balance;




					if( $btcconfirmations > 0)
					{


						$dep_already = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$dep_id),'wiix_mydeposits');
						if(!$dep_already)
						{
							$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

							$fetchBTCbalance     = $getwallets->LTC;
							$updateBTCbalance     = $fetchBTCbalance+$bitcoin_balance;



							DB::table('wiix_mydeposits')->insert(['user_id'=>$btcuserId,'dep_currency'=>"LTC",'dep_amount'=>$bitcoin_balance,'txn_id'=>$dep_id,'confirmation'=>$btcconfirmations,'dep_status'=>0,'crypto_address'=>$btcaddress]);      
						}
						else
						{

							if($btcconfirmations >= 3)
							{


				                    	//echo "dasfdf";

								$query = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$dep_id,'dep_status'=>0),'wiix_mydeposits');
								if($query)
								{

									$userdata = array('dep_status' => 1);

									$where = array('txn_id'=>$dep_id);
									Basic::updatedatas($where,$userdata,'wiix_mydeposits');



									$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

									$fetchBTCbalance     = $getwallets->LTC;


									$updateBTCbalance     = $fetchBTCbalance+$bitcoin_balance;

											// insert data into transaction history

									$data = array('LTC' => $updateBTCbalance);

									$where = array('user_id'=>$btcuserId);
									Basic::updatedatas($where,$data,'wiix_user_wallet');






								} 

							}


						}

					} 








				}

			}

		}


	}
	function doge_deposit_process()
	{
		$cur_date = date('Y-m-d');
		$cur_time = date('H:i:s');

		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"dogecoin"),'wiix_cryptodetails');
		
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress);


		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");


		$bitcoin_isvalid         =     $bitcoin->listtransactions();
       // echo "<pre>";
       // print_r($bitcoin_isvalid);
       // exit;


		if($bitcoin_isvalid)
		{

			for($i=0;$i<count($bitcoin_isvalid);$i++)
			{

				$account      =      $bitcoin_isvalid[$i]['account'];
				$category     =      $bitcoin_isvalid[$i]['category'];
				$btctxid      =      $bitcoin_isvalid[$i]['txid'];
				if($category=="receive")
				{

					$isvalid = $bitcoin->gettransaction($btctxid);
					echo "<pre>";
					print_r($isvalid);

					$det_category  =   $isvalid['details'][0]['category'];
					if($det_category=="receive")
					{
						$btcaccount            =   $isvalid['details'][0]['account'];
						$btcaddress         =     $isvalid['details'][0]['address'];
						$bitcoin_balance     =     $isvalid['details'][0]['amount']; 
						$btcconfirmations     =     $isvalid['confirmations']; 
					}
					else
					{
						$btcaccount            =   $isvalid['details'][1]['account'];
						$btcaddress         =     $isvalid['details'][1]['address'];
						$bitcoin_balance     =     $isvalid['details'][1]['amount']; 
						$btcconfirmations     =     $isvalid['confirmations']; 

					}
					$finmail=$btcaccount;
					$splitemail=explode("@",$finmail);
					$firstmailid=Helper::encrypt_decrypt("encrypt",$splitemail[0]);
					$secmailid=Helper::encrypt_decrypt("encrypt",$splitemail[1]);

					$getidsuser=Basic::getsinglerow(array('secret_key'=>$firstmailid,'display'=>$secmailid),'wiix_consumers');



					$btcuserId             = $getidsuser->user_id;
					$dep_id             = $btctxid;
					$bitcoin_balance     = $bitcoin_balance;




					if( $btcconfirmations > 0)
					{


						$dep_already = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$dep_id),'wiix_mydeposits');
						if(!$dep_already)
						{
							$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

							$fetchBTCbalance     = $getwallets->DOGE;
							$updateBTCbalance     = $fetchBTCbalance+$bitcoin_balance;



							DB::table('wiix_mydeposits')->insert(['user_id'=>$btcuserId,'dep_currency'=>"DOGE",'dep_amount'=>$bitcoin_balance,'txn_id'=>$dep_id,'confirmation'=>$btcconfirmations,'dep_status'=>0,'crypto_address'=>$btcaddress]);      
						}
						else
						{

							if($btcconfirmations >= 3)
							{


				                    	//echo "dasfdf";

								$query = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$dep_id,'dep_status'=>0),'wiix_mydeposits');
								if($query)
								{

									$userdata = array('dep_status' => 1);

									$where = array('txn_id'=>$dep_id);
									Basic::updatedatas($where,$userdata,'wiix_mydeposits');



									$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

									$fetchBTCbalance     = $getwallets->DOGE;


									$updateBTCbalance     = $fetchBTCbalance+$bitcoin_balance;

											// insert data into transaction history

									$data = array('DOGE' => $updateBTCbalance);

									$where = array('user_id'=>$btcuserId);
									Basic::updatedatas($where,$data,'wiix_user_wallet');






								} 

							}


						}

					} 








				}

			}

		}


	}
	function dash_deposit_process()
	{
		$cur_date = date('Y-m-d');
		$cur_time = date('H:i:s');

		$bitcoin_row=Basic::getsinglerow(array('coinname'=>"dashcoin"),'wiix_cryptodetails');
		
		$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress);


		$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");


		$bitcoin_isvalid         =     $bitcoin->listtransactions();
       // echo "<pre>";
       // print_r($bitcoin_isvalid);
       // exit;


		if($bitcoin_isvalid)
		{

			for($i=0;$i<count($bitcoin_isvalid);$i++)
			{

				$account      =      $bitcoin_isvalid[$i]['account'];
				$category     =      $bitcoin_isvalid[$i]['category'];
				$btctxid      =      $bitcoin_isvalid[$i]['txid'];
				if($category=="receive")
				{

					$isvalid = $bitcoin->gettransaction($btctxid);
					echo "<pre>";
					print_r($isvalid);

					$det_category  =   $isvalid['details'][0]['category'];
					if($det_category=="receive")
					{
						$btcaccount            =   $isvalid['details'][0]['account'];
						$btcaddress         =     $isvalid['details'][0]['address'];
						$bitcoin_balance     =     $isvalid['details'][0]['amount']; 
						$btcconfirmations     =     $isvalid['confirmations']; 
					}
					else
					{
						$btcaccount            =   $isvalid['details'][1]['account'];
						$btcaddress         =     $isvalid['details'][1]['address'];
						$bitcoin_balance     =     $isvalid['details'][1]['amount']; 
						$btcconfirmations     =     $isvalid['confirmations']; 

					}
					$finmail=$btcaccount;
					$splitemail=explode("@",$finmail);
					$firstmailid=Helper::encrypt_decrypt("encrypt",$splitemail[0]);
					$secmailid=Helper::encrypt_decrypt("encrypt",$splitemail[1]);

					$getidsuser=Basic::getsinglerow(array('secret_key'=>$firstmailid,'display'=>$secmailid),'wiix_consumers');



					$btcuserId             = $getidsuser->user_id;
					$dep_id             = $btctxid;
					$bitcoin_balance     = $bitcoin_balance;




					if( $btcconfirmations > 0)
					{


						$dep_already = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$dep_id),'wiix_mydeposits');
						if(!$dep_already)
						{
							$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

							$fetchBTCbalance     = $getwallets->DASH;
							$updateBTCbalance     = $fetchBTCbalance+$bitcoin_balance;



							DB::table('wiix_mydeposits')->insert(['user_id'=>$btcuserId,'dep_currency'=>"DASH",'dep_amount'=>$bitcoin_balance,'txn_id'=>$dep_id,'confirmation'=>$btcconfirmations,'dep_status'=>0,'crypto_address'=>$btcaddress]);      
						}
						else
						{

							if($btcconfirmations >= 3)
							{


				                    	//echo "dasfdf";

								$query = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$dep_id,'dep_status'=>0),'wiix_mydeposits');
								if($query)
								{

									$userdata = array('dep_status' => 1);

									$where = array('txn_id'=>$dep_id);
									Basic::updatedatas($where,$userdata,'wiix_mydeposits');



									$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

									$fetchBTCbalance     = $getwallets->DASH;


									$updateBTCbalance     = $fetchBTCbalance+$bitcoin_balance;

											// insert data into transaction history

									$data = array('DASH' => $updateBTCbalance);

									$where = array('user_id'=>$btcuserId);
									Basic::updatedatas($where,$data,'wiix_user_wallet');






								} 

							}


						}

					} 


				}

			}

		}


	}

	function eth_deposit_process() {
		
		$getETHAddress 		= CronController::getETHAddress(); 
		if(isset($getETHAddress) && !empty($getETHAddress)) {
			foreach($getETHAddress as $row) {
				$btcuserId 	= $row->user_id;
				$account 	= trim($row->ETH_address);
				if($account != ""){

			    	/*$result = $this->db->query('SELECT MAX(blocknumber) as max_blocknumber FROM cc_transaction_history')->row();
	            	$max_blocknumber = $result ->max_blocknumber;
					if($max_blocknumber =="") {
	                	$max_blocknumber ="4085142";
	                } */

	                $max_blocknumber ="3531100";

	                $output = file_get_contents('https://api.etherscan.io/api?module=account&action=txlist&address='.$account.'&endblock=latest');

	                $result = json_decode($output);

	                

	                if($result->message == 'OK'){

	                	$transaction=$result->result;

	                	for($tr=0;$tr<count($transaction);$tr++) {

	                		$block_number  = $transaction[$tr]->blockNumber;

	                		$address  = $transaction[$tr]->to; 

	                		$txid 		= $transaction[$tr]->hash;
	                		$value 		= $transaction[$tr]->value;
	                		$ether_balance = ($value/1000000000000000000); 
	                		$confirmations =$transaction[$tr]->confirmations;
	                		if($transaction[$tr]->confirmations > 0){





	                			$dep_already = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$txid,'crypto_address'=>$address),'wiix_mydeposits');

	                			$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');

	                			$getAdminDetails = Helper::encrypt_decrypt("decrypt",$getngwalts->ETH_address);

	                			$adminEthAddr 	 = $getAdminDetails;
	                			if(!$dep_already){

	                				if($address != $adminEthAddr){

	                					DB::table('wiix_mydeposits')->insert(['user_id'=>$btcuserId,'dep_currency'=>"ETH",'dep_amount'=>$ether_balance,'txn_id'=>$txid,'confirmation'=>$confirmations,'dep_status'=>0,'crypto_address'=>$address]);


	                				}
	                			}
	                			else {
	                				if( $transaction[$tr]->confirmations >= 3 ) {

	                					$query=Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$txid,'dep_status'=>0),'wiix_mydeposits');






	                					if($query) {
	                						$userdata = array('dep_status' => 1);

	                						$where = array('txn_id'=>$txid);
	                						Basic::updatedatas($where,$userdata,'wiix_mydeposits');


	                						$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

	                						$fetchETHbalance     = $getwallets->ETH;




	                						$updateBTCbalance     = $fetchETHbalance+$ether_balance;


	                						$data = array('ETH' => $updateBTCbalance);

	                						$where = array('user_id'=>$btcuserId);
	                						Basic::updatedatas($where,$data,'wiix_user_wallet');							
	                					} 
	                				}
	                			}
	                		}
	                	}
	                }
	            }
	        }
	    }	   
	}

	public static function getETHAddress() {

		

		$query = DB::table('wiix_consumers')->count();

		if($query > 0) {                   
			$row = DB::table('wiix_consumers')->get();			
			return $row;			
		} 
		else{     
			return false;		
		}
	}
	public static function getETCAddress() {

		

		$query = DB::table('wiix_consumers')->count();

		if($query > 0) {                   
			$row = DB::table('wiix_consumers')->get();			
			return $row;			
		} 
		else{     
			return false;		
		}
	}
		public static function getusers() {

		

		$query = DB::table('wiix_consumers')->count();

		if($query > 0) {                   
			$row = DB::table('wiix_consumers')->get();			
			return $row;			
		} 
		else{     
			return false;		
		}
	}
	public function XMR_deposit_process()
	{
		$max_blocknumber = 0;

		$alluserdetails = DB::table('wiix_consumers')->get();
     // echo $this->db->last_query(); echo "<br>";
		foreach($alluserdetails as $row)
		{
			$payment_id = trim($row->payment_id);
			$user_id    = trim($row->user_id);
			if($payment_id!=''){
				$transactions = moneroController::get_bulk_payments($payment_id,$max_blocknumber);
      // echo '<pre>'; print_r($transactions); exit();
				$output = json_decode(json_encode($transactions), True);
				$unlock_time = $output['payments'][0]['unlock_time'];
				if($unlock_time !== 0)
				{
					continue;
				}

				$res_pid = $output['payments'][0]['payment_id'];
				if (strpos($res_pid, $payment_id) !== false) {
					$amount       = $output['payments'][0]['amount'] / 1000000000000;
					$block_height = $output['payments'][0]['block_height'];
					$cpayment_id   = $output['payments'][0]['payment_id'];
					$tx_hash      = $output['payments'][0]['tx_hash'];




					$query = Basic::getsinglerow(array('txn_id'=>$tx_hash),'wiix_mydeposits');
          // echo $this->db->last_query();

					$dep_already = $query;
          // echo 'dep_already - > ' . $dep_already;
          // exit();

					$getwiixuser=Basic::getsinglerow(array('user_id'=>$user_id),'wiix_consumers');

					$xmr_address = $getwiixuser->XMR_address;
          // echo $this->db->last_query(); exit();
					if(!$dep_already)
					{

         // $tot_amount = $amount - $admin_fee;

						$tot_amount = $amount;


						// Commented by Chockslingam


						// DB::table('wiix_mydeposits')->insert(['user_id'=>$user_id,'dep_currency'=>"XMR",'dep_amount'=>$amount,'txn_id'=>$tx_hash,'confirmation'=>'','dep_status'=>0,'crypto_address'=>$xmr_address]);



						DB::table('wiix_mydeposits')->insert(['user_id'=>$user_id,'dep_currency'=>"XMR",'dep_amount'=>$amount,'txn_id'=>$tx_hash,'confirmation'=>'','dep_status'=>1,'crypto_address'=>$xmr_address]);

						/*BAlance update*/


						$getwallets=Basic::getsinglerow(array('user_id'=>$user_id),'wiix_user_wallet');

						$fetchETHbalance     = $getwallets->XMR;




						$updateBTCbalance     = $fetchETHbalance+$amount;


						$data = array('XMR' => $updateBTCbalance);

						$where = array('user_id'=>$user_id);
						Basic::updatedatas($where,$data,'wiix_user_wallet');

						/*Balance end*/



						$message = 'Your deposit order '.$amount.' XMR confirmed.';


					}

				}



			}
		}
	}



function etc_deposit_process() {

	$getETHAddress 		= CronController::getETCAddress(); 
	if(isset($getETHAddress) && !empty($getETHAddress)) {
		foreach($getETHAddress as $row) {
			$btcuserId 	= $row->user_id;
			$account 	= trim($row->ETC_address);
			if($account != ""){

			    	/*$result = $this->db->query('SELECT MAX(blocknumber) as max_blocknumber FROM cc_transaction_history')->row();
	            	$max_blocknumber = $result ->max_blocknumber;
					if($max_blocknumber =="") {
	                	$max_blocknumber ="4085142";
	                } */

	                $max_blocknumber ="3531100";

	                $output = file_get_contents('https://api.etherscan.io/api?module=account&action=txlist&address='.$account.'&startblock='.$max_blocknumber.'&endblock=latest');

	                $result = json_decode($output);

	                echo "<pre>";
	                print_r($result); die;

	                if($result->message == 'OK'){

	                	$transaction=$result->result;

	                	for($tr=0;$tr<count($transaction);$tr++) {

	                		$block_number  = $transaction[$tr]->blockNumber;

	                		$address  = $transaction[$tr]->to; 

	                		$txid 		= $transaction[$tr]->hash;
	                		$value 		= $transaction[$tr]->value;
	                		$ether_balance = ($value/1000000000000000000); 
	                		$confirmations =$transaction[$tr]->confirmations;
	                		if($transaction[$tr]->confirmations > 0){





	                			$dep_already = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$txid,'crypto_address'=>$address),'wiix_mydeposits');

	                			$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_walletsaddress');

	                			$getAdminDetails = $getngwalts->etc_address;

	                			$adminEthAddr 	 = $getAdminDetails;
	                			if(!$dep_already){

	                				if($address != $adminEthAddr){

	                					DB::table('wiix_mydeposits')->insert(['user_id'=>$btcuserId,'dep_currency'=>"ETC",'dep_amount'=>$ether_balance,'txn_id'=>$txid,'confirmation'=>$confirmations,'dep_status'=>0,'crypto_address'=>$address]);


	                				}
	                			}
	                			else {
	                				if( $transaction[$tr]->confirmations >= 3 ) {

	                					$query=Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$txid,'dep_status'=>0),'wiix_mydeposits');



	                					if($query) {
	                						$userdata = array('dep_status' => 1);

	                						$where = array('txn_id'=>$txid);
	                						Basic::updatedatas($where,$userdata,'wiix_mydeposits');


	                						$getwallets=Basic::getsinglerow(array('user_id'=>$btcuserId),'wiix_user_wallet');

	                						$fetchETHbalance     = $getwallets->ETC;




	                						$updateBTCbalance     = $fetchETHbalance+$ether_balance;


	                						$data = array('ETC' => $updateBTCbalance);

	                						$where = array('user_id'=>$btcuserId);
	                						Basic::updatedatas($where,$data,'wiix_user_wallet');							
	                					} 
	                				}
	                			}
	                		}
	                	}
	                }
	            }
	        }
	    }	   
	}

	public function xrp_deposit_process()
	{

        $cur_date       = date('Y-m-d');
        $cur_time       = date('H:i:s');
        $alluserdetails = CronController::getusers();        
        if ($alluserdetails) {
             foreach ($alluserdetails as $row) {  
                $result['payments'] = array();
                $ripple_address = trim($row->XRP_address); 
                $btcuserId = $row->user_id; 
                if($ripple_address!=''){   
                $url = "https://data.ripple.com/v2/accounts/" . $ripple_address . "/payments?currency=XRP&type=received"; 
                $output = file_get_contents($url);
                $result = json_decode($output,true);

                if($result){   

                    print_r($result);print_r("result");
                    if($result['result'] == 'success' && count($result['payments'])){
                        for($i=0;$i<count($result['payments']);$i++){
                            $txidnew        =    $result['payments'][$i]['tx_hash']; 
                //$txidnew        = $result['payments'][0]['tx_hash'];
				print_r("hai");

                $dep_already = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$txidnew,'crypto_address'=>$address),'wiix_mydeposits');


                if (!$dep_already) {                
                $address = $result['payments'][$i]['destination'];
                $amount  = $result['payments'][$i]['amount']; 
                         $wallet      = "XRP";  
                         $txid        = 'TD' . $btcuserId . time();
                         


                            DB::table('wiix_mydeposits')->insert(['user_id'=>$btcuserId,'dep_currency'=>"XRP",'dep_amount'=>$amount,'txn_id'=>$txid,'confirmation'=>$confirmations,'dep_status'=>0,'crypto_address'=>$address,'rand_txid' => $txidnew,]);


                            $dep_already = Basic::getsinglerow(array('user_id'=>$btcuserId,'txn_id'=>$txidnew,'crypto_address'=>$address),'wiix_mydeposits');

                            $arrtrans=Basic::getsinglerow(array('txn_id'=>$txidnew),'wiix_mydeposits');

                        

                            $user_id    = $row->user_id;

                            $balance=Basic::getsinglerow(array('user_id'=>$user_id),'wiix_user_wallet');

	                		
                            $update_bal = $balance->XRP + $amount;

                            $updateBTCbalance     = $update_bal;


	                						$data = array('XRP' => $updateBTCbalance);

	                						$where = array('user_id'=>$btcuserId);
	                						Basic::updatedatas($where,$data,'wiix_user_wallet');	


                         
                            } 

                            $userdata = array('dep_status' => 0);

	                						$where = array('txn_id'=>$dep_already->txn_id);
	                						Basic::updatedatas($where,$userdata,'wiix_mydeposits');


                              
 
                                            $url = "https://data.ripple.com/v2/accounts/" . $ripple_address . "/balances?currency=XRP";
                                            $output = file_get_contents($url);
                                            $result = json_decode($output,true);
                                            echo "<pre>";
                                            //print_r($result);
                                            if($result['result'] == 'success' && count($result['balances'])){
                                            $value =  $result['balances'][0]['value'];
                                            }else{
                                                $value=0;
                                            }

                                   
                                            if ($value > 21) {
                                            $newvalue = $value-20;
                                                $ripple_secret = $row->xrp_note; 
                                                 $bitcoin_row=Basic::getsinglerow(array('coinname'=>"ripple"),'wiix_cryptodetails');


                                                $xrp_addr = $bitcoin_row->address_ripple;
                                                $address  = trim($xrp_addr);
                                                $ripple_secret  = trim($ripple_secret);
                                                echo 'cd /var/www/html; /usr/bin/node ripple_sendcoins.js "' . $address . '" "' . $newvalue . '" "' . $ripple_address . '" "' . $ripple_secret . '"';
                                                $transaction = shell_exec('cd /var/www/html; /usr/bin/node ripple_sendcoins.js "' . $address . '" "' . $newvalue . '" "' . $ripple_address . '" "' . $ripple_secret . '"');
                                                echo $transaction; 
                                               // print_r($transaction);
                                                $testt   = explode('NaN', $transaction);
                                                $a       = $testt[1];
                                                $b       = $testt[2];
                                                $aa      = json_decode($a, true);
                                                $txxid1  = $aa['txid'];
                                                $bb      = json_decode($b, true);
                                                $messag2 = $bb['resultCode'];
                                                if ($messag2 == 'tesSUCCESS' || $messag2=='terQUEUED') {
                                                	 $userdata = array('dep_status' => 1);

	                						$where = array('txn_id'=>$dep_already->txn_id);
	                						Basic::updatedatas($where,$userdata,'wiix_mydeposits');
                                           
                                                } 
                                                
                                            }

                                            }
                                        }


    }
    }

    }

    }
    }
    
	
	     function xrpadminwallet()
     {
          $cur_date       = date('Y-m-d');
            $cur_time       = date('H:i:s');
            $alluserdetails = CronController::getusers();         
            if ($alluserdetails) {
                foreach ($alluserdetails as $row) {  
                    $ripple_address = $row->XRP_address; 
                    $btcuserId = $row->user_id; 
                    if($ripple_address!=''){   
                        $output  = shell_exec('cd /var/www/html; /usr/bin/node ripple_transaction.js "' . $ripple_address . '"');
                        $result = json_decode($output);   
                                if($result){  
                                    foreach ($result as $res) {
                                        if($res->outcome->result == 'tesSUCCESS'){

                                            $output  = shell_exec('cd /var/www/html; /usr/bin/node ripple_balance.js "' . $ripple_address . '"');
                                            $output1 = json_decode($output);
                                            if ($output1) {
                                            foreach ($output1 as $key => $value) {
                                            $value = $value->value;
                                            }
                                            if ($value > 21) {
                                            $newvalue = $value-20;
                                                $ripple_secret = $row->ripple_secret;

                                                  $bitcoin_row=Basic::getsinglerow(array('coinname'=>"ripple"),'wiix_cryptodetails');


                                                $xrp_addr = $bitcoin_row->address_ripple;

                                               
                                                $address  = trim($xrp_addr);
                                                $transaction = shell_exec('cd /var/www/html; /usr/bin/node ripple_sendcoins.js "' . $address . '" "' . $newvalue . '" "' . $ripple_address . '" "' . $ripple_secret . '"');
                                                if ($transaction) {
                                                $testt   = explode('NaN', $transaction);
                                                $a       = $testt[1];
                                                $b       = $testt[2];
                                                $aa      = json_decode($a, true);
                                                $txxid1  = $aa['txid'];
                                                $bb      = json_decode($b, true);
                                                $messag2 = $bb['resultCode'];
                                                if ($messag2 == 'tesSUCCESS' || $messag2=='terQUEUED') {
                                        
                                                echo 'inserted';
                                                }
                                                }
                                            }

                                            }
                                        }
                                    } 
                                }  
                    }
                }
            }
    } 
    function generate_adminaddress()
{
	$output = shell_exec('curl -H "Content-Type: application/json" -X POST --data \'{"jsonrpc":"2.0","method":"personal_newAccount","params":["wiixneiomqLhQaGertdcjLjCqyBKyLa"],"id":1}\' "18.218.213.253:8545"');
						$res  = json_decode($output);
						$address = $res->result;
						print_r($address);
}	

   function ethadminwallet()
    {
    	

    	$getaddeth   	 = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
		$eth_addr    	 = trim($getaddeth->ETH_address);
		$address         = '"'.Helper::encrypt_decrypt('decrypt',$eth_addr).'"';
		$eth_user_key    = trim($getaddeth->ETH_user_key);
		$key     	     = '"'.Helper::encrypt_decrypt('decrypt',$eth_user_key).'"';

       	$data 		     = array('key'=>$key,'adminaddress'=>$address);
		$output 	     = Helper::connecteth('toadminwallet',$data);
		echo "<pre>";print_r($output);
		// exit;
       
    	// $getaddeth=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
     //    $eth_addr = "0x14a12f46f4b86198f38fd619f246513b11085047";
     //    $address = trim($eth_addr);
        // $result = exec('cd /var/www/html; node eth_transferadmin.js '.$address.' 2>&1',$output1, $return_var1);
        // echo "<pre>";print_r($output1);print_r($return_var1);exit;

    } 

    function etcadminwallet()
    {
    	

    	$getaddeth   	 = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
		$etc_addr    	 = trim($getaddeth->ETC_address);
		$address         = '"'.Helper::encrypt_decrypt('decrypt',$etc_addr).'"';
		$etc_user_key    = trim($getaddeth->ETC_user_key);
		$key     	     = '"'.Helper::encrypt_decrypt('decrypt',$etc_user_key).'"';

       	$data 		     = array('key'=>$key,'adminaddress'=>$address);
		$output 	     = Helper::connectetc('toadminwallet',$data);
		echo "<pre>";print_r($output);
		// exit;
       
    	// $getaddeth=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
     //    $eth_addr = "0x14a12f46f4b86198f38fd619f246513b11085047";
     //    $address = trim($eth_addr);
        // $result = exec('cd /var/www/html; node eth_transferadmin.js '.$address.' 2>&1',$output1, $return_var1);
        // echo "<pre>";print_r($output1);print_r($return_var1);exit;

    } 
}
