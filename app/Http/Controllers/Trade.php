<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Cloudinary;

// use App\Model\Googleauthenticator;
// use App\Model\jsonRPCClient;
use App\Model\User;
use App\Model\EmailTemplate;

use Validator;
use Auth;
use Redirect;
use DB;
use URL;
use Session;
use Mail;

class Trade extends Controller {
	
	public function __construct() {
		$userid   = Session::get('wiix_userId');
		$username = Session::get('wiix_username');
		if($userid == '' || $username == '')
			return redirect('/');
	}

	function index() {
		$page_key = 'trade';
		$data=compact('page_key');
		return view('front.trade.trade',$data);
	}


	

	// end class

}
