<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Cloudinary;

// use App\Model\Googleauthenticator;
// use App\Model\jsonRPCClient;
use App\Model\User;
use App\Model\ConsumerVerification;
use App\Model\Currency;
use App\Model\Wallet;
use App\Model\CoinAddress;
use App\Model\Deposit;

use Validator;
use Auth;
use Redirect;
use DB;
use URL;
use Session;
use Mail;
use App\Http\Controllers\CryptoAddress;

class Credit extends Controller {
	//
	public function __construct() {
		//Configure cdn with credentials
		Cloudinary::config(array(
		    "cloud_name" => "dh0kb7sbx",
		    "api_key" => "578432172636126",
		    "api_secret" => "BpYGipiQuAnzS8LohSmmtadF6iE"
		));
	}

	public function index($type) {
		$page_key = 'deposit';
		$userId = session('wiix_userId');

		$currency = Currency::select('id','symbol','name')->get();
		$currency = $currency->toArray();
		$getKey = array_search($type, array_column($currency, 'symbol'));
		$currencyId = $currency[$getKey]['id'];

		$getUser = User::where('id',$userId)
				->with(['deposits' => function($query) { return $query->orderBy('created_at', 'desc')->select('created_at','currency','amount','status','user_id','transfer_amount')->limit(15); }])
				->with(['coinAddress' => function($query) use ($currencyId) { return $query->where('token',$currencyId)->select('address','user_id'); }])
				->with(['wallet' => function($query) use ($currencyId) { return $query->where('token',$currencyId)->select('remaining','user_id'); }])
				->select('id','wiix_content','unusual_user_key')->first();
		$wallet = $getUser->wallet;
		$address = $getUser->coinAddress;

		if(empty($address)) {
			$email = User::endecryption(2,$getUser->wiix_content).User::endecryption(2,$getUser->unusual_user_key);
			$coin['address'] = $coinAddr = CryptoAddress::createAddress($type,$email);
			$insertData = array('user_id'=>$userId,'token'=>$currencyId,'address'=>$coinAddr);
			CoinAddress::create($insertData);
		} else {
			$coin['address'] = $coinAddr = $address[0]->address;
		}
		$coin['wallet'] = $balance = $wallet[0]->remaining;
		$transitAmount = Deposit::transitAmount($userId,$currencyId);
		$availableBalance = $balance - $transitAmount;
		$coin['transit'] = number_format($transitAmount,8,'.','');
		$coin['available'] = number_format($availableBalance,8,'.','');
		$coin['QR_code'] = "https://chart.googleapis.com/chart?cht=qr&chs=173x171&chl=$coinAddr&choe=UTF-8&chld=L";

		$data = compact('page_key','getUser','currency','coin','type');
		return view('front.deposit.deposit',$data);
	}

	public function getCryptoInfo(Request $request) {
		$userId = session('wiix_userId');
		$type = $request['currency'];

		$currency = Currency::select('id','symbol','name')->get();
		$currency = $currency->toArray();
		$getKey = array_search($type, array_column($currency, 'symbol'));
		$currencyId = $currency[$getKey]['id'];

		$getUser = User::where('id',$userId)
				->with(['coinAddress' => function($query) use ($currencyId) { return $query->where('token',$currencyId)->select('address','user_id'); }])
				->with(['wallet' => function($query) use ($currencyId) { return $query->where('token',$currencyId)->select('remaining','user_id'); }])
				->select('id','wiix_content','unusual_user_key')->first();

		$wallet = $getUser->wallet;
		$address = $getUser->coinAddress;

		if($address->isEmpty()) {
			$email = User::endecryption(2,$getUser->wiix_content).User::endecryption(2,$getUser->unusual_user_key);
			$coin['address'] = $coinAddr = CryptoAddress::createAddress($type,$email);
			$insertData = array('user_id'=>$userId,'token'=>$currencyId,'address'=>$coinAddr);
			CoinAddress::create($insertData);
		} else {
			$coin['address'] = $coinAddr = $address[0]->address;
		}
		$coin['wallet'] = $balance = $wallet[0]->remaining;
		$transitAmount = Deposit::transitAmount($userId,$currencyId);
		$availableBalance = $balance - $transitAmount;
		$coin['transit'] = number_format($transitAmount,8,'.','');
		$coin['available'] = number_format($availableBalance,8,'.','');
		$coin['QR_code'] = "https://chart.googleapis.com/chart?cht=qr&chs=173x171&chl=$coinAddr&choe=UTF-8&chld=L";

		echo json_encode($coin);
	}

	

}
