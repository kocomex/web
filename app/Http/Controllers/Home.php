<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Model\User;
use App\Model\EmailTemplate;
use App\Model\ConsumerVerification;
use App\Model\Currency;
use App\Model\Wallet;
use App\Model\UserActivity;

use Validator;
use Auth;
use Redirect;
use DB;
use URL;
use Session;
use Mail;

class Home extends Controller {
	//
	public function __construct() {
		
	}

	//To return index page
	public function index() {
		$data['page_key'] = 'index';
		return view('front.index.index')->with('data',$data);
	}

	// if method is post, we'll save user details in DB and to send activation email to the user and to display auth mail page. if method is get we'll return signup page.
	public function register(Request $request) {
		if ($request->isMethod('post')) {
			$this->validate($request, [
					'username' => 'required|unique_username',
					'usermail' => 'required|email|unique_email',
					'password' => 'required|confirmed|min:8',
					'password_confirmation' => 'required|min:8',
					'fill_captcha' => 'required',
				], [
					'username.required' => 'Username is required',
					'password.required' => 'Password is required',
					'password_confirmation.required' => 'Confirm Password is required',
					'usermail.required' => 'Email id is required',
					'fill_captcha.required' => 'Captcha code is required',
					'usermail.email' => 'Enter valid email id',
					'usermail.unique_email' => 'Email id already exists',
					'username.unique_username' => 'Username already exists'
				]
			);

			$captcha = strip_tags($request['fill_captcha']);
			if($captcha != session('captcha')) {
				Session::flash('error', "Enter valid captcha code");
				return redirect('signup');
			}

			$username = strip_tags($request['username']);
			$usermail = strtolower(strip_tags($request['usermail']));
			$password = strip_tags($request['password']);

			$first_mail  = User::endecryption(1,User::firstEmail($usermail));
			$second_mail = User::endecryption(1,User::secondEmail($usermail));
			$password 	 = User::endecryption(1,$password);

			$userdata['consumer_name'] = $username;
			$userdata['wiix_content'] = $first_mail;
			$userdata['unusual_user_key'] = $second_mail;
			$userdata['wiix_protect_key'] = $password;
			$userdata['status'] = "inactive";
			$insert_data = User::create($userdata);

			if($insert_data) {
				$userId = $insert_data->id;
	  			ConsumerVerification::create(['user_id'=>$userId]);
	  			$encryptUname = User::endecryption(1,$username);
	  			$securl = url("/activation/".$encryptUname);
				$getEmail = EmailTemplate::where('id',1)->first();
				$getSiteDetails  = Controller::getEmailTemplateDetails();
				$info = array('###USERNAME###'=>$username,'###LINK###'=>$securl);
				$replace = array_merge($getSiteDetails, $info);
				$emaildata = array('content' =>strtr($getEmail->template,$replace));

				$toDetails['useremail'] = $usermail;
				$toDetails['subject'] = $getEmail->subject;
				$toDetails['from'] = $getSiteDetails['contact_mail_id'];
				$toDetails['name'] = $getSiteDetails['site_name'];

				$sendEmail = Controller::sendEmail($emaildata, $toDetails);

				if(count(Mail::failures()) > 0) {
		  			Session::flash('error', 'Please try again later.');
				} else {
					$mail['usermail'] = $usermail;
					$mail['email_content'] = json_encode($emaildata);
					$mail['toDetails'] = json_encode($toDetails);
			  		return view('front.common.auth_mail')->with('mail',$mail);
				}
			} else {
				Session::flash('error', "Failed to create User");
				return redirect('signup');
			}
		} else {
			if(session('wiix_userId')!='') {
		   		return redirect('trade/deal');
		    }
		    $recaptcha_code = User::recaptchacode1();
		    session(['captcha' => $recaptcha_code]);  //store the captcha
			$page_key = 'register';
			$data = compact('page_key','recaptcha_code');
			return view('front.common.register',$data);
		}
	}

	// refesh the captcha content
	function captcharefresh() {
		$recaptcha_code = User::recaptchacode1();
		session(['captcha' => $recaptcha_code]);  //store the captcha
		return $recaptcha_code;
	}

	//To return view after the user clicks resend email button
	public function auth_mail(Request $request) {
		if ($request->isMethod('post')) {
			$data = $request;
		    $emaildata = json_decode($data['email_content'],true);
		    $toDetails = json_decode($data['toDetails'],true);
		    $usermail = $toDetails['useremail'];
		    $first  = User::endecryption(1,User::firstEmail($usermail));
			$second = User::endecryption(1,User::secondEmail($usermail));
			$checkStatus = User::where('wiix_content', $first)->where('unusual_user_key', $second)->where('status', 'inactive')->count();
			if($checkStatus == 1) {
				$sendEmail = Controller::sendEmail($emaildata, $toDetails);
			    if(count(Mail::failures()) > 0) {
		  			Session::flash('error', 'Please try again later.');
				} else {
					$mail['usermail'] = $usermail;
					$mail['email_content'] = json_encode($emaildata);
					$mail['toDetails'] = json_encode($toDetails);
			  		return view('front.common.auth_mail')->with('mail',$mail);
				}
			} else {
				Session::flash('error', 'Email already activated.');
				return redirect('login');
			}
		} else {
			if(session('wiix_userId')!='') {
		   		return redirect('trade/deal');
		    }
		    return redirect('login');
		}
	}

	//To activate email and to create wallet with 0 balance for the user
	public function activateEmail($id) {
		if(session('wiix_userId')!='') {
	   		return redirect('trade/deal');
	    }
	    $userName = User::endecryption(2,$id);

	    $getUser = User::where('consumer_name',$userName)->where('status','inactive')->select('id')->first();
	    if($getUser) {
	    	$userId = $getUser->id;
	    	$currencies = Currency::select('id')->get();
	    	$insertData = array();
	    	foreach($currencies as $currency) {
	    		$insertData[] = [
	    			'user_id' => $userId,
	    			'token' => $currency->id,
	    			'remaining' => 0
	    		];
	    	}
	    	$createWallet = Wallet::insert($insertData);
	    	$update = User::where('id',$userId)->update(['status'=>'active']);
	    	if($update) {
	    		return view('front.common.join_complete');
	    	} else {
	    		Session::flash('error', 'Failed to activate email.');
	    	}
	    } else {
    		Session::flash('error', 'Email already activated.');
	    }
	    return redirect('login');
	}

	//if method is post, we'll check DB and proceed with login process. if method is get we'll return login page
	public function login(Request $request) {
		if ($request->isMethod('post')) {
			$this->validate($request, [
					'email' => 'required|email',
					'password' => 'required|min:8',
					'fill_captcha' => 'required'
				], [
					'password.required' => 'Password is required',
					'email.required' => 'Email id is required',
					'fill_captcha.required' => 'Captcha code is required',
					'email.email' => 'Enter valid email id'
				]
			);
			
			$captcha = strip_tags($request['fill_captcha']);
			if($captcha != session('captcha')) {
				Session::flash('error', "Enter valid captcha code");
				return redirect('login');
			}

			$usermail = strtolower(strip_tags($request['email']));
			$password = strip_tags($request['password']);

			$first  = User::endecryption(1,User::firstEmail($usermail));
			$second = User::endecryption(1,User::secondEmail($usermail));
			$password 	 = User::endecryption(1,$password);

			$login = User::where('wiix_content', $first)->where('unusual_user_key', $second)->where('wiix_protect_key', $password)->select('id','status','tfa_status','consumer_name')->first();
			if($login) {
				if($login['tfa_status'] == "enable") {
	  				$loginId = User::endecryption(1,$login['id']);
	  				return redirect("tfaLogin/".$loginId);
	  			}

	  			if($login['status'] == "active") {
	  				$ip = Controller::getIpAddress();
	  				$ipPlugin = 'extreme-ip-lookup.com/json/'.$ip;
					$addrDetails = Controller::getContents($ipPlugin);
					$ipDetail = json_decode($addrDetails);
					$city = $ipDetail->city;
					$country = $ipDetail->country;
			        if($city != "") {
			        	$activity['city'] = $city;
			        } else {
			        	$activity['city'] = 'Madurai';
			        }
			        if($country != "") {
			        	$activity['country'] = $country;
			        } else {
			        	$activity['country'] = 'India';
			        }
			        $activity['ip_address'] = $ip;
			        $activity['browser_name'] = Controller::getBrowser();
			        $activity['activity'] = "Login";
			        $activity['user_id'] = $login['id'];
			        $activity['platform'] = Controller::getPlatform();
			        UserActivity::create($activity);

					Session::flash('success', 'Successfully logged in.');
		  			session(['wiix_userId' => $login['id'],'wiix_userName' => $login['consumer_name']]);
		  			return redirect('trade/deal');
	  			} elseif($login['status'] == "inactive") {
	  				Session::flash('error', 'Please activate your account from registered email.');
	  			} else {
	  				Session::flash('error', 'Your email deactivated by admin.');
	  			}
	  		} else {
	  			Session::flash('error', 'Invalid login credentials!');
	  		}
	  		return redirect('login');
		} else {
			if(session('wiix_userId')!='') {
		   		return redirect('trade/deal');
		    }
		    $recaptcha_code = User::recaptchacode1();
		    session(['captcha' => $recaptcha_code]);  //store the captcha
			$page_key = 'login';
			$data = compact('page_key','recaptcha_code');
			return view('front.common.login',$data);
		}
	}

	//logout
	public function logout(Request $request) {
		if(session('wiix_userId')!='') {
			$ip = Controller::getIpAddress();

			$ipPlugin = 'extreme-ip-lookup.com/json/'.$ip;
			$addrDetails = Controller::getContents($ipPlugin);
			$ipDetail = json_decode($addrDetails);
			$city = $ipDetail->city;
			$country = $ipDetail->country;
	        if($city != "") {
	        	$activity['city'] = $city;
	        } else {
	        	$activity['city'] = 'Madurai';
	        }
	        if($country != "") {
	        	$activity['country'] = $country;
	        } else {
	        	$activity['country'] = 'India';
	        }
		    $activity['ip_address'] = $ip;
		    $activity['browser_name'] = Controller::getBrowser();
		    $activity['activity'] = "Logout";
		    $activity['user_id'] = session('wiix_userId');
		    $activity['platform'] = Controller::getPlatform();
		    UserActivity::create($activity);

		    Session::flush();
		    Session::flash('success', 'Logged out');
		    return redirect('login');	
		} else {
			Session::flash('error', 'Session Expired!');
		    return redirect('login');
		}
  	}

}
