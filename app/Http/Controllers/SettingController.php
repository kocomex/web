<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Basic;
use App\profile;
use App\wallet;
use App\bank_details;

use Googleauthenticator;
use Illuminate\Contracts\Filesystem\Filesystem;


class SettingController extends Controller
{

	

	public function profile(Request $request)
	{

		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }



		$getusers= Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_consumers');
		$usersta=$getusers->status;

		if($usersta == 0)
		{
			AuthController::logout($request);
			return redirect('/');
		}

 $ga = new Googleauthenticator();  
       if($request->isMethod('post'))
        {


        	$wherepro = array('user_id'=>Session::get('user_id'));
		$profie= Basic::getsinglerow($wherepro,'wiix_user_profile');

        	$path=public_path('verify/document');









			if ($request->hasFile('prof_pic')) 
			{
				
        			$fileExtensions = ['jpeg','jpg','png'];
				$fileName = $_FILES['prof_pic']['name'];
				$fileType = $_FILES['prof_pic']['type'];
				$explode = explode('.',$fileName);
	  			$extension = end($explode);
	  			$fileExtension = strtolower($extension);
	  			$mimeImage = mime_content_type($_FILES["prof_pic"]['tmp_name']);
	  			$explode = explode('/', $mimeImage);

	  			if (!in_array($fileExtension,$fileExtensions)) {
	  				Session::flash('error', 'Invalid file type. Only image files are accepted.');  
					return Redirect::back();
	  			} else {
	  				if($explode[0] != "image") {
	  					Session::flash('error', 'Invalid file type. Only image files are accepted.');  
						return Redirect::back();
	  				}
	  								$fileName =$_FILES['prof_pic']['name'];
				//$request->file('prof_pic')->move($path,$fileName);

	  			/*S3 bucket*/
				$image = $request->file('prof_pic');
				$imageFileName1 = time() . '.' . $image->getClientOriginalExtension();



				$s3 = \Storage::disk('s3');
$filePath = '/wiix/' . $imageFileName1;
$s3->put($filePath, file_get_contents($image), 'public');
				/*End*/
	  			}

	  			$prof_pic = Helper::s3bucketURL().$imageFileName1;
	  			$data = array('first_name' => $request['fname'],'last_name' => $request['lname'],'address1' => $request['address1'],'address2' => $request['address2'],'city' => $request['city'],'prof_image' => $prof_pic,'country' => $request['country'],'postal_code' => $request['postal']);

			}
			else
			{
				$fileName = $profie->prof_image;
				$data = array('first_name' => $request['fname'],'last_name' => $request['lname'],'address1' => $request['address1'],'address2' => $request['address2'],'city' => $request['city'],'country' => $request['country'],'postal_code' => $request['postal']);
			}
			
			
			// if($fileName) 
			// {
			// 	$prof_pic = Helper::s3bucketURL().$imageFileName1;
			// }
			// else{
			// 	$prof_pic =  $profie->prof_image;
			// }

			// $data = array('first_name' => $request['fname'],'last_name' => $request['lname'],'address1' => $request['address1'],'address2' => $request['address2'],'city' => $request['city'],'prof_image' => $prof_pic,'country' => $request['country'],'postal_code' => $request['postal']);

			$where = array('user_id'=>Session::get('user_id'));
			Basic::updatedatas($where,$data,'wiix_user_profile');



$request->session()->flash('success', 'Hi,'.' '.Session::get('consumer_name').' '.'your profile updated successfully!!!'); 

echo json_encode(array('status' => TRUE,'title' => 'sucess'));
        }


        else{
        	

        	     $where1 = array('user_id'=>Session::get('user_id'));
		$data['userdetails']= Basic::getsinglerow($where1,'wiix_consumers');

        		$siteSettings1 = DB::table('wiix_site_settings')->where('id',1)->get();
		$siteSettings=$siteSettings1[0];



     if(trim($data['userdetails']->secret)!=''){
            $secret_code = $data['userdetails']->secret;
            $tfa_url = $data['userdetails']->tfa_url;
    }else{
         $secret_code = $ga->createSecret(); 
         $tfa_url = $ga->getQRCodeGoogleUrl($siteSettings->site_name, $secret_code);
    }                 
    $data['secret_code'] = $secret_code;
    $data['auth_url'] = $tfa_url;  

        	        $where1 = array('user_id'=>Session::get('user_id'));
		$data['users']= Basic::getsinglerow($where1,'wiix_consumers');
		$femail=Helper::encrypt_decrypt("decrypt",$data['users']->secret_key);
		$semail=Helper::encrypt_decrypt("decrypt",$data['users']->display);
		$data['final']=$femail."@".$semail;

		$where = array('page_name'=>'Profile');
		$data['prof']= Basic::getsinglerow($where,'wiix_cms');




		$wherepro = array('user_id'=>Session::get('user_id'));
		$data['profie']= Basic::getsinglerow($wherepro,'wiix_user_profile');
		$data['bank_det']= Basic::getsinglerow($wherepro,'wiix_user_bank');
$walletwhere = array('user_id'=>Session::get('user_id'));
		$data['wallet']= Basic::getsinglerow($walletwhere,'wiix_user_wallet');

		$notify_count=DB::table('wiix_notification')->count();

		if($notify_count > 0)
{

$get_notify1=DB::table('wiix_notification')->get();
$data['get_notify']=$get_notify1[0];

}
else{
  $data['get_notify']="";
}




		$data['verify_content']= Basic::getsinglerow(array('page_name'=>'verify content'),'wiix_cms');


		$data['settings']=HomeController::mainfunction();
		$data['title'] = $data['prof']->meta_title;
		$data['meta_keyword'] = $data['prof']->meta_keyword;
		$data['meta_description'] = $data['prof']->meta_description;

		return view('consumer/profile',$data);
        }



	}


	public function bank_details(Request $request)
	{

		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }

 if($request->isMethod('post'))
        {

        	$data = array('bank_name' => $request['bankname'],'holder_name' => $request['accholder'],'account_number' => $request['accno'],'branch' => $request['bankcity'],'iban' => $request['swiftcode'],'country' => $request['bankcountry'],'status' => 1);

			$where = array('user_id'=>Session::get('user_id'));
			Basic::updatedatas($where,$data,'wiix_user_bank');



$request->session()->flash('success', 'Hi,'.' '.Session::get('consumer_name').' '.'your bank details updated successfully!!!'); 

echo json_encode(array('status' => TRUE,'title' => 'sucess'));
        }

        else{

        $where1 = array('user_id'=>Session::get('user_id'));
		$data['users']= Basic::getsinglerow($where1,'wiix_consumers');
		$femail=Helper::encrypt_decrypt("decrypt",$data['users']->secret_key);
		$semail=Helper::encrypt_decrypt("decrypt",$data['users']->display);
		$data['final']=$femail."@".$semail;

		$where = array('page_name'=>'Profile');
		$data['prof']= Basic::getsinglerow($where,'wiix_cms');

		$wherepro = array('user_id'=>Session::get('user_id'));
		$data['profie']= Basic::getsinglerow($wherepro,'wiix_user_profile');

		$data['bank_det']= Basic::getsinglerow($wherepro,'wiix_user_bank');


		$data['settings']=HomeController::mainfunction();
		$data['title'] = $data['prof']->meta_title;
		$data['meta_keyword'] = $data['prof']->meta_keyword;
		$data['meta_description'] = $data['prof']->meta_description;

		return view('consumer/profile',$data);
        }



	}


	public function verification(Request $request)
	{

		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }

 if($request->isMethod('post'))
        {
        	$wherepro = array('user_id'=>Session::get('user_id'));
		$profie= Basic::getsinglerow($wherepro,'wiix_user_profile');

        	$path=public_path('verify/document');

			if ($request->hasFile('add_proof')) 
			{


        			$fileExtensions = ['jpeg','jpg','png'];
				$fileName = $_FILES['add_proof']['name'];
				$fileType = $_FILES['add_proof']['type'];
				$explode = explode('.',$fileName);
	  			$extension = end($explode);
	  			$fileExtension = strtolower($extension);
	  			$mimeImage = mime_content_type($_FILES["add_proof"]['tmp_name']);
	  			$explode = explode('/', $mimeImage);

	  			if (!in_array($fileExtension,$fileExtensions)) {
	  				Session::flash('error', 'Invalid file type. Only image files are accepted.');  
					return Redirect::back();
	  			} else {
	  				if($explode[0] != "image") {
	  					Session::flash('error', 'Invalid file type. Only image files are accepted.');  
						return Redirect::back();
	  				}
				$fileName =$_FILES['add_proof']['name'];
				//$request->file('add_proof')->move($path,$fileName);


				/*S3 bucket*/
				$image = $request->file('add_proof');
				$imageFileName = time() . '.' . $image->getClientOriginalExtension();

				$s3 = \Storage::disk('s3');
$filePath = '/wiix/' . $imageFileName;
$s3->put($filePath, file_get_contents($image), 'public');
				/*End*/
	  			}


			}
			else
			{
				$fileName = $profie->address_prof;
			}
			
			
			if($fileName) 
			{
				$add_proof = Helper::s3bucketURL().$imageFileName;
			}
			else{
				$add_proof =  $profie->address_prof;
			}


			if ($request->hasFile('id_proof')) 
			{

				        			$fileExtensions = ['jpeg','jpg','png'];
				$fileName = $_FILES['id_proof']['name'];
				$fileType = $_FILES['id_proof']['type'];
				$explode = explode('.',$fileName);
	  			$extension = end($explode);
	  			$fileExtension = strtolower($extension);
	  			$mimeImage = mime_content_type($_FILES["id_proof"]['tmp_name']);
	  			$explode = explode('/', $mimeImage);

	  			if (!in_array($fileExtension,$fileExtensions)) {
	  				Session::flash('error', 'Invalid file type. Only image files are accepted.');  
					return Redirect::back();
	  			} else {
	  				if($explode[0] != "image") {
	  					Session::flash('error', 'Invalid file type. Only image files are accepted.');  
						return Redirect::back();
	  				}
				$fileName1 =$_FILES['id_proof']['name'];
				//$request->file('id_proof')->move($path,$fileName1);

							/*S3 bucket*/
				$image = $request->file('id_proof');
				$imageFileName1 = time() . '.' . $image->getClientOriginalExtension();



				$s3 = \Storage::disk('s3');
$filePath = '/wiix/' . $imageFileName1;
$s3->put($filePath, file_get_contents($image), 'public');
				/*End*/
	  			}

				
			}
			else
			{
				$fileName1 = $profie->photo_id;
			}
			
			
			if($fileName1) 
			{
				$id_proof = Helper::s3bucketURL().$imageFileName1;
			}
			else{
				$id_proof =  $profie->photo_id;
			}

			


			$test=$profie->photo_id_status;
			$test1=$profie->address_prof_status;



			if($test == 2)

			{
			$value=0;
					//update
			}
			else if($test == 0)

			{
			$value=0;
					//update
			}
			else{
				$value=1;
			}
			if($test1 == 2)

			{
				$value1=0;
					//update
			}
				else if($test1 == 0)

			{
			$value1=0;
					//update
			}
			else{
				$value1=1;
			}


        	$data = array('photo_id_status' => $value,'address_prof_status' => $value1,'photo_id' => $id_proof,'address_prof' => $add_proof);

			$where = array('user_id'=>Session::get('user_id'));
			Basic::updatedatas($where,$data,'wiix_user_profile');



$request->session()->flash('success', 'Hi,'.' '.Session::get('consumer_name').' '.'your documents was successfullysend to our team !!!'); 

echo json_encode(array('status' => TRUE,'title' => 'sucess'));
        }

        else{

        $where1 = array('user_id'=>Session::get('user_id'));
		$data['users']= Basic::getsinglerow($where1,'wiix_consumers');
		$femail=Helper::encrypt_decrypt("decrypt",$data['users']->secret_key);
		$semail=Helper::encrypt_decrypt("decrypt",$data['users']->display);
		$data['final']=$femail."@".$semail;

		$where = array('page_name'=>'Profile');
		$data['prof']= Basic::getsinglerow($where,'wiix_cms');

		$wherepro = array('user_id'=>Session::get('user_id'));
		$data['profie']= Basic::getsinglerow($wherepro,'wiix_user_profile');

		$data['bank_det']= Basic::getsinglerow($wherepro,'wiix_user_bank');


		$data['settings']=HomeController::mainfunction();
		$data['title'] = $data['prof']->meta_title;
		$data['meta_keyword'] = $data['prof']->meta_keyword;
		$data['meta_description'] = $data['prof']->meta_description;

		return view('consumer/profile',$data);
        }



	}

	public function tfa(Request $request)
	{

		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }
      $where1 = array('user_id'=>Session::get('user_id'));
		$userdetails= Basic::getsinglerow($where1,'wiix_consumers');

    $ga = new Googleauthenticator();  

 if($request->isMethod('post'))
        {


 if(isset($request['onecode']) && trim($request['onecode'])    != ''){    
            if($ga->verifyCode($request['secret_code'], $request['onecode'], 3)){        
                if($userdetails->randcode == 'enable')
                    
                DB::table('wiix_consumers')->where('user_id',Session::get('user_id'))->update(['secret'=>'','tfa_url'=>'','randcode'=>'disable']);

                else


               DB::table('wiix_consumers')->where('user_id',Session::get('user_id'))->update(['secret'=>$request['secret_code'],'tfa_url'=>$request['auth_url'],'randcode'=>'enable']);

$request->session()->flash('success','Hey!,'.' '.Session::get('consumer_name').' '.'Your Two-Factor authentication was updated successfully');


echo json_encode(array('status' => TRUE,'title' => 'sucess'));
         
            }else{
                $request->session()->flash('error','Oops!,'.' '.Session::get('consumer_name').' '.'Please enter a valid code');


echo json_encode(array('status' => FALSE,'title' => 'Please enter a valid code'));
            }
        }


        }

        else{
   $where1 = array('user_id'=>Session::get('user_id'));
		$data['userdetails']= Basic::getsinglerow($where1,'wiix_consumers');

        		$siteSettings1 = DB::table('wiix_site_settings')->where('id',1)->get();
		$siteSettings=$siteSettings1[0];



     if(trim($data['userdetails']->secret)!=''){
            $secret_code = $data['userdetails']->secret;
            $tfa_url = $data['userdetails']->tfa_url;
    }else{
         $secret_code = $ga->createSecret(); 
         $tfa_url = $ga->getQRCodeGoogleUrl($siteSettings->site_name, $secret_code);
    }                 
    $data['secret_code'] = $secret_code;
    $data['auth_url'] = $tfa_url; 
         
        $where1 = array('user_id'=>Session::get('user_id'));
		$data['users']= Basic::getsinglerow($where1,'wiix_consumers');
		$femail=Helper::encrypt_decrypt("decrypt",$data['users']->secret_key);
		$semail=Helper::encrypt_decrypt("decrypt",$data['users']->display);
		$data['final']=$femail."@".$semail;

		$where = array('page_name'=>'Profile');
		$data['prof']= Basic::getsinglerow($where,'wiix_cms');

		$wherepro = array('user_id'=>Session::get('user_id'));
		$data['profie']= Basic::getsinglerow($wherepro,'wiix_user_profile');

		$data['bank_det']= Basic::getsinglerow($wherepro,'wiix_user_bank');


		$data['settings']=HomeController::mainfunction();
		$data['title'] = $data['prof']->meta_title;
		$data['meta_keyword'] = $data['prof']->meta_keyword;
		$data['meta_description'] = $data['prof']->meta_description;

		return view('consumer/profile',$data);
        }



	}


	

	public function changepassword(Request $request)
	{

		if(Session::get('user_id')=="")
      {
        return redirect('/');
      }

 if($request->isMethod('post'))
        {

        	$oldPassword = $request['old_password'];

     

$getd=DB::table('wiix_consumers')->where('user_id',Session::get('user_id'))->get();


$get_pass=$getd[0]->wallet_key;
$pass_decrypt=Helper::encrypt_decrypt("decrypt",$get_pass);

//print_r($oldPassword);print_r("hai");print_r($pass_decrypt);die;

if($pass_decrypt != $oldPassword)
{

  $request->session()->flash('error', 'Oops! Incorrect old password.Please give correct old password');
  echo json_encode(array('status' => FALSE,'title' => 'Incorrect old password.Please give correct old password'));die;
}




     $password1 = Helper::encrypt_decrypt("encrypt",$request['password']);



  $update_pass=DB::table('wiix_consumers')->where('user_id',Session::get('user_id'))->update([
      'wallet_key'=>$password1
 
      ]);   


 

     AuthController::logout($request);
       
        $request->session()->flash('success', 'Your password has been changed and please login again');
         echo json_encode(array('status' => TRUE,'title' => 'success'));
     
      
        }

        else{

        $where1 = array('user_id'=>Session::get('user_id'));
		$data['users']= Basic::getsinglerow($where1,'wiix_consumers');
		$femail=Helper::encrypt_decrypt("decrypt",$data['users']->secret_key);
		$semail=Helper::encrypt_decrypt("decrypt",$data['users']->display);
		$data['final']=$femail."@".$semail;

		$where = array('page_name'=>'Profile');
		$data['prof']= Basic::getsinglerow($where,'wiix_cms');

		$wherepro = array('user_id'=>Session::get('user_id'));
		$data['profie']= Basic::getsinglerow($wherepro,'wiix_user_profile');

		$data['bank_det']= Basic::getsinglerow($wherepro,'wiix_user_bank');


		$data['settings']=HomeController::mainfunction();
		$data['title'] = $data['prof']->meta_title;
		$data['meta_keyword'] = $data['prof']->meta_keyword;
		$data['meta_description'] = $data['prof']->meta_description;

		return view('consumer/profile',$data);
        }



	}

	// mobile OTP start here
	public function checkotp(Request $request)
	{
		if(Session::get('user_id')==""){
			return redirect('/');
		}else{
			if($request->isMethod('post')){
				$otp      = $request['otp'];
				$getusers = Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_user_profile');
				$userotp  = $getusers->last_otp;
				$en_otp   = Helper::encrypt_decrypt("encrypt",$otp);
				if($en_otp == $userotp){
					$userdata = array('mobile_verify' => 1);
					$where 	  = array('user_id'=>Session::get('user_id'));
					Basic::updatedatas($where,$userdata,'wiix_consumers');
					$request->session()->flash('success', 'Your mobile numbre has been verified.');
         			echo json_encode(array('status' => "success",'title' => 'success'));
				}else{
					$request->session()->flash('error', 'Oops! Incorrect OTP , Please enter correct OTP. ');
  					echo json_encode(array('status' => "error",'title' => 'Incorrect old password. Please give correct old password'));die;
				}
			}else{
				return redirect('/profile'); 
			}
		}
	}

	public function sendotp(Request $request)
	{
		if(Session::get('user_id')==""){
			return redirect('/');
		}else{
			if($request->isMethod('post')){
				$countrycode      = $request['register_countrycode'];
				$mobileNumber     = $request['mobilenumber'];
				$sendNumber       = $countrycode.$mobileNumber;
				$keyCode 		   = Helper::generateRandomnumber(6);

				$text = 'Your OTP Code is '.$keyCode;
				// $text = $keyCode;
				$sendSMS = Helper::sendSMS(trim($sendNumber),$text);
			
				$sms_result = json_decode($sendSMS);

				if(isset($sms_result->error)){
					$request->session()->flash('error', 'Oops! OTP is not sent , Please try again later. ');
  					echo json_encode(array('status' => "error",'title' => 'OTP is not sent , Please try again later.'));die;
				}else{
					$encryptKey = Helper::encrypt_decrypt("encrypt",$keyCode);
					$userdata = array('last_otp' => $encryptKey,'mobile'=>trim($sendNumber));
					$where 	  = array('user_id'=>Session::get('user_id'));
					Basic::updatedatas($where,$userdata,'wiix_user_profile');

					$request->session()->flash('success', 'OTP was sent into your moblile number . Please check it.');
         			echo json_encode(array('status' => "success",'title' => 'success'));
				}
			}else{
				return redirect('/profile'); 
			}
		}
	}

	// mobile OTP end here here

	public function notification(Request $request){


   if($request->isMethod('post')) 
    {

    $login_check = $request['login_check'];
    $trade_check = $request['trade_check'];
    $trans_check = $request['trans_check'];


$notifyuser_count=DB::table('wiix_notification')->where('user_id',Session::get('user_id'))->count();

    if($notifyuser_count == 0)
{

  $actuserid = DB::table('wiix_notification')->max('id') + 1;

DB::table('wiix_notification')->insert(['user_id'=> Session::get('user_id'),'id'=> $actuserid,'loginalert'=> $login_check,'trade'=> $trade_check,'transaction'=> $trans_check]);
}
else
{
DB::table('wiix_notification')->where('user_id',Session::get('user_id'))->update(['loginalert'=> $login_check,'trade'=> $trade_check,'transaction'=> $trans_check]);
}
$request->session()->flash('success', 'Hi, Your notification status updated successfully');
return redirect('/profile'); 

    }

    else{

    	return redirect('/profie'); 
    			



    }


	}
}