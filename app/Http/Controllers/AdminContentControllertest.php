<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Illuminate\Support\Facades\Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;

use App\Basic;


class AdminContentControllertest extends Controller
{
	

	//test chockalingam start
	public function addFaqtest(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		if($request->isMethod('post'))
		{
			$data = array(
				"faq_question" => $request['question'],
				"faq_answer" => $request['answer'],
				"status" => $request['status'],
				"last_updated" => date('Y-m-d H:i:s'),
				"created_on" => date('Y-m-d H:i:s')
				);
			$insert_id = Basic::insertdatas($data,'wiix_faq');
			if($insert_id){
				$request->session()->flash('success','FAQ added sucessfully');
				return redirect('WiPlytaIIX2/addFaqtest');
			}else{
				$request->session()->flash('error','Error in adding FAQ');
				return redirect('WiPlytaIIX2/addFaqtest');
			}
		}
		$data["title"] = "Add FAQ";
		return view('admin/testfaq.add',$data);
		
	}

	public function editFaqtest(Request $request,$id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		$data["faq_data"] = Basic::getsinglerow(array("faq_id"=>$id),'wiix_faq');	
		$data["faqs_id"]  = $id;

		if($request->isMethod('post'))
		{
			
			$data = array("faq_question" => $request['question'],
				
				"faq_answer"  => $request['answer'],
				"status"  => $request['status'],
				
				"last_updated" => date('Y-m-d H:i:s')
				);

			$where = array('faq_id'=>$id);					
			$is_update = Basic::updatedatas($where, $data,'wiix_faq');
			if($is_update){
				$request->session()->flash('success','FAQ updated sucessfully');
				return redirect('/WiPlytaIIX2/editFaq/'.$id);
			}else{
				$request->session()->flash('error','Error in updating FAQ');
				return redirect('/WiPlytaIIX2/editFaq/'.$id);
			}
		}else{
			$data["title"] = "Edit FAQ";
			
			return view('admin/faq.edit',$data);
		}
		

	}

	public function deleteFaqtest(Request $request,$id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	
		if($id)
		{
			$condition = array('id' => $id);
			$delete = DB::table('wiix_faq')->where('faq_id', $id)->delete();

			$request->session()->flash('success', 'FAQ deleted successfully');
			return redirect('/WiPlytaIIX2/viewFaqs');

		}
	}

	public function viewFaqstest(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	


		$where = array();
		$order= array('faq_id','desc');
		
		$data["faqs"] = Basic::getmultiplerow($where,'wiix_faq',$order);
		$data["title"] = "View FAQ";
		return view('admin/testfaq.view_all',$data);

	}
	//test chockalingam end

	

}

