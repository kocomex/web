<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\WithdrawCrypto;
use App\Basic;


class AdminHistory extends Controller
{
	public function dephistory(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');


		$where = array();
		$order= array('id','desc');
		$data["title"] = "Deposit user list";

		$data["details"] = Basic::getmultiplerow($where,'wiix_mydeposits',$order);


		
		return view('admin/history.dephistory',$data);




	}
	public function viewDephistory(Request $request,$id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');



		$result = '';

		$dep_det=Basic::getsinglerow(array("id"=>$id),'wiix_mydeposits');

		$get_userid=$dep_det->user_id;


		$get_users=Basic::getsinglerow(array("user_id"=>$get_userid),'wiix_consumers');

		$email[0] = Helper::encrypt_decrypt("decrypt",$get_users->secret_key);
		$email[1] = Helper::encrypt_decrypt("decrypt",$get_users->display);
		$secure_email=$email[0]."@".$email[1];



		$i=0;
		

		if($dep_det->dep_status == '0')
		{
			$sta=   '<span class="tx_orng2">Pending</span>';
		}
		else  if($dep_det->dep_status == '1')
		{
			$sta= '<span class="tx_orng2">Approved</span>';
		}
		else  if($dep_det->dep_status == '2')
		{
			$sta= '<span class="tx_orng2">Rejected</span>';
		}
		else  if($dep_det->dep_status == '3')
		{
			$sta= '<span class="tx_orng2">Cancelled</span>';
		}
		else  if($dep_det->dep_status == '4')
		{
			$sta= '<span class="tx_orng2">Waiting for admin mail confirmation</span>';
		}
		else  if($dep_det->dep_status == '5')
		{
			$sta= '<span class="tx_orng2">Completed</span>';
		}
		else{
			$sta='';
		}

		if($dep_det->dep_currency == "KRW")
		{
			$transno=$dep_det->txn_id;
		}
		else{
			$transno=$dep_det->crypto_address;
		}




		$data["admin_bank1"] = Basic::getsinglerow(array("id"=>1),'wiix_adminbank');
		if($dep_det->payment_method == "bankwire")
		{

		if($dep_det->bank_value == 1)
		{

			$data["admin_bank"]=$data["admin_bank1"]->bank_name1;

		}
		else if($dep_det->bank_value == 2)
		{
			$data["admin_bank"]=$data["admin_bank1"]->bank_name2;
		}
		if($dep_det->bank_value == 3)
		{
			$data["admin_bank"]=$data["admin_bank1"]->bank_name3;
		}
}
else{
	$data["admin_bank"]="nil";
}


		$result = '<div class="row" style="text-align:center;">
		<div class="col-xl-4">
			<tr>
				<th scope="row"><h5><b>User email</b></h5></th>
				<td>'.$secure_email.'</td>
			</tr>
		</div>
		<div class="col-xl-8">
			<table class="table">


				<tr>
					<th scope="row">Transaction number/Crypto Address</th>
					<td>'.$transno.'</td>
				</tr>
				<tr>
					<th scope="row">Deposit amount</th>
					<td>'.$dep_det->dep_amount.'</td>
				</tr>
				<tr>
					<th scope="row">Deposit Currency</th>
					<td>'.$dep_det->dep_currency.'</td>
				</tr>
				<tr>
					<th scope="row">Deposit status</th>
					<td>'.$sta.'</td>
				</tr>
				<tr>
					<th scope="row">Date and time</th>
					<td>'.$dep_det->created_date.'</td>
				</tr>

				<tr>
					<th scope="row">Admin bank name</th>
					<td>'.$data["admin_bank"].'</td>
				</tr>


				';
				

				$result .= '</table></div>';

				if($dep_det->dep_status==0){
					$result .= '<div class="col-xl-12"><form id="dep_approval" action="{{ url("/WiPlytaIIX2/depositApproval") }}" method="POST" novalidate="">



					<div class="form-group row"><label for="inputEmail3" class="col-sm-2 form-control-label">Comment</label><div class="col-sm-10"><textarea class="form-control" name="comment" id="comment" style="resize:none;" rows="3"></textarea></div></div> <a class="btn btn-pill-left btn-danger" href="javascript:depositrejectotp('.$id.');">Reject</a> <a class="btn btn-pill-right btn-success" href="javascript:depositacceptotp('.$id.');">Accept</a></form></div>';
				}	

				$result .= '</div>';

				$i++;



				echo $result;
			}


			public function depositrejectotp(Request $request,$id)
			{

				$result='<form method="POST" id="dep_approval2"><div class="form-group">
                                            <label class="control-label">OTP</label>
                                            <input type="text" class="form-control boxed" placeholder="Enter OTP code" id="otp_accept" name="otp_accept"> </div><div class="form-group"><a href="javascript:sendOTP();" class="btn btn-oval btn-success">Send OTP</a></div><div class="form-group"><a href="javascript:rejectdeposit('.$id.');" class="btn btn-oval btn-danger">Reject</a></div></form>';
				echo $result;


			}

			public function depositacceptotp(Request $request,$id)
			{
$urls=url('/WiPlytaIIX2/generate_transid');
				$result='<form method="POST" id="dep_approval1"><div class="form-group">
                                            <label class="control-label">OTP</label>
                                            <input type="text" class="form-control boxed" placeholder="Enter OTP code" id="otp_accept" name="otp_accept"> </div><div class="form-group"><a href="javascript:sendOTP();" class="btn btn-oval btn-success">Send OTP</a></div>
                                            <div class="form-group"><a href="javascript:acceptdeposit('.$id.');" class="btn btn-oval btn-info">Approve</a></div></form>';
				echo $result;


			}

			public function depositApproval(Request $request,$id)
			{

				$get_mobile=Basic::getsinglerow(array("id"=>1),'wiix_site_settings');
				$otp=Basic::getsinglerow(array("id"=>1),'wiix_otpcode');
				
				$mbldecrypt=Helper::encrypt_decrypt("decrypt",$otp->wallet_code);
				$passcode = $otp->otp_code;

					$condition = array("admin_id"=>1);


			$adminkey = Basic::getsinglerow($condition,'wiix_admin');

				$maildecrypt=Helper::encrypt_decrypt("decrypt",$adminkey->secret_key);



			$otp_acceptcode=$request['otp_accept'];

			

			if($otp_acceptcode != ($passcode))
			{

				echo json_encode(array('status' => FALSE,'title' => 'Enter a valid OTP code'));die;
				
			}
			else{

				Basic::updatedatas(array("id"=>1,"wallet_code"=>$get_mobile->wallet_code),array("otp_code"=>'expire000000000000'),'wiix_otpcode');
$data = explode("-",$id);


					switch($data[1]){
						case "accept":

$exists=Basic::getsinglerow(array("id"=>$id),'wiix_mydeposits');
						/*Update code*/
$balance = Basic::getsinglerow(array("user_id"=>$exists->user_id),'wiix_user_wallet');

						$curr = $exists->dep_currency;

						$update_bal = $balance->$curr + $exists->dep_amount;


						Basic::updatedatas(array("user_id"=>$exists->user_id), array($exists->dep_currency => $update_bal),'wiix_user_wallet');



						$update_values['dep_status'] = 5;

						$updated = Basic::updatedatas(array("id"=>$id),$update_values,'wiix_mydeposits');


						/*End code*/

						$msg=DB::table('wiix_email_templates')->where('mail_key','admin_deposit')->get();
						$to=$maildecrypt;


						$message=$msg[0]->message;
						$date=date('M d, Y');

						

						$message=str_replace("##NAME##","Admin",$message);
						$message=str_replace("##AMOUNT##",$exists->dep_amount,$message);
						$message=str_replace("##STATUS##","Pending",$message);
						
						$message=str_replace("##CURRENCY##",$exists->dep_currency,$message);
						$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
						$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);

						$email_template = 'Deposit status';

						Helper::mailsend($email_template,$message,$to);



						$request->session()->flash('success','User deposit request approved successfully');
						echo json_encode(array('status' => TRUE,'title' => 'User deposit request approved successfully'));
						break;
						case "reject":

						$condition=array("id"=>$id);
						$update_values['dep_status'] = 2;
						$update_values['reject_reason'] = $request['comment'];



						$is_update = Basic::updatedatas($condition, $update_values,'wiix_mydeposits');


						$getdepdet=Basic::getsinglerow(array("id"=>$id),'wiix_mydeposits');

						$getuser_id=$getdepdet->user_id;

						$getuser_det=Basic::getsinglerow(array("user_id"=>$getuser_id),'wiix_consumers');



						/*To send mail*/

		 $email[0] = Helper::encrypt_decrypt("decrypt",$getuser_det->secret_key);
        $email[1] = Helper::encrypt_decrypt("decrypt",$getuser_det->display);
        $secure_email = $email[0]."@".$email[1];

						$msg=DB::table('wiix_email_templates')->where('mail_key','user_deprejection')->get();
						$to=$secure_email;


						$message=$msg[0]->message;
						$date=date('M d, Y');

						

						$message=str_replace("##NAME##",$getuser_det->device_token,$message);
						$message=str_replace("##TYPE##","Deposit",$message);
						$message=str_replace("##COMMENT##",$request['comment'],$message);
						
						$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
						$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);

						$email_template = 'User Transactions status';

						Helper::mailsend($email_template,$message,$to);

						/*End of mail functionality*/



						$request->session()->flash('success','User deposit request rejected successfully');
						echo json_encode(array('status' => TRUE,'title' => 'User deposit request rejected successfully'));
						break;
					}
			}

				
				}


public static function  generate_transid()
{
	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < 2; $i++) {
	   $randomString .= $characters[rand(0, 26 - 1)];
	}

	$characters1 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters1);
	$randomString1 = '';
	for ($i = 0; $i < 6; $i++) {
	    $randomString1 .= $characters1[rand(0, 10 - 1)];
	}
	$id = $randomString.$randomString1;

	$get_mobile=Basic::getsinglerow(array("id"=>1),'wiix_site_settings');
				$otp=Basic::getsinglerow(array("id"=>1),'wiix_otpcode');
				
				$mbldecrypt=Helper::encrypt_decrypt("decrypt",$otp->wallet_code);
				$passcode = $id;
				$condition = array("admin_id"=>1);


			$adminkey = Basic::getsinglerow($condition,'wiix_admin');

				$maildecrypt=Helper::encrypt_decrypt("decrypt",$adminkey->secret_key);




Basic::updatedatas(array("id"=>1),array("otp_code"=>$passcode),'wiix_otpcode');

$msg=DB::table('wiix_email_templates')->where('mail_key','otp_code')->get();
			$to=$maildecrypt;



			$message=$msg[0]->message;
			$date=date('M d, Y');
			$message=str_replace("##USERNAME##","Admin",$message);
			$message=str_replace("##OTPCODE##",$passcode,$message);
			$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
			$message=str_replace("##SITENAME##",Helper::getSiteName(),$message);



			$email_template = 'OTP Code';
			Helper::mailsend($email_template,$message,$to);

			$text="Your OTP is :".$passcode;

			//$test=Helper::sendSMS($mbldecrypt,$text);
echo json_encode(array('status' => TRUE,'title' => 'OTP send to your mail id'));die;

}




			public function withhistory(Request $request)
			{
				if(Session::get('admin_id')=="")
					return redirect('WiPlytaIIX2/login');



				$where = array();
				$order= array('id','desc');
				$data["title"] = "Withdraw user list";

				$data["details"] = Basic::getmultiplerow($where,'wiix_withdraw',$order);


				return view('admin/history.withhistory',$data);

			}

			public function viewWithhistory(Request $request,$id)
			{
				if(Session::get('admin_id')=="")
					return redirect('WiPlytaIIX2/login');



				$result = '';


				$dep_det=Basic::getsinglerow(array("id"=>$id),'wiix_withdraw');

				$get_userid=$dep_det->user_id;


				$get_users=Basic::getsinglerow(array("user_id"=>$get_userid),'wiix_consumers');

				$email[0] = Helper::encrypt_decrypt("decrypt",$get_users->secret_key);
				$email[1] = Helper::encrypt_decrypt("decrypt",$get_users->display);
				$secure_email=$email[0]."@".$email[1];



				$dep_detailsbank = Basic::getsinglerow(array("id"=>$id),'wiix_withdraw');


				$bankval=$dep_detailsbank->wit_bankaccount;

				$data = explode("_",$bankval);

				$bankdetails=$data[1];


				$getbandet=Basic::getsinglerow(array("id"=>$bankdetails),'wiix_user_bank');


				$i=0;


				if($dep_det->wit_status == '0')
				{
					$sta=   '<span class="tx_orng2">Pending</span>';
				}
				else  if($dep_det->wit_status == '1')
				{
					$sta= '<span class="tx_orng2">Approved</span>';
				}
				else  if($dep_det->wit_status == '2')
				{
					$sta= '<span class="tx_orng2">User cancelled</span>';
				}
				else  if($dep_det->wit_status == '3')
				{
					$sta= '<span class="tx_orng2">waiting</span>';
				}
				else  if($dep_det->wit_status == '4')
				{
					$sta= '<span class="tx_orng2">Waiting for admin mail confirmation</span>';
				}
				else  if($dep_det->wit_status == '5')
				{
					$sta= '<span class="tx_orng2">Admin cancelled</span>';
				}
				else{
					$sta='';
				}

				if($dep_det->wit_currency == "KRW")
				{
					$transno=$dep_det->txn_id;
					$withamt=number_format($dep_det->wit_amount,2);
					$withfee=number_format($dep_det->wit_fee,2);
					$withfinal=number_format($dep_det->wil_get,2);
				}
				else{
					$transno=$dep_det->crypto_address;
					$withamt=number_format($dep_det->wit_amount,8);
					$withfee=number_format($dep_det->wit_fee,8);
					$withfinal=number_format($dep_det->wil_get,8);
				}


				
				$result = '<div class="row">
				<div class="col-xl-4">
				<h5>User bank details document</h5>
						<div class="mb-10">
							<b>User email: </b><br>
							'.$secure_email.'
						</div>	
					
						<div class="mb-10">
							<b>User bank name: </b><br>
							'.$getbandet->bank_name.'
						</div>
						<div class="mb-10">
							<b>Holder name: </b><br>
							'.$getbandet->holder_name.'
						</div>
						<div class="mb-10">
							<b>Account number: </b><br>
							'.$getbandet->account_number.'
						</div>
						<div class="mb-10">
							<b>Swift/IFSC/IBAN C<br>ode: </b>
							'.$getbandet->iban.'
						</div>

						<div class="mb-10">
							<b>Country: </b><br>
							'.$getbandet->country.'
						</div>
					</div>
					<div class="col-xl-8">
						<table class="table">
							<tr>
								<th scope="row">Transaction number/Crypto Address</th>
								<td>'.$transno.'</td>
							</tr> 
							<tr>
								<th scope="row">Withdraw amount</th>
								<td>'.$withamt.'</td>
							</tr>
							<tr>
								<th scope="row">Withdraw Currency</th>
								<td>'.$dep_det->wit_currency.'</td>
							</tr>

							<tr>
								<th scope="row">Withdraw fee</th>
								<td>'.$withfee.'</td>
							</tr>

							<tr>
								<th scope="row">Final amount</th>
								<td>'.$withfinal.'</td>
							</tr>
							<tr>
								<th scope="row">Withdraw status</th>
								<td>'.$sta.'</td>
							</tr>
							<tr>
								<th scope="row">Date and time</th>
								<td>'.$dep_det->with_created_date.'</td>
							</tr>




							';


							$result .= '</table></div>';

							if($dep_det->wit_status==0){
								$result .= '<div class="col-xl-12"><form id="dep_approval" action="/WiPlytaIIX2/withdrawApproval/" method="POST" novalidate="">


								<div class="form-group row"><label for="inputEmail3" class="col-sm-2 form-control-label">Comment</label><div class="col-sm-10"><textarea class="form-control" name="comment" id="comment" style="resize:none;" rows="3"></textarea></div></div> <a class="btn btn-pill-left btn-danger" href="javascript:withrejectotp('.$id.');">Reject</a> <a class="btn btn-pill-right btn-success" href="javascript:withacceptotp('.$id.');">Accept</a></form></div>';
							}	

							$result .= '</div>';

							$i++;

							echo $result;
						}

						public function withacceptotp(Request $request,$id)
			{
$urls=url('/WiPlytaIIX2/generate_transid');
				$result='<form method="POST" id="with_approval1"><div class="form-group">
                                            <label class="control-label">OTP</label>
                                            <input type="text" class="form-control boxed" placeholder="Enter OTP code" id="otp_accept" name="otp_accept"> </div><div class="form-group"><a href="javascript:sendOTP();" class="btn btn-oval btn-success">Send OTP</a></div>
                                            <div class="form-group"><a href="javascript:acceptdeposit('.$id.');" class="btn btn-oval btn-info">Approve</a></div></form>';
				echo $result;


			}

			public function withrejectotp(Request $request,$id)
			{

				$result='<form method="POST" id="dep_approval2"><div class="form-group">
                                            <label class="control-label">OTP</label>
                                            <input type="text" class="form-control boxed" placeholder="Enter OTP code" id="otp_accept" name="otp_accept"> </div><div class="form-group"><a href="javascript:sendOTP();" class="btn btn-oval btn-success">Send OTP</a></div><div class="form-group"><a href="javascript:rejectdeposit('.$id.');" class="btn btn-oval btn-danger">Reject</a></div></form>';
				echo $result;


			}


public function withdrawApproval(Request $request,$id)

{
							$get_mobile=Basic::getsinglerow(array("id"=>1),'wiix_site_settings');
				$otp=Basic::getsinglerow(array("id"=>1),'wiix_otpcode');
				
				$mbldecrypt=Helper::encrypt_decrypt("decrypt",$otp->wallet_code);
				$passcode = $otp->otp_code;
				$condition = array("admin_id"=>1);


			$adminkey = Basic::getsinglerow($condition,'wiix_admin');

				$maildecrypt=Helper::encrypt_decrypt("decrypt",$adminkey->secret_key);



			$otp_acceptcode=$request['otp_accept'];

			

			if($otp_acceptcode != ($passcode))
			{

				echo json_encode(array('status' => FALSE,'title' => 'Enter a valid OTP code'));die;
				
			}
			else{

				Basic::updatedatas(array("id"=>1,"wallet_code"=>$get_mobile->wallet_code),array("otp_code"=>'expire000000000000'),'wiix_otpcode');
$data = explode("-",$id);


								$data = explode("-",$id);


								switch($data[1]){
									case "accept":
									$exists=Basic::getsinglerow(array("id"=>$id),'wiix_withdraw');


									/*Crypto withdraw codeing starts*/
									$curss=$exists->wit_currency;
									$toadd=$exists->crypto_address;
									$cryptoamount=$exists->wil_get;

									if($curss == "BTC")
									{
										WithdrawCrypto::btc_withdraws($toadd,$cryptoamount);
									}
									else if($curss == "ETH")
									{
										WithdrawCrypto::eth_withdraws($toadd,$cryptoamount);	
									}
									else if($curss == "LTC")
									{
										WithdrawCrypto::ltc_withdraws($toadd,$cryptoamount);
									}
									else if($curss == "XRP")
									{
										WithdrawCrypto::xrp_withdraws($toadd,$cryptoamount);
									}
									else if($curss == "DOGE")
									{
										WithdrawCrypto::doge_withdraws($toadd,$cryptoamount);
									}
									else if($curss == "ETC")
									{
										WithdrawCrypto::etc_withdraws($toadd,$cryptoamount);	
									}
									else if($curss == "XMR")
									{
										WithdrawCrypto::xmr_withdraws($toadd,$cryptoamount);	
									}
									else if($curss == "IOTA")
									{

									}
									else if($curss == "BTG")
									{
										WithdrawCrypto::btg_withdraws($toadd,$cryptoamount);
									}
									else if($curss == "DASH")
									{
										WithdrawCrypto::dash_withdraws($toadd,$cryptoamount);
									}


									/*Crypto withdraw codeing ends*/



									
					/*Coin_profit*/


					$date	= date('Y-m-d');
					$time	= date("h:i:s");
					$coin_fee=$exists->wit_fee;
					$coin_currency=$exists->wit_currency;
					$coin_user_id=$exists->user_id;


					$datass = array(
						'userId'=>$coin_user_id,
						'theftAmount'=>$coin_fee,
						'theftCurrency'=>$coin_currency,
						
						'Type'=>"Withdraw",
						
					
						);

					Basic::insertdatas($datass,'wiix_coin_theft');
				


					/*Coin profit ends*/

									$update_values['wit_status'] = 1;

									Basic::updatedatas(array("id"=>$exists->id),$update_values,'wiix_withdraw');


									$adminemail=$maildecrypt;

									



									$msg=DB::table('wiix_email_templates')->where('mail_key','crypto_withdraw')->get();
									$to=$adminemail;


									$message=$msg[0]->message;
									$date=date('M d, Y');

									

									$message=str_replace("##NAME##","Admin",$message);
									$message=str_replace("##AMOUNT##",$exists->wit_amount,$message);
									$message=str_replace("##STATUS##","Pending",$message);
									
									$message=str_replace("##CURRENCY##",$exists->wit_currency,$message);
									$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
									$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);

									$email_template = 'Withdraw status';

									Helper::mailsend($email_template,$message,$to);


									$request->session()->flash('success','Withdraw request approved successfully');
									echo json_encode(array('status' => TRUE,'title' => 'Withdraw request approved successfully'));
									break;
									case "reject":
									$update_values['wit_status'] = 5;
									$update_values['reject_reason'] = $request['comment'];


									Basic::updatedatas(array("id"=>$id),$update_values,'wiix_withdraw');

									$exists=Basic::getsinglerow(array("id"=>$id),'wiix_withdraw');




									$balance = Basic::getsinglerow(array("user_id"=>$exists->user_id),'wiix_user_wallet');

									$curr = $exists->wit_currency;

									$update_bal = $balance->$curr + $exists->wit_amount;

									$is_update = Basic::updatedatas(array('user_id' => $exists->user_id),array($exists->wit_currency => $update_bal),'wiix_user_wallet');




						$getdepdet=Basic::getsinglerow(array("id"=>$id),'wiix_withdraw');
					

						$getuser_id=$getdepdet->user_id;

						$getuser_det=Basic::getsinglerow(array("user_id"=>$getuser_id),'wiix_consumers');



						/*To send mail*/

		 $email[0] = Helper::encrypt_decrypt("decrypt",$getuser_det->secret_key);
        $email[1] = Helper::encrypt_decrypt("decrypt",$getuser_det->display);
        $secure_email = $email[0]."@".$email[1];

						$msg=DB::table('wiix_email_templates')->where('mail_key','user_deprejection')->get();
						$to=$secure_email;


						$message=$msg[0]->message;
						$date=date('M d, Y');

						

						$message=str_replace("##NAME##",$getuser_det->device_token,$message);
						$message=str_replace("##TYPE##","Withdraw",$message);
						$message=str_replace("##COMMENT##",$request['comment'],$message);
						
						$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
						$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);

						$email_template = 'User Transactions status';

						Helper::mailsend($email_template,$message,$to);

						/*End of mail functionality*/



									$request->session()->flash('success','User withdraw request rejected successfully');
									echo json_encode(array('status' => TRUE,'title' => 'User deposit request rejected successfully'));
					
									break;
								}
							}
						}

						function adminverify_withdraw(Request $request,$id)
						{
							$decrid=Helper::encrypt_decrypt("decrypt",$id);


							$res = Basic::getsinglerow(array("id"=>$decrid),'wiix_withdraw');



							if($res){

								if($res->wit_status == '1'){
									$request->session()->flash('error','Amount already withdraw to this account');
									return redirect('/WiPlytaIIX2/withhistory');
								}else{

									$update_values['wit_status'] = 1;

									Basic::updatedatas(array("id"=>$decrid),$update_values,'wiix_withdraw');


									$request->session()->flash('success','Amount successfully withdraw to this account');
									return redirect('/WiPlytaIIX2/withhistory');
								}

							}
						}

					}