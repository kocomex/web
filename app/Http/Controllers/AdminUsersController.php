<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;

use App\Basic;


class AdminUsersController extends Controller
{
	public function users(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');	

		$where = array();
		$order= array('user_id','desc');
		
$data["users"] = DB::table('wiix_consumers')->leftjoin('wiix_user_profile','wiix_user_profile.user_id','=','wiix_consumers.user_id')->orderBy('id','desc')->get();


		$data["title"] = "Users Overview";
		return view('admin/users.listUsers',$data);

	}

	public function viewusers($user_id)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');

		if($user_id==0){

			$request->session()->flash('error','Invalid user');
			return redirect('/WiPlytaIIX2/users');

		}	


		
		$data["user"] = Basic::getsinglerow(array("user_id"=>$user_id),'wiix_consumers');
		$data["profile"] = Basic::getsinglerow(array("user_id"=>$user_id),'wiix_user_profile');


		$data["user_bank"] = DB::table('wiix_user_bank')->where('user_id',$user_id)->get();

		$data["user_bank1"]=$data["user_bank"][0];
		

		$data["wallet"] = Basic::getsinglerow(array("user_id"=>$user_id),'wiix_user_wallet');


		$data["title"] = "View User";
		$data["user_id"] = $user_id;
		
		return view('admin/users.viewUser',$data);
		
	}

	public function bankInfo(Request $request,$id)
	{
		$result = '';
		$bank_details = DB::table('wiix_user_bank')->where('id',$id)->get();

		
		if($bank_details){
			$i=0;
			foreach($bank_details as $bank){
				
				$result = '<div class="row" style="text-align:center;">
				
				
					<table class="table">
						<tr>
							<th scope="row">Bank Name</th>
							<td>'.$bank->bank_name.'</td>
						</tr>
						<tr>
							<th scope="row">Account Holder</th>
							<td>'.$bank->holder_name.'</td>
						</tr>
						<tr>
							<th scope="row">Account Number</th>
							<td>'.$bank->account_number.'</td>
						</tr>
						<tr>
							<th scope="row">IBAN</th>
							<td>'.$bank->iban.'</td>
						</tr>
						<tr>
							<th scope="row">Branch</th>
							<td>'.$bank->branch.'</td>
						</tr>
						<tr>
							<th scope="row">Country</th>
							<td>'.$bank->country.'</td>
						</tr>
						';
						
						
						$result .= '</table></div>';
						
						if($bank->status==0 || $bank->status==2 || $bank->status==1){
							$result .= '<div class="col-xl-12"><form id="bank_approval" action="{{ url("/WiPlytaIIX2/bankApproval") }}" method="POST" novalidate="">

							
							<div class="form-group row"><label for="inputEmail3" class="col-sm-2 form-control-label">Comment</label><div class="col-sm-10"><textarea class="form-control" name="comment" id="comment" style="resize:none;" rows="3" required></textarea></div></div> <a class="btn btn-pill-left btn-danger" href="javascript:rejectBank('.$id.');">Reject</a> <a class="btn btn-pill-right btn-success" href="javascript:acceptBank('.$id.');">Accept</a></form></div>';
						}	
						
						$result .= '</div>';
						
						$i++;
					}
					
				}
				echo $result;
				
			}
			public function bankApproval(Request $request,$id=''){
				if($id!=''){
					$data = explode("-",$id);
					switch($data[1]){
						case "accept":
						$condition = array("id" => $data[0]);
						$update_data = array("status"=>3);


						$is_update = Basic::updatedatas($condition, $update_data,'wiix_user_bank');
						if($is_update){



							$get_id=Basic::getsinglerow(array("id"=>$data[0]),'wiix_user_bank');
							$bank_userid = $get_id->user_id;



							$userdetails=Basic::getsinglerow(array("user_id"=>$bank_userid),'wiix_consumers');
							$user = $userdetails;


							$email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
							$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
							$secure_email=$email[0]."@".$email[1];



							$msg=DB::table('wiix_email_templates')->where('mail_key','bankverfy_success')->get();
							$to=$secure_email;


							$message=$msg[0]->message;
							$date=date('M d, Y');
							
							$message=str_replace("##STATUS##","Verified",$message);
							$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
							$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
							
							$email_template = 'Bank verification status';
							
							Helper::mailsend($email_template,$message,$to);


							
							$request->session()->flash('success','Bank proof approved updated sucessfully');
						}else{
							$request->session()->flash('error','Bank proof status updation Failed');
						}	
						break;
						case "reject":
						
						$condition = array("id" => $data[0]);
						$update_data = array("status"=>2,"comment"=>$request['comment']);
						$is_update = Basic::updatedatas($condition, $update_data,'wiix_user_bank');
						if($is_update){

							$get_id=Basic::getsinglerow(array("id"=>$data[0]),'wiix_user_bank');
							$bank_userid = $get_id->user_id;



							$userdetails=Basic::getsinglerow(array("user_id"=>$bank_userid),'wiix_consumers');
							$user = $userdetails;



							$email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
							$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
							$secure_email=$email[0]."@".$email[1];



							$msg=DB::table('wiix_email_templates')->where('mail_key','bankverfy_reject')->get();
							$to=$secure_email;


							$message=$msg[0]->message;
							$date=date('M d, Y');
							
							$message=str_replace("##MESSAGE##",$request['comment'],$message);
							$message=str_replace("##STATUS##","Rejected",$message);
							$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
							$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
							
							$email_template = 'Bank verification status';
							
							Helper::mailsend($email_template,$message,$to);

							

							$request->session()->flash('success','Bank proof status rejected sucessfully');
						}else{
							$request->session()->flash('error','Bank proof rejected updation Failed');
						}
						break;
					}
				}else{
					$request->session()->flash('error','Internal Error Occured try after sometime');
				}
			}


			public function proofApproval(Request $request,$id=''){
				if($id!=''){
					
					$panstatus=$request['pan_status'];
					$addressaddr=$request['address_prof_status'];
					$pancmnt=$request['pan_comment'];
					$addcmnt=$request['address_prof_comment'];
					$condition = array("user_id" => $id);




					$userdetails=Basic::getsinglerow(array("user_id"=>$id),'wiix_user_profile');

					

					if(($panstatus == 0) && ($userdetails->photo_id_status !=1))
					{
						$p_sta=2;
					}
					else{
						$p_sta=1;
					}
					if(($addressaddr == 0) && ($userdetails->address_prof_status !=1))
					{
						$a_sta=2;

					}
					else{
						$a_sta=1;
					}

					$update_data = array("photo_id_comment"=>$request['pan_comment'],"photo_id_status"=>$p_sta,"address_prof_status"=>$a_sta,"address_prof_comment"=>$request['address_prof_comment']);

					if($panstatus == 1)
					{

						$userdetails=Basic::getsinglerow(array("user_id"=>$id),'wiix_consumers');
						$user = $userdetails;
						$email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
						$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
						$secure_email=$email[0]."@".$email[1];


						$msg=DB::table('wiix_email_templates')->where('mail_key','verfy_success')->get();
						$to=$secure_email;


						$message=$msg[0]->message;
						$date=date('M d, Y');
						
						
						$message=str_replace("##STATUS##","Photo Id Verified",$message);
						$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
						$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
						
						$email_template = '	Document verification status';
						
						Helper::mailsend($email_template,$message,$to);


					}
					else
					{
						$userdetails=Basic::getsinglerow(array("user_id"=>$id),'wiix_consumers');
						$user = $userdetails;
						$email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
						$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
						$secure_email=$email[0]."@".$email[1];




						$msg=DB::table('wiix_email_templates')->where('mail_key','verfy_reject')->get();
						$to=$secure_email;


						$message=$msg[0]->message;
						$date=date('M d, Y');
						
						$message=str_replace("##MESSAGE##",$pancmnt,$message);
						$message=str_replace("##STATUS##","Photo Id Rejected",$message);
						$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
						$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
						
						$email_template = 'Document verification status';
						
						Helper::mailsend($email_template,$message,$to);
					}

					/*start*/
					if($addressaddr == 1)
					{


						$userdetails=Basic::getsinglerow(array("user_id"=>$id),'wiix_consumers');
						$user = $userdetails;
						$email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
						$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
						$secure_email=$email[0]."@".$email[1];




						$msg=DB::table('wiix_email_templates')->where('mail_key','verfy_success')->get();
						$to=$secure_email;


						$message=$msg[0]->message;
						$date=date('M d, Y');
						
						$message=str_replace("##MESSAGE##",$pancmnt,$message);
						$message=str_replace("##STATUS##","Address proof Verified",$message);
						$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
						$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
						
						$email_template = 'Document verification status';
						
						Helper::mailsend($email_template,$message,$to);


					}
					else{


						$userdetails=Basic::getsinglerow(array("user_id"=>$id),'wiix_consumers');
						$user = $userdetails;
						$email[0] = Helper::encrypt_decrypt("decrypt",$user->secret_key);
						$email[1] = Helper::encrypt_decrypt("decrypt",$user->display);
						$secure_email=$email[0]."@".$email[1];




						$msg=DB::table('wiix_email_templates')->where('mail_key','verfy_reject')->get();
						$to=$secure_email;


						$message=$msg[0]->message;
						$date=date('M d, Y');
						
						$message=str_replace("##MESSAGE##",$pancmnt,$message);
						$message=str_replace("##STATUS##","Address proof Rejected",$message);
						$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
						$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
						
						$email_template = 'Document verification status';
						
						Helper::mailsend($email_template,$message,$to);




					}




					$is_update = Basic::updatedatas($condition, $update_data,'wiix_user_profile');
					if($is_update){
						$request->session()->flash('success',' proof status updated sucessfully');
						return redirect("/WiPlytaIIX2/viewusers/".$id);
					}else{
						$this->session->set_flashdata('error',' proof status updation Failed');
						return redirect("/WiPlytaIIX2/viewusers/".$id);
					}	
					
					
				}else{
					$request->session()->flash('error','Internal Error Occured try after sometime');
					return redirect("/WiPlytaIIX2/viewusers/".$id);
				}
			}

			public function user_sta($user_id=0,$status,Request $request)
			{
				$update_data = array(
							"status" => $status,
							
						);

				$condition = array("user_id" => $user_id);
				$is_update = Basic::updatedatas($condition, $update_data,'wiix_consumers');


			
			if($is_update){
				$request->session()->flash('success','User status updated sucessfully');
				return redirect('/WiPlytaIIX2/users/');
			}else{
				$request->session()->flash('error','Error in Updating User status ');
				return redirect('WiPlytaIIX2/users/');
			}
			}


			public function tfa_status($user_id,$status,Request $request)
				{
		//print_r($user_id);exit;

		if($status == 0)
      {
        $randcode = 'disable';
      }
      else if($status == 1)
      {
        $randcode = 'enable';
      }

			$update_data = array(
							"randcode" => $randcode,
							
						);

			$condition = array("user_id" => $user_id);
				$is_update = Basic::updatedatas($condition, $update_data,'wiix_consumers');


			if($is_update){
				$request->session()->flash('success_message','TFA status updated sucessfully');
				return redirect('WiPlytaIIX2/users/');
			}else{
				$request->session()->flash('error_message','Error in Updating TFA status ');
				return redirect('WiPlytaIIX2/users/');
			}
				}


			public function mobile_status($user_id,$status,Request $request)
			{
				$update_data = array(
					"mobile_verify" => 0,
				);

				$condition = array("user_id" => $user_id);
				$is_update = Basic::updatedatas($condition, $update_data,'wiix_consumers');


				if($is_update){
				$request->session()->flash('success_message','Mobile status updated sucessfully');
				return redirect('WiPlytaIIX2/users/');
				}else{
				$request->session()->flash('error_message','Error in Updating Mobile status ');
				return redirect('WiPlytaIIX2/users/');
				}
			}
		}