<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageHandler;
use App\Basic;
use App\profile;
use App\register;
use App\wallet;
use App\bank_details;

use Googleauthenticator;

use jsonRPCClient;
use App\Http\Controllers\moneroController;

class AuthController extends Controller
{

	

	public function register(Request $request)
	{

		if(Session::get('user_id')!="")
		{
			return redirect('/profile');
		}
		if($request->isMethod('post'))
		{
			$email  = $request['email'];

			$email1 = explode('@',$email);
			$firstemail  = Helper::encrypt_decrypt('encrypt',$email1[0]);
			$secondemail = Helper::encrypt_decrypt('encrypt',$email1[1]);


			$count=DB::table('wiix_consumers')->where('secret_key',$firstemail)->where('display',$secondemail)->count();

			if($count > 0)
			{
				$request->session()->flash('error','The Email you given was already registered');

				echo json_encode(array('status' => FALSE,'title' => 'The Email you given was already registered'));
			}



			$email    = $request['email'];


			$email_part = explode('@',$email);
			
			$first_part_email = Helper::encrypt_decrypt('encrypt',$email_part[0]);
			$second_part_email = Helper::encrypt_decrypt('encrypt',$email_part[1]);

			$password = Helper::encrypt_decrypt('encrypt',$request['pwd']);
			$username = $request['username'];

			$activation_code = Helper::encrypt_decrypt('encrypt',$request['email']);
			$BTC_address = ""; 
			$ETH_address = "";
			$LTC_address = ""; 
			$XRP_address = "";
			$DOGE_address = ""; 
			$ETC_address = "";
			$XMR_address = ""; 
			$IOTA_address = "";
			$BTG_address = "";
			$DASH_address = ""; 
			
			

			$email = $request['email'];


			$additional_data = [
			'device_token' => $username,

			'secret_key' => $first_part_email,
			'display' => $second_part_email,

			'activation_code'=> $activation_code,
			'status' => 0,

			'device_type' => 'Web',


			'wallet_key'=>$password,

			'ip_address'=>$_SERVER['REMOTE_ADDR'],


			];



			$res=register::insert($additional_data);

			$actuserid = DB::table('wiix_user_wallet')->max('user_id') + 1;


			/* Insert basic details*/

			$wallet=DB::table('wiix_user_wallet')->where('user_id',$actuserid)->count();


			if($wallet == 0)
			{
				$wallet_insert = ['user_id'=>$actuserid,'BTC'=>0,'ETH'=>0,'LTC'=>0,'XRP'=>0,'DOGE'=>0,'ETC'=>0,'XMR'=>0,'IOTA'=>0,'BTG'=>0,'DASH'=>0,'KRW'=>0];
				wallet::insert($wallet_insert);

			}

			$profile=DB::table('wiix_user_profile')->where('user_id',$actuserid)->count();

			if($profile == 0)
			{
				profile::insert(['user_id'=>$actuserid,'photo_id_status' => '0','address_prof_status' => '0', 'log_notify' => 0,'buy_sell_notify' => 0,'deposit_withdraw_notify' => 0,]);
			}

			$bank_det=DB::table('wiix_user_bank')->where('user_id',$actuserid)->count();

			if($bank_det == 0)
			{

				$ex_bank = ['user_id'=>$actuserid,'status'=>'0'];
				bank_details::insert($ex_bank);

			}



			DB::table('wiix_notification')->insert(['user_id'=> $actuserid,'id'=> $actuserid,'loginalert'=> 0,'trade'=> 0,'transaction'=> 0,'verification'=> 0]);

			/* End Insert basic details*/


			if($res)
			{



				$msg=DB::table('wiix_email_templates')->where('mail_key','register')->get();



				$email    = $request['email'];
				$linking=url('/activation_verify').'/'.$activation_code;

				$message=$msg[0]->message;
				$date=date('M d, Y');
				$message=str_replace("##USERNAME##",$username,$message);
				$message=str_replace("##CLIENTID##",$email,$message);
				$message=str_replace("##LINK##",$linking,$message);

				$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
				$message=str_replace("##SITENAME##",Helper::getSiteName(),$message);







				$email_template = 'Confirmation of Registration';

				$mail_send= Helper::mailsend($email_template,$message,$email);




				$request->session()->flash('success','Registration completed successfully. please check your registered email for email verification');
				echo json_encode(array('status' => TRUE,'title' => 'success'));
			}
			else
			{
				$request->session()->flash('error','Please Try Again');

				echo json_encode(array('status' => FALSE,'title' => 'Please Try Again'));die;
			}


		}

		else{

			$where = array('page_name'=>'register');
			$det=Basic::getsinglerow($where,'wiix_cms');

			$data['settings']=HomeController::mainfunction();

			$data['title'] = $det->meta_title;
			$data['meta_keyword'] = $det->meta_keyword;
			$data['meta_description'] = $det->meta_description;


			return view('consumer.register',$data);

		}


	}

	function email_check(Request $request)
	{
		$email  = strtolower($request['email']);

		$email1 = explode('@',$email);
		$firstemail  = Helper::encrypt_decrypt('encrypt',$email1[0]);
		$secondemail = Helper::encrypt_decrypt('encrypt',$email1[1]);


		$count=DB::table('wiix_consumers')->where('secret_key',$firstemail)->where('display',$secondemail)->count();


		echo json_encode(($count > 0) ? FALSE : TRUE) ;



	}
	public function activation_verify(Request $request,$activation_code)
	{
		$decrypt_status = DB::table('wiix_consumers')->where('activation_code', $activation_code)->count();
//print_r($activation_code);exit;
		if($decrypt_status == 0)
		{
			$request->session()->flash('error', "Invalid activation code");
			return redirect('/');
		}

		$uid =  Helper::encrypt_decrypt('decrypt',$activation_code);



		$status1 = DB::table('wiix_consumers')->where('activation_code', $activation_code)->get();
		$statuscount=DB::table('wiix_consumers')->where('activation_code', $activation_code)->count();


		if($statuscount > 0)
		{
		//print_r($status);die;

			$status=$status1[0];


			if($status->status == 0)
			{
				$users=DB::table('wiix_consumers')->where('activation_code',$activation_code)->update([
					'status'=>'1',

					]);


				$request->session()->flash('success','Your account activated successfully.Please login to continue');
			}
			else
			{
				$request->session()->flash('error', "Your Account activated already");
			}
		}

		return redirect('/');
	}

	public function login(Request $request){

		if(Session::get('user_id')!="")
		{
			return redirect('/profile');
		}
		else
		{
			if($request->isMethod('post'))
			{


				$email_id=$request['emailid'];

				$email_part = explode('@',$email_id);
				$firstemail = Helper::encrypt_decrypt('encrypt',$email_part[0]);
				$secondemail = Helper::encrypt_decrypt('encrypt',$email_part[1]);

				$password_id=Helper::encrypt_decrypt("encrypt",$request['password']);

				$getd=DB::table('wiix_consumers')->where('secret_key',$firstemail)->where('display',$secondemail)->where('wallet_key',$password_id)->get();

				$status=DB::table('wiix_consumers')->where('secret_key',$firstemail)->where('display',$secondemail)->get();
				$statuscount=DB::table('wiix_consumers')->where('secret_key',$firstemail)->where('display',$secondemail)->count();

				if($statuscount > 0)
				{

					$status_verify=$status[0];


					if($status_verify->status == "0")
					{
						echo json_encode(array('status' => FALSE,'title' => 'Please activate your account'));die();
					}

				}

 //print_r("hgfgai");exit;      
				if(count($getd)){

					$session_arr = array(
						'user_id' => $getd[0]->user_id,

						'consumer_name' => $getd[0]->device_token,

						);

					Session::put($session_arr);

					$userdetails1 = DB::table('wiix_consumers')->where('user_id',Session::get('user_id'))->get();
					$userdetails = $userdetails1[0];



					if($userdetails->randcode == 'enable')
					{


						if($request['onecode'] != '')
						{
							$valid = AuthController::checktfa($request['onecode']);
							if(!$valid)
							{
								echo json_encode(array('status' => FALSE,'title' => 'Enter a valid code'));die();
							}
						}
						else
						{
							echo json_encode(array('status' => TRUE,'title' => 'tfa'));die();
						}
					}

					/*Cryptocurrency address creation*/

					/*BTC*/

					$where = array('user_id'=>Session::get('user_id'));
					$userdetails=Basic::getsinglerow($where,'wiix_consumers');


					if(trim($userdetails->BTC_address) == '' || trim($userdetails->BTC_address) == NULL){


						$BTC_address = LiveCryptoController::create_bitcoin_address();

					}
					if(trim($userdetails->ETH_address) == '' || trim($userdetails->ETH_address) == NULL){

						$ETH_address = LiveCryptoController::create_ethereum_address();

					}
					if(trim($userdetails->DOGE_address) == '' || trim($userdetails->DOGE_address) == NULL){
						$DOGE_address = LiveCryptoController::create_doge_address();

					}
					if(trim($userdetails->LTC_address) == '' || trim($userdetails->LTC_address) == NULL){
						$LTC_address = LiveCryptoController::create_litecoin_address();

					}

					if(trim($userdetails->DASH_address) == '' || trim($userdetails->DASH_address) == NULL){
						$DASH_address = LiveCryptoController::create_dash_address();

					}

					if(trim($userdetails->ETC_address) == '' || trim($userdetails->ETC_address) == NULL){
						$ETC_address = LiveCryptoController::create_etc_address();

					}

					if(trim($userdetails->XMR_address) == '' || trim($userdetails->XMR_address) == NULL){
						$XMR_address = LiveCryptoController::create_xmr_address();

					}
					if(trim($userdetails->BTG_address) == '' || trim($userdetails->BTG_address) == NULL){
						$BTG_address = LiveCryptoController::create_btg_address();

					}
					if(trim($userdetails->XRP_address) == '' || trim($userdetails->XRP_address) == NULL){
						$XRP_address = LiveCryptoController::create_ripple_address();

					}
					/*End Address creation*/

					$ip_address = PageHandler::get_client_ip();
					$browser=Helper::getBrowser();

					DB::table('wiix_log_history')->insert(['user_id'=>Session::get('user_id'),'ip'=>$ip_address,'browser'=>$browser,'type'=>"Login"]);


					$wherepro = array('user_id'=>Session::get('user_id'));
					$noti= Basic::getsinglerow($wherepro,'wiix_notification');



					if($noti->loginalert == 1)
					{
						/*Email send*/

						$msg=DB::table('wiix_email_templates')->where('mail_key','login_alert')->get();



						$email    = $request['emailid'];


						$message=$msg[0]->message;
						$date=date('M d, Y');
						$message=str_replace("##USERNAME##",$userdetails->device_token,$message);
						$message=str_replace("##BROWSER##",$browser,$message);
						$message=str_replace("##IPADDRESS##",$ip_address,$message);
						$message=str_replace("##DATE##",$userdetails->date,$message);
						$message=str_replace("##SITELOGO##",Helper::getSiteLogo(),$message);
						$message=str_replace("##SITENAME##",Helper::getSiteName(),$message);




						$email_template = 'Login Alert';

						$mail_send= Helper::mailsend($email_template,$message,$email);

						/*End*/
					}

					$request->session()->flash('success', 'Welcome back,'.' '.Session::get('consumer_name').' '.'you are logged in successfully!!!');  

					echo json_encode(array('status' => TRUE,'title' => 'sucess'));

				}
				else{

					echo json_encode(array('status' => FALSE,'title' => 'Invalid Email or password'));




				}

			}
			else{

			}


		}
	}


	public static function logout(Request $request)
	{


		$request->session()->flush();
		$request->session()->flash('success', 'You are logged out successfully');
		return redirect('/');
	}

	public static function checktfa($code)
	{

		$ga     = new Googleauthenticator();
		$email_user  =  Session::get('user_id');



		$userdetails1 = DB::table('wiix_consumers')->where('user_id',Session::get('user_id'))->get();
		$result = $userdetails1[0];

        //print_r($result);exit;

		$code   = $code;
		if(count($result)){
			$secret = $result->secret;
			$oneCode = $ga->verifyCode($secret,$code,$discrepancy = 4);
			if($oneCode==1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}else
		return false;
	}

	public function forgetpwd(Request $request){

		if($request->isMethod('post'))
		{


			$for_email=$request['for_email'];
			$email_part = explode('@',$for_email);
			$firstemail = Helper::encrypt_decrypt('encrypt',$email_part[0]);
			$secondemail = Helper::encrypt_decrypt('encrypt',$email_part[1]);





			$usercount=DB::table('wiix_consumers')->where('display',$secondemail)->where('secret_key',$firstemail)->count();

			$userstatus=DB::table('wiix_consumers')->where('display',$secondemail)->where('secret_key',$firstemail)->get();


			

			if($usercount > 0)
			{
				$user_det=$userstatus[0];

				if($user_det->status == "0")
				{
					$request->session()->flash('error', 'Oops! Your account is deactivated.Kindly contact site admin');

					return redirect('/');
				}

				$getd=DB::table('wiix_consumers')->where('display',$secondemail)->where('secret_key',$firstemail)->get();
				$get1=$getd[0];
				$activekey=$firstemail.$secondemail;


				$update=DB::table('wiix_consumers')->where('display',$secondemail)->where('secret_key',$firstemail)->update([
					'forgetkey'=>$activekey

					]);

				$linking=url('/resetpassword').'/'.$activekey;

				$msg=DB::table('wiix_email_templates')->where('mail_key','reset_pwd')->get();




				$message=$msg[0]->message;
				$date=date('M d, Y');
				$message=str_replace("##USERNAME##",$get1->device_token,$message);
				$message=str_replace("##LINK##",$linking,$message);


				$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
				$message=str_replace("##SITENAME##",Helper::getSiteName(),$message);



				$email_template = 'forget password';

				Helper::mailsend($email_template,$message,$request['for_email']);
				$request->session()->flash('success', 'Your password reset link sent successfully to your email address. Please check mail and reset password');
				return redirect('/');
			}
			else{
				$request->session()->flash('error', 'This Email address not registered. Please enter your account email address');
				return redirect('/forgetpwd');
			}
		}

		else{
			$data['settings']=HomeController::mainfunction();
			$data['title'] = "Home";
			$data['meta_keyword'] = "";
			$data['meta_description'] = "";
			return view('consumer/forgetpassword',$data);
		}

	}

	public function resetpassword(Request $request,$forgetcode){

		$usercount=DB::table('wiix_consumers')->where('forgetkey',$forgetcode)->count();

		if($usercount > 0)
		{

			if($request->isMethod('post'))
			{
				$val=DB::table('wiix_consumers')->where('forgetkey',$forgetcode)->get();
				$getdets=$val[0];

				$getpwd=Helper::encrypt_decrypt("decrypt",$getdets->wallet_key);
				$gettyprpwd=$request['res_pwd'];

				if($getpwd == $gettyprpwd)
				{
					$request->session()->flash('error', 'Old password and new password should not be same');
					return back(); 
				}

				$reset_pwd=$request['res_pwd'];

				$update=DB::table('wiix_consumers')->where('forgetkey',$forgetcode)->update([
					'wallet_key'=>Helper::encrypt_decrypt('encrypt',$request['res_pwd']),'forgetkey'=>""

					]);
				$request->session()->flash('success', 'Your reset password updated successfully.please login by using this password');
				return redirect('/');

			}



		}
		else{
			$request->session()->flash('error', 'Incorrect forget code or this password is changed to this account');
			return redirect('/');
		}
		$data['settings']=HomeController::mainfunction();
		$data['forgetcode']=$forgetcode;
		$data['title'] = "Home";
		$data['meta_keyword'] = "";
		$data['meta_description'] = "";
		return view('consumer/resetpassword',$data);
	}

	public function test_address(){

				// echo "<-----------BTC address--------->";
				// $bitcoin_row        =   Basic::getsinglerow(array('coinname'=>"bitcoin"),'wiix_cryptodetails');
				// $bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				// $bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				// $bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				// $bitcoin_ipaddress  = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


				// $bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
				// echo $createAddress = $bitcoin->getaccountaddress("info@wiix.io");echo "<br>";
				// echo $key     	    = '"'.Helper::encrypt_decrypt('encrypt',$createAddress).'"';echo "<br>";
				// echo $key     	    = '"'.Helper::encrypt_decrypt('decrypt',$key).'"';echo "<br>";

				// echo "<-----------LTC address--------->";
				// $bitcoin_row=Basic::getsinglerow(array('coinname'=>"litecoin"),'wiix_cryptodetails');
				// $bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				// $bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				// $bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				// $bitcoin_ipaddress  = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


				// $bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
				// echo $createAddress = $bitcoin->getaccountaddress("info@wiix.io");echo "<br>";
				// echo $key     	    = '"'.Helper::encrypt_decrypt('encrypt',$createAddress).'"';echo "<br>";
				// echo $key     	    = '"'.Helper::encrypt_decrypt('decrypt',$key).'"';echo "<br>";


				// echo "<-----------doge address--------->";
				// $bitcoin_row=Basic::getsinglerow(array('coinname'=>"dogecoin"),'wiix_cryptodetails');
				// $bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				// $bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				// $bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				// $bitcoin_ipaddress  = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


				// $bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
				// echo $createAddress = $bitcoin->getaccountaddress("info@wiix.io");echo "<br>";
				// echo $key     	    = '"'.Helper::encrypt_decrypt('encrypt',$createAddress).'"';echo "<br>";
				// echo $key     	    = '"'.Helper::encrypt_decrypt('decrypt',$key).'"';echo "<br>";


				// $bitcoin_row=Basic::getsinglerow(array('coinname'=>"bitcoingold"),'wiix_cryptodetails');
    //             $bitcoin_username   =   Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
    //             $bitcoin_password   =   Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
    //             $bitcoin_portnumber =   Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
    //             $bitcoin_ipaddress =    Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


              
    //             /*Curl request*/

    //             $url = "http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber";

    //             $createAddress = Helper::curl_request($url,"getaccountaddress","info@wiix.io");

    //             echo $key     	    = '"'.Helper::encrypt_decrypt('encrypt',$createAddress).'"';echo "<br>";
				// echo $key     	    = '"'.Helper::encrypt_decrypt('decrypt',$key).'"';echo "<br>";exit;


				// $condition = array("admin_id"=>1);
				// $adminkey = Basic::getsinglerow($condition,'wiix_admin');

				// echo $sendNumber=Helper::encrypt_decrypt("decrypt",$adminkey->secret_key);	exit;




				// echo "<-----------dash address--------->";
				// $bitcoin_row=Basic::getsinglerow(array('coinname'=>"dashcoin"),'wiix_cryptodetails');
				// $bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				// $bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				// $bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				// $bitcoin_ipaddress  = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 


				// $bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
				// echo $createAddress = $bitcoin->getaccountaddress("info@wiix.io");
				// echo $key     	    = '"'.Helper::encrypt_decrypt('encrypt',$createAddress).'"';echo "<br>";
				// echo $key     	    = '"'.Helper::encrypt_decrypt('decrypt',$key).'"';echo "<br>";
				// echo "br";exit;

				 	// $output=array(); 
      //               $result =     shell_exec('cd /var/www/html; /usr/bin/node ripple.js'); 
      //               $oparray = preg_split('/\s+/', trim($result)); 
      //               $res1 =  trim($oparray[2],',');
      //               $ripple_secret =  trim($res1,"'"); 
      //               echo $createAddress =  trim($oparray[4],"'");exit;
				// echo $_SERVER['SERVER_NAME'];exit;
					// $xmrcoin=moneroController::make_integrated_address();
					// if($xmrcoin->integrated_address!='') {
					// 	$checkAddress_xmr=$xmrcoin->integrated_address;
					// 	$payment_id =$xmrcoin->payment_id;
					// }

					// echo "address--->".$update_xmr = $checkAddress_xmr;echo "<br>";
					// echo "payment_id--->".$payment_id = $payment_id;echo "<br>";exit;
				// echo $createAddress = $bitcoin->getaccountaddress("testatm");echo "br";exit;
		// atm eth check 
		
		// for eth 
		//eth address 
				// $data = array('key'=>'edgfgdfghQa2FawWjeyjCqyBzypd');
				// $createAddress 	   = $this->connecteth('create',$data);
				// echo "eth";
				// echo $createAddress;exit;
		
		// echo "test eth";echo "<br>";
		// echo $ETH_address = LiveCryptoController::create_etc_address();exit;

		// block count 
				// $data = array('data'=>'count');
				// $createAddress 	   = $this->connecteth('blockcount',$data);
				// print_r($createAddress);
				// exit;

		// for etc 
		//etc address 
				// $data = array('key'=>'edgfgdfghQa2FawWjeyjCqyBzypd');
				// $createAddress 	   = $this->connecteth('create',$data);
				// echo $createAddress;exit;

		// block count 
				// $data = array('data'=>'count');
				// $createAddress 	   = $this->connectetc('blockcount',$data);
				// print_r($createAddress);
				// exit;

				// $data = array('key'=>'edgfgdfghQa2FawWjeyjCqyBzypd');
				// $createAddress 	   = $this->connectetc('create',$data);
				// // echo "etc";
				// print_r($createAddress);
				// exit;

				// $getaddeth=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
				// $eth_addr = $getaddeth->ETH_admin_pwd;
				// echo Helper::encrypt_decrypt('decrypt',$eth_addr);echo "<br>";exit;

				// echo Helper::encrypt_decrypt('encrypt',"waitimxaettmc");echo "<br>";
				// echo Helper::encrypt_decrypt('decrypt',"WGlQbHRLa1ZEeUFHWkdTZ1ltb09zZz09");echo "<br>";exit;
				// $getaddeth   	 = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
				// echo $eth_addr    	 = Helper::encrypt_decrypt('decrypt',trim($getaddeth->ETH_keyword));echo "<br>";
				// echo $eth_addr    	 = Helper::encrypt_decrypt('decrypt',trim($getaddeth->ETC_keyword));echo "<br>";exit;


				// $getaddeth   	 = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
				// $eth_addr    	 = trim($getaddeth->ETH_address);
				// $address         = '"'.Helper::encrypt_decrypt('decrypt',$eth_addr).'"';
				// $eth_user_key    = trim($getaddeth->ETC_user_key);
				// $key     	     = '"'.Helper::encrypt_decrypt('decrypt',$eth_user_key).'"';

				// $data 		     = array('key'=>$key,'adminaddress'=>$address);
				// $output 	     = Helper::connectetc('toadminwallet',$data);
				// echo "<pre>";print_r($output);exit;


				// $getaddeth=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
				// // $eth_addr = "0x14a12f46f4b86198f38fd619f246513b11085047";
				// $etc_addr = $getaddeth->ETC_address;
				// $address = '"'.Helper::encrypt_decrypt('decrypt',$etc_addr).'"';
				// $etc_eth_key = $getaddeth->ETC_admin_pwd;
				// $etc_eth_key = '"'.Helper::encrypt_decrypt('decrypt',$etc_eth_key).'"';
				// $to_address = 'toaddress';
				// $to 	 = '"'.trim($to_address).'"';
				// $amount  = 0.001; 
				// $key         = $etc_eth_key; 


				// $data = array('adminaddress'=>$address,'toaddress'=>$to,'amount'=>$amount,'key'=>$key);
    //             $createAddress     = Helper::connectetc('ethwithdrawjson',$data);
    //     		print_r($createAddress)	;
    //     		exit;


				
				// $getaddeth       = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
    //             $eth_user_key    = trim($getaddeth->ETH_user_key);
    //             $key             = Helper::encrypt_decrypt('decrypt',$eth_user_key);
    //             $data = array('key'=>$key);
    //             $createAddress     = Helper::connecteth('blockcount',$data);
    //             print_r($createAddress);exit;

				
				
				echo "test";exit;

				$xmrcoin=moneroController::make_integrated_address();
				if($xmrcoin->integrated_address!='') {
					$checkAddress_xmr=$xmrcoin->integrated_address;
					$payment_id =$xmrcoin->payment_id;
				}

				$update_xmr = $checkAddress_xmr;
				echo $key     	     = '"'.Helper::encrypt_decrypt('encrypt',$update_xmr).'"';echo "<br>";
				echo $key     	     = '"'.Helper::encrypt_decrypt('decrypt',$key).'"';echo "<br>";
				$payment_id = $payment_id;
				echo $key     	     = '"'.Helper::encrypt_decrypt('encrypt',$payment_id).'"';echo "<br>";
				echo $key     	     = '"'.Helper::encrypt_decrypt('decrypt',$key).'"';echo "<br>";exit;



				$getaddeth   	 	 = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
				echo $address        = '"'.Helper::encrypt_decrypt('decrypt',$getaddeth->ETC_address).'"';echo "<br>";
				echo $key     	     = '"'.Helper::encrypt_decrypt('decrypt',$getaddeth->ETC_pwd).'"';echo "<br>";
				echo $key     	     = '"'.Helper::encrypt_decrypt('decrypt',$getaddeth->ETC_admin_pwd).'"';echo "<br>";
				echo $key     	     = '"'.Helper::encrypt_decrypt('decrypt',$getaddeth->ETC_user_key).'"';echo "<br>";
				echo $key     	     = '"'.Helper::encrypt_decrypt('decrypt',$getaddeth->ETC_keyword).'"';echo "<br>";
				
				echo $key     	     = '"'.Helper::encrypt_decrypt('encrypt',"0xbceb835f292efff794e3727932adf6c9b53f68cf").'"';echo "<br>";
				echo $key     	     = '"'.Helper::encrypt_decrypt('decrypt',$key).'"';echo "<br>";


				// $data = array('key'=>'password');
    //             $createAddress     = Helper::connectetc('create',$data);
    //             $data = array('ETC_address' => $createAddress);
    //             print_r($data);
				exit;

				


				
	}
	 
}