<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Validator;
use Auth;
use Redirect;
use DB;
use URL;
use Session;
use Mail;
use Config;

use App\Model\Transaction;
use App\Model\CoinProfit;
use App\Model\SiteSettings;
use App\Model\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //To get client IP address
    public static function getIpAddress() {
        $remote = !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
        $ip = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $remote;
        return $ip;
    }

    //To get client browser
    public static function getBrowser() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browser = "Unknown Browser";
        $browser_array 	= array('/msie/i'       =>  'Internet Explorer',
                            '/firefox/i'    =>  'Firefox',
                            '/safari/i'     =>  'Safari',
                            '/chrome/i'     =>  'Chrome',
                            '/edge/i'       =>  'Edge',
                            '/opera/i'      =>  'Opera',
                            '/netscape/i'   =>  'Netscape',
                            '/maxthon/i'    =>  'Maxthon',
                            '/konqueror/i'  =>  'Konqueror',
                            '/mobile/i'     =>  'Handheld Browser');

        foreach ($browser_array as $regex => $value) { 
            if (preg_match($regex, $user_agent)) {
                $browser = $value;
            }
        }
        return $browser;
    }

    //To get client platform
    public static function getPlatform() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $platform = 'Unknown';
        if (preg_match('/linux/i', $user_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $user_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $user_agent)) {
            $platform = 'windows';
        }
        return $platform;
    }

    //deposit withdraw chart function
    public static function userTransactionDetails($type,$start,$end,$currency) {
        $chart = Transaction::select(DB::raw('SUM(amount) as amount'))
                        ->whereDate('created_at', '>=', $start)
                        ->whereDate('created_at', '<=', $end)
                        ->where('type',$type)->where('t_status','completed')
                        ->where('currency_name',$currency)->first();
        if($chart->count() == 0) {
          return "0";
        } else {
            if($chart->amount != "") {
                return $chart->amount;
            } else {
                return "0";
            }
        }
    }

    //profit chart function
    public static function getProfitDetails($start,$end,$currency) {
        $query = CoinProfit::select(DB::raw('SUM(theftAmount) as Amount'))
                ->where('theftCurrency',$currency)
                ->whereDate('created_at', '>=', $start)
                ->whereDate('created_at', '<=', $end)->first();
        if($query->count() == 0) {
          return "0";
        } else {
            if($query->Amount != "") {
                return $query->Amount;
            } else {
                return "0";
            }
        }
    }

    //To get basic email template details to replace
    public static function getEmailTemplateDetails() {
    	$site  = SiteSettings::select('admin_content','site_name','site_logo','contact_number','contact_address','city')->where('id',1)->first();
    	$replace = array('###img###'=>$site->site_logo,'###SITENAME###'=>$site->site_name,'###address###'=>$site->contact_address,'###city###'=>$site->city,'###phone###'=>$site->contact_number,'contact_mail_id'=>$site->admin_content,'site_name'=>$site->site_name);
    	return $replace;
    }

    //To send email to the given email address
    public static function sendEmail($emaildata, $toDetails) {
        $getFile = file_get_contents(app_path('Model/notebook.php'));
        $data = explode(" || ", $getFile);

        Config::set('mail.host', User::endecryption(2,$data[0]));
        Config::set('mail.port', User::endecryption(2,$data[1]));
        Config::set('mail.username', User::endecryption(2,$data[2]));
        Config::set('mail.password', User::endecryption(2,$data[3]));

        if($toDetails['from'] == $toDetails['useremail']) {
            $toDetails['useremail'] = User::endecryption(2,$toDetails['useremail']);
        }

        $toDetails['from'] = User::endecryption(2,$toDetails['from']);

        $sendEmail = Mail::send('email.template', $emaildata, function($message) use ($toDetails) {
            $message->to($toDetails['useremail']);
            $message->subject($toDetails['subject']);
            $message->from($toDetails['from'],$toDetails['name']);
        });
        return $sendEmail;
    }

    //get url contents using curl
    public static function getContents($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $html = curl_exec($ch);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
