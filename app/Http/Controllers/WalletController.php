<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Googleauthenticator;
// use App\jsonRPCClient;
use jsonRPCClient;

use Validator;
use Auth;
use Redirect;
use DB;
use URL;
use Session;


use App\CoinProfit;
use App\AdminWallet;
use App\wallet;
use App\BlockIP;
use App\LoginAttempt;
use App\AdminActivity;
use App\ContactUs;

use App\SiteWallet;
use App\CoinAddress;
use App\Trade;
use App\Deposit;
use App\Transaction;
use App\Basic;
use Helper;
use App\Http\Controllers\WithdrawCrypto;

class WalletController extends Controller
{
    public function __construct() {
		
	}

	public function index() {
		if(session('walletId')!='') {
			$data['user_btc'] = Wallet::select(DB::raw('SUM(BTC) as BTC'))->first();
			$data['user_eth'] = Wallet::select(DB::raw('SUM(ETH) as ETH'))->first();
			
			// btc balance 
				$bitcoin_row=Basic::getsinglerow(array('coinname'=>"bitcoin"),'wiix_cryptodetails');
				$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 

				$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber/");
				$getwalletinfo    = $bitcoin->getwalletinfo();
				$data['btc_balance'] = $getwalletinfo['balance'];

			// btg balance 
				$bitcoin_row          = Basic::getsinglerow(array('coinname'=>"bitcoingold"),'wiix_cryptodetails');
				$bitcoin_username	  =	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				$bitcoin_password 	  =	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				$bitcoin_portnumber   = Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				$bitcoin_ipaddress    = Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 

				$bitcoin 			  = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber/");
				$getwalletinfo        = $bitcoin->getwalletinfo();
				$data['btg_balance']  = $getwalletinfo['balance'];
				
			//ltc balance
				$bitcoin_row          = Basic::getsinglerow(array('coinname'=>"litecoin"),'wiix_cryptodetails');
				$bitcoin_username	  =	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				$bitcoin_password	  =	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				$bitcoin_portnumber   = Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				$bitcoin_ipaddress    = Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 
				
				$bitcoin 			  = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
				$getwalletinfo        = $bitcoin->getwalletinfo();
				$data['ltc_balance']  = $getwalletinfo['balance'];
				
			//doge balance 
				$bitcoin_row          =   Basic::getsinglerow(array('coinname'=>"dogecoin"),'wiix_cryptodetails');
				$bitcoin_username	  =	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				$bitcoin_password	  =	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				$bitcoin_portnumber   = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				$bitcoin_ipaddress    = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 
				
				$bitcoin 			  = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
				$getwalletinfo        = $bitcoin->getwalletinfo();
				$data['doge_balance'] = $getwalletinfo['balance'];
				
			//dash balance
				$bitcoin_row=Basic::getsinglerow(array('coinname'=>"dashcoin"),'wiix_cryptodetails');
				$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
				$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
				$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
				$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 
				
				$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
				$getwalletinfo        = $bitcoin->getwalletinfo();
				$data['dash_balance'] = $getwalletinfo['balance'];
				
			// // btg balance 
			// 	$bitcoin_row=Basic::getsinglerow(array('coinname'=>"bitcoingold"),'wiix_cryptodetails');
			// 	$bitcoin_username	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
			// 	$bitcoin_password	=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
			// 	$bitcoin_portnumber = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
			// 	$bitcoin_ipaddress = 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress); 
			// 	$bitcoin 			= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber/");
			// 	$getwalletinfo    = $bitcoin->getwalletinfo();
			// 	$data['btg_balance'] = $getwalletinfo['balance'];
			
			// echo "<pre>";print_r($data);exit;
			//return view('wallet.dashboard')->with('data',$data);
			
			return view('wallet.dashboard',$data);
		}
		return view('wallet.login');
	}

	//send otp to login
	public function sendOtpToLogin(Request $request) {
		$keyCode = Helper::generateRandomString(8);
		$encryptKey = Helper::encrypt_decrypt("encrypt",$keyCode);
		$update = AdminWallet::where('id',2)->update(['user_key' => $encryptKey]);
		if($update) {
			$getNumber = AdminWallet::where('id',2)->select('username')->first();
			$sendNumber = Helper::encrypt_decrypt("decrypt",$getNumber->username);
			// $sendNumber = '+821057452991';

			$text = 'Your OTP Code is '.$keyCode;
			$sendSMS = Helper::sendSMS($sendNumber,$text);
			
			$result = array('status'=>'1','key_code'=>$keyCode);
		} else {
			$result = array('status'=>'0');
		}
		echo json_encode($result);
	}

	public function adminLogin() {
		$data = Input::all();

	  	$Validation = Validator::make($data, wallet::$adminLoginRule);
	  	if($Validation->fails()) {
	      Session::flash('error', $Validation->messages());
	      return Redirect::to('Wi72IX/billetera');
	  	}
	  	if($data == array_filter($data)) {
	  		$email = explode('@',strip_tags($data['username']));
	  		$first = Helper::encrypt_decrypt("encrypt",$email[0]);
	  		$second = Helper::encrypt_decrypt("encrypt",$email[1]);
	  		$password = Helper::encrypt_decrypt("encrypt",strip_tags($data['user_pwd']));
	  		$pattern = Helper::encrypt_decrypt("encrypt",strip_tags($data['pattern_code']));
	  		$keyCode = Helper::encrypt_decrypt("encrypt",strip_tags($data['key_code']));


			$ip = Controller::getIpAddress();
			$browser = Controller::getBrowser();
			$platform = Controller::getPlatform();

	  		$login = AdminWallet::where('address', $first)
	  		 		->where('application', $second) 
	  		 		->where('sub_key', $password) 
	  		 		->where('access', $pattern)
	  		 		->where('user_key',$keyCode)->first();

	  		if($login) {
		        LoginAttempt::where('ip_address',$ip)->where('status','new')->update(['status' => 'olds']);

		        $activity['ip_address'] = $ip;
		        $activity['browser_name'] = $browser;
		        $activity['os'] = $platform;
		        $activity['activity'] = "login";
		        $activity['admin_id'] = $login['id'];
		        AdminActivity::create($activity);

		        session(['walletId' => $login['id'], 'walletName' => Helper::encrypt_decrypt("decrypt",$login['name'])]);
		        Session::flash('success', 'Login Success');
		        return Redirect::to('Wi72IX/billetera');
	  		} else {
		        $getCount = LoginAttempt::where('ip_address',$ip)->where('status','news')->count();
		        if($getCount >= 2) {
		          $getBlockCount = BlockIP::where('ip_addr',$ip)->count();
		          if($getBlockCount == 0) {
		            $updata = array('ip_addr'=>$ip,'status'=>'active');
		            BlockIP::create($updata);
		          } else {
		            BlockIP::where('ip_addr',$ip)->update(['status' => 'active']);
		          }

		      
		        }
		        $createAttempt = array('ip_address' =>$ip,'os' =>$platform,'browser' =>$browser,'status'=>'news','username'=>strip_tags($data['username']),'password'=>$data['user_pwd']);
		        LoginAttempt::create($createAttempt);

	  			Session::flash('error', 'Invalid login credentials!');
	  			return Redirect::to('Wi72IX/billetera');
	  		}
	  	} else {
	      Session::flash('error', 'Please fill all fields!');
	      return Redirect::to('Wi72IX/billetera');
	    }
	}

	public function logout() {
		if(session('walletId')!='') {
			$ip = Controller::getIpAddress(); 
			$activity['ip_address'] = $ip;
			$activity['browser_name'] = Controller::getBrowser();
			$activity['os'] = Controller::getPlatform();
			$activity['activity'] = "logout";
			$activity['admin_id'] = session('walletId');
			AdminActivity::create($activity);

			Session::flush();
			Session::flash('error', 'Logged out!');
			return Redirect::to('Wi72IX/billetera');
		} else {
			Session::flash('error', 'Session Expired!');
			return Redirect::to('Wi72IX/billetera');
		}
	}

	//Forgot password actions
	public function forgotPassword() {
		$data = Input::all();

		$Validation = Validator::make($data, AdminWallet::$forgotRule);
		if($Validation->fails()) {
		  Session::flash('error', $Validation->messages());
		  return Redirect::to('Wi72IX/billetera');
		}

		if($data == array_filter($data)) {
		  $useremail = strip_tags($data['useremail']);
		  $email = explode('@',$useremail);
		  $first = Helper::encrypt_decrypt("encrypt",$email[0]);
		  $second = Helper::encrypt_decrypt("encrypt",$email[1]);

		  $getDetail = AdminWallet::select('id','name')
		      ->where('address', $first)
		      ->where('application', $second)->first();
		  if($getDetail->count()) {
		    $id = Helper::encrypt_decrypt("encrypt",$getDetail->id);
		    $generateCode = Helper::generateRandomString(8);
		    $randomCode = Helper::encrypt_decrypt("encrypt",$generateCode);
		    $update = AdminWallet::where('id',$getDetail->id)->update(['code'=>$randomCode,'status'=>'active']);
		    $securl =URL::to('Wi72IX/billetera/resetPassword/'.$id.'/'.$randomCode);

		    /*Mail starts*/


		    $msg=DB::table('wiix_email_templates')->where('mail_key','admin_walletforget')->get();
							$to=$useremail;


							$message=$msg[0]->message;
							$date=date('M d, Y');
							
							$message=str_replace("##LINK1##",$securl,$message);
							
							$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
							$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
							
							$email_template = 'Forget password- Admin wallet';
							
							Helper::mailsend($email_template,$message,$to);



		    /*Mail ends*/

		   
		   $result_data = array('status'=>'1','msg'=>'Reset password Link has sent to Your Mail Id');
		  } else {
		    $result_data = array('status'=>'0','msg'=>'Invalid Email ID');
		  }
		}
		echo json_encode($result_data);
	}

	public function resetPassword($arg1,$arg2) {
		$id = Helper::encrypt_decrypt("decrypt",$arg1);
		$getDetail = AdminWallet::select('id','status')
		      ->where('id', $id)->first();
		if($getDetail->status == "active") {
		  $data['fcode'] = $arg2;
		  $data['ucode'] = $arg1;
		  return view('wallet.reset')->with('data',$data);
		} else {
		  Session::flash('error', 'URL expired');
		  return Redirect::to('Wi72IX/billetera');
		}
	}

	public function updatePassword() {
		$data = Input::all();

		$Validation = Validator::make($data, AdminWallet::$passwordRule);
		if($Validation->fails()) {
		  Session::flash('error', $Validation->messages());
		  return Redirect::to('Wi72IX/billetera');
		}

		if($data == array_filter($data)) {
		  $new_pwd = strip_tags($data['new_pwd']);
		  $cnfirm_pwd = strip_tags($data['cnfirm_pwd']);
		  $fcode = strip_tags($data['fcode']);
		  $userid = Helper::encrypt_decrypt("decrypt",strip_tags($data['ucode']));
		  if($new_pwd == $cnfirm_pwd) {
		    $getDetail = AdminWallet::select('id','status')
		      ->where('id', $userid)->first();
		    if($getDetail->status == "active") {
		      $password = Helper::encrypt_decrypt("encrypt",$cnfirm_pwd);
		      $update = AdminWallet::where('id',$userid)->update(['sub_key' => $password,'code'=>"", 'status'=>'deactive']);
		      if($update) {
		        Session::flash('success', 'Password Reset Success');
		        return Redirect::to('Wi72IX/billetera');
		      } else {
		        Session::flash('error', 'Please Try Again');
		      }
		    } else {
		      Session::flash('error', 'Reset Url Expired');
		    }
		  } else {
		    Session::flash('error', 'Password Must match');
		  }
		} else {
		  Session::flash('error', 'Fill All the Field');
		}
	}

	//return view for change password
	public function changePassword() {
		if(session('walletId')!='') {
			return view('wallet.changePassword');
		}
		Session::flash('error', 'Session expired!');
	  	return Redirect::to('Wi72IX/billetera');
	}

	//change password
	public function checkPassword() {
		if(session('walletId')!='') {
	     	$pwd = Helper::encrypt_decrypt("encrypt",$_GET['current_pwd']);
	     	$getCount = AdminWallet::where('id',session('walletId'))
	     			->where('sub_key',$pwd)->count();
	     	echo ($getCount == 1) ? "true" : "false";
		} else {
			echo "false";
		}
	}

	//update change password
	public function changeUpdatePassword() {
		if(session('walletId')!='') {
			$data = Input::all();
			$adminId = session('walletId');
			$Validation = Validator::make($data, AdminWallet::$pwdRule);
			if($Validation->fails()) {
				Session::flash('error', $Validation->messages());
				return Redirect::back();
			}
			if($data == array_filter($data)) {
				$new_pwd = strip_tags($data['new_pwd']);
				$confirm_pwd = strip_tags($data['confirm_pwd']);
				$pwd = Helper::encrypt_decrypt("encrypt",$data['current_pwd']);
	     	$getCount = AdminWallet::where('id',session('walletId'))
	     			->where('sub_key',$pwd)->count();
							
				if($getCount == 1) {
					if($new_pwd == $confirm_pwd) {
						$updateData = Helper::encrypt_decrypt("encrypt",$confirm_pwd);
						$result = AdminWallet::where('id',$adminId)->update(['sub_key' => $updateData]);
					}
				}
				if($result) {
					Session::flash('success', 'password Updated Successfully');
				} else {
					Session::flash('error', 'Failed to update.');
				}
				return Redirect::back();
			}
		}
		Session::flash('error', 'Session Expired');
		return Redirect::to('Wi72IX/billetera');
	}

	//return view for admin profit
	public function walletProfit() {
		if(session('walletId')!='') {
	        $data = CoinProfit::orderBy('theft_id','desc')->get();

	        $query = CoinProfit::select(DB::raw('SUM(theftAmount) as Amount'))
	                  ->first();
	        if($query->Amount != null) {
	          $profit['total_amount'] = ($query->count() == 0) ? "0.00" :$query->Amount;
	        } else {
	          $profit['total_amount'] = "0.00";
	        }
	        $profit['BTC_profit'] = Controller::adminProfit('BTC');
	        $profit['ETH_profit'] = Controller::adminProfit('ETH');
	        $profit['LTC_profit'] = Controller::adminProfit('LTC');
	        $profit['XRP_profit'] = Controller::adminProfit('XRP');
	        $profit['DOGE_profit'] = Controller::adminProfit('DOGE');
	        $profit['ETC_profit'] = Controller::adminProfit('ETC');
	        $profit['XMR_profit'] = Controller::adminProfit('XMR');
	        $profit['BTG_profit'] = Controller::adminProfit('BTG');
	        $profit['DASH_profit'] = Controller::adminProfit('DASH');
	        $profit['KRW_profit'] = Controller::adminProfit('KRW');
	   
	        
	        return view('wallet.theft')->with('result',$data)->with('profit',$profit);
	    }
	    Session::flash('error', 'Session Expired');
	    return Redirect::to('Wi72IX/billetera');
	}

	//return wallet deposit page
	public function walletDeposit() {
		if(session('walletId')!='') {
	        $getEmail = AdminWallet::where('id',session('walletId'))->select('address','application','username')->first();
	        $adminEmail = Helper::encrypt_decrypt("decrypt",$getEmail->address).'@'.Helper::encrypt_decrypt("decrypt",$getEmail->application);
	        
	        // address generation
	        	$getaddeth   	 	 = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
	        	$eth_address 		 = $getaddeth->ETH_address;	
	        	$etc_address 		 = $getaddeth->ETC_address;	
	        	$btc_address 		 = $getaddeth->BTC_address;	
	        	$ltc_address 		 = $getaddeth->LTC_address;	
	        	$xrp_address 		 = $getaddeth->XRP_address;	
	        	$doge_address 		 = $getaddeth->DOGE_address;	
	        	$xmr_address 		 = $getaddeth->XMR_address;	
	        	$dash_address 		 = $getaddeth->DASH_address;	
	        	$btg_address 		 = $getaddeth->BTG_address;	
	        	$iota_address 		 = $getaddeth->IOTA_address;	

	        	// eth address 
	        	$data['eth_address'] = Helper::encrypt_decrypt("decrypt",$eth_address);

	        	// etc address
	        	$data['etc_address'] = Helper::encrypt_decrypt("decrypt",$etc_address);

	        	// btc address
				$data['btc_address'] = Helper::encrypt_decrypt("decrypt",$btc_address);	        	

				// ltc address
				$data['ltc_address'] = Helper::encrypt_decrypt("decrypt",$ltc_address);	        	

				// // xrp address
				// // $data['xrp_address'] = Helper::encrypt_decrypt("decrypt",$xrp_address);	        	

				// // doge address
				// $data['doge_address'] = Helper::encrypt_decrypt("decrypt",$doge_address);	        	

				// // xmr address
				// $data['xmr_address'] = Helper::encrypt_decrypt("decrypt",$xmr_address);	        	

				// dash address
				$data['dash_address'] = Helper::encrypt_decrypt("decrypt",$dash_address);	        	

				// btg address
				$data['btg_address'] = Helper::encrypt_decrypt("decrypt",$btg_address);	        	

				// iota address
				// $data['iota_address'] = Helper::encrypt_decrypt("decrypt",$iota_address);	        	

	        	


	        return view('wallet.walletDeposit',$data);
	    }
	    Session::flash('error', 'Session Expired');
	    return Redirect::to('Wi72IX/billetera');
	}

	//send confirmation to withdraw
	public function sendWithdrawOtp() {
		$keyCode = Helper::generateRandomString();
		$encryptKey = Helper::encrypt_decrypt("encrypt",$keyCode);
		$update = AdminWallet::where('id',2)->update(['code' => $encryptKey,'status' => 'deactivate']);
		if($update) {
			


			$condition = array("admin_id"=>1);


			$adminkey = Basic::getsinglerow($condition,'wiix_admin');

				$sendNumber=Helper::encrypt_decrypt("decrypt",$adminkey->secret_key);
				$sendNumber="+821057452991";



			$text = 'Your OTP Code is '.$keyCode;
			$sendSMS = Helper::sendSMS($sendNumber,$text);

			$result = array('status'=>'1','key_code'=>$keyCode);
		} else {
			$result = array('status'=>'0');
		}
		echo json_encode($result);
	}

	//clear otp from db
	public function clearConfirmOtp() {
		$keyCode = User::generate_random_url(8);
		$encryptKey = Helper::encrypt_decrypt("encrypt",$keyCode);
		$update = AdminWallet::where('id',2)->update(['code' => $encryptKey,'status' => 'deactive']);
		if($update) {
			$result = array('status'=>'1');
		} else {
			$result = array('status'=>'0');
		}
		echo json_encode($result);
	}

	//return withdraw page
	public function walletWithdraw() {
		if(session('walletId')!='') {
	        return view('wallet.walletWithdraw');
	    }
	    Session::flash('error', 'Session Expired');
	    return Redirect::to('Wi72IX/billetera');
	}

	//check bitcoin address
	public function bitCoinAddress(Request $request) {
		if(session('walletId')!='') {
	        $data = $request->all();
	        $currency = $data['currency'];
	        if($currency == 'BTC') {
	        	$address = $data['address'];
	        	$origbase58 = $address;
    			$dec = "0";
    			for ($i = 0; $i < strlen($address); $i++) {
    				$dec = bcadd(bcmul($dec,"58",0),strpos("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz",substr($address,$i,1)),0);
    			}
    			$address = "";
    			while (bccomp($dec,0) == 1) {
    				$dv = bcdiv($dec,"16",0);
			        $rem = (integer)bcmod($dec,"16");
			        $dec = $dv;
			        $address = $address.substr("0123456789ABCDEF",$rem,1);
    			}
    			$address = strrev($address);
    			for ($i = 0; $i < strlen($origbase58) && substr($origbase58,$i,1) == "1"; $i++) {
    				$address = "00".$address;
    			}
    			if (strlen($address)%2 != 0) {
    				$address = "0".$address;
    			}
    			if (strlen($address) != 50) {
    				echo "false"; exit();
    			}
    			$final=substr(strtoupper(hash("sha256",hash("sha256",pack("H*",substr($address,0,strlen($address)-8)),true))),0,8) == substr($address,strlen($address)-8);
	        	echo ($final == 1) ? "true" : "false";
	        } else {
	        	echo "true";
	        }
	    } else {
	    	Session::flash('error', 'Session Expired');
	    	return Redirect::to('Wi72IX/billetera');
	    }
	}

	//withdraw admin wallet amount
	public function withdrawCryptoAmountWallet() {
		if(session('walletId')!='') {
	        $data = Input::all();
	        $Validation = Validator::make($data, AdminWallet::$withdrawRule);
	        if($Validation->fails()) {
	        	Session::flash('error', $Validation->messages());
				return Redirect::back();
	        }
	        $password = strip_tags($data['password']);
	        $otp = strip_tags($data['confirm_code']);
	        $encryptKey = Helper::encrypt_decrypt("encrypt",$password);
	        $encryptOtp = Helper::encrypt_decrypt("encrypt",$otp);

	        $checkAdmin = AdminWallet::where('id',2)->where('sub_key',$encryptKey)->where('code',$encryptOtp)->where('status','deactivate')->select('address', 'application')->first();

	        if($checkAdmin) {
	        	AdminWallet::where('id',2)->update(['code' => '','status' => 'deactive']);
	        	$useremail = Helper::encrypt_decrypt("decrypt",$checkAdmin->address).'@'.Helper::encrypt_decrypt("decrypt",$checkAdmin->application);

	        	$currency = strip_tags($data['currency']);
	        	if($currency == 'BTC') {
					$paymentMethod = "bitcoin";
				} else if($currency == 'ETH') {
					$paymentMethod = "ethereum";
				}
				else if($currency == 'LTC') {
					$paymentMethod = "litecoin";
				}
				else if($currency == 'XRP') {
					$paymentMethod = "ripple";
				} 

				else if($currency == 'DOGE') {
					$paymentMethod = "doge";
				}
				else if($currency == 'ETC') {
					$paymentMethod = "ethereum classic";
				}
				else if($currency == 'XMR') {
					$paymentMethod = "Monero";
				}
				else if($currency == 'BTG') {
					$paymentMethod = "bitcoin gold";
				}

				else if($currency == 'DASH') {
					$paymentMethod = "dash";
				}
				
		        $address = strip_tags($data['address']);
		        $amount = strip_tags($data['amount']);
		        $ip = Controller::getIpAddress();
		        $keyCode = Helper::generateRandomString();
				$encryptKey =Helper::encrypt_decrypt("encrypt",$keyCode);

		        $insdata = array('amount'=>$amount,'token_id'=>$encryptKey,'currency'=>$currency, 'payment_method' =>$paymentMethod, 'status'=>'pending','ip_address'=>$ip, 'to_address'=>$address, 'type'=>'withdraw');
				$createWithdraw = ContactUs::create($insdata);
				$txId = $createWithdraw->id;
				$transid = Helper::encrypt_decrypt("encrypt",$txId);

				$securl1 = URL::to('Wi72IX/billetera/confirmWithdraw/'.$transid.'/'.$keyCode);
				$securl2 = URL::to('Wi72IX/billetera/rejectWithdraw/'.$transid.'/'.$keyCode);
/*
				$getEmail = EmailTemplate::where('id',14)->first();
	  			$getSiteDetails  = SiteSettings::select('site_logo','site_name','contact_mail_id','contact_number','contact_address','city','country')->where('id',1)->first();
	  			$replace = array('###USER###'=>"Admin",'###LINK1###'=>$securl1,'###LINK2###'=>$securl2, '###AMT###' =>$amount." ".$currency,'###FEE###'=>"0 ".$currency, '###SITENAME###'=>$getSiteDetails->site_name, '###img###'=>$getSiteDetails->site_logo, '###address###'=>$getSiteDetails->contact_address, '###city###'=>$getSiteDetails->city, '###phone###'=>$getSiteDetails->contact_number);
	  			
	  			$emaildata = array('content' =>strtr($getEmail->template,$replace));
	  			$toDetails['useremail'] = $useremail;
	  			$toDetails['subject'] = $getEmail->subject;
	  			$toDetails['from'] = $getSiteDetails->contact_mail_id;
	  			$toDetails['name'] = $getSiteDetails->site_name;

	  			$sendEmail = Controller::sendEmail($emaildata, $toDetails);*/

			$msg=DB::table('wiix_email_templates')->where('mail_key','admin_wallet_withdraw')->get();
			$to=$useremail;
			$to="chockalingam@osiztech.com";
			$to="jongmink1992@gmail.com";
			


			$message=$msg[0]->message;
			$date=date('M d, Y');
			$message=str_replace("##AMT##",$amount,$message);
			$message=str_replace("##CURRENCY##",$currency,$message);
			$message=str_replace("##LINK1##",$securl1,$message);
			$message=str_replace("##LINK2##",$securl2,$message);
			$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
			$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);


			$email_template = 'Withdraw confirmation -Admin wallet';
			Helper::mailsend($email_template,$message,$to);

	  	
                	Session::flash('success', 'Confirmation sent to your registered email.');
              
              	return Redirect::back();
	        } else {
	        	Session::flash('error', 'Invalid OTP/ Password.');
	    		return Redirect::back();
	        }
	    }
	    Session::flash('error', 'Session Expired');
	    return Redirect::to('Wi72IX/billetera');
	}

	//confirm withdraw from email
	public function confirmCryptoWithdraw($tid,$token) {
		if(session('walletId')!='') {
	        $tid = Helper::encrypt_decrypt("decrypt",strip_tags($tid));
        	$token = Helper::encrypt_decrypt("encrypt",strip_tags($token));
        	$checkStatus = ContactUs::where('id', $tid)->where('token_id',$token)->select('status','currency','to_address','amount')->first();
        	if($checkStatus->status == "pending") {
        		$toAddress = $checkStatus->to_address;
        		$amount = $checkStatus->amount;
        		if($checkStatus->currency == "BTC") {
        			// $sendAmount = Controller::sendBtcFundToAddress($toAddress,$amount);
        			WithdrawCrypto::btc_withdraws($toAddress,$amount);
        		} elseif($checkStatus->currency == "ETH") {
        			// $fromAddr = User::getEthAddress(0);
        			// $sendAmount = Controller::sendEthFundToAddress($fromAddr, $amount, $toAddress);
        			echo 'ETH withdraw';exit;
        			WithdrawCrypto::eth_withdraws($toAddress,$amount);
        		} 
        		elseif($checkStatus->currency == "LTC") {
        			echo 'LTC withdraw';exit;
        			WithdrawCrypto::ltc_withdraws($toAddress,$amount);
        		}
        		elseif($checkStatus->currency == "XRP") {
        			$sendAmount =0;
        			echo 'XRP withdraw';exit;
        			WithdrawCrypto::xrp_withdraws($toAddress,$amount);

        		}
        		elseif($checkStatus->currency == "DOGE") {
        			$sendAmount =0;
        			echo 'DOGE withdraw';exit;
        			WithdrawCrypto::doge_withdraws($toAddress,$amount);

        		}
        		elseif($checkStatus->currency == "ETC") {
        			// $sendAmount =0;
        			echo 'ETC withdraw';exit;
        			WithdrawCrypto::etc_withdraws($toAddress,$amount);	
        		}
        		elseif($checkStatus->currency == "XMR") {
        			// $sendAmount =0;
        			echo 'XMR withdraw';exit;
        			WithdrawCrypto::xmr_withdraws($toAddress,$amount);	

        		}
        		elseif($checkStatus->currency == "BTG") {
        			echo 'BTG withdraw';exit;
        			WithdrawCrypto::btg_withdraws($toAddress,$amount);

        		}
        		elseif($checkStatus->currency == "DASH") {
        			echo 'DASH withdraw';exit;
        			WithdrawCrypto::dash_withdraws($toAddress,$amount);

        		}
        		$update = ContactUs::where('id', $tid)->update(['reference_no'=>$sendAmount,'status'=>'completed']);
        		Session::flash('success', 'Withdraw has been completed Successfully.');
        		return Redirect::to('Wi72IX/billetera/viewAdminWithdraw');
        	} else {
        		Session::flash('success', 'Transaction already been processed.');
        		return Redirect::to('Wi72IX/billetera/viewAdminWithdraw');
        	}
	    }
	    Session::flash('error', 'Session Expired');
	    return Redirect::to('Wi72IX/billetera');
	}

	//reject withdraw from email
	public function rejectCryptoWithdraw($tid,$token) {
		if(session('walletId')!='') {
	        $tid = Helper::encrypt_decrypt("decrypt",strip_tags($tid));
        	$token = Helper::encrypt_decrypt("encrypt",strip_tags($token));
        	$checkStatus = ContactUs::where('id', $tid)->where('token_id',$token)->select('status')->first();
        	if($checkStatus->status == "pending") {
        		ContactUs::where('id', $tid)->update(['status'=>'cancelled']);
        		Session::flash('success', 'Withdraw has been rejected Successfully.');
        		return Redirect::to('Wi72IX/billetera/viewAdminWithdraw');
        	} else {
        		Session::flash('success', 'Transaction already been processed.');
        		return Redirect::to('Wi72IX/billetera/viewAdminWithdraw');
        	}
	    }
	    Session::flash('error', 'Session Expired');
	    return Redirect::to('Wi72IX/billetera');
	}

	//wallet deposit history
	public function walletDepositHist() {
		if(session('walletId')!='') {
			$data = ContactUs::where('type','deposit')->orderBy('id','desc')->get();
	        return view('wallet.depoHistory')->with('deposit',$data);
	    }
	    Session::flash('error', 'Session Expired');
	    return Redirect::to('Wi72IX/billetera');
	}

	//wallet deposit history
	public function walletWithdrawHist() {
		if(session('walletId')!='') {
			$data = ContactUs::where('type','withdraw')->orderBy('id','desc')->get();
	        return view('wallet.withHistory')->with('withdraw',$data);
	    }
	    Session::flash('error', 'Session Expired');
	    return Redirect::to('Wi72IX/billetera');
	}

	//cron to save btc deposit
	public function bitCoinDepositProcess() {
		require_once app_path('jsonRPCClient.php');

        $getDetails = SiteWallet::where('id',1)->first();
        $bitcoin_username = Helper::encrypt_decrypt("decrypt",$getDetails->username);
        $bitcoin_password = Helper::encrypt_decrypt("decrypt",$getDetails->password);
        $bitcoin_portnumber = Helper::encrypt_decrypt("decrypt",$getDetails->portnumber);
        $bitcoin_host =Helper::encrypt_decrypt("decrypt",$getDetails->ip_address);

        $bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_host:$bitcoin_portnumber/");
        $bitcoin_isvalid = $bitcoin->listtransactions();

        if($bitcoin_isvalid) {
        	for($i=0;$i<count($bitcoin_isvalid);$i++) {
        		$account = $bitcoin_isvalid[$i]['account'];
        		$category = $bitcoin_isvalid[$i]['category'];
        		$btctxid = $bitcoin_isvalid[$i]['txid'];
        		if($category=="receive") {
        			$isvalid = $bitcoin->gettransaction($btctxid);
        			$det_category  =   $isvalid['details'][0]['category'];
        			if($det_category=="receive") {
        				$btcaccount = $isvalid['details'][0]['account'];
        				$btcaddress = $isvalid['details'][0]['address'];
		                $bitcoin_balance = $isvalid['details'][0]['amount']; 
		                $btcconfirmations = $isvalid['confirmations'];
        			} else {
        				$btcaccount = $isvalid['details'][1]['account'];
		                $btcaddress = $isvalid['details'][1]['address'];
		                $bitcoin_balance = $isvalid['details'][1]['amount']; 
		                $btcconfirmations = $isvalid['confirmations'];
        			}
        			$getUser = CoinAddress::where('address',$btcaddress)->where('currency','BTC')->select('user_id')->first();
        			$btcuserId = $getUser->user_id;
        			$dep_id = $btctxid;
        			$bitcoin_balance = $bitcoin_balance;

        			if( $btcconfirmations > 0) {
        				if($btcuserId != 0) {
	        				$dep_already = Controller::checkDeposit($btcuserId,$dep_id);
	        				if($dep_already == "0") {
	        					$ip = Controller::getIpAddress();
	        					$depositData = array('amount'=>$bitcoin_balance,'payment_method' =>'bitcoin','status'=>'pending','user_id'=>$btcuserId,'ip_addr'=>$ip,'currency'=>'BTC', 'reference_no'=>$dep_id,'block_confirm'=>$btcconfirmations,'address_info'=>$btcaddress);
								$createDeposit = Deposit::create($depositData);
	        				} else {
	        					if($btcconfirmations >= 8) {
	        						$checkConfirm = Deposit::where('user_id',$btcuserId)->where('reference_no',$dep_id)->where('status','pending')->count();
	        						if($checkConfirm >= 1) {
	        							Deposit::where('user_id',$btcuserId)->where('reference_no',$dep_id)->update(['status'=>'completed']);

	        							$transactionData = array('user_id'=>$btcuserId,'type'=>"Deposit",'currency_name'=>'BTC','amount'=>$bitcoin_balance,'total'=>$bitcoin_balance,'method'=>'bitcoin','payment_method'=>'bitcoin','t_status'=>'completed','payment_status' =>"paid",'transaction_id'=>$dep_id);
      									Transaction::create($transactionData);
	        							$fetchBTCbalance = Trade::fetchuserbalancebyId($btcuserId,'BTC');
	        							$updateBTCbalance = $fetchBTCbalance + $bitcoin_balance;
	        							Wallet::where('user_id',$btcuserId)->update(['BTC'=>$updateBTCbalance]);
	        						}
	        					}
	        				}
        				} else {
        					$dep_already = Controller::checkAdminDeposit($dep_id);
        					if($dep_already == "0") {
        						$ip = Controller::getIpAddress();
        						$insdata = array('amount'=>$bitcoin_balance,'token_id'=>'bitcoindeposit','currency'=>'BTC', 'payment_method' =>'bitcoin', 'status'=>'pending','ip_address'=>$ip, 'to_address'=>$btcaddress,'reference_no'=>$dep_id, 'type'=>'deposit');
        						$createDeposit = ContactUs::create($insdata);
        					} else {
        						if($btcconfirmations >= 8) {
        							$checkConfirm = ContactUs::where('reference_no',$dep_id)->where('status','pending')->count();
        							if($checkConfirm >= 1) {
        								ContactUs::where('reference_no',$dep_id)->update(['status'=>'completed']);
        							}
        						}
        					}
        				}
        			}
        		}
        	}
        }
	}

	//transfer amount to admin address
	public function ethereumAdminTransfer() {
		$getAddress = CoinAddress::where('currency','ETH')->where('user_id',0)->first();
        $address = $getAddress->address;
        echo $address; exit();
        $keyword = Helper::encrypt_decrypt("encrypt",'wiix');
		$output1 = array();
		$return_var1 = -1;

		$result = exec('cd /var/www/html/public/frontend; node eth_transferadmin.js '.$address.' '.$keyword.' ',$output1, $return_var1);
		print_r($result);
	}

	//cron to save eth deposit
	public function ethereumDepositProcess() {
		$getUsers = CoinAddress::where('currency','ETH')->get();
		if($getUsers) {
			foreach($getUsers as $row) {
				$btcuserId = $row->user_id;
            	$account = trim($row->address);
            	if($account != "") {
            		$max_blocknumber ="4762677";
            		// $result = Deposit::select(DB::raw('MAX(block_confirm) as max_blocknumber'))->first();
            		// if($result->max_blocknumber != null) {
            		// 	$max_blocknumber = ($result->count() == 0) ? "" : $result->max_blocknumber;
            		// }
            		$output = file_get_contents('https://api.etherscan.io/api?module=account&action=txlist&address='.$account.'&startblock='.$max_blocknumber.'&endblock=latest');
            		$result = json_decode($output);
            		if($result->message == 'OK') {
            			$transaction=$result->result;
            			for($tr=0;$tr<count($transaction);$tr++) {
            				$block_number = $transaction[$tr]->blockNumber;
            				$address = $transaction[$tr]->to;
            				$txid = $transaction[$tr]->hash;
					        $value = $transaction[$tr]->value;
					        $ether_balance = ($value/1000000000000000000); 
					        $confirmations = $transaction[$tr]->confirmations;
					        if($confirmations > 0) {
					        	$dep_already = Controller::checkDeposit($btcuserId,$txid);
					        	if($btcuserId != 0) {
					        	if($dep_already == "0") {
					        		if($address != '0xd92488a3b98f19042f3653b55a6e2a9f86550338') {
					        			$ip = Controller::getIpAddress();
			        					$depositData = array('amount'=>$ether_balance,'payment_method' =>'ethereum','status'=>'pending','user_id'=>$btcuserId,'ip_addr'=>$ip,'currency'=>'ETH', 'reference_no'=>$txid,'block_confirm'=>$confirmations,'address_info'=>$address);
										$createDeposit = Deposit::create($depositData);
					        		}
					        	} else {
					        		if($confirmations >= 3) {
					        			$checkConfirm = Deposit::where('user_id',$btcuserId)->where('reference_no',$txid)->where('status','pending')->count();
					        			if($checkConfirm >= 1) {
					        				Deposit::where('user_id',$btcuserId)->where('reference_no',$txid)->update(['status'=>'completed']);

					        				$transactionData = array('user_id'=>$btcuserId,'type'=>"Deposit",'currency_name'=>'ETH','amount'=>$ether_balance,'total'=>$ether_balance,'method'=>'ethereum','payment_method'=>'ethereum','t_status'=>'completed','payment_status' =>"paid",'transaction_id'=>$txid);
          									Transaction::create($transactionData);
		        							$fetchETHbalance = Trade::fetchuserbalancebyId($btcuserId,'ETH');
		        							$updateETHbalance = $fetchETHbalance + $ether_balance;
		        							Wallet::where('user_id',$btcuserId)->update(['ETH'=>$updateETHbalance]);
					        			}
					        		}
					        	}
					        	} else {
					        		$dep_already = Controller::checkAdminDeposit($txid);
					        		if($dep_already == "0") {
					        			$ip = Controller::getIpAddress();
		        						$insdata = array('amount'=>$ether_balance,'token_id'=>'ethereumdeposit','currency'=>'ETH', 'payment_method' =>'ethereum', 'status'=>'pending','ip_address'=>$ip, 'to_address'=>$address, 'reference_no'=>$txid, 'type'=>'deposit');
		        						$createDeposit = ContactUs::create($insdata);
					        		} else {
					        			if($confirmations >= 3) {
					        				$checkConfirm = ContactUs::where('reference_no',$txid)->where('status','pending')->count();
		        							if($checkConfirm >= 1) {
		        								ContactUs::where('reference_no',$txid)->update(['status'=>'completed']);
		        							}
					        			}
					        		}
					        	}
					        }
            			}
            		}
            	}
			}
		}
	}

	//cron to save ntc deposit
	public function natcoinDepositProcess() {
		exit();
		$getUsers = CoinAddress::where('currency','ETH')->get();
		if($getUsers) {
			foreach($getUsers as $row) {
				$btcuserId = $row->user_id;
            	$account = trim($row->address);
            	if($account != "") {
            		$fromAddr = User::getEthAddress(0);
        			$contractAddr = User::getNtcAddress(0);
        			$output = array();
        			$output1 = array();
					$return_var = -1;
            		$result = exec('cd /var/www/html/public/frontend; node ntcbalance.js ' .$contractAddr.' '.$fromAddr.' '.$account.' ',$output, $return_var);
            		echo "balance output";
            		echo "<pre>";
            		print_r($output);
            		$balance = $result/1000000000000000000;
            		if((float)$balance > 0) {
            			$resulteth = exec('cd /var/www/html/public/frontend; node ntcethfee.js ' .$account.' '."balance",$output, $return_var);
            			echo "resulteth";
	            		echo "<pre>";
	            		print_r($resulteth);
	            		echo "<pre>";
            			$ethbalance = $resulteth/1000000000000000000;

            			$resultfee = exec('cd /var/www/html/public/frontend; node ntcethfee.js ' .$account.' '."fee",$output, $return_var);
            			$ethfee = $resultfee/1000000000000000000;
            			if($ethbalance < 0 || $ethbalance < $ethfee) {
            				$amount = 0.009;
            				echo "txId"; exit();
            				$txId = Controller::sendEthFundToAddress($fromAddr,$amount,$account);
            				echo "txId";
            				echo "<pre>";
            				print_r($txId);
            				if($txId) {
            					$ip = Controller::getIpAddress();
	        					$depositData = array('amount'=>$amount,'payment_method' =>'ethereum','status'=>'completed','user_id'=>0,'ip_addr'=>$ip,'currency'=>'ETH', 'reference_no'=>$txid,'address_info'=>$account);
	        					Deposit::create($depositData);
            				} else {
            					echo "exitttttt";
            					exit();
            				}
            			} else {
            				$isvalid = "";
        					$block_number = "";
            				$pass = "WFBuQWkwQURQTUhtNDJhSFByMUlTQT09";
            				$resulttokentranfer = exec('cd /var/www/html/public/frontend; node ntctransfer.js '.$contractAddr.' '.$account.' '.$fromAddr.' '.$result.' '.$pass.' ',$output1, $return_var);
            				echo "password";
            				echo "<pre>";
            				print_r($resulttokentranfer);
            				if($resulttokentranfer && $return_var=='0') {
            					$transactionsJson = json_decode($resulttokentranfer, true);
            					echo "json response";
            					echo "<pre>";
            					print_r($transactionsJson);
            					$isvalid = $transactionsJson['transactionHash'];
            					$block_number = $transactionsJson['blockNumber'];
            					$isvalid = 1;
            					$block_number = 1;
            				}
            				if($isvalid != "") {
            					$fetchNTCbalance = Trade::fetchuserbalancebyId($btcuserId,'NTC');
    							$updateNTCbalance = $fetchNTCbalance + $balance;
    							Wallet::where('user_id',$btcuserId)->update(['NTC'=>$updateNTCbalance]);

    							$ip = Controller::getIpAddress();
    							$depositData = array('amount'=>$balance,'payment_method' =>'natcoin','status'=>'completed','user_id'=>$btcuserId,'ip_addr'=>$ip,'currency'=>'NTC', 'reference_no'=>$isvalid,'block_confirm'=>$block_number,'address_info'=>$account);
								Deposit::create($depositData);

								$transactionData = array('user_id'=>$btcuserId,'type'=>"Deposit",'currency_name'=>'NTC','amount'=>$balance,'total'=>$balance,'method'=>'natcoin','payment_method'=>'natcoin','t_status'=>'completed','payment_status' =>"paid",'transaction_id'=>$isvalid);
          						Transaction::create($transactionData);
            				}
            			}
            		}
            	}
			}
		}
	}

	 public function profitChart() {
    $year = $_GET['year'];
    $currency = $_GET['currency'];

    $data = array(array('Month'=>'Jan',
              'profit'=>Controller::getProfitDetails($year."-01-01",$year."-01-31",$currency),"color"=>"#FF0F00"),
           array('Month'=>'Feb',
              'profit'=>Controller::getProfitDetails($year."-02-01",$year."-02-31",$currency),"color"=>"#FF6600"),
          array('Month'=>'Mar',
              'profit'=>Controller::getProfitDetails($year."-03-01",$year."-03-31",$currency),"color"=> "#FF9E01"),
          array('Month'=>'Apr',
              'profit'=>Controller::getProfitDetails($year."-04-01",$year."-04-31",$currency),"color"=> "#FCD202"),
          array('Month'=>'May',
              'profit'=>Controller::getProfitDetails($year."-05-01",$year."-05-31",$currency),"color"=>"#F8FF01"),
          array('Month'=>'Jun',
              'profit'=>Controller::getProfitDetails($year."-06-01",$year."-06-31",$currency),"color"=> "#B0DE09"),
          array('Month'=>'Jul',
              'profit'=>Controller::getProfitDetails($year."-07-01",$year."-07-31",$currency),"color"=>"#04D215"),
          array('Month'=>'Aug',
              'profit'=>Controller::getProfitDetails($year."-08-01",$year."-08-31",$currency),"color"=> "#0D8ECF"),
          array('Month'=>'Sep',
              'profit'=>Controller::getProfitDetails($year."-09-01",$year."-09-31",$currency),"color"=> "#0D52D1"),
          array('Month'=>'Oct',
              'profit'=>Controller::getProfitDetails($year."-10-01",$year."-10-31",$currency),"color"=>"#2A0CD0"),
          array('Month'=>'Nov',
              'profit'=>Controller::getProfitDetails($year."-11-01",$year."-11-31",$currency),"color"=>"#8A0CCF"),
          array('Month'=>'Dec',
              'profit'=>Controller::getProfitDetails($year."-12-01",$year."-12-31",$currency),"color"=> "#CD0D74")
          );
    echo json_encode($data);
  }

}
