<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use App\Model\AdminModel;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;


class PageHandler extends Controller {
	
	
	
	public function index()
	{
		$url = "http://".$_SERVER['HTTP_HOST'];
		$url .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).$_SERVER['PATH_INFO'];
		$access_details = $_SERVER['HTTP_USER_AGENT'];
		 $ip = $this->get_client_ip();
		$currect_date = date("Y-m-d H:i:s");
		
		//check ip already found
		$condition = array(
						"user_agent" => $access_details,
						"ip"  => $ip,
						"url" => $url,
					);

		

		$blockcount = DB::table('page_handling')->where('user_agent',$access_details)->where('ip',$ip)->where('url',$url)->count();
		if($blockcount==0){			
			$insert_data = array(
						"user_agent" => $access_details,
						"ip"  => $ip,
						"url" => $url,
						"access_date" =>  date("Y-m-d H:i:s")
					);

			DB::table('page_handling')->insert(['user_agent'=>$access_details,'ip'=>$ip,'type'=>$request['subject'],'url'=>$url,'access_date'=>date('Y-m-d H:i:s')]);

			
		}
		return view('consumer/404');
	}
	
	
	
	public static function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
}
