<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;


use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;

use App\Basic;


class AdminCommon extends Controller
{
	public function template(Request $request)
	{

		if(Session::get('admin_id') =="")
		{
			return redirect('WiPlytaIIX2/login');
		}
		$title = "Email Templates";
		$emailTemplates = DB::table('wiix_email_templates')->orderBy('id','asc')->get();

		return view('admin/emailtemplate.template',['emailTemplates' => $emailTemplates,'title' => $title]);

	}

	public function template_edit(Request $request,$id)
	{	
		if(Session::get('admin_id') =="")
		{
			return redirect('WiPlytaIIX2/login');
		}

		if($request->isMethod('post'))
		{
			
			$name =  $request['name'];
			$subject =  $request['subject'];
			$message =  $request['content'];


			$update_data = array(
				"title" => $name,
				"subject" => $subject,
				"message" => $message,
				
				);

			$where = array('id'=>$id);					
			$is_update = Basic::updatedatas($where, $update_data,'wiix_email_templates');
			
			if($is_update){
				$request->session()->flash('success','Template updated sucessfully');
				return redirect('/WiPlytaIIX2/template');
			}else{
				$request->session()->flash('error','Error in Updating Template');
				return redirect('/WiPlytaIIX2/template');
			}
			
			

		}


		$get_temp=DB::table('wiix_email_templates')->where('id',$id)->get();
		$template = $get_temp[0];
		$title = "Edit template";
		
		return view('admin/emailtemplate.edit_template',['template' => $template,'title' => $title]);

	}

	public function emailSubscribers(Request $request)

	{

		if(Session::get('admin_id') =="")
		{
			return redirect('WiPlytaIIX2/login');
		}
		
		if($request->isMethod('post'))
		{
			//Send Email to all subscribers
			$content=$request['content'];

			

			$email_ids = implode(",",$request['id'])	;




			$msg=DB::table('wiix_email_templates')->where('mail_key','newsletter')->get();
			$to=$email_ids;


			$message=$msg[0]->message;
			$date=date('M d, Y');
			
			$message=str_replace("##MESSAGE##",$content,$message);
			$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
			$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
			
			

			$email_template = 'Newsletter';
			
			Helper::mailsend($email_template,$message,$to);



			
			$request->session()->flash('success','Subscriber Mail Sent sucessfully');
			return redirect('/WiPlytaIIX2/Subscribers');
			
		}	
		
		$subscribers = DB::table('wiix_subscribers')->get();
		$title = "Subscribers";

		return view('admin/subscribers.view_subscribers',['subscribers' => $subscribers,'title' => $title]);
		
	}
	public function enquires(Request $request)
	{
		if(Session::get('admin_id') =="")
		{
			return redirect('WiPlytaIIX2/login');
		}

		$where = array();
		$order= array('id','desc');
		
		$data["title"] = "support";
		$data["enquires"] = Basic::getmultiplerow($where,'wiix_enquires',$order);
		return view('admin/enquires.enquires',$data);
	}

	public function view_enquiry(Request $request,$id)
	{
		if(Session::get('admin_id') =="")
		{
			return redirect('WiPlytaIIX2/login');
		}

		if($id!=0){
			
			$data["enquiry_details"] = Basic::getsinglerow(array("id"=>$id),'wiix_enquires');


			if($request->isMethod('post'))
			{
				//Send Email to all subscribers
				
				$subject = $data["enquiry_details"]->user_subject;
				$email_to = $data["enquiry_details"]->user_mail;
				
				
				
				$data = array(
					"reply_msg"  => $request['reply'],
					"status"  => 1,
					"last_updated" => date('Y-m-d H:i:s')
					);
				
				$where = array('id'=>$id);					
				$is_update = Basic::updatedatas($where, $data,'wiix_enquires');


				$msg=DB::table('wiix_email_templates')->where('mail_key','support')->get();
				$to=$email_to;


				$message=$msg[0]->message;
				$date=date('M d, Y');
				
				$message=str_replace("##MESSAGE##",$subject,$message);
				$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
				$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
				
				

				$email_template = 'Support - WIIX-Exchange';
				
				Helper::mailsend($email_template,$message,$to);


				$request->session()->flash('success','Reply Mail Sent sucessfully');
				return redirect('/WiPlytaIIX2/enquires');
				
			}
			$data["id"] = $id;	
			$data["title"] = "View support details";
			return view('admin/enquires.enquiry_details',$data);
			
		}else{
			$request->session()->flash('error','Error in view enquiry details');
			return redirect('/WiPlytaIIX2/enquires');
		}
	}
}