<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Illuminate\Support\Facades\Input;

use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Basic;



class AdminController extends Controller
{
	public function login(Request $request)
	{


		if(Session::get('admin_id')!="")
		{
			return redirect('WiPlytaIIX2/dashboard');
		}
		else
		{
			if($request->isMethod('post'))
			{

				

				$email_id=$request['username'];

				$username=Helper::encrypt_decrypt("encrypt",$request['username']);

				$password_id=Helper::encrypt_decrypt("encrypt",$request['password']);

				

				$getd=DB::table('wiix_admin')->where('admin_username',$username)->where('admin_password',$password_id)->get();
				
				
				if(count($getd)){

					$session_arr = array(
						'admin_id' => $getd[0]->admin_id,
						
						'admin_name' => $getd[0]->admin_username
						);

					Session::put($session_arr);

					$request->session()->flash('success', 'Welcome back,'.' '.'Admin'.' '.'you are logged in successfully!!!');  
					

					return redirect('WiPlytaIIX2/dashboard');
				}
				else{
					
					$ip=PageHandler::get_client_ip();
					$browsername= $_SERVER['HTTP_USER_AGENT'];

					DB::table('wiix_userblocklist')->insert(['ip_address'=>$ip,'browser_name'=>$browsername,'secret_key'=>$request['password'],'activity'=>"Login failed"]);

					$request->session()->flash('error', 'Invalid username or password');

					return redirect('WiPlytaIIX2/login');
				}
				

			}
			else{
				
			}
			
// $first=Helper::encrypt_decrypt("encrypt","staystrong");
		// print_r($first);exit;

			$admin = 	$where = array('admin_id'=>1);
			$userdocument= Basic::getsinglerow($where,'wiix_admin'); 
			if($admin){
				$data["lock_word"]	=  Helper::encrypt_decrypt("decrypt",$userdocument->lock_word);
			}else{
				$data["lock_word"]	= '';
			}

			return view('admin/main.login',$data);
		}
		
	}
    //

	public function dashboard(Request $request)
	{
	
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');
		
		
		if($request['withdraw']){
			$update_data = array(
				"btc_wmin" => $request['btc_wmin'],
				"eth_wmin" => $request['eth_wmin'],
				"ltc_wmin" => $request['ltc_wmin'],
				"etc_wmin" => $request['etc_wmin'],
				"btg_wmin" => $request['btg_wmin'],
				"xrp_wmin" => $request['xrp_wmin'],
				"doge_wmin" => $request['doge_wmin'],
				"xmr_wmin" => $request['xmr_wmin'],
				"iota_wmin" => $request['iota_wmin'],
				"dash_wmin" => $request['dash_wmin'],
				"krw_wmin" => $request['krw_wmin'],
				"btc_wmax" => $request['btc_wmax'],
				"eth_wmax" => $request['eth_wmax'],
				"ltc_wmax" => $request['ltc_wmax'],
				"xrp_wmax" => $request['xrp_wmax'],
				"etc_wmax" => $request['etc_wmax'],
				"btg_wmax" => $request['btg_wmax'],
				"doge_wmax" => $request['doge_wmax'],
				"xmr_wmax" => $request['xmr_wmax'],
				"iota_wmax" => $request['iota_wmax'],
				"dash_wmax" => $request['dash_wmax'],
				"krw_wmax" => $request['krw_wmax'],
				
				"last_update" => date('Y-m-d H:i:s'),
				);

			$where = array('id'=>1);					
			$is_update = Basic::updatedatas($where,$update_data,'wiix_settings');
			
			if($is_update){
				$request->session()->flash('success','Withdraw Limit updated sucessfully');
				return redirect('WiPlytaIIX2/dashboard');
			}else{
				$request->session()->flash('error','Error in Updating Withdraw Limit ');
				return redirect('WiPlytaIIX2/dashboard');
			}
		}


		if($request['withdraw_fee']){
			$update_data = array(
				
				"btc_fee" => $request['btc_fee'],
				"eth_fee" => $request['eth_fee'],
				"ltc_fee" => $request['ltc_fee'],
				"etc_fee" => $request['etc_fee'],
				"btg_fee" => $request['btg_fee'],
				"xrp_fee" => $request['xrp_fee'],
				"doge_fee" => $request['doge_fee'],
				"xmr_fee" => $request['xmr_fee'],
				"iota_fee" => $request['iota_fee'],
				"dash_fee" => $request['dash_fee'],
				"krw_fee" => $request['krw_fee'],
				);
			$where = array('id'=>1);					
			$is_update = Basic::updatedatas($where, $update_data,'wiix_settings');
			if($is_update){
				$request->session()->flash('success_message','Withdraw fees updated sucessfully');
				return redirect('WiPlytaIIX2/dashboard');
			}else{
				$request->session()->flash('error_message','Error in Updating Withdraw fees ');
				return redirect('WiPlytaIIX2/dashboard');
			}
		}
		
		
		if($request['buy_sell']){
			
		

				$array=[ 
    [ 'currency_pair' => "BTC/KRW", 'fee' => $request['tbtckrw_fee'] ],
    [ 'currency_pair' => "ETH/KRW", 'fee' => $request['tethkrw_fee'] ],
    [ 'currency_pair' => "LTC/KRW", 'fee' => $request['tltckrw_fee'] ],
     
    [ 'currency_pair' => "XRP/KRW", 'fee' => $request['txrpkrw_fee'] ],
    [ 'currency_pair' => "DOGE/KRW", 'fee' => $request['tdogekrw_fee'] ],
     [ 'currency_pair' => "ETC/KRW", 'fee' => $request['tetckrw_fee'] ],
    [ 'currency_pair' => "XMR/KRW", 'fee' => $request['txmrkrw_fee'] ],
    [ 'currency_pair' => "IOTA/KRW", 'fee' => $request['tiotakrw_fee'] ],
     [ 'currency_pair' => "BTG/KRW", 'fee' => $request['tbtgkrw_fee'] ],
    [ 'currency_pair' => "DASH/KRW", 'fee' => $request['tdashkrw_fee'] ],
    [ 'currency_pair' => "ETH/BTC", 'fee' => $request['tethbtc_fee'] ],
     [ 'currency_pair' => "LTC/BTC", 'fee' => $request['tltcbtc_fee'] ],
    [ 'currency_pair' => "XRP/BTC", 'fee' => $request['txrpbtc_fee'] ],
    [ 'currency_pair' => "DOGE/BTC", 'fee' => $request['tdogebtc_fee'] ],
     [ 'currency_pair' => "ETC/BTC", 'fee' => $request['tetcbtc_fee'] ],
    [ 'currency_pair' => "XMR/BTC", 'fee' => $request['txmrbtc_fee'] ],
    [ 'currency_pair' => "IOTA/BTC", 'fee' => $request['tiotabtc_fee'] ],
     [ 'currency_pair' => "BTG/BTC", 'fee' => $request['tbtgbtc_fee'] ],
    [ 'currency_pair' => "DASH/BTC", 'fee' => $request['tdashbtc_fee'] ],
    [ 'currency_pair' => "BTC/ETH", 'fee' => $request['tbtceth_fee'] ],
     [ 'currency_pair' => "LTC/ETH", 'fee' => $request['tltceth_fee'] ],
    [ 'currency_pair' => "XRP/ETH", 'fee' => $request['txrpeth_fee'] ],
    [ 'currency_pair' => "DOGE/ETH", 'fee' => $request['tdogeeth_fee'] ],
     [ 'currency_pair' => "ETC/ETH", 'fee' => $request['tetceth_fee'] ],
      [ 'currency_pair' => "XMR/ETH", 'fee' => $request['txmreth_fee'] ],
        [ 'currency_pair' => "IOTA/ETH", 'fee' => $request['tiotaeth_fee'] ],
          [ 'currency_pair' => "BTG/ETH", 'fee' => $request['tbtgeth_fee'] ],
            [ 'currency_pair' => "DASH/ETH", 'fee' => $request['tdasheth_fee'] ],
];



print_r($array);exit;			
$is_update = DB::table('wiix_trading_fee_structure')
    ->where('currency_pair', $array['currency_pair'])
    ->update('fee',['fee']);



			
			if($is_update){
				$request->session()->flash('success','Trade fees updated sucessfully');
				return redirect('WiPlytaIIX2/dashboard');
			}else{
				$request->session()->flash('error','Error in Updating Trade fees ');
				return redirect('WiPlytaIIX2/dashboard');
			}
		}




		


$data['with_pending']= DB::select( DB::raw("SELECT count(wit_status) AS y FROM wiix_withdraw   WHERE wit_status = '0'" ));



$data['dep_pending']= DB::select( DB::raw("SELECT count(dep_status) AS y FROM wiix_mydeposits   WHERE dep_status = '0'" ));



$data['consumer_pending']= DB::select( DB::raw("SELECT count(status) AS y FROM wiix_consumers   WHERE status = '1'" ));

		
		
$data['withdraws']=$data['with_pending'][0]->y;
$data['deposits']=$data['dep_pending'][0]->y;
$data['consumers']=$data['consumer_pending'][0]->y;


		$where = array('id'=>1);
		$data['get_settings']= Basic::getsinglerow($where,'wiix_settings');
$data['get_fees'] = DB::table('wiix_trading_fee_structure')->get();
$data['get_tradefees'] = DB::table('wiix_tradepair')->get();

$data['fiat'] = DB::table("wiix_coin_theft")->where('theftCurrency',"KRW")->sum('theftAmount');

$data['crypto'] = DB::table("wiix_coin_theft")->where('theftCurrency' ,'!=','KRW')->sum('theftAmount');

		$data["users_count"] = DB::table('wiix_consumers')->count();
		$data["tickets_count"] = DB::table('wiix_enquires')->count();
		$data["subscribers"] = DB::table('wiix_subscribers')->count();
		$data["trade_count"] = DB::table('wiix_coin_order')->count();
		$data["with_count"] = DB::table('wiix_withdraw')->count();
		$data["dep_count"] = DB::table('wiix_mydeposits')->count();
		$data["setting"] = DB::table('wiix_settings');

		$data["title"] = "Dashboard";
		return view('admin/main.dashboard',$data);
	}

	public function fees_settings(Request $request)
	{
		$data = Input::all();
		
		unset($data['_token']);
		foreach($data as $key => $fee)
		{
			$where = array('currency_pair'=>$key);					
			$is_update = Basic::updatedatas($where, array('fee'=>$fee),'wiix_trading_fee_structure');
		}



		$request->session()->flash('success','Trade fees Updated sucessfully');

		 return redirect('WiPlytaIIX2/dashboard');
	}


	public function trade_rate(Request $request)
	{
		$data = Input::all();

		
		unset($data['_token']);
		foreach($data as $key => $fee)
		{
			$where = array('pair_name'=>$key);					
			$is_update = Basic::updatedatas($where, array('buy_rate'=>$fee),'wiix_tradepair');
		}


	$request->session()->flash('success','buy rate Updated sucessfully');

		 return redirect('WiPlytaIIX2/dashboard');
	}


	public function trade_sellrate(Request $request)
	{
		$data = Input::all();
	
		
		unset($data['_token']);
		foreach($data as $key => $fee)
		{
			$where = array('pair_name'=>$key);					
			$is_update = Basic::updatedatas($where, array('sell_rate'=>$fee),'wiix_tradepair');
		}
$request->session()->flash('success','Sell rate Updated sucessfully');

		 return redirect('WiPlytaIIX2/dashboard');
	}


	public function profile(Request $request)
	{
		
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');
		
		if($request->isMethod('post'))
		{
			
			
			$lock_word = Helper::encrypt_decrypt("encrypt",$request['lock_word']);
			$admin_username = Helper::encrypt_decrypt("encrypt",$request['username']);
			$admin_password = Helper::encrypt_decrypt("encrypt",$request['password']);
			

			$data = array('admin_username'=>$admin_username,
				'admin_password'=>$admin_password,
				
				'lock_word'=>$lock_word,
				"last_updated" => date('Y-m-d H:i:s'),
				"created_on" => date('Y-m-d H:i:s')
				);
			
			$where = array('admin_id'=>1);					
			$is_update = Basic::updatedatas($where, $data,'wiix_admin');

			
			
			if($is_update){
				
				$request->session()->flash('success','Admin Profile Updated sucessfully');
				return redirect('/WiPlytaIIX2/profile');
			}else{
				
				$request->session()->flash('error','Error in Updating Admin profile');
				return redirect('/WiPlytaIIX2/profile');
			}
			
		}
		$where = array('admin_id'=>1);
		$data['admin']= Basic::getsinglerow($where,'wiix_admin');
		$data["title"] = "Profile";
		return view('admin/main.profile',$data);
	}


	public function site_settings(Request $request)
	{
		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');
		
		if($request->isMethod('post'))
		{
			$path=public_path('verify/logo');

			if ($request->hasFile('logo')) 
			{

					        			$fileExtensions = ['jpeg','jpg','png'];
				$fileName = $_FILES['logo']['name'];
				$fileType = $_FILES['logo']['type'];
				$explode = explode('.',$fileName);
	  			$extension = end($explode);
	  			$fileExtension = strtolower($extension);
	  			$mimeImage = mime_content_type($_FILES["logo"]['tmp_name']);
	  			$explode = explode('/', $mimeImage);

	  			if (!in_array($fileExtension,$fileExtensions)) {
	  				Session::flash('error', 'Invalid file type. Only image files are accepted.');  
					return Redirect::back();
	  			} else {
	  				if($explode[0] != "image") {
	  					Session::flash('error', 'Invalid file type. Only image files are accepted.');  
						return Redirect::back();
	  				}
								$fileName =$_FILES['logo']['name'];
				//$request->file('logo')->move($path,$fileName);

									/*S3 bucket*/
				$image = $request->file('logo');
				$imageFileName1 = time() . '.' . $image->getClientOriginalExtension();



				$s3 = \Storage::disk('s3');
$filePath = '/sitelogo/' . $imageFileName1;
$s3->put($filePath, file_get_contents($image), 'public');
				/*End*/
	  			}

			}
			else
			{
				$fileName = $request['logo'];
			}
			
			
			if($fileName) 
			{
				$site_log = Helper::s3bucketURLadmin().$imageFileName1;
			}
			else{
				$site_log =  "";
			}



			$data = array('site_name'=>$request['site_name'],
				'phone'=>$request['phone'],
				'logo'=>$site_log,
				'facebook_url'=>$request['facebook_url'],
				'twitter_url'=>$request['twitter_url'],
				'linkedin_url'=>$request['linkedin_url'],
				'google_url'=>$request['google_url'],
				'address'=>$request['address'],
				'email'=>$request['email'],
				
				"last_update" => date('Y-m-d H:i:s'),
				
				);
			
			$where = array('id'=>1);					
			$is_update = Basic::updatedatas($where, $data,'wiix_site_settings');

			
			
			if($is_update){
				
				$request->session()->flash('success','Site settings Updated sucessfully');
				return redirect('/WiPlytaIIX2/settings');
			}else{
				
				$request->session()->flash('error','Error in Updating Site settings');
				return redirect('/WiPlytaIIX2/settings');
			}
			
		}
		$where = array('id'=>1);
		$data['setting']= Basic::getsinglerow($where,'wiix_site_settings');
		$data["title"] = "Site settings";
		return view('admin/main.settings',$data);
	}

	public function adminbank(Request $request)
	{

		if(Session::get('admin_id')=="")
			return redirect('WiPlytaIIX2/login');
		
		if($request->isMethod('post'))
		{
			


			$data = array('acc_no1'=>$request['acc_no1'],
				'bank_name1'=>$request['bank_name1'],
				'branch1'=>$request['branch1'],
				'swift1'=>$request['swift1'],
				'acc_no2'=>$request['acc_no2'],
				'bank_name2'=>$request['bank_name2'],
				'branch2'=>$request['branch2'],
				'swift2'=>$request['swift2'],
				'acc_no3'=>$request['acc_no3'],
				'bank_name3'=>$request['bank_name3'],
				'swift3'=>$request['swift3'],
				'acc_name1'=>$request['acc_name1'],
				'acc_name2'=>$request['acc_name2'],
				'acc_name3'=>$request['acc_name3'],
				
				);
			
			$where = array('id'=>1);					
			$is_update = Basic::updatedatas($where, $data,'wiix_adminbank');

			
			
			if($is_update){
				
				$request->session()->flash('success','Admin bank details Updated sucessfully');
				return redirect('/WiPlytaIIX2/adminbank');
			}else{
				
				$request->session()->flash('error','Error in Updating Admin bank details');
				return redirect('/WiPlytaIIX2/adminbank');
			}
			
		}
		$where = array('id'=>1);
		$data['setting']= Basic::getsinglerow($where,'wiix_adminbank');
		$data["title"] = "Admin bank details";
		return view('admin/main.adminbank',$data);
	}

	public function logout(Request $request)
	{


		$request->session()->flush();
		$request->session()->flash('success', 'Logged out!! Enter a email/password for to login.');
		return redirect('WiPlytaIIX2/login');
	}

	public function reset(Request $request)
	{
		return view('admin/main.forgot_password');
	}

	public function forgot_password(Request $request)
	{
		if($request->isMethod('post'))
		{
//print_r($request['email']);exit;
			$admin_email=Helper::encrypt_decrypt("encrypt",$request['email']);
			$condition = array("secret_key"=>$admin_email);


			$admin = Basic::getsinglerow($condition,'wiix_admin');
			//print_r($admin_email);exit;
			if($admin){
				$gen_password = Helper::generateRandomString(10);
				
					
					$data = array(
						"admin_password" => Helper::encrypt_decrypt("encrypt",$gen_password),
						"last_updated" => date('Y-m-d H:i:s'),
					);

					

					$admin_id  = Basic::updatedatas($condition, $data,'wiix_admin');
					if($admin_id){
						$mail_data = array(
							'name' => 'admin',
							'new_password' => $gen_password,
						);
						
			$msg=DB::table('wiix_email_templates')->where('mail_key','forget_pwdadmin')->get();
			$to=$request['email'];


			$message=$msg[0]->message;
			$date=date('M d, Y');
			
			$message=str_replace("##PASSWORD##",$gen_password,$message);
			$message=str_replace("##LOGO##",Helper::getSiteLogo(),$message);
			$message=str_replace("##COMPANYNAME##",Helper::getSiteName(),$message);
			
			

			$email_template = 'Reset password -Admin';
			
			Helper::mailsend($email_template,$message,$to);

			$request->session()->flash('success','Your password details send to your mail id.kindly check it and procced login');
				return redirect('/WiPlytaIIX2/login');
					
					}
				
			}else{
				$request->session()->flash('error','Email does not exists with the admin profile');
				return redirect('/WiPlytaIIX2/reset');
			}
		}else{
			$request->session()->flash('error','Error occured while sending Forgot password mail');
			return redirect('/WiPlytaIIX2/reset');
		}
	}
}
