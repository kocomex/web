<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\moneroController;
use App\Basic;

use App\wallet;

use jsonRPCClient;

class moneroController extends Controller
{
    //
    public function __construct() 
    {

			$xmr_row=Basic::getsinglerow(array('coinname'=>"xmrcoin"),'wiix_cryptodetails');

			$xmr_row_portnumber = 	$xmr_row->portnumber;
			$xmr_row_host 		= 	$xmr_row->ipaddress;
			
			$wallet_port = $wallet_portnumber = $xmr_row_portnumber;
			$wallet_ip   = $wallet_allow_ip   =	$xmr_row_host;
			$version	   = "2.0";
			$id = 0;
			$url = "http://$wallet_allow_ip:$wallet_portnumber/json_rpc";
}

public function index()
	{
		die(json_encode(array('status'=>'error','message'=>'Security Error')));
	}

	/**
	 * Remote procedure call handler
	 *
	 * @param string $cmd
	 * @param string $request
	 * @param string $postfield
	 * @return stdClass Returned fields
	 */
	public static function monero_request($cmd, $postfields=null) {
			$xmr_row=Basic::getsinglerow(array('coinname'=>"xmrcoin"),'wiix_cryptodetails');

			$xmr_row_portnumber = 	Helper::encrypt_decrypt("decrypt",$xmr_row->portnumber);
			$xmr_row_host 		= 	Helper::encrypt_decrypt("decrypt",$xmr_row->ipaddress);
			//print_r("hai");
			
			$wallet_port = $wallet_portnumber = $xmr_row_portnumber;
			$wallet_ip   = $wallet_allow_ip   =	$xmr_row_host;
			$version	   = "2.0";
			$id = 0;
			$url = "http://$wallet_allow_ip:$wallet_portnumber/json_rpc";
		$data = array();
		$data['jsonrpc'] = $version;
		$data['id'] 	= $id++;
		$data['method'] = $cmd;
		$data['params'] = $postfields;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		curl_setopt($ch, CURLOPT_POST, count($postfields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$ret = curl_exec($ch);
		curl_close($ch);
		//print_r($ret);exit;
		
		if($ret !== FALSE)
		{
			$formatted = moneroController::format_response($ret);
			
			if(isset($formatted->error))
			{
				echo $formatted->error;
			}
			else
			{
				return $formatted->result;
			}
		}
		else 
		{
			echo ("Server did not respond");
		}
	}

	private static function _transform($amount){
		$new_amount = $amount * 100000000;
		return $new_amount;
	}

	/**
	* Print json (for api)
	* @return $json
	*/
	public static function _print($json){
		$json_parsed = json_encode($json,  JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		echo $json_parsed;
	}

	public static function format_response($response)
	{
		return @json_decode($response);
	}

	/**
	* Print Monero Address as JSON Array
	*/
	public static function getaccountaddress($email=''){
		// echo '<br> emailt- > ' . $email; 
		// echo '<pre>'; print_r(array($email)); 
		return moneroController::monero_request('getaddress',array($email))->address;
	}

	/*
	* Print Monero Balance as JSON Array
	*/
	public static function getbalance(){
		$balance = moneroController::monero_request('getbalance');
		return $balance;
	}

	/*
	* Print Monero Height as JSON Array
	*/
	public static function getheight(){
		$height =moneroController::monero_request('getheight');
		return $height;
	}

    /*
    * Incoming Transfer
    * $type must be All 
    */
    public static function incoming_transfer($type = 'all'){
    	$incoming_parameters = array('transfer_type' => $type);
    	// echo "<pre>"; print_r($incoming_parameters); exit();
    	$incoming_transfers = moneroController::monero_request('incoming_transfers', $incoming_parameters);
    	return $incoming_transfers;
    }

    public static function get_transfers($input_type,$input_value){
    	$get_parameters = array($input_type => $input_value);
    	// echo '<pre>'; print_r($get_parameters);
    	// $get_parameters = array('pool' => true);
    	$get_transfers = moneroController::monero_request('get_transfers', $get_parameters);
    	return $get_transfers;
    }

    public static function view_key(){
    	$query_key = array('key_type' => 'view_key');
    	$query_key_method = moneroController::monero_request('query_key', $query_key);
    	return $query_key_method;
    }

    /* A payment id can be passed as a string
    A random payment id will be generatd if one is not given */
    public static function make_integrated_address($payment_id = ''){
    	$integrate_address_parameters = array('payment_id' => $payment_id);
    	$integrate_address_method = moneroController::monero_request('make_integrated_address', $integrate_address_parameters);
    	return $integrate_address_method;
    }

    public static function split_integrated_address($integrated_address){
    	if(!isset($integrated_address)){
    		echo "Error: Integrated_Address mustn't be null";
    	}
    	else{
    		$split_params = array('integrated_address' => $integrated_address);
    		$split_methods = moneroController::monero_request('split_integrated_address', $split_params);
    		return $split_methods;
    	}
    }

    public static function make_uri($address, $amount, $recipient_name = null, $description = null){
        // If I pass 1, it will be 1 xmr. Then 
    	$new_amount = $amount * 1000000000000;

    	$uri_params = array('address' => $address, 'amount' => $new_amount, 'payment_id' => '', 'recipient_name' => $recipient_name, 'tx_description' => $description);
    	$uri = moneroController::monero_request('make_uri', $uri_params);
    	return $uri;
    }


    public static function parse_uri($uri){
    	$uri_parameters = array('uri' => $uri);
    	$parsed_uri = moneroController::monero_request('parse_uri', $uri_parameters);
    	return $parsed_uri;
    }

    public static function transfer($amount, $address, $mixin = 4){
    	$new_amount = $amount  * 1000000000000;
    	$destinations = array('amount' => $new_amount, 'address' => $address);
    	$transfer_parameters = array('destinations' => array($destinations), 'mixin' => $mixin, 'get_tx_key' => true, 'unlock_time' => 0, 'payment_id' => '');
    	$transfer_method = moneroController::monero_request('transfer', $transfer_parameters);
    	return $transfer_method;
    }

    public static function get_payments($payment_id){
    	$get_payments_parameters = array('payment_id' => $payment_id);
    	$get_payments = moneroController::monero_request('get_payments', $get_payments_parameters);
    	return $get_payments;
    }

    public static function get_bulk_payments($payment_id, $min_block_height){
    	$get_bulk_payments_parameters = array('payment_id' => $payment_id, 'min_block_height' => $min_block_height);
    	$get_bulk_payments = moneroController::monero_request('get_bulk_payments', $get_bulk_payments_parameters);
    	return $get_bulk_payments;
    }

    public static function get_info(){
		$getinfo = moneroController::monero_request('get_info');
		return $getinfo;
	}

	public static function help(){
		$help = moneroController::monero_request('help');
		return $help;
	}

	public static function getwalletinfo(){
		return moneroController::monero_request('getwalletinfo');
	}

	public static function listtransactions(){
		return moneroController::monero_request('listtransactions');
	}

	public static function show_transfers(){
		return moneroController::monero_request('show_transfers');
	}

	public static function get_address_info($address,$view_key){
		$get_address_parameters = array('address' => $address, 'view_key' => $view_key);
		return moneroController::monero_request('get_address_info',$get_address_parameters);
	}

}
