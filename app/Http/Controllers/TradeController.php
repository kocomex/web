<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Basic;
use App\Trade;
use App\CoinOrder;
use App\wallet;

class TradeController extends Controller
{

	

	public function trade(Request $request)
	{
		if(Session::get('user_id')=="")
      {
      	Session::flash('error', 'Please login to continue!');
        return redirect('/');
      }

		$getusers= Basic::getsinglerow(array('user_id'=>Session::get('user_id')),'wiix_consumers');
		$usersta=$getusers->status;

		if($usersta == 0)
		{
			AuthController::logout($request);
			return redirect('/');
		}

		/*Get currency dynamically*/

		$where = array();
		$order= array('id','asc');
		$data["getpairs"] = Basic::getmultiplerow($where,'wiix_tradepair',$order);

		/*CMS datas*/
		$where = array('page_name'=>'Trade');
		$data['prof']= Basic::getsinglerow($where,'wiix_cms');

		    	$wheretr = array();
				$ordertr= array('id','asc');
$data['trade_rates']= Basic::getmultiplerow($wheretr,'wiix_tradepair',$ordertr);

      $data['settings']=HomeController::mainfunction();
		$data['title'] = $data['prof']->meta_title;
		$data['meta_keyword'] = $data['prof']->meta_keyword;
		$data['meta_description'] = $data['prof']->meta_description;

		return view('consumer/trade',$data);

	}

 public function getPairData() {
    	if(session('user_id')!='') {
    		$pair = strip_tags($_POST['pair']);
    		$uid = session('user_id');

    		$res = explode('/',$pair);
			$from_symbol = $res[0];
			$to_symbol = $res[1];
			if($to_symbol != "KRW") {
				$digits = 8;
			} else {
				$digits = 2;
			}

			$usdvolume = Trade::get_usdvolume($from_symbol,$to_symbol,$uid);
			if(!$usdvolume) {
				$usdvolume = 0;
			}
			$query = Trade::getfeedetails_new($pair);

			if($query) {
				$trade_comm	=	$query[0]->fee;
			} else {
				$trade_comm	= 0;
			}


			$buy_rate = Trade::lowestaskprice1($from_symbol,$to_symbol);
			$sell_rate = Trade::highestbidprice1($from_symbol,$to_symbol);

			$activeResult = Trade::get_active_order($from_symbol,$to_symbol,$uid);
			$stop_result = Trade::get_stop_orders($from_symbol,$to_symbol,$uid);


			$buyResult = Trade::fetchCoinorder('Buy',$from_symbol,$to_symbol);
			$sellResult = Trade::fetchCoinorder('Sell',$from_symbol,$to_symbol);



			$trade_result = Trade::fetchTransactionHistory($from_symbol,$to_symbol);
			$myTradeResult = Trade::getUserTradeHistory($from_symbol,$to_symbol,$uid);

			$balance = wallet::where('user_id',$uid)->first();

			$k=0;
			if(!empty($buyResult) &&  !$buyResult->isEmpty()) {
				$buy = "";
				foreach($buyResult as $buys) {
					$buyorderid = $buys->id;
					$buyPrice = $buys->Price;
					$buyAmount = $buys->amount;
					if($buyorderid != 'cryptotrade') {
						$buyfilledAmount = Trade::checkOrdertempdetails($buyorderid,'Buy');
						if($buyfilledAmount) {
							$buyfilledAmount = $buyAmount-$buyfilledAmount;
						} else {
							$buyfilledAmount = $buyAmount;
						}
					} else {
						$buyfilledAmount = $buyAmount;
					}

					$buyCalcTotal = $buyfilledAmount*$buyPrice;
					$buyCalcTotal=number_format((float)$buyCalcTotal, $digits, '.', '');
					$buyfilledAmount=number_format((float)$buyfilledAmount, 8, '.', '');
					$buyPrice=number_format((float)$buyPrice, $digits, '.', '');

					$buy.= '<tr align="center" id="trbuyfilledAmount_'. $k.'" ><td width="30%">' .$buyPrice.'</td><td width="40%">'.$buyfilledAmount.'</td><td width="30%">'. $buyCalcTotal.'</td></tr>';
					$k++;
				}
			} else {
				$buy =  '<tr><td colspan="3" style="text-align:center;">No buy orders at the moment</td></tr>';
			}

			if(!empty($sellResult) &&  !$sellResult->isEmpty()) {
				$sell = "";
				$k=0;

				foreach($sellResult as $sells) {

					$sellorderid = $sells->id;
			        $sellPrice = $sells->Price;
			        $sellAmount = $sells->amount;
			       

			        if($sellorderid != 'cryptotrade') {
			        	$sellfilledAmount = Trade::checkOrdertempdetails($sellorderid,'Sell');
			        	if($sellfilledAmount) {
			        		$sellfilledAmount = $sellAmount-$sellfilledAmount;
			        	} else {
			        		$sellfilledAmount = $sellAmount;
			        	}
			        } else {
			        	$sellfilledAmount = $sellAmount;
			        }
					$sellCalcTotal = $sellfilledAmount*$sellPrice;
				    $sellPrice =number_format((float)$sellPrice, $digits, '.', '');
				    $sellfilledAmount = number_format((float)$sellfilledAmount, 8, '.', '');
				    $sellCalcTotal = number_format((float)$sellCalcTotal, $digits, '.', '');
				    $sell.='<tr align="center" id="trsellfilledAmount_'. $k.'" ><td width="30%">'.$sellPrice.'</td><td width="40%">'.$sellfilledAmount.'</td><td width="30%">'.$sellCalcTotal.'</td></tr>';

				    $k++;
				}
			} else {
				$sell = '<tr><td colspan="3" style="text-align:center;">No sell orders at the moment</td></tr>';
			}

			if($activeResult) {
				$activeorder = "";
				foreach($activeResult as $active) {
					$activeTradeid = $active->id;
					$activePrice = $active->Price;
					$activeAmount = $active->Amount;
					$activeType = $active->Type;
					$activeTotal = $active->Total;
					$activeDate = $active->orderDate;
					$activeTime = $active->orderTime;
					$activefee = $active->Fee;
					$status = $active->status;
					$fee_per = $active->fee_per;
					$activets = $activeDate." ".$activeTime;
					$activefilledAmount = Trade::checkOrdertempdetails($activeTradeid,$activeType);
					if($activefilledAmount) {
						$activefilledAmount = $activeAmount-$activefilledAmount;
					} else {
						$activefilledAmount = $activeAmount;
					}

					$tradeid = Helper::encrypt_decrypt("encrypt",$activeTradeid);
					$activeCalcTotal = $activefilledAmount*$activePrice;
					$activefilledAmount = number_format((float)$activefilledAmount, 8, '.', '');
					$activePrice = number_format((float)$activePrice, $digits, '.', '');
					$activeCalcTotal = number_format((float)$activeCalcTotal, $digits, '.', '');
					$fee_amt = ($activeCalcTotal * $fee_per) / 100;
					$activeorder.= '<tr class=""><td>'.$activeType.'</td><td>'. $activets.'</td><td>'. $activefilledAmount.'</td><td>'. $activePrice.'</td><td>'.number_format($fee_amt,$digits, '.', '').'</td><td>'. number_format($activeCalcTotal,$digits, '.', '').'</td>
          				<td><a class="cancel_trade_order" onclick="cancel_order(\''.$tradeid.'\','."1".')"><i class="fa fa-times-circle pad-rht"></i></a></td></tr>';
				}
			} else {
				$activeorder = '<tr><td colspan="7" style="text-align:center;">No active orders at the moment</td></tr>';
			}

			if($stop_result) {

				$stop ="";
				foreach($stop_result as $active) {

					$type = $active->Type;
					$date = $active->orderDate." ".$active->orderTime;
		            $amount = $active->Amount;
		            $stoporderprice = $active->stoporderprice;
		            $trigger_price = $active->trigger_price;
		            $trade_id = $active->id;
		            $stopfee = $active->Fee;
		            $stopTotal = $active->Total;
		            $fee_per = $active->fee_per;
		            $trid = Helper::encrypt_decrypt("encrypt",$trade_id);



		            $stopPrice = number_format($stoporderprice,$digits, '.', '');
		            $stopfilledAmount = number_format((float)$amount, 8, '.', '');
		            $stopCalcTotal = $stopfilledAmount*$stoporderprice;
		            $fee_amt = ($stopCalcTotal * $fee_per) / 100;
		            $stop.= '<tr class="">
	                    <td>'. $type.'</td>
	                    <td>'. $date.'</td>
	                    <td>'. $stopfilledAmount.'</td>
	                    <td>'. $stopPrice.'</td>
	                    <td>'. $fee_amt.'</td>
	                    <td>'. number_format($stopCalcTotal,$digits, '.', '').'</td>
	                    <td>
	                    <a class="cancel_trade_order" onclick="cancel_order(\''.$trid.'\','."2".')"><i class="fa fa-times-circle pad-rht"></i></a>
	                    </td>
	                    </tr>';

				}
			} else {
				$stop = '<tr><td colspan="7" style="text-align:center;">No stop orders at the moment</td></tr>';
			}

			if ($trade_result) {
				$trade_result1 = "";
				foreach($trade_result as $row) {
					$status = $row->status;
		            $Amount = $row->Amount;
		            $trade_id = $row->id;
		            $Price = $row->Price;
		            $Total = $row->Total;
		            $Type = $row->Type;
		            $datetime = $row->tradetime;
		            $trade_total = $Price * $Amount;
	            	$trade_result1.='<tr id="'.$row->id.'"><td>'.$Type.'  </td>
	            		<td>'. $datetime.'</td>
		         		<td>'. number_format((float)$Amount, 8, '.', '').'</td>
		         		<td>'. number_format($Price,$digits, '.', '').'</td>
		         		<td>'. number_format($trade_total,$digits, '.', '').'</td>
		         		</tr>';
				}
			} else {
				$trade_result1 = '<tr><td colspan="5" style="text-align:center;">No trade history at the moment</td></tr>';
			}

			if ($myTradeResult) {
				$trade_result2 = "";
				foreach($myTradeResult as $row) {
					$status = $row->status;
		            $Amount = $row->Amount;
		            $trade_id = $row->id;
		            $Price = $row->Price;
		            $Total = $row->Total;
		            $Type = $row->Type;
		            $Fee = $row->Fee;
		            if($row->tradetime != "") {
		            	$datetime = $row->tradetime;
		            } else {
		            	$datetime = $row->datetime;
		            }
		            if($status == "cancelled") {
		            	$activefilledAmount = Trade::checkOrdertempdetails1($trade_id,$Type);
		            	$partial_date = Trade::checkOrdertempdetails_date($trade_id,$Type);
		            	if($activefilledAmount) {
		            		$Amount = $activefilledAmount;
                			$trade_total = $Amount * $Price;
                			$trade_result2.='<tr id="'.$row->id.'"><td>'.$Type.'  </td>
			            		<td>'. $datetime.'</td>
				         		<td>'. number_format((float)$Amount, 8, '.', '').'</td>
				         		<td>'. number_format($Price,$digits, '.', '').'</td>
				         		<td>'. number_format($trade_total,$digits, '.', '').'</td>
				         		<td>'. $Fee.'</td>
				         		<td>'. $status.'</td>
				         		</tr>';
		            	} else {
		            		$trade_total = $Price * $Amount;
		            		$trade_result2.='<tr id="'.$row->id.'"><td>'.$Type.'  </td>
			            		<td>'. $datetime.'</td>
				         		<td>'. number_format((float)$Amount, 8, '.', '').'</td>
				         		<td>'. number_format($Price,$digits, '.', '').'</td>
				         		<td>'. number_format($trade_total,$digits, '.', '').'</td>
				         		<td>'. $Fee.'</td>
				         		<td>'. $status.'</td>
				         		</tr>';
		            	}
		            } else {
		            	$trade_total = $Price * $Amount;
	            		$trade_result2.='<tr id="'.$row->id.'"><td>'.$Type.'  </td>
		            		<td>'. $datetime.'</td>
			         		<td>'. number_format((float)$Amount, 8, '.', '').'</td>
			         		<td>'. number_format($Price,$digits, '.', '').'</td>
			         		<td>'. number_format($trade_total,$digits, '.', '').'</td>
			         		<td>'. $Fee.'</td>
			         		<td>'. $status.'</td>
			         		</tr>';
		            }
				}
			} else {
				$trade_result2 = '<tr><td colspan="6" style="text-align:center;">No trade history at the moment</td></tr>';
			}

			// header data last trade,high,low,24 hr vol
		 	$cur_date = date('Y-m-d');
		 	$cur_time = date('Y-m-d H:i:s');
		 	$status = array('filled');
		 	$prevDate = date('Y-m-d H:i:s',(strtotime ( '-1 day' , strtotime($cur_time))));

		 	$last_bid = CoinOrder::select('Price')->orderBy('id','desc')->where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->limit(1)->get();
		 	$lastbid = ($last_bid->isEmpty()) ? "0.00" : number_format($last_bid[0]->Price,$digits, '.', '');

		 	$high_24hr = CoinOrder::select('Price')->where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->where('tradetime','>=',$prevDate)->where('tradetime','<=',$cur_time)->orderBy('Price','desc')->first();
		 	if($high_24hr == null) {
		 		$high24hr = "0.00";
		 	} else {
		 		$high24hr = ($high_24hr->count() == 0) ? "0.00" : number_format($high_24hr->Price,$digits, '.', '');
		 	}

		 	$low_24hr = CoinOrder::select('Price')->where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->where('tradetime','>=',$prevDate)->where('tradetime','<=',$cur_time)->orderBy('Price','asc')->first();
		 	if($low_24hr == null) {
		 		$low24hr = "0.00";
		 	} else {
		 		$low24hr = ($low_24hr->count() == 0) ? "0.00" : number_format($low_24hr->Price,$digits, '.', '');
		 	}

		 	$base_Volume = CoinOrder::where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->where(DB::raw('DATE(tradetime)'),$cur_date)->select(DB::raw('SUM(Price) as totalPrice'))->first();
		 	if($base_Volume->totalPrice == null) {
		 		$baseVolume = "0.00";
		 	} else {
		 		$baseVolume = ($base_Volume->count() == 0) ? "0.00" : number_format($base_Volume->totalPrice,$digits, '.', '');
		 	}
		 	
		 	$yesrter = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $cur_date) ) ));
			$time = date('H:i:s');
			$yesterday = CoinOrder::where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->where(DB::raw('DATE(tradetime)'),$yesrter)->where(DB::raw('CAST(tradetime as time)'),'>',$time)->orderBy('Price','asc')->select('Price')->get();
			$yesterday = ($yesterday->isEmpty()) ? "0.00" : $yesterday[0]->Price;

			$today = CoinOrder::where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->where(DB::raw('DATE(tradetime)'),$cur_date)->where(DB::raw('CAST(tradetime as time)'),'<',$time)->orderBy('Price','asc')->select('Price')->get();
			$today = ($today->isEmpty()) ? "0.00" : $today[0]->Price;

			if($today > $yesterday) {
				$arrow = "up";
			} elseif($today < $yesterday) {
				$arrow = "down";
			} else {
				$arrow = "none";
			}

			$diff = $today - $yesterday;
			if($yesterday == "0.00") {
				$percent = "0 %";
			} else {
				$percent = ($diff / $yesterday) * 100;
				$percent = number_format($percent,2, '.', '')." %";
			}


			$data = array('pair'=>$pair,'buy_rate'=>$buy_rate,'sell_rate'=>$sell_rate,'activeResult'=>$activeorder,'buyResult'=>$buy,'sellResult'=>$sell,'stop_result'=>$stop,'trade_result'=>$trade_result1,'my_trade_result'=>$trade_result2, 'balance'=>$balance,'fees'=>$trade_comm,'lastbid'=>$lastbid,'change'=>$percent,'high'=>$high24hr,'low'=>$low24hr,'volume'=>$baseVolume,'arrow'=>$arrow);
			
			echo json_encode($data);
    	} else {
    		Session::flash('error', 'Please login to continue!');
	    	return Redirect::to('login');
    	}
    }


    public function getTradeData() {
    	$pairs = array('0' => 'BTC/KRW','1' => 'ETH/KRW','2' => 'LTC/KRW','3' => 'XRP/KRW','4' => 'DOGE/KRW','5' => 'ETC/KRW','6' => 'XMR/KRW','7' => 'IOTA/KRW','8' => 'BTG/KRW','9' => 'DASH/KRW','10' => 'ETH/BTC','11' => 'LTC/BTC','12' => 'XRP/BTC','13' => 'DOGE/BTC','14' => 'ETC/BTC','15' => 'XMR/BTC','16' => 'IOTA/BTC','17' => 'BTG/BTC','18' => 'DASH/BTC','19' => 'BTC/ETH','20' => 'LTC/ETH','21' => 'XRP/ETH','22' => 'DOGE/ETH','23' => 'ETC/ETH','24' => 'XMR/ETH','25' => 'IOTA/ETH','26' => 'BTG/ETH','27' => 'DASH/ETH');
    	$tradeResult = array();
    	foreach($pairs as $pair) {
    		$res = explode('/',$pair);
			$from_symbol = $res[0];
			$to_symbol = $res[1];

			if($to_symbol != "KRW") {
				$digits = 8;
			} else {
				$digits = 2;
			}

			$cur_date = date('Y-m-d');
		 	$status = array('filled');

		 	$last_bid = CoinOrder::select('Price')->orderBy('id','desc')->where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->limit(1)->get();
		 	$tradeData['lastbid'] = ($last_bid->isEmpty()) ? "0.00 ".$to_symbol : number_format($last_bid[0]->Price,$digits, '.', '')." ".$to_symbol;

		 	$base_Volume = CoinOrder::where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->where(DB::raw('DATE(tradetime)'),$cur_date)->select(DB::raw('SUM(Price) as totalPrice'))->first();
		 	if($base_Volume->totalPrice == null) {
		 		$tradeData['baseVolume'] = "0.00"." ".$to_symbol;
		 	} else {
		 		$tradeData['baseVolume'] = ($base_Volume->count() == 0) ? "0.00"." ".$to_symbol : number_format($base_Volume->totalPrice,$digits, '.', '')." ".$to_symbol;
		 	}
		 	// print_r($base_Volume); exit();
		 	$yesrter = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $cur_date) ) ));
			$time = date('H:i:s');
			$yesterday = CoinOrder::where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->where(DB::raw('DATE(tradetime)'),$yesrter)->where(DB::raw('CAST(tradetime as time)'),'>',$time)->orderBy('Price','asc')->select('Price')->get();
			$yesterday = ($yesterday->isEmpty()) ? "0.00" : $yesterday[0]->Price;

			$today = CoinOrder::where('firstCurrency',$from_symbol)->where('secondCurrency',$to_symbol)->whereIn('status',$status)->where(DB::raw('DATE(tradetime)'),$cur_date)->where(DB::raw('CAST(tradetime as time)'),'<',$time)->orderBy('Price','asc')->select('Price')->get();
			$today = ($today->isEmpty()) ? "0.00" : $today[0]->Price;

			if($today > $yesterday) {
				$tradeData['arrow'] = "up";
			} elseif($today < $yesterday) {
				$tradeData['arrow'] = "down";
			} else {
				$tradeData['arrow'] = "none";
			} 
			$diff = $today - $yesterday;
			if($yesterday == "0.00") {
				$tradeData['percent'] = "0 %";
			} else {
				$percent = ($diff / $yesterday) * 100;
				$tradeData['percent'] = number_format($percent,2, '.', '')." %";
			}
			array_push($tradeResult,$tradeData);
    	}
    	echo json_encode($tradeResult);
    }

    //mapping
    public function tradeMapping(Request $request) {
    	if(session('user_id')!='') {
    		$data = $request->all();
    		$type = strip_tags($data['type']);
    		$pair = strip_tags($data['pair']);
    		$freshorderid = strip_tags($data['freshorderid']);
    		$result = Trade::mapping($type,$pair,$freshorderid);
    		if($result=="empty") {
    			echo "failure";
    		} else {
    			echo "success_".$result;
    		}
    	} else {
    		Session::flash('error', 'Please login to continue!');
	    	return Redirect::to('login');
    	}
    }

    //create buy order
    public function createBuyOrder(Request $request) {

    	if(session('user_id')!='') {
    		$data = $request->all();
    		$userId = session('user_id');
    		$amount = strip_tags($data['amount']);
			$order_type = strip_tags($data['order_type']);
			$price = ($order_type == "instant") ? strip_tags($data['price1']) : strip_tags($data['price']);
			if($amount != "" && $price != "") {
				if(is_numeric($amount) && is_numeric($price)) {
					$pair = strip_tags($data['pair']);
					$pair1 = explode('/',$pair);
					$from_symbol = $pair1[0];
					$to_symbol = $pair1[1];
					if($to_symbol != "KRW") {
						$digits = 8;
					} else {
						$digits = 2;
					}
					$order = Controller::calculate_order($pair,$amount,$price,'buy',$userId);

					$total = $order['total'];
					$fee_amt =  $order['fee_amt'];
					$fee_per = $order['fee_per'];

					$balance = Trade::fetchuserbalancebyId($userId,$to_symbol);
					if($total <= $balance) {
						$updatesecondBalance = $balance-$total;
						$updatesecondBalance = number_format(trim($updatesecondBalance), $digits, '.', '');
						$updatedata = array($to_symbol =>$updatesecondBalance);
						$updatequery = Wallet::where('user_id',$userId)->update($updatedata);
						$current_date = date('Y-m-d'); 
			    		$current_time = date('H:i:s'); 
			    		$datetime = date("Y-m-d H:i:s");

			    		$data   =   array(
					        'user_id'=>$userId,
					        'Amount'=>number_format($amount,8, '.', ''),
					        'Price'=>number_format($price,$digits, '.', ''),
					        'Total'=>number_format($total,$digits, '.', ''),
					        'Fee'=>number_format($fee_amt,$digits, '.', ''),
					        'firstCurrency'=>$from_symbol,
					        'secondCurrency'=>$to_symbol,
					        'Type'=>"Buy",
					        'orderDate'=>$current_date,
					        'orderTime'=>$current_time,
					        'datetime'=>$datetime,
					        'pair'=>$pair,
					        'status'=>"active",
					        'fee_per'=>$fee_per,
					        'ordertype'=>$order_type
				        );
				        $createOrder = CoinOrder::create($data);
				        $result_data = array('status'=>'1','msg'=>'Buy Order Placed Successfully','lastid'=>$createOrder->id);
					} else {
						$result_data = array('status'=>'0','msg'=>'Insufficient Balance');
					}
				} else {
					$result_data = array('status'=>'0','msg'=>'Enter Valid Amount / Price');
				}
			} else {
				$result_data = array('status'=>'0','msg'=>'Enter Amount / Price');
			}
    	} else {
    		$result_data = array('status'=>'0','msg'=>'Login failure');
    	}
    	echo json_encode($result_data);
    }

    public function createBuyStopOrder(Request $request) {
    	if(session('user_id')!='') {
    		$data = $request->all();
    		$userId = session('user_id');
    		$amount = strip_tags($data['amount']);
			$order_type = strip_tags($data['order_type']);
			$price = strip_tags($data['price']);
			if($amount != "" && $price != "") {
				if(is_numeric($amount) && is_numeric($price)) {
					$pair = strip_tags($data['pair']);
					$pair1 = explode('/',$pair);
					$from_symbol = $pair1[0];
					$to_symbol = $pair1[1];
					if($to_symbol != "KRW") {
						$digits = 8;
					} else {
						$digits = 2;
					}

					$order = Controller::calculate_order($pair,$amount,$price,'buy',$userId);
					$total = $order['total'];
					$fee_amt =  $order['fee_amt'];
					$fee_per = $order['fee_per'];

					$sell_rate = Trade::highestbidprice1($from_symbol,$to_symbol);
					if($price > $sell_rate) {
						$balance = Trade::fetchuserbalancebyId($userId,$to_symbol);
						if($total <= $balance) {
							$updatesecondBalance = $balance-$total;
							$updatesecondBalance = number_format(trim($updatesecondBalance), $digits, '.', '');
							$updatedata = array($to_symbol =>$updatesecondBalance);
							$updatequery = wallet::where('user_id',$userId)->update($updatedata);
							$current_date = date('Y-m-d'); 
				    		$current_time = date('H:i:s'); 
				    		$datetime =date("Y-m-d H:i:s");
				    		$data = array(
						        'user_id'=>$userId,
						        'Amount'=>number_format($amount,8, '.', ''),
						        'Price'=>number_format($price,$digits, '.', ''),
						        'Total'=>number_format($total,$digits, '.', ''),
						        'Fee'=>number_format($fee_amt,$digits, '.', ''),
						        'firstCurrency'=>$from_symbol,
						        'secondCurrency'=>$to_symbol,
						        'Type'=>"Buy",
						        'orderDate'=>$current_date,
						        'orderTime'=>$current_time,
						        'datetime'=>$datetime,
						        'pair'=>$pair,
						        'status'=>"stoporder",
						        'fee_per'=>$fee_per,
						        'ordertype'=>$order_type,
						        'stoporderprice' => $price
					        );
					        $createOrder = CoinOrder::create($data);
				        	$result_data = array('status'=>'1','msg'=>'Buy Stop Order Placed Successfully','lastid'=>$createOrder->id);
						} else {
							$result_data = array('status'=>'0','msg'=>'Insufficient Balance'); 
						}
					} else {
						$result_data = array('status'=>'0','msg'=>'Please enter above market price');
					}
				} else {
					$result_data = array('status'=>'0','msg'=>'Enter Valid Amount / Price');
				}
			} else {
				$result_data = array('status'=>'0','msg'=>'Enter Amount / Price');
			}
    	} else {
    		$result_data = array('status'=>'0','msg'=>'Login failure');
    	}
    	echo json_encode($result_data);
    }

    public function createSellOrder(Request $request) {
    	if(session('user_id')!='') {
    		$data = $request->all();
    		$userId = session('user_id');
    		$amount = strip_tags($data['amount']);
			$order_type = strip_tags($data['order_type']);
			$price = ($order_type == "instant") ? strip_tags($data['price1']) : strip_tags($data['price']);
			if($amount != "" && $price != "") {
				if(is_numeric($amount) && is_numeric($price)) {
					$pair = strip_tags($data['pair']);
					$pair1 = explode('/',$pair);
					$from_symbol = $pair1[0];
					$to_symbol = $pair1[1];
					if($to_symbol != "KRW") {
						$digits = 8;
					} else {
						$digits = 2;
					}
					//goto
					$order = Controller::calculate_order($pair,$amount,$price,'sell',$userId);
					$total = $order['total'];
					$fee_amt =  $order['fee_amt'];
					$fee_per = $order['fee_per'];
					$balance = Trade::fetchuserbalancebyId($userId,$from_symbol);
					if($amount <= $balance) {
						$updatefirstBalance = $balance-$amount;
			    		$updatefirstBalance = number_format(trim($updatefirstBalance),8, '.', '');

			    		$updatedata = array($from_symbol =>$updatefirstBalance);
			    		$updatequery = wallet::where('user_id',$userId)->update($updatedata);

			    		$current_date = date('Y-m-d'); 
			    		$current_time = date('H:i:s'); 
			    		$datetime = date("Y-m-d H:i:s");
			    		$data = array(
					        'user_id'=>$userId,
					        'Amount'=>number_format($amount,8, '.', ''),
					        'Price'=>number_format($price,$digits, '.', ''),
					        'Total'=>number_format($total,$digits, '.', ''),
					        'Fee'=>number_format($fee_amt,$digits, '.', ''),
					        'firstCurrency'=>$from_symbol,
					        'secondCurrency'=>$to_symbol,
					        'Type'=>"Sell",
					        'orderDate'=>$current_date,
					        'orderTime'=>$current_time,
					        'datetime'=>$datetime,
					        'pair'=>$pair,
					        'status'=>"active",
					        'fee_per'=>$fee_per,
					        'ordertype'=>$order_type
				        );
				        $createOrder = CoinOrder::create($data);
			        	$result_data = array('status'=>'1','msg'=>'Sell Order Placed Successfully','lastid'=>$createOrder->id);
					} else {
						$result_data = array('status'=>'0','msg'=>'Insufficient Balance');
					}
				} else {
					$result_data = array('status'=>'0','msg'=>'Enter Valid Amount / Price');
				}
			} else {
				$result_data = array('status'=>'0','msg'=>'Enter Amount / Price');
			}
    	} else {
			$result_data = array('status'=>'0','msg'=>'Login failure');
    	}
    	echo json_encode($result_data);
    }

    public function createSellStopOrder(Request $request) {
    	if(session('user_id')!='') {
    		$data = $request->all();
    		$userId = session('user_id');
    		$amount = strip_tags($data['amount']);
			$order_type = strip_tags($data['order_type']);
			$price = strip_tags($data['price']);
			if($amount != "" && $price != "") {
				if(is_numeric($amount) && is_numeric($price)) {
					$pair = strip_tags($data['pair']);
					$pair1 = explode('/',$pair);
					$from_symbol = $pair1[0];
					$to_symbol = $pair1[1];
					if($to_symbol != "KRW") {
						$digits = 8;
					} else {
						$digits = 2;
					}

					$order = Controller::calculate_order($pair,$amount,$price,'sell',$userId);
					$total = $order['total'];
					$fee_amt =  $order['fee_amt'];
					$fee_per = $order['fee_per'];

					$sell_rate = Trade::lowestaskprice1($from_symbol,$to_symbol);
					if($price < $sell_rate) {
						$balance = Trade::fetchuserbalancebyId($userId,$from_symbol);
						if($total <= $balance) {
							$updatefirstBalance = $balance-$total;
							$updatefirstBalance = number_format(trim($updatefirstBalance), $digits, '.', '');
							$updatedata = array($from_symbol =>$updatefirstBalance);
							$updatequery = wallet::where('user_id',$userId)->update($updatedata);
							$current_date = date('Y-m-d'); 
				    		$current_time = date('H:i:s'); 
				    		$datetime =date("Y-m-d H:i:s");
				    		$data = array(
						        'user_id'=>$userId,
						        'Amount'=>number_format($amount,8, '.', ''),
						        'Price'=>number_format($price,$digits, '.', ''),
						        'Total'=>number_format($total,$digits, '.', ''),
						        'Fee'=>number_format($fee_amt,$digits, '.', ''),
						        'firstCurrency'=>$from_symbol,
						        'secondCurrency'=>$to_symbol,
						        'Type'=>"Sell",
						        'orderDate'=>$current_date,
						        'orderTime'=>$current_time,
						        'datetime'=>$datetime,
						        'pair'=>$pair,
						        'status'=>"stoporder",
						        'fee_per'=>$fee_per,
						        'ordertype'=>$order_type,
						        'stoporderprice' => $price
					        );
					        $createOrder = CoinOrder::create($data);
				        	$result_data = array('status'=>'1','msg'=>'Sell Stop Order Placed Successfully','lastid'=>$createOrder->id);
						} else {
							$result_data = array('status'=>'0','msg'=>'Insufficient Balance'); 
						}
					} else {
						$result_data = array('status'=>'0','msg'=>'Please enter below market price');
					}
				} else {
					$result_data = array('status'=>'0','msg'=>'Enter Valid Amount / Price');
				}
			} else {
				$result_data = array('status'=>'0','msg'=>'Enter Amount / Price');
			}
    	} else {
    		$result_data = array('status'=>'0','msg'=>'Login failure');
    	}
    	echo json_encode($result_data);
    }

    public function cancelOrder(Request $request) {
    	if(session('user_id')!='') {
    		$data = $request->all();
    		$userId = session('user_id');
    		$id = Helper::encrypt_decrypt("decrypt",strip_tags($data['tradeid']));

    		$res = CoinOrder::where('id',$id)->select('status')->first();
    		$status = $res->status;
    		if($status == "cancelled" || $status == "filled") {
    			$result_data = array('status'=>'0','msg'=>'Already order cancelled');
    		} else {
    			$res_order = Trade::remove_active_model($id);
    			$result_data = array('status'=>'1','msg'=>'Actve order cancelled cuccessfully');
    		}
    	} else {
    		$result_data = array('status'=>'2','msg'=>'Login failure');
    	}
    	echo json_encode($result_data);
    }

    public function cancelStopOrder(Request $request) {
    	if(session('user_id')!='') {
    		$data = $request->all();
    		$userId = session('user_id');
    		$id = Helper::encrypt_decrypt("decrypt",strip_tags($data['tradeid']));
    		$res = CoinOrder::where('id',$id)->select('status')->first();
    		$status = $res->status;
    		if($status == "cancel stop order" || $status == "filled") {
    			$result_data = array('status'=>'0','msg'=>'Already stop order cancelled');
    		} else {
    			$result = Trade::cancel_stop_order($id);
    			$result_data = array('status'=>'1','msg'=>'Stop order cancelled successfully');
    		}
    	} else {
    		$result_data = array('status'=>'2','msg'=>'Login failure');
    	}
    	echo json_encode($result_data);
    }

    public function getChartData() {
    	$pair = strip_tags($_GET['pair']);
		$exp = explode('/',$pair);
		$firstCurrency   = strtoupper($exp[0]);
		$secondCurrency = strtoupper($exp[1]);
		$end_date = date("Y-m-d H:i:s");
		$start_date = date('Y-m-d H:i:s', strtotime($end_date . ' - 45 days'));

		$chartdata=array();
		for($i = 1;$i<=45; $i++) {
			$interval = date('Y-m-d H:i:s', strtotime($start_date . ' + '.$i.' days'));
			$date = explode(' ',$interval)[0];

			$chartResult = Trade::forLowHigh($date,$pair);
			$low 	= $chartResult['low']; 
			$high 	= $chartResult['high'];
			$open 	= $chartResult['open'];
			$close 	= $chartResult['close'];

			if($low=='') { $low = 0; } 
			if($high=='') { $high = 0; } 
			if($open=='') { $open = 0; } 
			if($close=='') { $close = 0; }
			$dat = array('date'=>$date,'open'=>$open,'high'=>$high,'low'=>$low,'close'=>$close);
			array_push($chartdata,$dat);
		}
		echo json_encode($chartdata);
    }

    //check if the given amount is upto withdraw limit
	public function checkSecondCurrency(Request $request) {
		$data = $request->all();
	  	if(session('user_id')!='') {
	  		$currency = $data['currency'];
	  		$amount = $data['amount'];
	  		
	  		if($currency != 'KRW') {
	  			$min = 0.00000001;
	  			if($amount < $min) {
	  				echo "Enter greater than 0.00000001";
	  			} else {
	  				if (!preg_match('/^\d{0,50}(\.\d{0,8})?$/i',$amount)) {
	  					echo "Only 8 decimals allowed after decimal point.";
	  				} else {
	  					echo "true";
	  				}
	  			}
	  		} else {
	  			$min = 0.01;
	  			if($amount < $min) {
	  				echo "Enter greater than 0.01";
	  			} else {
	  				if (!preg_match('/^\d{0,50}(\.\d{0,2})?$/i',$amount)) {
	  					echo "Only 2 decimals allowed after decimal point.";
	  				} else {
	  					echo "true";
	  				}
	  			}
	  		}
      	} else {
      		Session::flash('error', 'Please login to continue!');
	    	return Redirect::to('/');
      	}
	}
}