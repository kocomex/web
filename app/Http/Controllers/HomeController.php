<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Basic;
use App\contact;
use App\subscirber;


class HomeController extends Controller
{

	

	public function home()
	{
		$data['settings']=HomeController::mainfunction();

		$where = array('page_name'=>"Banner content1");
    	$data['banner1']= Basic::getsinglerow($where,'wiix_cms');

    	$ban2 = array('page_name'=>"Banner content2");
    	$data['banner2']= Basic::getsinglerow($ban2,'wiix_cms');

    	$easyuse = array('page_name'=>"Easy to Use");
    	$data['easyuse']= Basic::getsinglerow($easyuse,'wiix_cms');

    	$easyinterface = array('page_name'=>"Easy interface");
    	$data['easyinterface']= Basic::getsinglerow($easyinterface,'wiix_cms');

    	$speedreliable = array('page_name'=>"speed and reliable");
    	$data['speedreliable']= Basic::getsinglerow($speedreliable,'wiix_cms');

    	$efficient = array('page_name'=>"Efficient");
    	$data['efficient']= Basic::getsinglerow($efficient,'wiix_cms');

    	$where1 = array();
				$order= array('id','asc');
$data['trade_rates']= Basic::getmultiplerow($where1,'wiix_tradepair',$order);


		$data['title'] = "Home";
		$data['meta_keyword'] = "";
		$data['meta_description'] = "";
		return view('consumer.Home',$data);

	}

	public static function mainfunction()
	{
		$where = array('id'=>1);
    	$set= Basic::getsinglerow($where,'wiix_site_settings');

    	return $set;
	}

	public function about()
	{
		
		$data['settings']=HomeController::mainfunction();

		$where = array('page_name'=>'About Us');
		$data['about']= Basic::getsinglerow($where,'wiix_cms');


		$data['title'] = $data['about']->meta_title;
		$data['meta_keyword'] = $data['about']->meta_keyword;
		$data['meta_description'] = $data['about']->meta_description;

		return view('consumer.about',$data);

	}

	public function faq()
	{
		$data['settings']=HomeController::mainfunction();

		$where = array('page_name'=>'FAQ');
		$det=Basic::getsinglerow($where,'wiix_cms');
		
		$data['faq']= DB::table('wiix_faq')->where('status','1')->get();

		
		$data['title'] = $det->meta_title;
		$data['meta_keyword'] = $det->meta_keyword;
		$data['meta_description'] = $det->meta_description;

		return view('consumer.faq',$data);

	}

		public function terms()
	{
		$data['settings']=HomeController::mainfunction();

		$where = array('page_name'=>'Terms and condition');
		$data['terms']= Basic::getsinglerow($where,'wiix_cms');


		$data['title'] = $data['terms']->meta_title;
		$data['meta_keyword'] = $data['terms']->meta_keyword;
		$data['meta_description'] = $data['terms']->meta_description;

		return view('consumer.terms',$data);

	}

		public function policy()
	{
		$data['settings']=HomeController::mainfunction();

		$where = array('page_name'=>'Private Policy');
		$data['policy']= Basic::getsinglerow($where,'wiix_cms');


		$data['title'] = $data['policy']->meta_title;
		$data['meta_keyword'] = $data['policy']->meta_keyword;
		$data['meta_description'] = $data['policy']->meta_description;

		return view('consumer.privacy',$data);

	}

		public function fees()
	{
		$data['settings']=HomeController::mainfunction();
		$where = array('page_name'=>'Fees');
		$data['fees']= Basic::getsinglerow($where,'wiix_cms');


		$where1 = array();
				$order= array('id','asc');
$data['trade_fees']= Basic::getmultiplerow($where1,'wiix_trading_fee_structure',$order);




		$data['title'] = $data['fees']->meta_title;
		$data['meta_keyword'] = $data['fees']->meta_keyword;
		$data['meta_description'] = $data['fees']->meta_description;

		return view('consumer.fees',$data);

	}
public function howitworks()
{
	$data['settings']=HomeController::mainfunction();
	$where = array('page_name'=>'How it works');
		$data['works']= Basic::getsinglerow($where,'wiix_cms');


		$data['title'] = $data['works']->meta_title;
		$data['meta_keyword'] = $data['works']->meta_keyword;
		$data['meta_description'] = $data['works']->meta_description;

		return view('consumer.howitworks',$data);
}

	public function news()
	{
		$data['settings']=HomeController::mainfunction();

		$where = array('page_name'=>'News');
		$det=Basic::getsinglerow($where,'wiix_cms');
		
		$data['news']= DB::table('wiix_news')->where('status','1')->get();

		
		$data['title'] = $det->meta_title;
		$data['meta_keyword'] = $det->meta_keyword;
		$data['meta_description'] = $det->meta_description;

		return view('consumer.news',$data);

	}

	public function contact(Request $request)
	{
		if($request->isMethod('post'))
		{


			contact::insert(['user_name' => $request['user_name'],'user_mail' => $request['user_mail'],'user_subject'=>$request['user_subject'],'user_message'=>$request['user_message'],'status'=>0]);


			$msg=DB::table('wiix_email_templates')->where('mail_key','contact')->get();

			$getadminemail=Basic::getsinglerow(array("admin_id"=>1),'wiix_admin');
          $to=Helper::encrypt_decrypt("decrypt",$getadminemail->secret_key);



          $message=$msg[0]->message;
          $date=date('M d, Y');
          $message=str_replace("##FIRSTNAME##",$request['user_name'],$message);
          $message=str_replace("##EMAIL##",$request['user_mail'],$message);
          $message=str_replace("##SUBJECT##",$request['user_subject'],$message);
          $message=str_replace("##MESSAGE##",$request['user_message'],$message);
          $message=str_replace("##SITELOGO##",Helper::getSiteLogo(),$message);
          $message=str_replace("##SITENAME##",Helper::getSiteName(),$message);
         
         

      $email_template = 'Support Form';
   
      Helper::mailsend($email_template,$message,$to);

      $request->session()->flash('success','Your request was sent successfully!!! .Our team will respond you within 24hours');
						return Redirect::to('/');

		}
		$data['settings']=HomeController::mainfunction();

		$where = array('page_name'=>'contact');
		$det=Basic::getsinglerow($where,'wiix_cms');

		$data['title'] = $det->meta_title;
		$data['meta_keyword'] = $det->meta_keyword;
		$data['meta_description'] = $det->meta_description;
		return view('consumer.contact',$data);
	}
public function subscribe(Request $request)
	{
		if($request->isMethod('post'))
		{


			subscirber::insert(['email' => $request['email']]);

			$request->session()->flash('success','Your subscribe request send successfully!!! .Our team will respond you within 24hours');
						return Redirect::to('/');
		}
	}

	function subemail_check(Request $request)
{
	$email  = strtolower($request['email']);


$count=DB::table('wiix_subscribers')->where('email',$email)->count();

	
echo json_encode(($count > 0) ? FALSE : TRUE) ;



}


}