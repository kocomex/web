<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Cloudinary;

// use App\Model\Googleauthenticator;
use App\Model\User;
use App\Model\ConsumerVerification;

use Validator;
use Auth;
use Redirect;
use DB;
use URL;
use Session;
use Mail;

class Users extends Controller {

	public function __construct() {
		//Configure cdn with credentials
		Cloudinary::config(array(
		    "cloud_name" => "dh0kb7sbx",
		    "api_key" => "578432172636126",
		    "api_secret" => "BpYGipiQuAnzS8LohSmmtadF6iE"
		));
	}

	//To return profilr view page with user details 
	public function mypage() {
		$userId = session('wiix_userId');
		$getUser = User::where('id',$userId)->select('wiix_content','unusual_user_key','consumer_name','phone')->first();
		return view('front.profile.mypage')->with('user',$getUser);
	}
}