<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Helper;
use View;
use Input;
use Redirect;
use Validator;
use Session;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageHandler;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\moneroController;
use App\Basic;
use App\profile;
use App\wallet;
use App\bank_details;
use jsonRPCClient;

class adminCronController extends Controller
{
   
    //bitcoin admin cron function
	function admin_btc_deposit_process()
	{
		$cur_date = date('Y-m-d');
		$cur_time = date('H:i:s');
		
		$bitcoin_row 			=   Basic::getsinglerow(array('coinname'=>"bitcoin"),'wiix_cryptodetails');
		$bitcoin_username		=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password		=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber 	= 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress 		= 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress);
		$bitcoin 				= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
		$bitcoin_isvalid        =     $bitcoin->listtransactions();
        
        //echo "<pre>";
        //print_r($bitcoin_isvalid);
        //exit;
		
		if($bitcoin_isvalid)
		{
			for($i=0;$i<count($bitcoin_isvalid);$i++)
			{
				$account      =      $bitcoin_isvalid[$i]['account'];
				$category     =      $bitcoin_isvalid[$i]['category'];
				$btctxid      =      $bitcoin_isvalid[$i]['txid'];
					if($category=="receive"){
							$isvalid = $bitcoin->gettransaction($btctxid);
							echo "<pre>";
							//print_r($isvalid);

							$det_category  =   $isvalid['details'][0]['category'];
							if($det_category=="receive"){
								$btcaccount         =   $isvalid['details'][0]['account'];
								$btcaddress         =     $isvalid['details'][0]['address'];
								$bitcoin_balance    =     $isvalid['details'][0]['amount']; 
								$btcconfirmations   =     $isvalid['confirmations']; 
							}else{
								$btcaccount         =   $isvalid['details'][1]['account'];
								$btcaddress         =     $isvalid['details'][1]['address'];
								$bitcoin_balance    =     $isvalid['details'][1]['amount']; 
								$btcconfirmations   =     $isvalid['confirmations']; 
							}
							
							$dep_id              = $btctxid;
							$bitcoin_balance     = $bitcoin_balance;
							//check deposit already
							$dep_already = Basic::getsinglerow(array('currency'=>'BTC','reference_no'=>$dep_id,'type'=>'deposit'),'wiix_contactus');
								if(!$dep_already){
									//get admin adderss 
									$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
									$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->BTC_address);
									
										if(strtolower($btcaccount) == strtolower($adminaccount)){
											if( $btcconfirmations > 2){
												$ip = Controller::getIpAddress();
												$insdata = array('amount'=>$bitcoin_balance,'currency'=>"BTC",'reference_no'=>$dep_id, 'payment_method' =>'Bitcoin', 'status'=>'completed','ip_address'=>$ip, 'type'=>'deposit');
												$createWithdraw = ContactUs::create($insdata);
											}
										}
								}
					}
			}

		}
	}

	//ltccoin admin cron function
	function admin_ltc_deposit_process()
	{
		$cur_date = date('Y-m-d');
		$cur_time = date('H:i:s');
		
		$bitcoin_row 			=   Basic::getsinglerow(array('coinname'=>"litecoin"),'wiix_cryptodetails');
		$bitcoin_username		=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password		=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber 	= 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress 		= 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress);
		$bitcoin 				= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
		$bitcoin_isvalid        =     $bitcoin->listtransactions();
        
        //echo "<pre>";
        //print_r($bitcoin_isvalid);
        //exit;
		
		if($bitcoin_isvalid)
		{
			for($i=0;$i<count($bitcoin_isvalid);$i++)
			{
				$account      =      $bitcoin_isvalid[$i]['account'];
				$category     =      $bitcoin_isvalid[$i]['category'];
				$btctxid      =      $bitcoin_isvalid[$i]['txid'];
					if($category=="receive"){
							$isvalid = $bitcoin->gettransaction($btctxid);
							echo "<pre>";
							//print_r($isvalid);

							$det_category  =   $isvalid['details'][0]['category'];
							if($det_category=="receive"){
								$btcaccount         =   $isvalid['details'][0]['account'];
								$btcaddress         =     $isvalid['details'][0]['address'];
								$bitcoin_balance    =     $isvalid['details'][0]['amount']; 
								$btcconfirmations   =     $isvalid['confirmations']; 
							}else{
								$btcaccount         =   $isvalid['details'][1]['account'];
								$btcaddress         =     $isvalid['details'][1]['address'];
								$bitcoin_balance    =     $isvalid['details'][1]['amount']; 
								$btcconfirmations   =     $isvalid['confirmations']; 
							}
							
							$dep_id              = $btctxid;
							$bitcoin_balance     = $bitcoin_balance;
							//check deposit already
							$dep_already = Basic::getsinglerow(array('currency'=>'LTC','reference_no'=>$dep_id,'type'=>'deposit'),'wiix_contactus');
								if(!$dep_already){
									//get admin adderss 
									$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
									$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->LTC_address);
									
										if(strtolower($btcaccount) == strtolower($adminaccount)){
											if( $btcconfirmations > 2){
												$ip = Controller::getIpAddress();
												$insdata = array('amount'=>$bitcoin_balance,'currency'=>"LTC",'reference_no'=>$dep_id, 'payment_method' =>'LTCcoin', 'status'=>'completed','ip_address'=>$ip, 'type'=>'deposit');
												$createWithdraw = ContactUs::create($insdata);
											}
										}
								}
					}
			}

		}
	}

	//dogecoin admin cron function
	function admin_doge_deposit_process()
	{
		$cur_date = date('Y-m-d');
		$cur_time = date('H:i:s');
		
		$bitcoin_row 			=   Basic::getsinglerow(array('coinname'=>"dogecoin"),'wiix_cryptodetails');
		$bitcoin_username		=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password		=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber 	= 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress 		= 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress);
		$bitcoin 				= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
		$bitcoin_isvalid        =     $bitcoin->listtransactions();
        
        //echo "<pre>";
        //print_r($bitcoin_isvalid);
        //exit;
		
		if($bitcoin_isvalid)
		{
			for($i=0;$i<count($bitcoin_isvalid);$i++)
			{
				$account      =      $bitcoin_isvalid[$i]['account'];
				$category     =      $bitcoin_isvalid[$i]['category'];
				$btctxid      =      $bitcoin_isvalid[$i]['txid'];
					if($category=="receive"){
							$isvalid = $bitcoin->gettransaction($btctxid);
							echo "<pre>";
							//print_r($isvalid);

							$det_category  =   $isvalid['details'][0]['category'];
							if($det_category=="receive"){
								$btcaccount         =   $isvalid['details'][0]['account'];
								$btcaddress         =     $isvalid['details'][0]['address'];
								$bitcoin_balance    =     $isvalid['details'][0]['amount']; 
								$btcconfirmations   =     $isvalid['confirmations']; 
							}else{
								$btcaccount         =   $isvalid['details'][1]['account'];
								$btcaddress         =     $isvalid['details'][1]['address'];
								$bitcoin_balance    =     $isvalid['details'][1]['amount']; 
								$btcconfirmations   =     $isvalid['confirmations']; 
							}
							
							$dep_id              = $btctxid;
							$bitcoin_balance     = $bitcoin_balance;
							//check deposit already
							$dep_already = Basic::getsinglerow(array('currency'=>'DOGE','reference_no'=>$dep_id,'type'=>'deposit'),'wiix_contactus');
								if(!$dep_already){
									//get admin adderss 
									$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
									$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->DOGE_address);
									
										if(strtolower($btcaccount) == strtolower($adminaccount)){
											if( $btcconfirmations > 2){
												$ip = Controller::getIpAddress();
												$insdata = array('amount'=>$bitcoin_balance,'currency'=>"DOGE",'reference_no'=>$dep_id, 'payment_method' =>'DOGEcoin', 'status'=>'completed','ip_address'=>$ip, 'type'=>'deposit');
												$createWithdraw = ContactUs::create($insdata);
											}
										}
								}
					}
			}

		}
	}

	//dashcoin admin cron function
	function admin_dash_deposit_process()
	{
		$cur_date = date('Y-m-d');
		$cur_time = date('H:i:s');
		
		$bitcoin_row 			=   Basic::getsinglerow(array('coinname'=>"dashcoin"),'wiix_cryptodetails');
		$bitcoin_username		=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->username);
		$bitcoin_password		=	Helper::encrypt_decrypt("decrypt",$bitcoin_row->password);
		$bitcoin_portnumber 	= 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->portnumber); 
		$bitcoin_ipaddress 		= 	Helper::encrypt_decrypt("decrypt",$bitcoin_row->ipaddress);
		$bitcoin 				= new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@$bitcoin_ipaddress:$bitcoin_portnumber");
		$bitcoin_isvalid        =     $bitcoin->listtransactions();
        
        //echo "<pre>";
        //print_r($bitcoin_isvalid);
        //exit;
		
		if($bitcoin_isvalid)
		{
			for($i=0;$i<count($bitcoin_isvalid);$i++)
			{
				$account      =      $bitcoin_isvalid[$i]['account'];
				$category     =      $bitcoin_isvalid[$i]['category'];
				$btctxid      =      $bitcoin_isvalid[$i]['txid'];
					if($category=="receive"){
							$isvalid = $bitcoin->gettransaction($btctxid);
							echo "<pre>";
							//print_r($isvalid);

							$det_category  =   $isvalid['details'][0]['category'];
							if($det_category=="receive"){
								$btcaccount         =   $isvalid['details'][0]['account'];
								$btcaddress         =     $isvalid['details'][0]['address'];
								$bitcoin_balance    =     $isvalid['details'][0]['amount']; 
								$btcconfirmations   =     $isvalid['confirmations']; 
							}else{
								$btcaccount         =   $isvalid['details'][1]['account'];
								$btcaddress         =     $isvalid['details'][1]['address'];
								$bitcoin_balance    =     $isvalid['details'][1]['amount']; 
								$btcconfirmations   =     $isvalid['confirmations']; 
							}
							
							$dep_id              = $btctxid;
							$bitcoin_balance     = $bitcoin_balance;
							//check deposit already
							$dep_already = Basic::getsinglerow(array('currency'=>'DASH','reference_no'=>$dep_id,'type'=>'deposit'),'wiix_contactus');
								if(!$dep_already){
									//get admin adderss 
									$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
									$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->DASH_address);
									
										if(strtolower($btcaccount) == strtolower($adminaccount)){
											if( $btcconfirmations > 2){
												$ip = Controller::getIpAddress();
												$insdata = array('amount'=>$bitcoin_balance,'currency'=>"DASH",'reference_no'=>$dep_id, 'payment_method' =>'DASHcoin', 'status'=>'completed','ip_address'=>$ip, 'type'=>'deposit');
												$createWithdraw = ContactUs::create($insdata);
											}
										}
								}
					}
			}

		}
	}

	//ethcoin admin cron function
	function admin_eth_deposit_process() {
		
		//get admin adderss 
		$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
		$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->ETH_address);

		if($adminaccount != ""){

			/*$result = $this->db->query('SELECT MAX(blocknumber) as max_blocknumber FROM cc_transaction_history')->row();
			$max_blocknumber = $result ->max_blocknumber;
			if($max_blocknumber =="") {
		    	$max_blocknumber ="4085142";
		    } */
		    $max_blocknumber ="3531100";
		    $output = file_get_contents('https://api.etherscan.io/api?module=account&action=txlist&address='.$adminaccount.'&endblock=latest');
		    $result = json_decode($output);

		    if($result->message == 'OK'){
		    	$transaction=$result->result;
		    	for($tr=0;$tr<count($transaction);$tr++) {
		    		$block_number  = $transaction[$tr]->blockNumber;
		    		$address  = $transaction[$tr]->to; 
		    		$txid 		= $transaction[$tr]->hash;
		    		$value 		= $transaction[$tr]->value;
		    		$ether_balance = ($value/1000000000000000000); 
		    		$confirmations =$transaction[$tr]->confirmations;
		            	if($address == $adminaccount){
	                		if($transaction[$tr]->confirmations > 2){
	                			$dep_already = Basic::getsinglerow(array('currency'=>'ETH','reference_no'=>$txid,'type'=>'deposit'),'wiix_contactus');
								if(!$dep_already){
									//get admin adderss 
									$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
									$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->ETH_address);

									if(strtolower($address) == strtolower($adminaccount)){
										if( $confirmations > 2 ){
											$ip = Controller::getIpAddress();
											$insdata = array('amount'=>$ether_balance,'currency'=>"ETH",'reference_no'=>$txid, 'payment_method' =>'ETHcoin', 'status'=>'completed','ip_address'=>$ip, 'type'=>'deposit');
											$createWithdraw = ContactUs::create($insdata);
										}
									}
								}
							}
		            	}
		        }
		    }
		}
	}

	//etccoin admin cron function
	function admin_etc_deposit_process() {
		
		//get admin adderss 
		$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
		$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->ETC_address);

		if($adminaccount != ""){

			/*$result = $this->db->query('SELECT MAX(blocknumber) as max_blocknumber FROM cc_transaction_history')->row();
			$max_blocknumber = $result ->max_blocknumber;
			if($max_blocknumber =="") {
		    	$max_blocknumber ="4085142";
		    } */
		    $max_blocknumber ="3531100";
		    $output = file_get_contents('https://api.etherscan.io/api?module=account&action=txlist&address='.$adminaccount.'&endblock=latest');
		    $result = json_decode($output);

		    if($result->message == 'OK'){
		    	$transaction=$result->result;
		    	for($tr=0;$tr<count($transaction);$tr++) {
		    		$block_number  = $transaction[$tr]->blockNumber;
		    		$address  = $transaction[$tr]->to; 
		    		$txid 		= $transaction[$tr]->hash;
		    		$value 		= $transaction[$tr]->value;
		    		$ether_balance = ($value/1000000000000000000); 
		    		$confirmations =$transaction[$tr]->confirmations;
		            	if($address == $adminaccount){
	                		if($transaction[$tr]->confirmations > 2){
	                			$dep_already = Basic::getsinglerow(array('currency'=>'ETC','reference_no'=>$txid,'type'=>'deposit'),'wiix_contactus');
								if(!$dep_already){
									//get admin adderss 
									$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
									$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->ETC_address);

									if(strtolower($address) == strtolower($adminaccount)){
										if( $confirmations > 2 ){
											$ip = Controller::getIpAddress();
											$insdata = array('amount'=>$ether_balance,'currency'=>"ETC",'reference_no'=>$txid, 'payment_method' =>'ETCcoin', 'status'=>'completed','ip_address'=>$ip, 'type'=>'deposit');
											$createWithdraw = ContactUs::create($insdata);
										}
									}
								}
							}
		            	}
		        }
		    }
		}
	}
	
	//xmrcoin admin cron function
	function admin_xmr_deposit_process(){
		//get admin adderss 
		$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
		$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->XMR_address);
		$paymentid    = Helper::encrypt_decrypt("decrypt",$getngwalts->XMR_paymentid);

		if(trim($adminaccount) != ""){
			$max_blocknumber = 0;
			$payment_id 	 = trim($paymentid);
			if($payment_id!=''){
				$transactions = moneroController::get_bulk_payments($payment_id,$max_blocknumber);

				$output = json_decode(json_encode($transactions), True);
					for($i=0;$i<count($output['payments']);$i++){
						$unlock_time = $output['payments'][$i]['unlock_time'];
						
						if($unlock_time !== 0){
							continue;
						}

						$res_pid = $output['payments'][$i]['payment_id'];

						if (strpos($res_pid, $payment_id) !== false) {
							$amount       = $output['payments'][$i]['amount'] / 1000000000000;
							$block_height = $output['payments'][$i]['block_height'];
							$cpayment_id   = $output['payments'][$i]['payment_id'];
							$tx_hash      = $output['payments'][$i]['tx_hash'];

							$dep_already = Basic::getsinglerow(array('currency'=>'XMR','reference_no'=>$tx_hash,'type'=>'deposit'),'wiix_contactus');
							if(!$dep_already){
								$ip = Controller::getIpAddress();
								$insdata = array('amount'=>$amount,'currency'=>"XMR",'reference_no'=>$tx_hash, 'payment_method' =>'XMRcoin', 'status'=>'completed','ip_address'=>$ip, 'type'=>'deposit');
								$createWithdraw = ContactUs::create($insdata);
							}
						}
					}
			}	
		}
	}
	

	//xrpcoin admin cron function
	function admin_xrp_deposit_process(){
		//get admin adderss 
		$getngwalts=Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
		$adminaccount = Helper::encrypt_decrypt("decrypt",$getngwalts->XRP_address);
		
		if(trim($adminaccount) != ""){
			$url = "https://data.ripple.com/v2/accounts/" . $adminaccount . "/payments?currency=XRP&type=received"; 
			$output = file_get_contents($url);
			$result = json_decode($output,true);
			print_r($result);print_r("result");
			if($result['result'] == 'success' && count($result['payments'])){
				for($i=0;$i<count($result['payments']);$i++){
					$txidnew        =    $result['payments'][$i]['tx_hash']; 
					$address = $result['payments'][$i]['destination'];
                	$amount  = $result['payments'][$i]['amount'];
					$dep_already = Basic::getsinglerow(array('currency'=>'XRP','reference_no'=>$txidnew,'type'=>'deposit'),'wiix_contactus');
					if(!$dep_already){
						$ip = Controller::getIpAddress();
						$insdata = array('amount'=>$amount,'currency'=>"XRP",'reference_no'=>$txidnew, 'payment_method' =>'XRPcoin', 'status'=>'completed','ip_address'=>$ip, 'type'=>'deposit');
						$createWithdraw = ContactUs::create($insdata);
					}
				}
			}
		}	
	}
	

	
    

}
