<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminActivity extends Model
{
   protected $table = 'wiix_admin_activity';

    protected $guarded = [];
}
