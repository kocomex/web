<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTemp extends Model
{
   protected $table = 'wiix_ordertemp';

    protected $guarded = [];
}
