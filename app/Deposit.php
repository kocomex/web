<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{

    public $table = 'wiix_mydeposits';
    protected $guarded = array();
    protected $fillable = array();
}
