<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Trade extends Model
{
    public static function get_usdvolume($firstCurrency,$secondCurrency,$user_id) {
    	$curdate = date('Y-m-d');
    	$start_date = date('Y-m-d', strtotime($curdate . ' -1 month'));

    	$query = CoinOrder::where('user_id',$user_id)
				->where('firstCurrency',$firstCurrency)
				->where('secondCurrency',$secondCurrency)
				->whereDate('orderDate', '>=', $start_date)
				->whereDate('orderDate', '<=', $curdate)
				->whereIn('status', ['filled', 'partially'])
				->select(DB::raw('SUM(Total) as usdvolume'))->first();
    	if($query->count() == 0) {
			return false;
		} else {
			return $query->usdvolume;
		}
    }

    public static function getfeedetails_new($currency) {
    	$query = TradingFee::where('currency_pair',$currency)->get();

    	if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
    }

    public static function lowestaskprice1($firstCurrency,$secondCurrency) {
    	$query = CoinOrder::where('firstCurrency',$firstCurrency)
				->where('secondCurrency',$secondCurrency)
				->where('Type', 'Buy')
				->whereIn('status', ['active', 'partially'])
				->orderBy('Price', 'desc')->limit(1)->get();
    	if($query->isEmpty()) {
			$buy_rate = TradePairs::select('buy_rate')
						->where('from_symbol',$firstCurrency)
						->where('to_symbol',$secondCurrency)->get();

			return $buy_rate[0]->buy_rate;
		} else {
			return $query[0]->Price;
		}
    }

    public static function highestbidprice1($firstCurrency,$secondCurrency) {
    	$query = CoinOrder::where('firstCurrency',$firstCurrency)
				->where('secondCurrency',$secondCurrency)
				->where('Type', 'Sell')
				->whereIn('status', ['active', 'partially'])
				->orderBy('Price', 'asc')->limit(1)->get();
    	if($query->isEmpty()) {
			$sell_rate = TradePairs::select('sell_rate')
						->where('from_symbol',$firstCurrency)
						->where('to_symbol',$secondCurrency)->get();
			return $sell_rate[0]->sell_rate;
		} else {
			return $query[0]->Price;
		}
    }

    public static function countsellorder($type,$secondCurrency) {
    	$query = CoinOrder::where('secondCurrency',$secondCurrency)
				->where('Type', $type)
				->whereIn('status', ['active', 'partially'])
				->orderBy('Price', 'asc')->count();
    	return $query;
    }

    public static function get_active_order($firstCurrency,$secondCurrency,$user_id) {
    	$query = CoinOrder::where('firstCurrency',$firstCurrency)
				->where('secondCurrency',$secondCurrency)
				->where('user_id', $user_id)
				->whereIn('status', ['active', 'partially'])
				->orderBy('id', 'desc')->get();
    	if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
    }

    public static function fetchCoinorder($type,$one,$two) {
    	if($type == "Sell") {
    		$query = CoinOrder::where('firstCurrency',$one)
    				->where('secondCurrency',$two)
    				->where('Type', $type)
    				->whereIn('status', ['active', 'partially'])
    				->orderBy('Price', 'asc')->groupBy('Price')
    				->select(DB::raw('SUM(Amount) as amount'),'id','Price','secondCurrency')->get();
    	} else {
    		$query = CoinOrder::where('firstCurrency',$one)
    				->where('secondCurrency',$two)
    				->where('Type', $type)
    				->whereIn('status', ['active', 'partially'])
    				->orderBy('Price', 'desc')->groupBy('Price')
    				->select(DB::raw('SUM(Amount) as amount'),'id','Price','secondCurrency')->get();
    	}


    	if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
    }

    public static function get_stop_orders($from_symbol,$to_symbol,$user_id) {
    	$query = CoinOrder::where('user_id',$user_id)
    	->where('firstCurrency',$from_symbol)
				->where('secondCurrency',$to_symbol)
				->whereIn('status', ['trailing_stop', 'stoporder'])
				->get();
				
		if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
    }

    public static function fetchTransactionHistory($firstCurrency,$secondCurrency) {
    	$query = CoinOrder::where('firstCurrency',$firstCurrency)
				->where('secondCurrency',$secondCurrency)
				->where('status', 'filled')
				->orderBy('tradetime', 'desc')->get();
		if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
    }

    public static function getUserTradeHistory($first,$second,$userId) {
    	$query = CoinOrder::where('firstCurrency',$first)
				->where('secondCurrency',$second)
				->where('user_id', $userId)
				->whereIn('status', ['cancelled', 'partially', 'filled'])
				->orderBy('tradetime', 'desc')->get();
		if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
    }

    public static function checkOrdertempdetails($id,$type) {
    	if($type=="Buy") {
    		$query = OrderTemp::where('buyorderId',$id)
    				->select(DB::raw('SUM(filledAmount) as totalamount'))->first();
    	} else {
    		$query = OrderTemp::where('sellorderId',$id)
    				->select(DB::raw('SUM(filledAmount) as totalamount'))->first();
    	}
    	if($query->count() == 0) {
			return false;
		} else {
			return $query->totalamount;
		}
    }

    public static function fetchuserbalancebyId($id,$currency) {
    	$query = wallet::where('user_id',$id)->first();
    	if($query->count() == 0) {
			return false;
		} else {
			$currency = trim($currency);
			$getValue = Trade::to_decimal($query->$currency);
			return $getValue;
		}
    }

    public static function to_decimal($value, $places=8) {
	    if(trim($value)=='')
	    	return 0;
	    else if((float)$value==0)
	    	return 0;
	    if((float)$value==(int)$value)
	    	return (int)$value;   
	    else {       
	        $value = number_format($value, $places, '.','');
	        $value1 = $value;                   
	        if(substr($value,-1) == '0')
	        $value = substr($value,0,strlen($value)-1);
	        if(substr($value,-1) == '0')
	        $value = substr($value,0,strlen($value)-1);
	        if(substr($value,-1) == '0')
	        $value = substr($value,0,strlen($value)-1);
	        if(substr($value,-1) == '0')
	        $value = substr($value,0,strlen($value)-1);
	        if(substr($value,-1) == '0')
	        $value = substr($value,0,strlen($value)-1);
	        if(substr($value,-1) == '0')
	        $value = substr($value,0,strlen($value)-1);     
	        if(substr($value,-1) == '0')
	        $value = substr($value,0,strlen($value)-1);         
	        return $value;
	    }
	}

	//mapping
    public static function mapping($type,$pair,$freshorderid) {
        $result = Trade::check_stop_order($pair);
        $final="";
        $buyResult = Trade::getBuyorders();



        if($buyResult) {
            foreach($buyResult as $buy) {
                $buyorderId = $buy->id; 
                $buyuserId = $buy->user_id;
                $buyPrice = $buy->Price;
                $buyAmount = $buy->Amount;
                $buyfirstCurrency = $buy->firstCurrency;
                $buysecondCurrency = $buy->secondCurrency;
                $buyType = $buy->Type; 
                $fee_per = $buy->fee_per; 
                $buySumamount = Trade::checkbuyOrdertemp($buyorderId);
                if($buySumamount) {
                    $buySumamount= round($buyAmount-$buySumamount,8);
                } else {
                    $buySumamount = $buyAmount;
                }

                $fetchsellRecords = Trade::getParticularsellorders($buyPrice,$buyuserId,$buyfirstCurrency,$buysecondCurrency);
                if($fetchsellRecords) {
                    $k=0;
                    foreach($fetchsellRecords as $sell) {
                        $k++;
                        $sellorderId = $sell->id;
                        $selluserId = $sell->user_id;
                        $sellPrice = $sell->Price;
                        $sellAmount = $sell->Amount;
                        $sellfirstCurrency = $sell->firstCurrency;
                        $sellSumamount = Trade::checkOrdertemp($sellorderId);
                        if($sellSumamount) {
                            $approxiAmount = $sellAmount-$sellSumamount;
                        } else {
                            $approxiAmount = $sellAmount;
                        }

                        if($approxiAmount >= $buySumamount) {
                            $amount = $buySumamount;
                        } else {
                            $amount = $approxiAmount;
                        }

                        $buy_status = Trade::get_buyorder_status($buyorderId);
                        if($buy_status) {
                            $inserted = Trade::insertOrdertemp($sellorderId,$selluserId,$sellAmount,$sellPrice,$amount,$buyorderId,$buyuserId,$buyfirstCurrency,$buysecondCurrency);
                            if($inserted) {
                                if($buyPrice>$sellPrice) {
                                    $theftprice =   ($amount*$buyPrice)-($amount*$sellPrice);
                                    Trade::insertTheftprice($theftprice,$buyuserId,$buysecondCurrency,$buyType,$buySumamount,$fee_per);
                                }
                                if(trim($approxiAmount)==trim($amount)) {
                                    $tradeSell = Trade::removeOrder($sellorderId,"Sell",$inserted,$approxiAmount,$sellfirstCurrency);
                                    $final = $amount;
                                    $msg1 = "Your sell order of ".$final." ".$sellfirstCurrency." has been traded successfully";
                                } else {
                                    $tradeSell = Trade::changeOrderstatus($sellorderId,$amount,"Sell",$buyorderId);
                                    $final = $amount;
                                    $msg1 = "Your sell order of ".$final." ".$sellfirstCurrency." has been traded partially";
                                }
                              
                                //complete buyer order
                                if($approxiAmount==$buySumamount) {
                                    $tradeBuy = Trade::removeOrder($buyorderId,"Buy",$inserted,$buySumamount,$buyfirstCurrency);
                                    $final = $amount;
                                    $msg2 = "Your buy order of ".$final." ".$buysecondCurrency." has been traded successfully";
                                } else if($approxiAmount>$buySumamount) {
                                    $tradeBuy = Trade::removeOrder($buyorderId,"Buy",$inserted,$buySumamount,$buyfirstCurrency);
                                    $final = $amount;
                                    $msg2 = "Your buy order of ".$final." ".$buysecondCurrency." has been traded successfully";
                                } else {
                                    $tradeBuy = Trade::changeOrderstatus($buyorderId,$amount,"Buy",$sellorderId);
                                    $buyfillamount = Trade::get_buy_ordertemp($buyorderId);
                                    $buySumamount = $buyAmount-$buyfillamount;
                                    $final = $amount;
                                    $msg2 = "Your buy order of ".$final." ".$buysecondCurrency." has been traded partially";
                                }
                               
                            
                            }
                        }
                    }
                }
            }
        }
        // $result = Trade::check_stop_order($pair);
        $filled_final = Trade::testFunction($freshorderid);
        if($filled_final) {
            return $filled_final;
        }
        $partially_final = Trade::testFunction2($type,$freshorderid);
        if($partially_final) {
            return $partially_final;
        }
        $active_final = Trade::testFunction3($freshorderid);
      
        if($active_final) {
            return "empty";
        }
    }

	public static function check_stop_order($pair) {
		$pair1 = explode("/", $pair);
	    $first_currency = $pair1[0];
	    $second_currency = $pair1[1];

	    $sell_rate = Trade::lowestaskprice1($first_currency,$second_currency);
	    $buy_rate = Trade::highestbidprice1($first_currency,$second_currency);
	    $stop_orders = Trade::get_sell_stoporders($sell_rate);

	    if($stop_orders) {
	    	foreach($stop_orders as $sell_row) {
	    		$status = $sell_row->status;
	            $trade_id = $sell_row->id;
	            $trigger_price = $sell_row->trigger_price;
	            $Type = $sell_row->Type;
	            $Fee = $sell_row->Fee;
	            $amount = $sell_row->Amount;
	            $stoporderprice = $sell_row->stoporderprice;
	            if($status=='stoporder') {
	            	$total = ($amount*$buy_rate)-$Fee;
	            	CoinOrder::where('id',$trade_id)->update(['Price'=>$stoporderprice,'Total'=>$total,'status'=>'active']);
	            }
	    	}
	    	Trade::mapping($Type,$pair,$trade_id);
	    }
	    $buystop_orders = Trade::get_buy_stoporders($buy_rate);
	    if($buystop_orders) {
	    	foreach($buystop_orders as $buy_row) {
	    		$status = $buy_row->status;
	            $trade_id = $buy_row->id;
	            $trigger_price = $buy_row->trigger_price;
	            $Fee = $buy_row->Fee;
	            $Type = $buy_row->Type;
	            $amount = $buy_row->Amount;
	            $stoporderprice = $buy_row->stoporderprice;
	            if($status=='stoporder') {
	            	$total = ($amount*$buy_rate)+$Fee;
	            	CoinOrder::where('id',$trade_id)->update(['Price'=>$stoporderprice,'Total'=>$total,'status'=>'active']);
	            }
	    	}
	    	Trade::mapping($Type,$pair,$trade_id);
	    }
	    return true;
	}

	public static function getBuyorders() {
		$query = CoinOrder::where('Type', 'Buy')
				->whereIn('status', ['active', 'partially'])
				->orderBy('Price', 'desc')->get();
    	if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
	}

	public static function checkbuyOrdertemp($id) {
		$query = OrderTemp::where('buyorderId',$id)
				->select(DB::raw('SUM(filledAmount) as totalamount'))->first();
		if($query->count() == 0) {
			return false;
		} else {
			return $query->totalamount;
		}
	}

	public static function getParticularsellorders($buyPrice,$buyuserId,$firstCurrency,$secondCurrency) {
		$query = CoinOrder::where('firstCurrency',$firstCurrency)
				->where('secondCurrency',$secondCurrency)
				->where('user_id', '!=' ,$buyuserId)
				->where('Price', '<=', $buyPrice)
				->where('Type', 'Sell')
				->whereIn('status', ['active', 'partially'])
				->orderBy('Price', 'asc')->get();
		if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
	}

	public static function get_buyorder_status($buyorderId) {
		$query = CoinOrder::where('id',$buyorderId)->first();
		if($query->count() == 0) {
			return false;
		} else {
			$status = $query->status;
			if($status=='partially' || $status=='active') {
				return true;
			} else {
				return false;
			}
		}
	}

	public static function insertOrdertemp($sellorderId,$selluserId,$sellAmount,$sellPrice,$amount,$buyorderId,$buyuserId,$buyfirstCurrency,$buysecondCurrency) {
		$date   =date('Y-m-d');
	    $time   =date("H:i:s");
	    $datetime   =date("Y-m-d H:i:s");
	    $pair = $buyfirstCurrency."/".$buysecondCurrency;
	    $data   = array(
                'sellorderId'=>$sellorderId,
                'sellerUserid'=>$selluserId,
                'askAmount'=>$sellAmount,
                'askPrice'=>$sellPrice,
                'firstCurrency'=>$buyfirstCurrency,
                'secondCurrency'=>$buysecondCurrency,
                'filledAmount'=>$amount,
                'buyorderId'=>$buyorderId,
                'buyerUserid'=>$buyuserId,
                'sellerStatus'=>"inactive",
                'buyerStatus'=>"inactive",
                "date"=>$date,
                "time"=>$time,
                "pair"=>$pair,
                "datetime"=>$datetime
            );
	    $createOrderTemp = OrderTemp::create($data);
	    $insid = $createOrderTemp->id;

	    //fetch buyer order details
	    $buytrade = Trade::getCoinorder($buyorderId);
	    $tradetradeId = $buytrade->id;
        $tradeuserId = $buytrade->user_id;
        $tradePrice = $buytrade->Price;
        $tradeAmount = $buytrade->Amount;
        $tradeFee = $buytrade->Fee;
        $tradeType = $buytrade->Type;
        $tradeTotal = $buytrade->Total;
        $tradefirstCurrency = $buytrade->firstCurrency;
        $tradesecondCurrency = $buytrade->secondCurrency;
        
        $CalculatebuyTotal = $amount*$tradePrice;

        $buycomment = "Buy ".$amount." ".$tradefirstCurrency."  (-0.2%) from order #".$tradetradeId ."by price ".$tradePrice." ".$tradesecondCurrency;

        //insert for buyer
    	$buyertransactiondata = array(
		                    "user_id"=>$tradeuserId,
		                    "order_id"=>$tradetradeId,
		                    "type"=>"Trade",
		                    "first_currency"=>$tradefirstCurrency,
		                    "second_currency"=>$tradesecondCurrency,
		                    "amount"=>$amount,
		                    "price"=>$tradePrice,
		                    "total"=>$CalculatebuyTotal,
		                    "fee"=>$tradeFee,
		                    "comment"=>$buycomment,
		                    "t_status"=>"active"
                    	);
    	Transaction::create($buyertransactiondata);

    	//fetch seller order details
    	$selltrade = Trade::getCoinorder($sellorderId);
        $selltradeId = $selltrade->id;
        $selluserId = $selltrade->user_id;
        $sellPrice = $selltrade->Price;
        $sellAmount = $selltrade->Amount;
        $sellFee = $selltrade->Fee;
        $sellType = $selltrade->Type;
        $sellTotal = $selltrade->Total;
        $sellfirstCurrency = $selltrade->firstCurrency;
        $sellsecondCurrency = $selltrade->secondCurrency;
        
        $CalculatesellTotal = $amount*$sellPrice;
   
         $sellcomment = "Bought ".$amount." ".$sellfirstCurrency." from order #".$selltradeId ."by price ".$sellPrice." ".$sellsecondCurrency." total ".$CalculatesellTotal." ".$sellsecondCurrency." (-0.2%)";
        $sellertransactiondata = array(
			            "user_id"=>$selluserId,
			            "order_id"=>$selltradeId,
			            "type"=>"Trade",
			            "first_currency"=>$sellfirstCurrency,
			            "second_currency"=>$sellsecondCurrency,
			            "amount"=>$amount,
			            "price"=>$sellPrice,
			            "total"=>$CalculatesellTotal,
			            "fee"=>$sellFee,
			            "comment"=>$sellcomment,
			            "t_status"=>"active"
			        );
        Transaction::create($buyertransactiondata);
        return $insid;
	}

	public static function insertTheftprice($theftprice,$buyuserId,$buysecondCurrency) {
		$date = date('Y-m-d');
    	$time = date("H:i:s");
    	$data = array(
    		'userId'=>$buyuserId,
            'theftAmount'=>$theftprice,
            'theftCurrency'=>$buysecondCurrency,
            
            'user_amount' => 0,
            'Type'=>'Trade Bonus'
    	);
    	$query = CoinProfit::create($data);
	}

	public static function removeOrder($id,$status,$inserted,$approxiAmount,$sellfirstCurrency) {
		$request_time=date("Y-m-d H:i:s");
		// update status "filled" into coin order
		$query = CoinOrder::where('id',$id)->update(['status'=>"filled",'tradetime'=>$request_time]);
		if($query) {
			if($status=="Buy") {
				OrderTemp::where('id',$inserted)->where('buyorderId',$id)->update(['buyerStatus'=>"active"]);
			} else {
				OrderTemp::where('id',$inserted)->where('sellorderId',$id)->update(['sellerStatus'=>"active"]);
			}

			$trade = Trade::getCoinorder($id);
			$tradetradeId = $trade->id;
	        $tradeuserId = $trade->user_id;
	        $tradePrice = $trade->Price;
	        $tradeAmount = $trade->Amount; 
	        $tradeFee = $trade->Fee;
	        $tradeType = $trade->Type;
	        $fee_pers = $trade->fee_per;
	        $tradeTotal = $trade->Total;
	        $tradefirstCurrency = $trade->firstCurrency;
	        $tradesecondCurrency = $trade->secondCurrency;
	        $orderDate = $trade->orderDate;
	        $orderTime = $trade->orderTime;
	        Trade::get_theft_fee($tradeuserId,$tradeType,$approxiAmount,$tradePrice,$fee_pers,$tradesecondCurrency);
	        if($tradesecondCurrency == "BTC") {
				$digits = 8;
			} else {
				$digits = 2;
			}
	        if($tradeType=="Buy") {
	        	$con_result = Trade::conscutive_order($tradeAmount,$tradefirstCurrency,$tradesecondCurrency,$tradeuserId,$tradePrice);
	        	$userbalance = Trade::fetchuserbalancebyId($tradeuserId,$tradefirstCurrency);
	        	$updatebuyBalance = $userbalance+$approxiAmount;
	        	$updatebuyBalance = number_format($updatebuyBalance,8, '.', '');
	        	wallet::where('user_id',$tradeuserId)->update([$tradefirstCurrency=>$updatebuyBalance]);
	        } else {
	        	$userbalance = Trade::fetchuserbalancebyId($tradeuserId,$tradesecondCurrency);
	        	$con_result = Trade::conscutive_sellorder($tradeAmount,$tradefirstCurrency,$tradesecondCurrency,$tradeuserId,$tradePrice);
	        	$tradetoalcal = $approxiAmount * $tradePrice;
                $tradefee = $tradetoalcal * $fee_pers / 100;
                $updatesellBalance  = $userbalance+$tradetoalcal-$tradefee;
                $updatesellBalance  = number_format($updatesellBalance,$digits, '.', '');
                wallet::where('user_id',$tradeuserId)->update([$tradesecondCurrency=>$updatesellBalance]);
	        }
	        
            return true;
		} else {
			return false;
		}
	}

	public static function changeOrderstatus($id,$amount,$type,$second_id) {
		$result = CoinOrder::where('id',$id)->update(['status'=>'partially']);

		$order = Trade::getCoinorder($id);
		if($order) {
			$userId = $order->user_id;
	        $Type = $order->Type;
	        $tradepair = $order->pair;
	        $activeAmount = $order->Amount;
	        $activeTradeid = $order->id;
	        $Total = $order->Total;
	        $fee = $order->Fee;
	        $fee_per = $order->fee_per;
	        $activePrice = $order->Price; 
	        $tradesecondCurrency = $order->secondCurrency;
	        $tradefirstCurrency = $order->firstCurrency;
	        $api_trade_id = $order->api_trade_id;
	        $pair = $order->pair;
		}

		if($Type == "Buy") {
			$type = "Trade Buy";
			$order1 = Trade::getCoinorder($second_id);
			if($order1) {
				$userId1 = $order1->user_id;
			}
			$activefilledAmount = $activeAmount-$amount;
	        $fee_partial = ($amount*$activePrice) * $fee_per / 100;
	        $currentusdbalance = Trade::fetchuserbalancebyId($userId,$tradefirstCurrency);

	        $date    = date('Y-m-d');
        	$time    = date("h:i:s");
        	$data = array(
		        'userId'=>$userId,
		        'theftAmount'=>$fee_partial,
		        'theftCurrency'=>$tradesecondCurrency,
		        'user_amount' => 0,
		        'type'=>$type,
		       
	        );
	        $query = CoinProfit::create($data);

	        // coin profit for partial order end
	        $activeCalcTotal = $amount*$activePrice; 
	        $updateusdbalance = $currentusdbalance+$amount;
	        $updateusdbalance = number_format($updateusdbalance,8, '.', '');
	        wallet::where('user_id',$userId)->update([$tradefirstCurrency=>$updateusdbalance]);
		} else {
			$type = "Trade Sell";
			$currentusdbalance = Trade::fetchuserbalancebyId($userId,$tradesecondCurrency);
			$fee_partial = ($amount*$activePrice) * $fee_per / 100;

			$date = date('Y-m-d');
        	$time = date("h:i:s");
        	$data = array(
		        'userId'=>$userId,
		        'theftAmount'=>$fee_partial,
		        'theftCurrency'=>$tradesecondCurrency,
		        'user_amount' => 0,
		        'type'=>$type,
		        
	        );
	        $query = CoinProfit::create($data);
	        if($tradesecondCurrency == "BTC") {
				$digits = 8;
			} else {
				$digits = 2;
			}

	        $activeCalcTotal = $amount*$activePrice; 
        	$updateusdbalance = $currentusdbalance+$activeCalcTotal-$fee_partial;
        	$updateusdbalance = number_format($updateusdbalance,$digits, '.', '');
        	wallet::where('user_id',$userId)->update([$tradesecondCurrency=>$updateusdbalance]);
		}
		return true;
	}

	public static function get_buy_ordertemp($id) {
		$query = OrderTemp::where('buyorderId',$id)
				->select(DB::raw('SUM(filledAmount) as totalamount'))->first();

    	if($query->count() == 0) {
			return false;
		} else {
			return $query->totalamount;
		}
	}

	public static function testFunction($id) {
		$query = CoinOrder::where('id',$id)
				->where('status','filled')->select('Amount')->get();
		if($query->isEmpty()) {
			return false;
		} else {
			return $query[0]->Amount;
		}
	}

	public static function testFunction2($type,$id) {
		$query = CoinOrder::where('id',$id)
				->where('status','partially')->get();
		if($query->isEmpty()) {
			return false;
		} else {
			if($type=="buy") {
				$queryonce = OrderTemp::where('buyorderId',$id)->select('filledAmount')->get();
			} else {
				$queryonce = OrderTemp::where('sellorderId',$id)->select('filledAmount')->get();
			}
			if($queryonce->isEmpty()) {
				return false;
			} else {
				return $queryonce[0]->filledAmount;
			}
		}
	}

	public static function testFunction3($id) {
		$query = CoinOrder::where('id',$id)
				->where('status','active')
				->select('Amount','Type','Total','Price','firstCurrency')->get();

		if($query->isEmpty()) {
			return false;
		} else {
			$amount = $query[0]->Amount;
	        $Type = $query[0]->Type;
	        $Total = $query[0]->Total;
	        $Price = $query[0]->Price;
	        $firstCurrency = $query[0]->firstCurrency;


				
	        if($firstCurrency=='BTC') {
	        	return true;
	        } else {
	        	return false;
	        }
		}
	}

	public static function get_sell_stoporders($buy_rate) {
		$query = CoinOrder::where('stoporderprice','>=',$buy_rate)
				->where('Type','Sell')
				->whereIn('status', ['stoporder', 'trailing_stop'])->get();
		if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
	}

	public static function get_buy_stoporders($buy_rate) {
		$query = CoinOrder::where('stoporderprice','<=',$buy_rate)
				->where('Type','Buy')
				->whereIn('status', ['stoporder', 'trailing_stop'])->get();
		if($query->isEmpty()) {
			return false;
		} else {
			return $query;
		}
	}

	public static function getCoinorder($id) {
		$query = CoinOrder::where('id',$id)->first();
		if($query->count() == 0) {
			return false;
		} else {
			return $query;
		}
	}

	public static function conscutive_order($tradeAmount,$tradefirstCurrency,$tradesecondCurrency,$tradeuserId,$tradePrice) {
		CoinOrder::where('Amount',$tradeAmount)
			->where('status',"buyconsecutive")
			->where('user_id',$tradeuserId)
			->where('firstCurrency',$tradefirstCurrency)
			->where('secondCurrency',$tradesecondCurrency)
			->update(['status'=>'active']);
		return true;
	}

	public static function conscutive_sellorder($tradeAmount,$tradefirstCurrency,$tradesecondCurrency,$tradeuserId,$tradePrice) {
		CoinOrder::where('Amount',$tradeAmount)
			->where('status',"sellconsecutive")
			->where('user_id',$tradeuserId)
			->where('firstCurrency',$tradefirstCurrency)
			->where('secondCurrency',$tradesecondCurrency)
			->update(['status'=>'active']);
		return true;
	}

	public static function cancel_stop_order($id) {
		$query = CoinOrder::where('id',$id)->first();
		if($query->count() == 0) {
			return false;
		} else {
			$userId = $query->user_id;
	        $amount = $query->Amount;
	        $stoporderprice = $query->stoporderprice;
	        $fees_amt = $query->Fee;
	        $type = $query->Type;
	        $total = $query->Total;
	        $firstCurrency=$query->firstCurrency;
	        $secondcurrency=$query->secondCurrency;
	        if($secondcurrency == "BTC") {
				$digits = 8;
			} else {
				$digits = 2;
			}
	        if($type=='Buy') {
	        	$balance = Trade::fetchuserbalancebyId($userId,$secondcurrency);
	        	$update_bal = $balance + $total;
	        	$update_bal = number_format($update_bal,$digits, '.', '');
	        	wallet::where('user_id',$userId)->update([$secondcurrency=>$update_bal]);
	        	CoinOrder::where('id',$id)->update(['status'=>'cancel stop order','tradetime'=>date('Y-m-d H:i:s')]);
	        	return $secondcurrency;
	        } else {
	        	$balance = Trade::fetchuserbalancebyId($userId,$firstCurrency);
	        	$update_bal = $balance+$amount;
	        	$update_bal = number_format($update_bal,8, '.', '');
	        	wallet::where('user_id',$userId)->update([$firstCurrency=>$update_bal]);
	        	CoinOrder::where('id',$id)->update(['status'=>'cancel stop order','tradetime'=>date('Y-m-d H:i:s')]);
	        	return $firstCurrency;
	        }
		}
	}

	public static function remove_active_model($id) {
		$order = Trade::getCoinorder($id);
		if(!empty($order)) {
			if($order) {
				$userId = $order->user_id;
	            $Type = $order->Type;
	            $tradepair = $order->pair;
	            $activeAmount = $order->Amount;
	            $activeTradeid = $order->id;
	            $Total = $order->Total;
	            $fee = $order->Fee;
	            $pair = $order->pair;
	            $fee_per = $order->fee_per;
	            $activePrice = $order->Price;
	            $tradesecondCurrency = $order->secondCurrency;
	            $tradefirstCurrency = $order->firstCurrency;
	            if($tradesecondCurrency == "BTC") {
					$digits = 8;
				} else {
					$digits = 2;
				}
	            if($Type=="Buy") {
	            	$activefilledAmount = Trade::checkOrdertempdetails($activeTradeid,$Type);
	            	if($activefilledAmount) {
	            		$activefilledAmount = $activeAmount-$activefilledAmount;
	            	} else {
	            		$activefilledAmount = $activeAmount;
	            	}
	            	$feess = ($activefilledAmount*$activePrice) * $fee_per / 100;
	            	$activeCalcTotal = $activefilledAmount*$activePrice+$feess;
	            	$currentbalance = Trade::fetchCurrencyBalance($tradesecondCurrency,$userId);
	            	$updatebalance = $currentbalance+$activeCalcTotal;
	            	$updatebalance = number_format($updatebalance,$digits, '.', '');
	            	wallet::where('user_id',$userId)->update([$tradesecondCurrency=>$updatebalance]);
	            } else if($Type=="Sell") {
	            	$activefilledAmount = Trade::checkOrdertempdetails($activeTradeid,$Type);
	            	if($activefilledAmount) {
	            		$activefilledAmount = $activeAmount-$activefilledAmount;
	            	} else {
	            		$activefilledAmount = $activeAmount;
	            	}
	            	$tradefirstCurrency = $order->firstCurrency;
	            	$currentbalance = Trade::fetchCurrencyBalance($tradefirstCurrency,$userId);
	            	$updatebalance = $currentbalance+$activefilledAmount;
	            	$updatebalance = number_format($updatebalance,8, '.', '');
	            	wallet::where('user_id',$userId)->update([$tradefirstCurrency=>$updatebalance]);
	            }
			}
			$query = CoinOrder::where('id',$id)->update(['status'=>"cancelled",'tradetime'=>date('Y-m-d H:i:s')]);
			if($query) {
				// fetch order details  
	            $tradetradeId = $order->id;
	            $tradeuserId = $order->user_id;
	            $tradePrice = $order->Price;
	            $tradeAmount = $order->Amount;
	            $tradeFee = $order->Fee;
	            $tradeType = $order->Type;
	            $tradeTotal = $order->Total;
	            $tradefirstCurrency = $order->firstCurrency;
	            $tradesecondCurrency = $order->secondCurrency;
	            $orderDate = $order->orderDate;
	            $orderTime = $order->orderTime;
                //Cancel order #165409225
                $comment = "Cancel order #".$tradetradeId;

                $transactiondata = array (
		            "user_id" =>  $tradeuserId,
		            "order_id" =>  $tradetradeId,
		            "type" =>  "Cancel",
		            "first_currency" =>  $tradefirstCurrency,
		            "second_currency" =>  $tradesecondCurrency,
		            "amount" =>  $tradeAmount,
		            "price" =>  $tradePrice,
		            "total" =>  $tradeTotal,
		            "comment"  =>  $comment,
		            "t_status" =>  "active"
	            );
	            Transaction::create($transactiondata);
			}
			$result = Trade::check_stop_order($pair);
		} else {
			return false;
		}
	}

	public static function fetchCurrencyBalance($currency,$user_id) {
		$query = wallet::where('user_id',$user_id)->first();
		if($query->count() == 0) {
			return "0";
		} else {
			$value = $query->$currency;
			return Trade::to_decimal($value);
		}
	}

	public static function checkOrdertemp($id) {
		$query = OrderTemp::where('sellorderId',$id)
				->select(DB::raw('SUM(filledAmount) as totalamount'))->first();
    	if($query->count() == 0) {
			return false;
		} else {
			return $query->totalamount;
		}
	}

	public static function get_theft_fee($id,$Type,$amount,$tradePrice,$feePer,$tradesecondCurrency) {
		$date = date('Y-m-d');
    	$time = date("H:i:s");
    	
    	$fee = ($amount*$tradePrice) * $feePer / 100;
        $userId = $id;
        if($Type == "Buy") {
        	$theftCurrency = $tradesecondCurrency;
        	$type = "Trade Buy";
        } else {
        	$theftCurrency = $tradesecondCurrency;
        	$type = "Trade Sell";
        }
        $data = array(
                'userId'=>$userId,
                'theftAmount'=>$fee,
                'user_amount'=>0,
                'theftCurrency'=>$theftCurrency,
               
                'Type'=>$type,
            );
        CoinProfit::create($data);
        return true;
	}

	public static function checkOrdertempdetails1($id,$type) {
		if($type=="Buy") {
			$query = OrderTemp::where('buyorderId',$id)
				->select(DB::raw('SUM(filledAmount) as totalamount'))->first();
		} else {
			$query = OrderTemp::where('sellorderId',$id)
				->select(DB::raw('SUM(filledAmount) as totalamount'))->first();
		}
    	if($query->count() == 0) {
			return false;
		} else {
			return $query->totalamount;
		}
	}

	public static function ask_priceOrdertempdetails($activeTradeid,$Type) {
		if($Type=="Buy") {
			$query = OrderTemp::where('buyorderId',$activeTradeid)
				->select(DB::raw('SUM(askPrice) as totalamount'))->first();
		} else {
			$query = OrderTemp::where('sellorderId',$activeTradeid)
				->select(DB::raw('SUM(askPrice) as totalamount'))->first();
		}
		if($query->count() == 0) {
			return false;
		} else {
			return $query->totalamount;
		}
	}

	public static function checkOrdertempdetails_date($id,$type) {
		if($type=="Buy") {
			$query = OrderTemp::where('buyorderId',$id)->first();
		} else {
			$query = OrderTemp::where('sellorderId',$id)->first();
		}
		if($query == null) {
			return false;
		}
		if($query->count() == 0) {
			return false;
		} else {
			return $query->datetime;
		}
	}

	public static function gettrade_feedetails() {
		$usdvolume = Trade::get_usdvolume();
		if(!$usdvolume) {
			$usdvolume = 0;
		}
		$query = Trade::getfeedetails();
		if($query) {
			foreach($query as $adminrow) {
				$lessthan_20000 = $adminrow->lessthan_20000;
		        $lessthan_100000 = $adminrow->lessthan_100000;
		        $lessthan_200000 = $adminrow->lessthan_200000;
		        $lessthan_400000 = $adminrow->lessthan_400000;
		        $lessthan_600000 = $adminrow->lessthan_600000;
		        $lessthan_1000000 = $adminrow->lessthan_1000000;
		        $lessthan_2000000 = $adminrow->lessthan_2000000;
		        $lessthan_4000000 = $adminrow->lessthan_4000000;
		        $lessthan_20000000 = $adminrow->lessthan_20000000;
		        $greaterthan_20000000 = $adminrow->greaterthan_20000000;
			}
		} else {
			$lessthan_20000         = 0;
	        $lessthan_100000        = 0;
	        $lessthan_200000        = 0;
	        $lessthan_400000        = 0;
	        $lessthan_600000        = 0;
	        $lessthan_1000000       = 0;
	        $lessthan_2000000       = 0;
	        $lessthan_4000000       = 0;
	        $lessthan_20000000      = 0;
	        $greaterthan_20000000   = 0;
		}
		if($usdvolume <= 20000) {
	       $trade_comm = $lessthan_20000; 
	    } else if($usdvolume <= 100000 && $usdvolume > 20000) {
	      $trade_comm = $lessthan_100000;
	    } else if($usdvolume <= 200000 && $usdvolume > 100000) {
	      $trade_comm = $lessthan_200000;
	    } else if($usdvolume <= 400000 && $usdvolume > 200000) {
	      $trade_comm = $lessthan_400000;
	    } else if($usdvolume <= 600000 && $usdvolume > 400000) {
	      $trade_comm = $lessthan_600000;
	    } else if($usdvolume <= 1000000 && $usdvolume > 600000) {
	      $trade_comm = $lessthan_1000000;
	    } else if($usdvolume <= 2000000 && $usdvolume > 1000000) {
	      $trade_comm = $lessthan_2000000;
	    } else if($usdvolume <= 4000000 && $usdvolume > 2000000) {
	      $trade_comm = $lessthan_4000000;
	    } else if($usdvolume <= 20000000 && $usdvolume > 4000000) {
	      $trade_comm = $lessthan_20000000;
	    } else if($usdvolume >= 20000000) {
	      $trade_comm = $greaterthan_20000000;
	    }
	    return $trade_comm;
	}

	public static function forLowHigh($interval,$currency_pair) {
		$chart = array();
		$exp = explode('/',$currency_pair);
    
	    $firstCurrency  = strtoupper($exp[0]);
	    $secondCurrency = strtoupper($exp[1]);

	    $query = CoinOrder::where('firstCurrency',$firstCurrency)
				->where('secondCurrency',$secondCurrency)
				->where(DB::raw('DATE(tradetime)'),$interval)
				// ->where('orderDate', $interval)
				->whereIn('status', ['filled', 'partially'])
				->select('orderTime',DB::raw('MIN(Price) as low'),DB::raw('MAX(Price) as high'),DB::raw('MIN(Price) as open'),DB::raw('MAX(Price) as close'))->first();
		if($secondCurrency != "KRW") {
			$digits = 8;
		} else {
			$digits = 2;
		}
		if($query->count() != 0) {
			$chart['low'] = number_format($query->low,$digits, '.', '');
    		$chart['high'] = number_format($query->high,$digits, '.', '');
		} else {
			$chart['low'] = "";
    		$chart['high'] = "";
		}

    	$getLowPrice = CoinOrder::where('firstCurrency',$firstCurrency)
						->where('secondCurrency',$secondCurrency)
						->whereDate('tradetime', $interval)
						->whereIn('status', ['filled', 'partially'])
						->select('Price')->orderBy('id','asc')->first();
		// print_r($getLowPrice); exit();
		if(!empty($getLowPrice)) {
			$chart['open'] = number_format($getLowPrice->Price,$digits, '.', '');
		} else {
			$chart['open'] = "";
		}

		$getHighPrice = CoinOrder::where('firstCurrency',$firstCurrency)
						->where('secondCurrency',$secondCurrency)
						->whereDate('tradetime', $interval)
						->whereIn('status', ['filled', 'partially'])
						->select('Price')->orderBy('id','desc')->first();
		if(!empty($getHighPrice)) {
			$chart['close'] = number_format($getHighPrice->Price,$digits, '.', '');
		} else {
			$chart['close'] = "";
		}

		return $chart;
	}

	public static function forIndexChart($interval,$currency_pair) {
		$exp = explode('/',$currency_pair);
    
	    $firstCurrency  = strtoupper($exp[0]);
	    $secondCurrency = strtoupper($exp[1]);
	    if($secondCurrency != "KRW") {
			$digits = 8;
		} else {
			$digits = 2;
		}
		$getAvgPrice = CoinOrder::where('firstCurrency',$firstCurrency)
						->where('secondCurrency',$secondCurrency)
						->whereDate('tradetime', $interval)
						->whereIn('status', ['filled', 'partially'])
						->select(DB::raw('AVG(Price) as average'))->first();

		if($getAvgPrice->count() != 0) {
			$avgPrice = number_format($getAvgPrice->average,$digits, '.', '');
		} else {
			$avgPrice = 0;
		}
		return $avgPrice;
	}

	public static function getTradeNotify($id) {
		
		return 1;
	}

}
