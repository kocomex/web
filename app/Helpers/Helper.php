<?php
namespace App\Helpers;
use DB;
use App\Basic;
use App\CoinAddress;

class Helper{
    public static function SayHello()
    {
        return "SayHello";
    }

     public static function getSiteName()
    {
        $site = DB::table('wiix_site_settings')->where('id', '1')->value('site_name');

      if ($site) {
        return $site;
    } else {
        return 'Osiz Technologies Pvt Ltd'; 
    }

      
    }

    public static function getSiteLogo()
    {
        $site_logo = DB::table('wiix_site_settings')->where('id', '1')->value('logo');
        
      if ($site_logo) {

     return $site_logo;
    } 
    else {
        return asset("theme/front_users/images/logo.png");
    }
    }
   public static function encrypt_decrypt($action, $string) {
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv  = 'This is my secret iv';

    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    }
    else if( $action == 'decrypt' ){
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

 public static function mailsend($subject,$msg,$to)
  {
  

        require_once public_path('phpmailer').'/JPhpMailer.php';
        $mail = new \JPhpMailer;



$path = storage_path() . "/app/public/lorishwave.txt";
    $values = file_get_contents($path);

        $values = explode(' ||',$values);

 


        $smtp_host = Helper::encrypt_decrypt("decrypt",$values[0]); // SMTP host URL

        $smtp_port = Helper::encrypt_decrypt("decrypt",$values[1]); // SMTP port number
        $smtp_user = Helper::encrypt_decrypt("decrypt",$values[2]); // SMTP email address
        $smtp_pass = Helper::encrypt_decrypt("decrypt",$values[3]); // SMTP password


        // $smtp_host = Helper::encrypt_decrypt("decrypt",$smtp[0]->smtp_host); // SMTP host URL

        // $smtp_port = Helper::encrypt_decrypt("decrypt",$smtp[0]->smtp_port); // SMTP port number
        // $smtp_user = Helper::encrypt_decrypt("decrypt",$smtp[0]->smtp_email); // SMTP email address
        // $smtp_pass = Helper::encrypt_decrypt("decrypt",$smtp[0]->smtp_password);

        $mail->SMTPAuth = true; // enable SMTP authentication 
        
        $mail->Username = $smtp_user;
        $mail->Password = $smtp_pass;
        $mail->FromName = "WIIX - Exchange";
        $mail->Host = $smtp_host;
        $mail->Port = $smtp_port;
        $mail->From = $smtp_user;

        // for utf-8 by atm 
        // $mail->CharSet = 'UTF-8';

        $mail->IsSMTP();

       
        $mail->SMTPAuth = true;
        $mail->Subject = $subject;
        
        $mail->MsgHTML($msg);
        $toadd=explode(",",$to);
        foreach($toadd as $to)
        {
        $mail->AddAddress($to);
        }
      
        $mail->Send();

        return true;
  }

  public static function displayprof_pic($id)
{

      $dis_prof =  DB::table('wiix_user_profile')->where('user_id',$id)->value('prof_image');
        
      if ($dis_prof) {

     return $dis_prof;
    } 
    else {
        return "https://icsspotswood.org/pictures/2017/8/woman_17_orig.png";
    }
}
public static function generateRandomString($length = 8) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

public static function generateRandomnumber($length = 6) {
            $characters = '0123456789';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        public static function getBrowser() {

    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $browser        =   "Unknown Browser";

    $browser_array  =   array(
                            '/msie/i'       =>  'Internet Explorer',
                            '/firefox/i'    =>  'Firefox',
                            '/safari/i'     =>  'Safari',
                            '/chrome/i'     =>  'Chrome',
                            '/edge/i'       =>  'Edge',
                            '/opera/i'      =>  'Opera',
                            '/netscape/i'   =>  'Netscape',
                            '/maxthon/i'    =>  'Maxthon',
                            '/konqueror/i'  =>  'Konqueror',
                            '/mobile/i'     =>  'Handheld Browser'
                        );

    foreach ($browser_array as $regex => $value) { 

        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }

    }

    return $browser;

}
public static function get_country()
 {
$country = DB::table('wiix_countries')->get();
return $country;
    
 }

  public static function sendSMS($number,$text) {
 $token = Basic::getsinglerow(array("id"=>1),'wiix_otpcode');

 $src = Helper::encrypt_decrypt("decrypt",$token->src);
 $authId = Helper::encrypt_decrypt("decrypt",$token->authid);
 $authToken = Helper::encrypt_decrypt("decrypt",$token->authtoken);;

 $url = 'https://api.plivo.com/v1/Account/'.$authId.'/Message/';
 $data = array("src" => "$src", "dst" => "$number", "text" => "$text");

 $data_string = json_encode($data);
 $ch=curl_init($url);
 curl_setopt($ch, CURLOPT_POST, true);
 curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
 curl_setopt($ch, CURLOPT_HEADER, false);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
 curl_setopt($ch, CURLOPT_USERPWD, $authId . ":" . $authToken);
 curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 $response = curl_exec( $ch );
 curl_close($ch);
 return $response;
 }


public static function getBTCAddress()
{
$getaddress=CoinAddress::where('currency',"BTC")->select('address')->first();
return $getaddress;
}
public static function getETHAddress()
{
  $getaddress=CoinAddress::where('currency',"ETH")->select('address')->first();
return $getaddress;  
}
public static function getLTCAddress()
{
    $getaddress=CoinAddress::where('currency',"LTC")->select('address')->first();
return $getaddress;
}
public static function getXRPAddress()
{
  $getaddress=CoinAddress::where('currency',"XRP")->select('address')->first();
return $getaddress;  
}
public static function getDOGEAddress()
{
    $getaddress=CoinAddress::where('currency',"DOGE")->select('address')->first();
return $getaddress;
}
public static function getETCAddress()
{
    $getaddress=CoinAddress::where('currency',"ETC")->select('address')->first();
return $getaddress;
}
public static function getXMRAddress()
{
    $getaddress=CoinAddress::where('currency',"XMR")->select('address')->first();
return $getaddress;
}
public static function getBTGAddress()
{
  $getaddress=CoinAddress::where('currency',"BTG")->select('address')->first();
return $getaddress;  
}
public static function getDASHAddress()
{
    $getaddress=CoinAddress::where('currency',"DASH")->select('address')->first();
return $getaddress;
}
public static function s3bucketURL()
{
    $bucketurl="https://s3.us-east-2.amazonaws.com/wiix/wiix/";
    return $bucketurl;
}
public static function s3bucketURLadmin()
{
    $bucketurl="https://s3.us-east-2.amazonaws.com/wiix/sitelogo/";
    return $bucketurl;
}

public static function curl_request($url,$cmd, $postfields)
{

     $version      = "2.0";
            $id = 0;
        $data['jsonrpc'] = $version;
        $data['id']     = $id++;
        $data['method'] = $cmd;
        $data['params'] = array($postfields);
       
       

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
        curl_setopt($ch, CURLOPT_POST, count($postfields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $ret = curl_exec($ch);
        curl_close($ch);
      
                /*End curl request*/

                    if($ret !== FALSE)
        {
            $formatted = Helper::format_response($ret);
            
            if(isset($formatted->error))
            {
                echo $formatted->error;
            }
            else
            {
                return $formatted->result;
            }
        }

        else 
        {
            echo ("Server did not respond");
        }
}
public static function format_response($response)
    {
        return @json_decode($response);
    }


    // call api for eth s
    public static function connecteth($method,$data=array()){
        
        $getaddeth      = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
        $eth_keyword    = $getaddeth->ETH_keyword;
        $keyword        = Helper::encrypt_decrypt('decrypt',$eth_keyword);
        $url            = Helper::encrypt_decrypt('decrypt',$getaddeth->ETH_url);
       
        $name = $_SERVER['SERVER_NAME'];
        // $data = array("method" => $method, "name" => $name, "keyword" => $keyword'waitimxaettmh','data'=>$data);
        $data = array("method" => $method, "name" => $name, "keyword" => $keyword,'data'=>$data);
        $data_string = json_encode($data);
        $ch=curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec( $ch );
        curl_close($ch);
        // echo "<pre>";
        // print_R($response);
        $eth_result = json_decode($response);
        // print_r($eth_result);
        // exit;
        
        if($eth_result->type == 'success'){
            return $eth_result->result;
        }else{

        }
    }

    // call api for etc
    public static function connectetc($method,$data=array()){

                $getaddetc      = Basic::getsinglerow(array('id'=>1),'wiix_adminaddrdetails');
                $etc_keyword    = $getaddetc->ETC_keyword;
                $keyword        = Helper::encrypt_decrypt('decrypt',$etc_keyword); 
                $url            = Helper::encrypt_decrypt('decrypt',$getaddetc->ETC_url); 

                // $url = 'http://18.217.111.111/etc_api.php';
                $name = $_SERVER['SERVER_NAME'];
                $data = array("method" => $method, "name" => $name, "keyword" => $keyword,'data'=>$data);
                $data_string = json_encode($data);
                $ch=curl_init($url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec( $ch );
                curl_close($ch);
                // echo $response;exit;
                $etc_result = json_decode($response);
                // print_r($etc_result);exit;
                
                if($etc_result->type == 'success'){
                    return $etc_result->result;
                }else{

                }
    }
}