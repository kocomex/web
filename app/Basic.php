<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\Quotation;

class Basic extends Model
{
				public static function getsinglerow($where,$table)
				{
				$cb_userdetailsemail= DB::table($table)->where($where)->first();
                    if($cb_userdetailsemail)
                        return $cb_userdetailsemail;
                    else
                        return false;
                }

				//get multiple row
				public static function getmultiplerow($where,$table,$order='')
				{
				$cb_userdetailsemail= DB::table($table)->where($where)->orderBy($order[0],$order[1])->get();
				if($cb_userdetailsemail)
				return $cb_userdetailsemail;
				else
				return false;		
				}

				//insert values tables
				public static function insertdatas($data,$table)
				{
			    $id = DB::table($table)->insertGetId($data);			
				return $id;
				}

				//update value tables
				public static function updatedatas($whare,$data,$table)
				{
				DB::table($table)->where($whare)->update($data);
				return true;	
				}

				//delete values tables
				public static function deletedatas($where,$table)
				{
				DB::table($table)->where($where)->delete();	
				return true;	 	
				}
				public static function generateredeemString($length = 8) {
					    $characters = '0123456789';
					    $randomString = '';
					    for ($i = 0; $i < $length; $i++) {
					        $randomString .= $characters[rand(0, strlen($characters) - 1)];
					    }
					    return $randomString;
                }

   		
		
				
}

