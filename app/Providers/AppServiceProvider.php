<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

use App\Model\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('unique_username', function($attribute, $value, $parameters) {
            $name = strip_tags($value);
            $getCount = User::where('consumer_name',$name)->count();
            if($getCount > 0)
                return false;
            else
                return true;
        });

        Validator::extend('unique_email', function($attribute, $value, $parameters) { 
            $email = strtolower(strip_tags($value));
            $first = User::endecryption(1,User::firstEmail($email));
            $second = User::endecryption(1,User::secondEmail($email)); 
            $getCount = User::where('wiix_content',$first)
                    ->where('unusual_user_key',$second)->count();
            if($getCount > 0) 
                return false; 
            else 
                return true; 
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
