<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradePairs extends Model
{
    protected $table = 'wiix_tradepair';

    protected $guarded = [];
}
