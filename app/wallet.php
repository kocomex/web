<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class wallet extends Model
{
   public $table = 'wiix_user_wallet';
    protected $fillable = array();


    	public static $adminLoginRule = array(
        'username' => array('required', 'email'),
        'user_pwd' => 'required',
        'pattern_code' => 'required',
        'key_code' => 'required'
    );
}
