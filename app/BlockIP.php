<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockIP extends Model
{
   protected $table = 'wiix_block_ip';

    protected $guarded = [];
}
