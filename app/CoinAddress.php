<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinAddress extends Model
{
    
    protected $table = 'wiix_coin_address';

    protected $guarded = [];
}
