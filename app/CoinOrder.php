<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinOrder extends Model
{
     protected $table = 'wiix_coin_order';

    protected $guarded = [];
}
