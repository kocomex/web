<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TradingFee extends Model
{
    protected $table = 'trading_fee_structure';

    protected $guarded = [];
}
