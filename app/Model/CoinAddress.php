<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CoinAddress extends Model
{
    protected $table = 'coin_address';

    protected $guarded = [];

    //associate with User
    public function user() {
	    return $this->belongsTo('App\Model\User', 'user_id');
	}
}
