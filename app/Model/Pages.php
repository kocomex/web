<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentGateway extends Model
{
    protected $table = 'pages';

    protected $guarded = [];

}
