<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdminBank extends Model
{
    protected $table = 'admin_bank_details';

    protected $guarded = [];
}
