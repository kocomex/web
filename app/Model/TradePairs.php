<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TradePairs extends Model
{
    protected $table = 'trade_pairs';

    protected $guarded = [];
}
