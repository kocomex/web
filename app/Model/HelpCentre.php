<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HelpCentre extends Model
{
    protected $table = 'help_centre';

    protected $guarded = [];
}
