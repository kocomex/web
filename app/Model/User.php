<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_info';

    protected $guarded = [];

    //To encrypt and decrypt given string using open ssl with manual secret key
    public static function endecryption($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'WiiX secret key';
        $secret_iv  = 'WiiX secret iv';

        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        //encryption
        if( $action == 1) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        //decryption
        else if( $action == 2){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    public static function randomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //validation rules

    public static $adminLoginRule = array(
        'username' => array('required', 'email'),
        'user_pwd' => 'required',
        'pattern_code' => 'required'
    );

    public static $userSignupRule = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => array('required', 'email', 'unique_email'),
        'agree' => 'required',
        'pwd' => 'required',
        'confirm_pwd' => 'required'
    );

    public static $userSignupMsg = array(
        'email.unique_email' => "Email address already exits",
        'email.email' => "Enter valid email"
    );

    public static $userLoginRule = array(
        'email' => array('required', 'email'),
        'password' => 'required'
    );

    public static $forgotRule = array(
        'useremail' => array('required', 'email')
    );

    public static $resetRule = array(
        'pwd' => 'required',
        'confirm_pwd' => 'required'
    );

    public static $passwordRule = array(
        'new_pwd' => 'required',
        'cnfirm_pwd' => 'required'
    );

    public static $changePasswordRule = array(
        'current_pwd' => 'required',
        'new_pwd' => 'required',
        'confirm_pwd' => 'required'
    );

    public static $profileRule = array(
        'firstname' => 'required',
        'lastname' => 'required',
        'phone' => 'required',
        'address' => 'required',
        'postal' => 'required',
        'city' => 'required',
        'dob' => 'required',
        'country' => 'required'
    );

    public static $depositRule = array(
        'deposit_amount' => array('required', 'numeric'),
        'transaction_ref' => 'required',
        'receipt_ref' => 'required'
    );

    public static $withdrawBankRule = array(
        'currency' => 'required',
        'withdraw_amount' => array('required', 'numeric')
    );

    public static $withdrawCryptoRule = array(
        'currency' => 'required',
        'coin_address' => 'required',
        'amount' => array('required', 'numeric')
    );

    public static $bankRule = array(
        'acc_name' => 'required',
        'acc_number' => array('required', 'numeric'),
        'bank_name' => 'required',
        'bank_branch' => 'required',
        'bank_swift' => 'required',
        'country' => 'required',
        'postal' => array('required', 'numeric'),
        'currency' => 'required'
    );

    public static $contactRule = array(
        'username' => 'required',
        'useremail' => array('required', 'email'),
        'subject' => 'required',
        'message' => 'required'
    );

    public static $helpRule = array(
        'subject' => 'required',
        'description' => 'required'
    );

    public static $queryRule = array(
        'description' => 'required',
        'reference_proof' => 'max:1024'
    );

    //get user profile picture for header
    public static function getProfile($id) {
        $profilePicture['user'] = User::where('id',$id)
            ->select('profile_picture','consumer_name')->first();
        return $profilePicture;
    }

    //To split the first 4 letters of email
    public static function firstEmail($a) {
        return substr($a, 0, 4);
    }

    //To split the second part of email after 4 letters
    public static function secondEmail($a) {
        return substr($a, 4);
    }

    //get site details for footer
    public static function getSiteDetails() {
        $getSiteDetails = SiteSettings::where('id',1)->select('contact_email','contact_number','contact_address','city','state','country','fb_url','twitter_url','linkedin_url','googleplus_url', 'copy_right_text','skype_id')->first();
        return $getSiteDetails;
    }

    //get site logo for header
    public static function getSiteLogo() {
        $getSiteDetails = SiteSettings::where('id',1)->select('site_logo','site_favicon')->first();
        return $getSiteDetails;
    }

    //get meta details
    public static function getMetaDetails() {
        $currentRoute = \Route::getCurrentRoute()->getActionName();
        $explodeRoute = explode('@', $currentRoute);
        $uri = $explodeRoute[1];
        return MetaContent::where('link',$uri)->first();
    }

    //get user notification
    public static function getNotification($id) {
        $getDetails = UserNotification::where('user_id',$id)->orderBy('id','desc')->limit(10)->get();
        if($getDetails->isEmpty()) {
            return "";
        } else {
            return $getDetails;
        }
    }

    //get btc address of the user
    public static function getAddress($id,$coin) {
        $getAddress = CoinAddress::where('user_id',$id)->where('currency',$coin)->select('address')->orderBy('id','desc')->first();
        if($getAddress->count() == 0) {
            return "";
        } else {
            return $getAddress->address;
        }
    }

    //retrieve country list
    public static function getCountry() {
        return Country::all();
    }

    //Associations
    //associte with user activities
    public function activities() {
        return $this->hasMany('App\Model\UserActivity');
    }

    //associte with user bank
    public function bank() {
        return $this->hasMany('App\Model\UserBank');
    }

    //associte with user verification
    public function verification() {
        return $this->hasOne('App\Model\ConsumerVerification');
    }

    //associate with wallet
    public function wallet() {
        return $this->hasMany('App\Model\Wallet');
    }

    //associate with transactions
    public function transactions() {
        return $this->hasMany('App\Model\Transaction');
    }

    //associate with deposit
    public function deposits() {
        return $this->hasMany('App\Model\Deposit');
    }

    //associate with withdraw
    public function withdraws() {
        return $this->hasMany('App\Model\Withdraw');
    }

    //associate with coin address
    public function coinAddress() {
        return $this->hasMany('App\Model\CoinAddress');
    }

    //To get captcha code for signup and login
    public static function recaptchacode1() {
        $ranStr = md5(microtime());
        $ranStr = substr($ranStr, 0, 6);
        return $ranStr;
    }
}
