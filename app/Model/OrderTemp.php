<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderTemp extends Model
{
    protected $table = 'ordertemp';

    protected $guarded = [];
}
