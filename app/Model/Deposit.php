<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Deposit extends Model
{
    protected $table = 'deposit';

    protected $guarded = [];

    //associate with User
    public function user() {
	    return $this->belongsTo('App\Model\User', 'user_id');
	}

	//To get total amount currently active in sell order
	public static function transitAmount($userId,$currencyId) {
		$query = CoinOrder::where('user_id',$userId)
				->where('firstCurrency',$currencyId)
				->where('Type','sell')->whereIn('status', ['active', 'partially'])
				->select(DB::raw('SUM(Amount) as amount'))->first();
		if($query->count() == 0) {
			return 0;
		} else {
			return $query->amount;
		}
	}
}
