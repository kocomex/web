<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CoinOrder extends Model
{
    protected $table = 'coin_order';

    protected $guarded = [];
}
