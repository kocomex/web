<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ConsumerVerification extends Model
{
    protected $table = 'consumer_verification';

    protected $guarded = [];

    //associate with User
    public function user() {
	    return $this->belongsTo('App\Model\User', 'user_id');
	}
}
