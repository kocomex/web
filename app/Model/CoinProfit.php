<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CoinProfit extends Model
{
    protected $table = 'coin_profit';

    protected $guarded = [];
}
