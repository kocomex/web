<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MetaContent extends Model
{
    protected $table = 'meta_content';

    protected $guarded = [];
}
