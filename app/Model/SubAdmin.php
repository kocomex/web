<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubAdmin extends Model
{
    protected $table = 'sub_admin';

    protected $guarded = [];

    public static $faqRule = array(
        'question' => 'required',
        'description' => 'required'
    );

    public static $newsRule = array(
        'title' => 'required',
        'content' => 'required',
        'author' => 'required'
    );

    public static $depositRule = array(
        'confirm_code' => 'required',
        'reason' => 'required',
        'actual_amount' => 'required'
    );

    public static $cmsRule = array(
        'title' => 'required',
        'content' => 'required'
    );

    public static $emailRule = array(
        'name' => 'required',
        'subject' => 'required',
        'template' => 'required'
    );

    public static $metaRule = array(
        'title' => 'required',
        'meta_keywords' => 'required',
        'meta_description' => 'required'
    );

    public static $profileRule = array(
        'admin_username' => 'required',
        'admin_phno' => 'required',
        'admin_address' => 'required',
        'admin_city' => 'required',
        'admin_state' => 'required',
        'admin_postal' => 'required',
        'country' => 'required'
    );

    public static $pwdRule = array(
        'current_pwd' => 'required',
        'new_pwd' => 'required|min:8',
        'confirm_pwd' => 'required|min:8'
    );

    public static $siteRule = array(
        'site_name' => 'required',
        'contact_email' => 'required',
        'contact_no' => 'required',
        'contact_address' => 'required',
        'city' => 'required',
        'country' => 'required',
        'copy_right_text' => 'required',
        'facebook_url' => 'required',
        'twitter_url' => 'required',
        'linkedin_url' => 'required',
        'googleplus_url' => 'required'
    );

    public static $addSubadminRule = array(
        'username' => 'required',
        'email_addr' => array('required', 'email'),
        'pattern_code' => 'required',
        'permission' => 'required'
    );

    public static $bankRule  = array(
        'bank_name' => 'required',
        'acc_name' => 'required',
        'acc_number' => array('required', 'numeric'),
        'bank_code' => 'required',
        'bank_branch' => 'required',
        'bank_country' => 'required'
    );

    public static $tradeFeeRule = array(
        'from_amt' => 'required',
        'to_amt' => 'required',
        'fee' => 'required'
    );

    public static $tradePairRule = array(
        'buy_comm' => 'required',
        'sell_comm' => 'required'
    );

    public static function getProfile($id) {
    	$profilePicture['admin'] = SubAdmin::where('id',$id)
    	       ->select('profile','username')->first();
    	return $profilePicture;
    }

    public static function getAdminUrl() {
        $getUrl = SiteSettings::where('id',1)->select('admin_redirect')->first();
        return $getUrl->admin_redirect;
    }

    public static function getPermission($id) {
        $permission = SubAdmin::where('id',$id)->select('permission')->first();
        return $permission->permission;
    }

    public static function getNotificationCount() {
        $count = AdminNotification::where('status','unread')->count();
        return $count;
    }

    public static function getAdminNotifcation() {
        $result = AdminNotification::orderBy('id','desc')->limit(10)->get();
        if($result->isEmpty()) {
            return "";
        } else {
            return $result;
        }
    }

    public static function getTimeAgo($date_time) {
        $date2 = date_create(date('Y-m-d H:i:s'));
        $date1 = date_create($date_time);
        $diff  = date_diff($date1, $date2);
        $left  = '0 sec ago'; 
      
        if ($date1 < $date2) {
            if ($diff->s != 0)
                $left = $diff->s.' sec ago';
            if ($diff->i != 0)
                $left = $diff->i.' mins ago';
            if ($diff->h != 0)
                $left = $diff->h . ' hours ago';
            if ($diff->d != 0)
                $left = $diff->d . ' days ago';
            if ($diff->m != 0)
                $left = $diff->m . ' months ago';
            if ($diff->y != 0)
                $left = $diff->y . ' years ago';
        }
        return $left;
    }

}
