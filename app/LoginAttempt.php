<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginAttempt extends Model
{
       protected $table = 'wiix_login_attempts';

    protected $guarded = [];
}
