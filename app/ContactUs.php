<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
   protected $table = 'wiix_contactus';

    protected $guarded = [];
}
