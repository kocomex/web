<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminWallet extends Model
{
	 protected $table = 'wiix_adminwallets';

    protected $guarded = [];
     public static $pwdRule = array(
        'current_pwd' => 'required',
        'new_pwd' => 'required|min:8',
        'confirm_pwd' => 'required|min:8'
    );
       public static $forgotRule = array(
        'useremail' => array('required', 'email')
    );

    public static $resetRule = array(
        'pattern_code' => 'required'
    );
    public static $passwordRule = array(
        'new_pwd' => 'required',
        'cnfirm_pwd' => 'required'
    );

     public static $withdrawRule = array(
        'currency' => 'required',
        'address' => 'required',
        'amount' => 'required',
        'password' => 'required',
        'confirm_code' => 'required'
    );
    
}
